#!/usr/bin/env perl

# Updating i18n data
# For each disrectory nb, lib, plugin, this script
#  - runs pygettext on the files containing the _( string
#    to produce a directory specific messages.pot file
#  - runs msgmerge to build an updated .po file
#  - runs msgfmt in check mode to verify the file
#
#  $ARGV[0] is the i18n string considered
#
# Copyright Jean-Pierre Chr�tien <chretien\@cert.fr> 2004
# General NB license
#
CHANGELOG
# july 21, 2004 age management, options.conf management, i18n initialization
# july 20, 2004 initial version
#

use strict

# current i18n languages
push(@lang,'fr-fr');
undef %is_lang; foreach (@lang) {$is_lang{$_}=1};

#initial messages.<i18nstring>.po content
$inimessages{'fr-fr'}='# Francisation de NewsBruiser
# Copyright (C) 2004
# Traduction initiale Jean-Pierre Chr�tien <chretien\@cert.fr>
#
msgid ""
msgstr ""
"Project-Id-Version: french_i18n_nb_0.0\n"
"POT-Creation-Date: Wed Jul 21 14:31:22 2004\n"
"PO-Revision-Date: 2004-07-21 10:56+0200\n"
"Last-Translator: Jean-Pierre Chr�tien <chretien@cert.fr>\n"
"Language-Team: nobody <nobody@nowhere.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.4\n"

';



# edit to fit your installation
# GNU gettext in the path
my $msgmerge='msgmerge';
my $msgfmt='msgfmt';
# Solaris (GNU gettext prefixed with letter g - complete replacement
#          of Solaris commands is a *bad* idea)
#$msgmerge='gmsgmerge';
#$msgfmt='gmsgfmt';

# where is pygettext
# in the path
my $pygettext='pygettext.py';
# elsewhere 
#$pygettext='<path to pygettext.py>';


# ---- end of configuration section ----------------------

# scripting begins
# needs an argument
die('Please submit an i18n string, e.g. fr-fr,') unless($ARGV[0]);
# needs a known language
die("$ARGV[0] is not a known i18n language for NewsBruiser,") unless($is_lang{$ARGV[0]});

# what are the dirs to update
my $dirfind=`find ../nb -name '[a-zA-Z]*' -type d`;
my @dirlist=split("\n",$dirfind);


# update loop


LOOP: foreach my $dir (@dirlist) {
# expand the option file if existing and if update needed
  if ((-e "$dir/options.conf") && ((!-e "$dir/options.conf.marked") ||((-M "$dir/options.conf") < (-M "$dir/options.conf.marked")))) {
$expandoptionscmd="/bin/cp -f $dir/options.conf $dir/options.conf.marked;
perl -p -i -e 's+\"+\\\\\"+g;s+([dg].*?\=)(.*\$)+\$1\_(\"\$2\")+;' $dir/options.conf.marked";
`$expandoptionscmd`;
}
# find i18ned files
my $pyfiles=`grep -l '[^_]_(' $dir/*.py $dir/options.conf.marked 2>/dev/null`;
my @pylist=split("\n",$pyfiles);
# check age
$updateneeded=0;
foreach my $fi (@pylist) {
$updateneeded=1 if ((!-e "$dir/messages.pot")||((-M "$fi") < (-M "$dir/messages.pot")));
} 
$pyfiles=join(' ', @pylist);
if ($#pylist<0){
print "No i18n markup available in $dir, skipping...\n";
next LOOP;
}

# pygettext
if ($updateneeded) {
my $pygettextcmd="$pygettext -p $dir $pyfiles";
print "Running pygettext on $pyfiles in $dir\n";
my $pygettextlog=`$pygettextcmd`;
}
else {
print "$dir/messages.pot is up to date, pygettext skipped..\n";
}
# msgmerge
if (-e "$dir/messages.$ARGV[0].po") {
my $msgmergecmd="cd $dir;
/bin/cp messages.$ARGV[0].po messages.$ARGV[0].po.old;
$msgmerge --no-wrap messages.$ARGV[0].po messages.pot -o messages.$ARGV[0].po.new 2>i18n.$ARGV[0].merge.err";
print "Running msgmerge in $dir\n";
`$msgmergecmd`;
print `cat $dir/i18n.$ARGV[0].merge.err` if ((-e "$dir/i18n.$ARGV[0].merge.err") && (!-z "$dir/i18n.$ARGV[0].merge.err"));
# check if merge was Ok
if (`grep fatal  $dir/i18n.$ARGV[0].merge.err`) {
print "Fatal error when merging in $dir, skipping...\n";
`/bin/rm $dir/i18n.$ARGV[0].*`;
next LOOP;
}
# make new current
my $msgmergecmdok="cd $dir;
/bin/mv messages.$ARGV[0].po.new messages.$ARGV[0].po;
chmod a+r messages.$ARGV[0].po;
$msgfmt -c --statistics messages.$ARGV[0].po";
print "Check fmt (for information, .mo files not used):\n";
`$msgmergecmdok`;
}
else {
print "No messages.$ARGV[0].po available in $dir: creating from template file...\n";
open(MESS,">$dir/messages.$ARGV[0].po");
print MESS $inimessages{$ARGV[0]};
close MESS;
my $msgmergecmd="cd $dir; 
$msgmerge --no-wrap  -o tempo.po messages.$ARGV[0].po messages.pot;
/bin/mv tempo.po  messages.$ARGV[0].po;
$msgfmt -c --statistics messages.$ARGV[0].po"; 
`$msgmergecmd`;
}
}

