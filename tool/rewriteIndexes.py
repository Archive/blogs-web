#!/usr/bin/python
#
# Note: to run this script you'll need to set your PYTHONPATH environment
# variable to include the NewsBruiser source directory, eg.
#
#    export PYTHONPATH=/home/me/public_html/nb/

import os
import sys
import time
from nb.NBConfig import NBConfig

def main():
    config = NBConfig()
    if os.environ.has_key('SERVER_PROTOCOL'):
        print 'Content-type: text/html\n'
    print "Rewriting notebook indices"
    for notebook in config.notebooks.values():
        print '', notebook.name,
        sys.stdout.flush()
        t1 = time.clock()
        notebook.recreateIndex()
        t2 = time.clock()
        print '[%s sec]' % (t2-t1)

if __name__=='__main__':main()
