#!/usr/bin/python

#This script asks you for a string and returns an encrypted version of
#the string.
import crypt
import getpass
import random
import string

chars = string.lowercase + string.uppercase + string.digits + './'
salt = ""

plaintext = getpass.getpass("Enter password plaintext to get encrypted version: ")

for i in range (0,2):
 salt = salt + random.choice(chars)

encrypted = crypt.crypt(plaintext,salt)
print "Encrypted version: %s" % encrypted
