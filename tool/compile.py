#!/usr/bin/python

class Compiler:
    
    """This is a simple program which precompiles the bytecode for
NewsBruiser, greatly improving performance. You should rerun this
program whenever you upgrade NewsBruiser or change the source
code."""

    def compile(self, dir=None):
        import compileall
        import os
        if not dir:
            dir = os.getcwd()
        compileall.compile_dir(dir, maxlevels=100, force=1)

if __name__ == '__main__':
    comp = Compiler()
    print comp.__doc__
    print
    print "Now compiling..."
    comp.compile()
    print
    print "All done compiling your NewsBruiser files. You are now fast!"
