<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html"/>

  <xsl:template match="rss">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
      <head>
        <title><xsl:value-of select="channel/title"/></title>
      </head>
      <body>
        <h1><xsl:value-of select="channel/title"/></h1>
        <xsl:apply-templates select="channel/item"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="item">
    <p>
      <b><xsl:value-of select="title"/></b>
      Posted <xsl:value-of select="pubDate"/>
      (<a href="{guid}">permalink</a>)
    </p>
    <blockquote>
      <p><tt><xsl:value-of select="description"/></tt></p>
    </blockquote>
  </xsl:template>

</xsl:stylesheet>
