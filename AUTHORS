Leonard Richardson (leonardr at segfault dot org) is the primary
author of NewsBruiser.

Gary Benson (gary at inauspicious dot org), Mark Fasheh (mark at
exothermic dot org), Josh Lucas (josh at stonecottage dot com), and
Dan Grassi (lexar at grassi dot org) have contributed code to
NewsBruiser. Rajarshi Guha (rajarshi at presidency dot com) and Nick
Moffitt (nick at zork dot net) provided CSS code.

Thanks to Brendan Adkins (xorph at xorph dot com) for his themes, and
to NewsBruiser users everywhere.

NewsBruiser uses and includes the following libraries and outside
applications (see COPYING for licensing details):

* The [indexer] module written by David Mertz (mertz at gnosis dot cx)

* The Reverend module written by Amir Bakhtiar (amir at divmod dot org).

* The StrippingParser class written by Itamar Shtull-Trauring 
  (itamar at itamarst dot org)

* The fcrypt module written by Carey Evans (careye at spamcop dot
  net), based on C code by Eric Young (eay at mincom dot oz dot au)
  and Tim Hudson (tjh at mincom dot oz dot au).

* The portalocker module written by Jonathan Feinberg (jdf at pobox
  dot com).

* The PyManila, blogger, and feedparser modules written by Mark
  Pilgrim (f8dy at diveintomark dot org).

* The XML-RPC client library and the RSS parser written by Fredrik
  Lund (fredrik at pythonware dot org).

* The I Want Options; Leonard's Obsequious Markup Producer; Internet
  Topic Exchange Client; Template!: The Musical; Transfusion;
  Beautiful Soup; and ASCII, Dammit libraries, written by Leonard
  Richardson.

* The HTMLTidy library written by Gary Benson.

* The Ahoy Javascript application written by Brian Donovan (brian at
  monokromatik dot com)

* The pydel library (in the entry/export/Delicious plugin) written by
  Darryl VanDorp.
