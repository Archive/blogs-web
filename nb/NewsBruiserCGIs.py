"""CGIs which operate on multiple notebooks or are otherwise outside of
the notebook system."""

import os
import pwd
import string
import sys
import types

import CommandMaps
import const
import Notebook
import NBConfig
from NBCGI import NBCGI
from Viewer import Viewer
import util

class DispatchCGI(NBCGI):

    """Dispatches to the NewsBruiser CGIs."""

    cgiModules = [__name__]

    def __init__(self):
        self.config = NBConfig.getSingleton()
        if os.environ.has_key('PATH_INFO'):
            self.pathInfo = os.environ['PATH_INFO'][1:]
        else:
            self.pathInfo = '/'
        cgiName = self.shiftPathInfo()

        cgiClass = None
        for module in self.cgiModules:
            for c in sys.modules[module].__dict__.values():
                if hasattr(c, 'NAME'):
                    if c.NAME == cgiName:
                        cgiClass = c
                        break
                if not cgiClass and hasattr(c, 'CGI_ALIASES'):
                    for name in c.CGI_ALIASES:
                        if name == cgiName:
                            cgiClass = c
                            break
            if cgiClass:
                break
        else:
            if cgiName:
                self.setMinimalVariables()
                self.printError(_("No such screen: %s") % cgiName)
            else:
                cgiClass = PortalCGI
        cgiClass(pathInfo=self.pathInfo)

class AdminCGI(NBCGI):

    """A CGI which can do administrative things like creating notebooks."""

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, self.nullPathInfoProcessor, pathInfo=pathInfo)

    def createNotebook(self, config, masterPassword, notebookName,
                       notebookPassword, ordinal):
        """Creates a notebook, automatically authenticates the creating user
        to the notebook, and returns the notebook."""
        notebook = Notebook.create(config, masterPassword, notebookName,
                                   notebookPassword, ordinal)
        if notebook.useCookies():
            auths = self.parseNotebookAuthCookie()
            auths[notebook.name] = notebookPassword
            self.setNotebookAuthCookie(auths)
        return notebook

class InstallCGI(AdminCGI):

    """A CGI for doing initial setup of NewsBruiser."""

    NAME = 'install'

    DATA_PATH = 'dataDir'    

    HAS_ADMIN_ACCESS_CGI_KEY = 'admin'

    SUBMIT_TEXT = _('Complete NewsBruiser installation')

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, self.nullPathInfoProcessor,
                       self.nullNotebookNameProcessor,
                       pathInfo=pathInfo)

    def run(self):
        if self.config.configFile:
            self.redirectTo(self.config.getDefaultNotebook().getURL(PortalCGI.NAME))
        self.defaultDataPath = util.getFileInNewsBruiserBaseDirectory('NewsBruiserData/')

        self.dataDir = self.var(self.DATA_PATH, self.defaultDataPath)
        if self.dataDir and self.dataDir[0] != '/':
            #They entered a relative path; absolutize it
            self.dataDir = os.path.join(os.getcwd(), self.dataDir)
        self.password = self.var(const.MASTER_PASSWORD_CGI_KEY)
        self.passwordVerify = self.var(const.PASSWORD_VERIFY_CGI_KEY)
        self.hasAdmin = self.var(self.HAS_ADMIN_ACCESS_CGI_KEY, None)
        if self.hasAdmin != None:
            self.hasAdmin = int(self.hasAdmin)
        self.notebookName = self.var(const.NOTEBOOK_NAME_CGI_KEY, 'weblog')
        submitted = self.var(const.MAKE_CHANGE_CGI_KEY)

        self.needPermDir = self.getHighestExistingDirectory(self.dataDir)
        self.hasPerm = os.access(self.needPermDir, os.W_OK)
        if self.hasPerm:
            self.hasAdmin = 2

        errors = []
        if submitted:

            if not self.hasPerm:
                errors.append(_("I don't have permission to write to <b>%s</b>.") % self.needPermDir)

            if not self.password:
                errors.append(_("You need to specify a master password."))
            elif not self.passwordVerify:
                errors.append(_("You need to verify the master password so that you don't fall prey to a typo."))
            elif self.passwordVerify != self.password:
                errors.append(_("Your password verification didn't match your actual password."))

            if not self.notebookName:
                errors.append(_("You need to provide a notebook name."))
            if string.find(self.notebookName, 'deleted-') == 0:
                errors.append(_("A notebook name can't start with 'deleted-' because NewsBruiser uses those names to denote deleted notebooks."))
            if self.notebookName == 'NewsBruiserMaster':
                errors.append(_('"NewsBruiserMaster" is a reserved notebook name; you can\'t use it.'))

        if submitted and not errors:
            if not self.setup():
                return            

        self.printHeader()
        title = _("Welcome to NewsBruiser!")
        self.startPage(title)
        self.printHeading('<a href="http://newsbruiser.tigris.org/doc/">%s</a>' % title)
        
        if errors:
            self.line()
            print "<p>%s</p>" % _("Sorry, I couldn't complete the NewsBruiser installation:")
            print '<ul>'
            for error in errors:
                print '<li>' + error
            print '</ul>'
            self.line()
        if not errors:
            self.printIntro()
        if self.hasAdmin != None:
            self.printForm()
        self.endPage()

    def setup(self):
        if not os.path.exists(self.dataDir):
            os.makedirs(self.dataDir, const.DIR_MODE)
        configFile = os.path.join(self.dataDir,
                                  const.DEFAULT_NEWSBRUISER_CONFIG_FILE_NAME) 
        file = open(configFile, 'w')
        file.write(util.encrypt(self.password))
        file.close()

        #Make it so that you can't access the NewsBruiser file from the web.
        htaccessFile = os.path.join(self.dataDir, '.htaccess')
        file = open(htaccessFile, 'w')
        file.write('Deny from all\n')
        file.close

        self.config.configFile = configFile
        self.config.readConfigFile()
        notebook = self.createNotebook(self.config, self.password,
                                       self.notebookName, self.password, 0)
        if self.dataDir == self.defaultDataPath:
            self.redirectTo(notebook.getURL(PortalCGI.NAME))
        title = _('NewsBruiser setup almost complete')
        self.startPage(title)
        self.printHeading(title)

        print "<p>%s</p>" % _("""Because you chose a non-default location for your NewsBruiser data, you now need to manually create a <code>config.location</code> file so that NewsBruiser knows where to look for the data. See <a href="http://newsbruiser.tigris.org/doc/configuration.html#config.location">the documentation</a> for details, but here's what to do:""")
        print '<p>%s</p>' % (_("Your <code>config.location</code> file should be placed in <code>%s</code>, and it should contain the following single line:") % util.getFileInNewsBruiserBaseDirectory('config.location'))

        print '<blockquote><code>%s</code></blockquote>' % os.path.join(self.dataDir, '')

        print "<p>%s</p>" % _("Until you create the <code>config.location</code> file, you won't be able to use NewsBruiser; any attempt to access a NewsBruiser page will take you to the installation screen.")

        print "<p>%s</p>" % _('Once you create that file, <a href="%s">click here</a> to begin your NewsBruiser adventure.') % notebook.getURL(PortalCGI.NAME)

        self.endPage()
        return 0

    def printIntro(self):
        print "<p>%s</p>" % (_("These are unfamiliar surroundings, so I assume you've just installed NewsBruiser here on %s. There are three steps to completing the installation, and I'll need some input or help from you on each one.") % os.environ.get('SERVER_NAME', 'this machine'))
        print '<ol>'
        print '<li>' + _('Creating the data directory.') + '</li>'
        print '<li>' + _('Choosing a master password.') + '</li>'
        print '<li>' + _('Naming your first notebook.') + '</li>'
        print '</ol>'
        links = []
        for i in (0, 1):
            links.append(self.getURL(self.NAME, self.HAS_ADMIN_ACCESS_CGI_KEY + '=' + str(i)))        
        if self.hasAdmin == None:
            if not self.hasPerm:
                print '<p>'
                print _("So I don't show you a bunch of directions you can't follow, I need to know first of all whether you have administrative (root) access to this machine.")
                print '</p>'
                print "<blockquote>"
                print '<a href="%s">%s</a><br />' % (links[i],
                                               _('Click here if you have root access to this machine.'))
                print '<a href="%s">%s</a>' % (links[0],
                                               _('Click here if you don\'t have root access to this machine.</a>'))
                print '</blockquote>'
        elif self.hasAdmin == 1:
            print '<p>' + _("Since you've got root access, we're going to create the data directory by making the NewsBruiser directory owned by the webserver.")
            print '<blockquote><a href="%s">%s</a></blockquote>' % (links[0],
                                                                    _("Click here if it turns out you actually don't have root access on this machine."))
        elif self.hasAdmin == 0:
            print _("Since you don't have root access to the machine, we're going to create the data directory by briefly making the NewsBruiser directory world-writable. This is a little bit risky, so be careful.")
            print '<blockquote><a href="%s">%s</a></blockquote>' % (links[1],
                                                                    _("Click here if it turns out you actually do have root access on this machine."))

        if self.hasAdmin != None:
            print '<hr />'

    def getHighestExistingDirectory(self, path):
        dir = path
        while dir and not os.path.exists(dir):
            dir = os.path.split(dir)[0]
        if dir and dir[-1] != os.sep:
            dir = dir + os.sep
        return dir

    def printForm(self):
        self.startForm(self.getURL(self.NAME, self.HAS_ADMIN_ACCESS_CGI_KEY + '=' + str(self.hasAdmin)))
        self.hiddenField(self.HAS_ADMIN_ACCESS_CGI_KEY, self.hasAdmin)
        self.printHeading(_('Step 1: Creating the data directory'), '2')

        print '<p>'
        print _('This is the tricky one.')

        if os.path.exists(self.dataDir):
            print _("I'd like to put some files into the directory <b>%s</b>. I can only do this if that directory is writable by the webserver. It looks like that's already the case, so you don't have to do anything here") % self.dataDir
        else:
            args = {'createDir' : self.dataDir,
                    'needPermDir' : self.needPermDir}
            print _("I'd like to create the directory <b>%(createDir)s</b>. I can only do this if the directory <b>%(needPermDir)s</b> is writable by the webserver, which it currently isn't.") % args
            print '</p>'
            if self.hasAdmin:
                newUID = pwd.getpwuid(os.getuid())[0]
                print _("Since you've got root access on this machine, you can temporarily make the NewsBruiser directory writable by the webserver. Then I can create the subdirectory and you can change the owner back to what it is now.")
                print '<p>'
                print _("<code>su</code> or <code>sudo</code> to root and issue the following commands.")
                print '<blockquote><code>chown %s %s<br />chmod u+w %s<br /></code></blockquote>' % (newUID, self.needPermDir, self.needPermDir)
                print '<p>'
                print _("(You probably won't need the second command; it's just to make sure.)")
                print '</p>'
                print '<p>'
                print _('Then fill out the <a href="#Step2">two steps below</a> and click the button to complete installation. Once NewsBruiser is set up you can chown the directory back to its previous owner (probably you).')
                print '</p>'
            else:
                print '<p>'
                print _("Since you don't have root access on this machine, you don't have a way of making it possible for the webserver to write to <b>%s</b> without letting anyone else on your system write to that directory as well. That's why this method is risky: during the time <b>%s</b> is world-writable, someone else on this machine could create their own files in that directory. That's why I recommend you check the NewsBruiser directory for new files and subdirectories after you switch it back from being world-writable.") % (self.needPermDir, self.needPermDir)
                print '</p><p>'
                print _("Here's how to do this. This method minimizes the amount of time the NewsBruiser directory is world-writable:")
                print '</p><ol>'
                print '<li>%s</li>' %  _('Fill out the <a href="#Step2">two steps below</a> and make sure everything on this page is set up correctly.')
                print '<li>%s</li>' % _('Run the command <code>chmod o+w %s</code>') % self.needPermDir
                print '</li>'
                print _('Click this button:')
                self.submitButton(self.SUBMIT_TEXT,
                                  const.MAKE_CHANGE_CGI_KEY)
                print '</li>'
                print '<li>%s</li>' % (_('When you see the main NewsBruiser screen, run the command <code>chmod o-w %s</code>') % self.needPermDir)
                print '<li>%s</li>' %_('Check for any suspicious new files or directories in <b>%s</b>.') % self.needPermDir
                print '</ol>'

        url = self.config.baseURL + os.path.split(os.path.split(self.dataDir)[0])[1]
        print "<p>%s</p>" % (_("""After setting up NewsBruiser, please make sure that the URL <a href="%s">%s</a> is inaccessible (you should get a "403 Forbidden" error). You don't want your passwords and settings to be viewable from the web.""") % (url, url))

        print "<blockquote>"
        print _('<b>Note:</b> If you want to create the data directory somewhere other than in <b>%s</b>, enter the destination directory here. Otherwise, just leave this as <b>%s</b>. If you change this, please see <a href="http://newsbruiser.tigris.org/doc/configuration.html#config.location">the documentation</a> about the manual step you must perform to get NewsBruiser to recognize your alternate choice.') % (self.defaultDataPath, self.defaultDataPath)
        print '<p>'
        print _("The only reason you might want to do this is if your webserver doesn't recognize per-directory .htaccess configuration files: because the webserver is something other than Apache, or because .htaccess configuration files are turned off.""")
        print '<br />'
        self.textField(self.DATA_PATH, self.dataDir, size=100, maxlength=None)
        print '</blockquote>'

        print '<a name="Step2"></a>'
        self.printHeading(_('Step 2: Choosing a master password'), '2')

        print '<p>'
        print _("The NewsBruiser master password allows you to create, delete, and reorder notebooks. This password will also be used as the password of your first notebook (you can change both the master password and the notebook password later).")
        
        print '<p><b>%s</b>' % _('Enter the master password:')
        self.passwordEntryField(const.MASTER_PASSWORD_CGI_KEY, '')
        print '<br/>'
        print '<b>%s</b>' % _('Verify the master password:')
        self.passwordEntryField(const.PASSWORD_VERIFY_CGI_KEY)
        print '</p>'

        self.printHeading(_('Step 3: Naming your first notebook'), '2')

        print '<p>'
        print _("""A notebook is a repository for weblog entries, bookmarks, notes to yourself, news stories: anything you want. A notebook has a short one-word name like "mynotebook" or "weblog", which is used in links to that notebook's entries. Choose a name for your initial notebook here. You can create more notebooks later, or delete this one.""")
        print '</p><p><b>'
        print _('Choose a notebook name:')
        print '</b>'
        self.textField(const.NOTEBOOK_NAME_CGI_KEY, self.notebookName, 50, 20)
        print '</p><p>'
        
        print _("Okay, that's it! Hopefully once you click this button NewsBruiser will be set up and you can start using it immediately. If there are any problems, this page will reload and display the error messages to you.")
        print '</p>'
        self.endForm(self.SUBMIT_TEXT, const.MAKE_CHANGE_CGI_KEY)        

class NewsBruiserAdministrationCGI(AdminCGI):

    """A CGI which lets the user rearrange the list of notebooks and change
    the master password."""

    import CoreCGIs

    NAME = 'administer'

    CHANGE_PASSWORD = 'change'
    NEW_MASTER_PASSWORD = 'mpNew'

    MOVE_BOTTOM = 'bottom'
    MOVE_TOP = 'top'
    MOVE_UP = 'up'
    MOVE_DOWN = 'down'

    CHANGE_PASSWORD='changePassword'

    def assertValidMasterPassword(self):
        masterPassword = self.var(const.MASTER_PASSWORD_CGI_KEY)
        if not self.config.validateMasterPassword(masterPassword):
            self.printError(_("Invalid master password."))
        return masterPassword

    def run(self):
        self.addNavLink(self.getURL(NotebookAddCGI.NAME), _('Create a notebook'))
        if self.var(self.CHANGE_PASSWORD):
            self.changeMasterPassword()
        for i in 'UP', 'DOWN', 'TOP', 'BOTTOM':
            if self.var(getattr(self, 'MOVE_' + i)):
                self.moveSelectedNotebook()
        what = _('NewsBruiser administration')
        self.startPage(what)
        self.startForm(self.getURL())
        self.printHeading(what)

        print '<p><b>%s</b>' % _('Master NewsBruiser password:')
        self.passwordEntryField(const.MASTER_PASSWORD_CGI_KEY)
        print '</p>'
        self.line()

        self.printMasterPasswordChangeForm()
        self.printOrdinalChangeForm()
        self.endForm(None)
        self.endPage()

    def changeMasterPassword(self):
        current = self.assertValidMasterPassword()
        new = self.var(self.NEW_MASTER_PASSWORD)
        verify = self.var(const.PASSWORD_VERIFY_CGI_KEY)
        if new:
            if not verify:
                self.error(_("You must verify the new master password."))
            elif verify != new:
                self.error(_("New password and verification do not match."))
            self.config.setMasterPassword(current, new)

    def moveSelectedNotebook(self):
        self.assertVars([const.NOTEBOOK_NAME_CGI_KEY])
        masterPassword = self.assertValidMasterPassword()
        notebook = self.config.notebooks[self.var(const.NOTEBOOK_NAME_CGI_KEY)]
        oldOrdinal = notebook.getOrdinal()
        newOrdinal = oldOrdinal
        maxOrdinal = len(self.config.getNotebookList())-1
        if self.var(self.MOVE_UP) and oldOrdinal > 0:
            newOrdinal = oldOrdinal - 1
        elif self.var(self.MOVE_DOWN) and oldOrdinal < maxOrdinal:
            newOrdinal = oldOrdinal + 1
        elif self.var(self.MOVE_TOP) and oldOrdinal > 0:
            newOrdinal = 0
        elif self.var(self.MOVE_BOTTOM) and oldOrdinal < maxOrdinal:
            newOrdinal = maxOrdinal

        if newOrdinal != oldOrdinal:
            notebook.setOrdinal(newOrdinal)
        self.config.updateNotebookOrdinals(masterPassword, notebook)

    def printMasterPasswordChangeForm(self):

        if self.config.masterConfigWritable():
            print '<p>'
            print _('Here you can change the NewsBruiser master password.')
            print '</p><p>'
            print _('Enter a new master password:')
            self.passwordEntryField(self.NEW_MASTER_PASSWORD, '')
            print '<br/>'
            print _('Verify the new master password:')
            self.passwordEntryField(const.PASSWORD_VERIFY_CGI_KEY)
            print '</p>'
            self.submitButton(_('Change master password'), self.CHANGE_PASSWORD)
        else:
            print '<p>'            
            print _("You can't change the NewsBruiser master password from here because the webserver doesn't have write permission on the master configuration file. You need to either change the master password manually or make the configuration file writable by the webserver.")
            print '</p>'            

    def printOrdinalChangeForm(self):        
        notebookNames = map(lambda x: x.name, self.config.getNotebookList())
        if len(notebookNames) > 1:
            self.line()
            print '<p>'
            print _('Here you can change the ordering of the list of notebooks. Select a notebook and hit the appropriate button to move it up or down.')
            print '</p>'
            print '<table><tr><td rowspan="4">'
            self.listBox(const.NOTEBOOK_NAME_CGI_KEY,
                         notebookNames, notebookNames,
                         size=len(self.config.notebooks))
            print '</td><td valign="top">'
            self.submitButton(_("Move to top-^"), self.MOVE_TOP)
            print '</td></tr><tr><td>'
            self.submitButton(_("^-Move up"), self.MOVE_UP)
            print '</td></tr>'
            print '<tr><td>'
            self.submitButton(_("v-Move down"), self.MOVE_DOWN)
            print '</td></tr><tr><td valign="bottom">'
            self.submitButton(_("Move to bottom-v"), self.MOVE_BOTTOM)
            print '</td></table>'
        
class NotebookAddCGI(AdminCGI):

    """A CGI which lets the user create a new notebook."""

    NAME = 'notebook-add'

    ADD_NOTEBOOK = 'add'
    NOTEBOOK_PASSWORD = 'notebookPassword'
    NOTEBOOK_PASSWORD_VERIFY = 'notebookPasswordVerify'

    def run(self):
        if self.var(self.ADD_NOTEBOOK):
            self.add()
        else:
            what = _('Creating a new notebook')
            self.startPage('NewsBruiser: %s' % what)
            self.printHeading(what)
            if self.config.notebookDirWritable():
                self.printForm()
            else:
                print '<p>'                
                print _("You can't create a notebook because the webserver doesn't have write permission to the main notebook directory. You need to make that directory writable by the webserver at least long enough to create a notebook.")
                print '</p>'
            self.endPage()

    def add(self):

        self.assertVars([const.NOTEBOOK_NAME_CGI_KEY])

        masterPassword = self.var(const.MASTER_PASSWORD_CGI_KEY)
        if not self.config.validateMasterPassword(masterPassword):
            self.printError(_("Invalid master password."))

        name = self.var(const.NOTEBOOK_NAME_CGI_KEY)
        password = self.var(self.NOTEBOOK_PASSWORD)
        passwordVerify = self.var(self.NOTEBOOK_PASSWORD_VERIFY)
        ordinal = self.var(const.NOTEBOOK_ORDINAL_CGI_KEY)

        if not password:
            self.printError(_('No notebook password specified.'))
        if not passwordVerify:
            self.printError(_('You must verify the notebook password.'))

        if self.config.getNotebooks().has_key(name):
            self.printError(_('There already is a notebook called "%s"; you can\'t create another one.') % name)

        if password != passwordVerify:
            self.printError(_('Notebook passwords do not match.'))

        if string.find(name, 'deleted-') == 0:
            self.printError(_('"deleted-" is a reserved notebook prefix.'))

        if string.find(name, 'NewsBruiserMaster') == 0:
            self.printError(_('"NewsBruiserMaster" is a reserved name.'))


        notebook = self.createNotebook(self.config, masterPassword, name,
                                       password, ordinal)
        self.redirectTo(notebook.getURL(PortalCGI.NAME))

    def printForm(self):
        self.startForm(self.getURL())

        print '<p>'
        self.masterPasswordField()
        print '</p>'
        self.line()

        print '<p><b>%s</b>' % _('Notebook name:')
        self.textField(const.NOTEBOOK_NAME_CGI_KEY,
                       self.var(const.NOTEBOOK_NAME_CGI_KEY), 50, 20)
        print '<br />'
        print _('This should be a one-word name like "mynotebook" or "weblog". It\'s used in URLs that view and edit the notebook. Currently this cannot be changed once the notebook is created (but future versions of NewsBruiser will allow this).')
        print '</p>'

        print '<p><b>%s</b>' % _('Notebook password:')
        self.passwordEntryField(self.NOTEBOOK_PASSWORD)
        print '<br/>'
        print '<b>%s</b>' % _('Verify notebook password:')
        self.passwordEntryField(self.NOTEBOOK_PASSWORD_VERIFY)
        print '<br/>'
        print _('Enter the administrative password for the new notebook. The administrator of the new notebook will have to enter this password to add entries or configure the notebook. The administrator of the new notebook can change this later.')
        print '</p>'

        if self.config.getNotebookList():
            print '<p><b>%s</b>' % _('Notebook order:')
            notebookNames = map(lambda x: x.name, self.config.getNotebookList())
            option = _('[new notebook]')
            default = len(notebookNames)
            optionValues = range(0, len(notebookNames)+1)
            optionDescriptions = [_('First (before "%s")') % notebookNames[0]]
            for i in optionValues[1:-1]:
                optionDescriptions.append(_('Between "%s" and "%s"') % (notebookNames[i-1], notebookNames[i]))
            optionDescriptions.append(_('Last (after "%s")') % notebookNames[-1])
            self.listBox(const.NOTEBOOK_ORDINAL_CGI_KEY, optionDescriptions,
                         optionValues, default)                     
            print '<br/>'
            print _("Select where in the list of existing notebooks the new notebook should be inserted. You can change this later.")
            print '</p>'

        self.endForm(_('Add notebook'), self.ADD_NOTEBOOK)

class PortalCGI(AdminCGI):

    """A CGI which prints out a control panel for each notebook or one
    particular notebook, with links to all of NewsBruiser's major
    functionality."""

    NAME = 'portal'
    CGI_ALIASES = ['index']

    linkBars = []

    def registerLinkBarAndDisplayPredicate(position, predicateFunc,
                                           renderFunc):
        linkBarsWithDisplayPredicates.append(predicateFunc, renderFunc)

    def addDefaultNavigationLinks(self):        
        if not self.usedDefaultNotebook:
            for notebook in self.config.getNotebookList():
                if notebook is not self.notebook and not notebook.isPrivate():
                    from nb.NewsBruiserCGIs import PortalCGI
                    self.navLinks.insert(0, (self.config.urlRewriter.rewrite(PortalCGI.NAME), _('Show all')))
                    break

    def run(self):
        "Runs the CGI, printing the control panel(s)."
        title = _('Notebook list')
        if not self.usedDefaultNotebook:
            title = self.notebook.getTitle()
        self.startPage(title)
        self.notebookAuths = self.parseNotebookAuthCookie()
        self.makeList()
        if self.notebookAuths and (self.usedDefaultNotebook or len(self.config.getNotebookList()) == 1):
            self.addNavLink(self.getURL(NotebookAddCGI.NAME),
                            _('Create a notebook'))
            self.addNavLink(self.getURL(NewsBruiserAdministrationCGI.NAME),
                            _('Site administration'))
        self.endPage()

    def headHook(self):
        "Override this method to print out some content in the HTML HEAD."
        return self.getRobotExclusionTag(0, 1)

    def assertViewability(self):
        if not (self.usedDefaultNotebook or self.preAuth or not self.notebook.isPrivate() or not self.notebook.useCookies()):
            self.redirectToLogin()        

    def makeList(self):
        "Prints the list of notebooks."

        if not self.usedDefaultNotebook:
            self.printNotebook(self.notebook)
        else:
            for notebook in self.config.notebookList:
                self.printNotebook(notebook)    

    def printNotebook(self, notebook):            
        loggedIn = self.getAuthenticationStatus(notebook)
        useCookies = notebook.useCookies()
        if notebook.isPrivate() and not loggedIn and \
               not (not self.usedDefaultNotebook and \
                    self.notebook == notebook and not useCookies):
            return

        prevEntries = notebook.getPreviousEntries(notebook.getNumberOfFrontPageEntries())
        viewer = Viewer(notebook, prevEntries, 0)
        viewer.out = sys.stdout
        viewer.cgi = self
        viewer.authenticated = loggedIn or not useCookies

        notebook.interpolateTemplate('main-page', CommandMaps.ENTRY_LIST_COMMAND_MAP, viewer, viewer, viewer.out)

#This is a hack to make the CoreCGIs module register itself with the
#dispatcher before the dispatcher actually runs.
import CoreCGIs

