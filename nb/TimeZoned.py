import os
import string
import time
from lib import DateArithmetic
from nb.lib import IWantOptions

class TimeZoned:
    """An abstract class each instance of which can have a timezone
    associated with it. Time zones are stored in Posix format or in
    GMT offset format."""

    def __init__(self, zone):
        self.zone = zone                    

    def getTimeZone(self):
        """This method should return a time zone in Posix XXX#YYY format,
        like 'PST-8PDT'"""
        return self.zone

    def setTimeZone(self):
        if os.environ.has_key('TZ'):
            self.oldTimeZone = os.environ['TZ']
        newZone = self.getTimeZone()
        if newZone:
            os.environ['TZ'] = newZone

    def getSystemTimeZone(self):
        normal, daylight = time.tzname
        minutes = time.timezone/60
        hours, minutes = divmod(minutes, 60)
        zone = normal + str(hours)
        if minutes:
            zone = zone + ':' + str(minutes)
        zone = zone + daylight
        return zone

    def unsetTimeZone(self):
        if hasattr(self, 'oldTimeZone'):
            os.environ['TZ'] = self.oldTimeZone
            
    def getGMTOffset(self, daylight=time.daylight):
        """Returns the offset of the current timezone, in seconds west
        of GMT. Unfortunately, we can't just set the time zone and use
        time.tzset(), because that's a feature of Python 2.3."""
        if not hasattr(self, 'gmtOffsetSeconds'):
            zone = self.getTimeZone()
            if zone:                
                i = 3
                while i < len(zone) and not zone[i] in string.letters and zone[i] != ':':
                    i = i + 1

                hours = int(zone[3:i])
                minutes = 0

                if not i == len(zone):
                    #There's more than just the hour offset, eg. GMT+08:00 instead of GMT+8
                    if zone[i] == ':':
                        oldI = i + 1
                        i = oldI
                        while i < len(zone) and not zone[i] in string.letters:
                            i = i + 1
                        minutes = int(zone[oldI:i])                                            

                #FIXME: Doesn't handle time zones where DST is not 1 hour.
                if daylight:
                    hours = hours - 1
                secs = (hours * 60 * 60) + (minutes * 60)
            else:
                if time.daylight:
                    secs = time.timezone
                else:
                    secs = time.altzone
            self.gmtOffsetSeconds = secs
#        import traceback
#        print("\nCalled [%s]\n" % (str(traceback.extract_stack())))
        return self.gmtOffsetSeconds

    def getTimeZoneName(self, dst, forceGMTOffset=0):
        """Returns the human-readable name of this time zone (eg 'EDT').
        If no human-readable name can be determined, uses a GMT offset.
        Will also use a GMT offset if forced."""        
        name = 'timeZoneName'
        if dst:
            name = name + 'DST'
        value = getattr(self, name, None)
        if not value:
            value = IWantOptions.timeZones.getAbbreviationFromName(self.getTimeZone())
            setattr(self, name, value)
        return value

    def today(self):
        "Returns the current date in this time zone."
        self.setTimeZone()
        todayForThisNotebook = DateArithmetic.today()
        self.unsetTimeZone()
        return todayForThisNotebook    
