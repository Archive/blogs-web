import string
import sys
import time
import types

from lib.LOMP import HtmlGenerator

class NotebookStatistics:

    """A container for statistics about a notebook."""

    def __init__(self, notebook):
        self.notebook = notebook
        self.statsByPriority = {}
        self.__registerStatClasses(sys.modules[self.__module__])
        self.stats = self.__sortStats()
        
    def gather(self):
        """Gather all the registered statistics."""
        self.entries = self.notebook.getEntries()
        if self.stats:
            for stat in self.stats:
                stat.processEntries(self.entries)

            for entry in self.entries:
                for stat in self.stats:
                    stat.processEntry(entry)        

    def __sortStats(self):
        "Returns a list of NotebookStat objects, ordered by priority."
        stats = []
        keys = self.statsByPriority.keys()
        keys.sort()
        for key in keys:
            stats.append(self.statsByPriority[key])
        return stats

    def __registerStatClasses(self, module):
        """Registers all stat classes defined in the provided
        module in the master list of stat classes."""
        for c in module.__dict__.values():
            if hasattr(c, 'STAT_NAME'):
                o = c()
                p = o.PRIORITY
                while self.statsByPriority.has_key(p):
                    p = p + 1
                self.statsByPriority[p] = o

### A bunch of base classes.

class NotebookStat:

    """Base class for notebook statistic handlers. Define a proprety
    called STAT_NAME to get picked up on the stats CGI."""

    #STAT_NAME = "Name of this stat."
    STAT_DESCRIPTION = ''
    PRIORITY = 0

    def printStatistic(self):
        self.abstract()

    def processEntries(self, entries):
        pass    

    def processEntry(self, entry):
        pass

    def abstract(self):
        raise self.__class__.__name__ + " is an abstract class!"

class EntryStat(NotebookStat, HtmlGenerator):

    """An abstract class for finding an entry with a certain property."""

    def __init__(self):
        self.entry = None
        self.entryField = None

    def printStatistic(self):
        if self.entry:
            self.linkTo(self.entry.getAbsoluteURL(),
            self.entry.sample())
            self.printField()
        else:
            print "N/A"

    def printField(self):
        pass

class ExtremalEntryStat(EntryStat):

    """An abstract class that finds the entry at some extreme."""

    def processEntry(self, entry):
        amount = self.measureEntry(entry)
        if not self.entryField \
               or self.extremityComparator(amount, self.entryField):
            self.entry = entry
            self.entryField = amount

    def measureEntry(self, entry):
        self.abstract()

    def extremityComparator(self, amt1, amt2):
        self.abstract()

class MostExtremeEntryStat(ExtremalEntryStat):

    """An abstract class that finds the most extreme entry, for some
    definition of "extreme"."""

    def extremityComparator(self, amt1, amt2):
        return amt1 > amt2
    
class LeastExtremeEntryStat(ExtremalEntryStat):

    """An abstract class that finds the least extreme entry, for some
    definition of "extreme"."""

    def extremityComparator(self, amt1, amt2):
        return amt1 < amt2

class ByteMeasurementStat:

    """An abstract class that keeps track of entries with some
    property based on byte count."""

    def measureEntry(self, entry):
        return int(entry.getTotalSizeBytes())

    def printField(self):
        print ' (%s bytes)' % self.entryField

class PositionalEntryStat(EntryStat):

    """An abstract class for finding an entry in a particular position."""

    def processEntries(self, entries):
        if entries:
            self.entry = entries[self.getOffset()]

    def processEntry(self, entry):
        pass

    def printField(self):
        print '(' + self.entry.getFormattedDate() + ')'

    def getOffset(self):
        self.abstract()

### The actual statistic classes.

class Count(NotebookStat):

    STAT_NAME = "Number of entries"
    STAT_DESCRIPTION = "The number of entries posted to this notebook."

    def __init__(self):
        self.total = 0

    def processEntries(self, entries):
        self.total = len(entries)

    def printStatistic(self):
        print self.total

class DaysRunning(NotebookStat):

    PRIORITY = 1
    STAT_NAME = "Days running"
    STAT_DESCRIPTION = "Days elapsed since this notebook's first entry."

    def __init__(self):
        self.daysElapsed = 0

    def printStatistic(self):
        print self.daysElapsed

    def processEntries(self, entries):
        if entries:
            first = entries[0]
            secondsElapsed = time.time() - time.mktime(first.getTimeTuple())
            self.daysElapsed = int(secondsElapsed / 60 / 60 / 24)

class MeanEntriesPerDay(DaysRunning):

    PRIORITY = 2
    STAT_NAME = "Average entries per day"
    STAT_DESCRIPTION = "The average number of entries posted each day since this notebook was started."

    def printStatistic(self):
        print "%.3f" % self.mean

    def processEntries(self, entries):
        DaysRunning.processEntries(self, entries)
        if self.daysElapsed:
            self.mean = float(len(entries)) / self.daysElapsed
        else:
            self.mean = 0

class TotalSize(NotebookStat):

    PRIORITY = 3
    STAT_NAME = "Total entry size"
    STAT_DESCRIPTION = "The total size of all entries ever posted."

    def __init__(self):
        self.total = 0

    def processEntry(self, entry):
        self.total = self.total + entry.getTotalSizeBytes()

    def printStatistic(self):
        print self.total, " bytes"

class MeanSize(TotalSize):

    PRIORITY = 4
    STAT_NAME = "Average entry size"
    STAT_DESCRIPTION = "The average size of an entry in this notebook."

    def printStatistic(self):
        if self.size:
            self.total = float(self.total)/self.size
        else:
            self.total = 0
        self.total = "%.3f" % self.total
        TotalSize.printStatistic(self)

    def processEntries(self, entries):
        self.size = len(entries)

class FirstEntry(PositionalEntryStat):

    PRIORITY = 5
    STAT_NAME = "First entry"
    STAT_DESCRIPTION = "The first entry ever posted to this notebook."            
    def getOffset(self):
        return 0

class MostRecentEntry(PositionalEntryStat):

    PRIORITY = 6
    STAT_NAME = "Most recent entry"
    STAT_DESCRIPTION = "The entry most recently posted to this notebook."

    def getOffset(self):
        return -1

class LongestEntry(ByteMeasurementStat, MostExtremeEntryStat):

    PRIORITY = 7
    STAT_NAME = "Longest entry"
    STAT_DESCRIPTION = "The longest entry ever posted to this notebook."

class ShortestEntry(ByteMeasurementStat, LeastExtremeEntryStat):

    PRIORITY = 8
    STAT_NAME = "Shortest entry"
    STAT_DESCRIPTION = "The shortest entry ever posted to this notebook."

class LongestTitle(MostExtremeEntryStat):
    PRIORITY=9

    STAT_NAME = "Longest title"
    STAT_DESCRIPTION = "The entry with the longest title."

    def measureEntry(self, entry):
        return len(entry.getTitle())

class MostEmphaticEntry(MostExtremeEntryStat):

    PRIORITY = 10
    STAT_NAME = "Most emphatic entry"
    STAT_DESCRIPTION = "The entry containing the most exclamation marks."

    def measureEntry(self, entry):
        return string.count(entry.text, '!')
    
    def printField(self):
        print ' (%s exclamation marks)' % self.entryField

class MostInsecureEntry(MostExtremeEntryStat):

    PRIORITY = 11
    STAT_NAME = "Most insecure entry"
    STAT_DESCRIPTION = "The entry containing the most question marks."

    def measureEntry(self, entry):
        return string.count(entry.text, '?')
    
    def printField(self):
        print ' (%s question marks)' % self.entryField

class MostLinkyEntry(MostExtremeEntryStat):

    PRIORITY = 12
    STAT_NAME = "Most linky entry"
    STAT_DESCRIPTION = "The entry containing the most HTML links."

    def measureEntry(self, entry):
        return string.count(entry.text, 'href')
    
    def printField(self):
        print ' (%s links)' % self.entryField

class MostFrequentlyUsedWords(NotebookStat):

    PRIORITY = 13
    STAT_NAME = "Most common words"
    STAT_DESCRIPTION = "The most common words used in this notebook's entries."

    CUTOFF = 50

    def processEntries(self, entries):
        self.popular = {}

    def processEntry(self, entry):
        for text in entry.getTitle(), entry.text:
            for word in string.split(text):
                if len(word) > 4 and not '<' in word:
                    self.popular[word] = self.popular.get(word, 0) + 1

    def printStatistic(self):
        top = {}
        i = 0
        topPop = 0
        for (word, pop) in self.popular.items():
            l = top.get(pop, [])
            l.append(word)
            top[pop] = l
        keys = top.keys()
        keys.sort()
        keys.reverse()

        done = 0
        i = 0
        print '<ol>'
        for key in keys:            
            for val in top[key]:
                print '<li>%s:%s</li>' % (val, key)
                i = i + 1
                if i >= self.CUTOFF:
                    done = 1
                    break
            if done:
                break
        print '</ol>'

