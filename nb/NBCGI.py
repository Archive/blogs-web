import cgi
import os
import re
import sys
import string
import types
import time

from lib import DateArithmetic
from lib.PriorityOuch import PriorityList

import const
import util

import CommandMaps
from HtmlGenerator import HtmlGenerator
from NBProcess import NBProcess
from EntryID import EntryID
from Viewer import Viewer

AUTH_COOKIE_NAME = 'NewsBruiser'
SPECIAL_COOKIE_CHARACTERS = [('%', '%25'), (';', '%3B'), (',', '%2C'),
                             (' ', '%30')]

### Static methods for use in initializing plugins.

def registerDisplaySnippet(c, position, func, priority):
    if not c.__dict__.has_key('displaySnippets'):
        c.displaySnippets = {}
    l = c.displaySnippets.get(position)
    if not l:
        l = PriorityList()
        c.displaySnippets[position] = l
    l.add((func, priority))

def registerHookAction(c, action, func):
    """Registers an action that corresponds with a display snippet
    that implemented form controls with a separate submit
    button. Currently not all CGIs respect this."""
    if not c.__dict__.has_key('hookActions'):
        c.hookActions = {}
    c.hookActions[action] = func

class NBCGI(NBProcess, HtmlGenerator): 

    """An abstract CGI class which gets some CGI variables and calls a 'run'
    function (variables and 'run' function supplied by descendants). If
    something goes wrong, it prints a traceback to the web browser."""

    def __init__(self, pathInfoProcessor=None, notebookNameProcessor=None,
                 submittedDataProcessor=None, pathInfo=''):
        """Get notebook, entry identifier, and any variables specified
        by the subclass."""        
        NBProcess.__init__(self)
        self.setMinimalVariables()
        self.pathInfo = pathInfo

        self.id = None
        if not self.config.configFile:
            from NewsBruiserCGIs import InstallCGI
            if not hasattr(self, 'NAME') or (self.NAME != InstallCGI.NAME):
                if self.inSSIInclude():
                    InstallCGI()
                    return
                else:
                    self.redirectTo(self.config.urlRewriter.rewrite(InstallCGI.NAME))
                    sys.exit(0)

        #If there are no notebooks, send them to the notebook add CGI
        #because otherwise NewsBruiser is useless.
        if self.config.configFile and not self.config.getNotebookList():
            from NewsBruiserCGIs import NotebookAddCGI
            addNotebook = NotebookAddCGI.NAME
            if hasattr(self, 'NAME') and self.NAME != addNotebook:
                self.redirectTo(self.config.urlRewriter.rewrite(addNotebook))

        if not pathInfoProcessor:
            pathInfoProcessor = self.processPathInfoAsEntryID
        if not notebookNameProcessor:
            notebookNameProcessor = self.processNotebookName
        if not submittedDataProcessor:
            submittedDataProcessor = self.processSubmittedDataAsFormFields

        timing = 0
        try:
            if self.config:
                self.notebooks = self.config.getNotebooks()
            else:
                self.config = None
            self.nice()

            self.variables={}
            submittedDataProcessor()

            #A brief digression; determine the notebook name.            
            notebookNameProcessor()

            if self.notebook and self.notebook.timeActions():
                timing = 1
                t = time.clock()

            #Redirect them to login if they can't see this page.
            self.assertViewability()

            #Now process the remaining path info with the per-CGI processor.
            self.entries = []
            pathInfoProcessor()
            self.viewer = self.buildViewer()            

            self.run()
        except SystemExit:
            #If we exit normally, don't print a traceback.
            pass
        except: #Something went wrong. Print a traceback.
            self.printHeader()
            print "\n\n<pre>"   
            import traceback
            ex = traceback.format_exception(sys.exc_type,sys.exc_value,sys.exc_traceback)
            exText = ''
            for line in ex:
                if line:
                    line = string.replace(line, "<", "&lt;")
                    line = string.replace(line, ">", "&gt;")
                    exText = exText + line
            print exText
            print "</pre>"
        if timing:
            print _('Time elapsed: %s sec') % (time.clock()-t)

    def shiftPathInfo(self):
        thisAndRest = string.split(self.pathInfo, '/', 1)    
        if thisAndRest:
            this = thisAndRest[0]
        if len(thisAndRest) > 1:
            self.pathInfo = thisAndRest[1]
        else:
            self.pathInfo = ''
        return this

    def unshiftPathInfo(self, val):
        if val:
            self.pathInfo = val + '/' + self.pathInfo

    def nullNotebookNameProcessor(self):
        pass

    def processNotebookName(self):
        notebookName = self.shiftPathInfo()
        if not notebookName:
            #No notebook at all, but maybe they specified a
            #notebook in a CGI field.
            notebookName = self.var(const.NOTEBOOK_CGI_KEY)

        self.usedDefaultNotebook = 1
        if notebookName:
            notebook = self.config.notebooks.get(notebookName, None)
            if notebook:
                self.usedDefaultNotebook = 0
                self.notebook = notebook
            else:
                if notebookName != 'drafts':
                    try:
                        #Maybe they specified the start of an entry ID
                        int(notebookName)
                    except ValueError:                        
                        self.notebook = self.config.getDefaultNotebook()
                        self.printError(_('No such notebook: %s') % notebookName)
                self.notebook = self.config.getDefaultNotebook()
                self.unshiftPathInfo(notebookName)
                notebookName = self.notebook.name
        else:
            self.notebook = self.config.getDefaultNotebook()
            self.unshiftPathInfo(notebookName)

        #If the user has a cookie containing a valid password for
        #this notebook, and the notebook is set to allow such
        #things, set it and set preAuth to true so that password
        #fields won't be printed out.        
        pw = self.var(const.PASSWORD_CGI_KEY)
        if not hasattr(self, 'ignoreAuthCookie') and self.notebook and self.notebook.useCookies():
            notebookAuths = self.parseNotebookAuthCookie()
            if pw and self.notebook.validatePassword(pw):
                notebookAuths[self.notebook.name] = pw
                self.setNotebookAuthCookie(notebookAuths)
                self.preAuth = 1
            elif notebookAuths.has_key(self.notebook.name):
                pw = notebookAuths[self.notebook.name]

        if self.notebook and self.notebook.validatePassword(pw):
            self.variables[const.PASSWORD_CGI_KEY] = pw
            self.preAuth = 1

    def setMinimalVariables(self):
        """Sets the minimal variables neccessary to do things like
        print errors."""
        for i in ('printedHeader', 'preAuth'):
            if not hasattr(self, i):
                setattr(self, i, 0)
        self.notebook = None
        self.navLinks = []
        self.usedDefaultEntryID = 0

    def assertViewability(self):
        """Redirect the user to the login page if they shouldn't be
        allowed to see this page."""
        if not self.preAuth and (self.notebook and self.notebook.isPrivate() \
                                 and self.notebook.useCookies()) \
                                 or (self.id and self.id.draft):
            self.redirectToLogin()

    def ignoreSubmittedData(self):
        "Does nothing to submitted data."
        pass
   
    def processSubmittedDataAsFormFields(self):
        "Move variables from CGI fields into the variables array."
        form = cgi.FieldStorage()
        if form.list:
            for key in form.keys():            
                if not self.variables.has_key(key):                    
                    if type(form[key]) == types.ListType:
                        #This is to handle one CGI field with multiple
                        #values. We want a list of the string values
                        #rather than a list of MiniFieldStorage objects.
                        value = []
                        for field in form[key]:
                            value.append(field.value)
                    elif form[key].filename:
                        value = form[key]
                    else:
                        value = form[key].value
                    self.variables[key] = value
                                                                
    def var(self, varname, default=''):
        """Returns the value(s) of the given variable, or the default
        if it's not defined."""
        return self.variables.get(varname, default)

    def getFormValuesWithPrefix(self, prefix):
        "Get all the values of form fields whose names match the given prefix."
        matches = {}
        for (key, value) in self.variables.items():
            if string.find(key, prefix) == 0:
                matches[key[len(prefix):]] = value
        return matches

    def assertVars(self,vars):
        """Prints an error if any of the provided variables were not
        found in CGI fields."""
        badvars = []
        for var in vars:
            if not self.variables.has_key(var):
                badvars.append(var)
        if badvars != []:
            err = _("Missing the following required variables: ")
            for var in badvars:
                err = err + '<u>' + var + '</u> '
            self.printError(err)

    def redirectTo(self, url):
        print 'Status: 302 Moved'        
        print 'Location: %s\n' % url

    def redirectToLogin(self):
        args = None
        args = os.environ.get('REQUEST_URI')
        if args:
            args = const.REFERER_CGI_KEY + '=' + args
        from nb.CoreCGIs import LoginCGI
        self.redirectTo(self.notebook.getURL(LoginCGI.NAME, args))

    def setCookie(self, name, value):
        for (char, hexChar) in SPECIAL_COOKIE_CHARACTERS:
            value = string.replace(value, char, hexChar)
        print 'Set-Cookie: %s=%s; path=%s' % (name, value,
                                              self.config.urlRewriter.getCookiePath())

    def getAuthenticationStatus(self, notebook=None):
        loggedIn = 0
        if not notebook:
            notebook = getattr(self, notebook, None)
        if notebook:
            useCookies = notebook.useCookies()
            notebookAuths = self.parseNotebookAuthCookie()
            if useCookies and notebookAuths.has_key(notebook.name) and notebook.validatePassword(notebookAuths[notebook.name]):
                loggedIn = 1            
        return loggedIn

    def clearAuthCookie(self):
        self.setCookie(AUTH_COOKIE_NAME, '')

    def setNotebookAuthCookie(self, authMap):
        values = []
        for (key, value) in authMap.items():
            values.append(key + ':' + value)
        self.setCookie(AUTH_COOKIE_NAME, string.join(values, '&'))

    def encodeCookie(self, s):
        for (char, hexChar) in SPECIAL_COOKIE_CHARACTERS:
            s = string.replace(s, char, hexChar)
        return s

    def decodeCookie(self, s):
        if s:
            for (char, hexChar) in SPECIAL_COOKIE_CHARACTERS:
                s = string.replace(s, hexChar, char)
        return s

    def parseCookies(self):
        self.cookies = {}        
        cookies = string.split(os.environ.get('HTTP_COOKIE', ''), '; ')
        if cookies:
            for cookie in cookies:
                if cookie:
                    (name, value) = string.split(cookie, '=', 1)
                    self.cookies[name] = value

    def parseNotebookAuthCookie(self):
        self.parseCookies()
        notebookAuths = {}
        value = self.cookies.get(AUTH_COOKIE_NAME)
        if value:
            value = self.decodeCookie(value)
            pairs=string.split(value, '&')
            for pair in pairs:
                try:
                    (key, value) = string.split(pair, ':')
                    notebookAuths[key] = value
                except:
                    pass
        return notebookAuths                              

    def addNavLink(self, url, description):
        "Adds a URL and a description to the navigation bar."
        self.navLinks.append((url, description))

    def headHook(self):
        "Override this method to return a string of content in the HTML HEAD."
        return self.getRobotExclusionTag(0, 0)

    def getURL(self, cgi=None, args='', pathInfo=''):
        """Returns the URL to the named CGI."""
        if not cgi:
            cgi = self.NAME
        if hasattr(self, 'notebook') and self.notebook:
            url = self.notebook.getURL(cgi, args, pathInfo)
        else:
            url = cgi
            if pathInfo:
                url = util.urljoin(url, pathInfo)
            if args:
                url = url + '?' + args
            url = self.config.urlRewriter.rewrite(url)
        return url

    def buildViewer(self):
        v = Viewer(self.notebook, self.entries)
        v.cgi = self
        v.out = getattr(self, 'out', sys.stdout)
        loggedIn = 0
        if hasattr(self, 'notebook') and not hasattr(v, 'authenticated'):
            notebookAuths = self.parseNotebookAuthCookie()
            if self.notebook:
                useCookies = self.notebook.useCookies()
                loggedIn = self.getAuthenticationStatus(self.notebook)
            else:
                useCookies = 1
                loggedIn = 0
            if useCookies and self.notebook and notebookAuths.has_key(self.notebook.name) and self.notebook.validatePassword(notebookAuths[self.notebook.name]):
                loggedIn = 1
        v.authenticated = loggedIn or not useCookies
        return v

    ### Methods to do with the plugin system
        
    def printDisplaySnippets(self, position):
        for func in getattr(self, 'displaySnippets', {}).get(position, []):
            func(self)

    def gatherDisplaySnippets(self, position):
        s = ''
        for func in getattr(self, 'displaySnippets', {}).get(position, []):
            s = s + func(self)
        return s

    def processHookActions(self):
        """Checks whether any plugin-specified action was triggered,
        and executes corresponding the hook function if so. Returns true
        iff one or more hook functions was executed."""
        ran = 0
        for key, func in getattr(self, 'hookActions', {}).items():
            if self.variables.has_key(key):
                func(self)
                ran = 1
        return ran

    ### Methods to do with display

    def startPage(self, title, baseURL=None, printTheme=1, titlePrefix=''):
        self.printHeader()
        self.title = title
        self.titlePrefix = titlePrefix
        self.baseURL = baseURL
        self.printTheme = printTheme
        viewer = getattr(self, 'viewer', None)
        out = sys.stdout
        if viewer:
            out = viewer.out
        if hasattr(self, 'notebook') and self.notebook:
            self.notebook.interpolateTemplate('master-header',
                                              CommandMaps.MASTER_HEADER_TEMPLATE_COMMAND_MAP,
                                              self, viewer, out)
        else:
            #Minimal page, for, eg. when you put in a notebook that
            #doesn't exist.
            print '<html><head><title>%s</title></head><body>' % title

    def getDoctype(self):
        return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n'

    def getHeadContents(self):
        s = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n'        

        if hasattr(self, 'baseURL') and self.baseURL:
            s = s + '<base href="%s" />\n' % self.baseURL
        
        if self.notebook and self.notebook.getStyleSheet():
                s = s + '<link rel="stylesheet" type="text/css" href="%s" />\n' % self.notebook.getStyleSheet()        
        s = s + self.gatherDisplaySnippets('headData')
        headData = self.headHook()
        if headData:
            s = s + headData
        return s

    def getTitle(self):
        if self.title:
            s = self.title
            if self.titlePrefix:
                s = self.titlePrefix + self.title
        else:
            s = 'NewsBruiser'
        return s

    def getBodyColors(self):
        if self.notebook and self.printTheme:
            return 'link=%s alink=%s vlink=%s bgcolor=%s text=%s' % self.notebook.getBasicTheme()
        return ''

    def endPage(self, defaultNavigationLinks=1):
        if defaultNavigationLinks:
            self.addDefaultNavigationLinks()        
        if hasattr(self, 'notebook') and self.notebook:
            viewer = getattr(self, 'viewer', None)
            if not viewer:
                viewer = Viewer(self.notebook, [])
                viewer.out = sys.stdout
            out = viewer.out
            self.notebook.interpolateTemplate('master-footer',
                                              CommandMaps.MASTER_FOOTER_TEMPLATE_COMMAND_MAP,
                                              self, viewer, out=out)
        else:
            print '</body></html>'

    def addDefaultNavigationLinks(self):
        if self.notebook:
            from nb.NewsBruiserCGIs import PortalCGI
            self.navLinks.insert(0, (self.notebook.getURL(PortalCGI.NAME),
                                     _('Main')))
    
    def getContextualLinks(self):
        s = ''
        from NewsBruiserCGIs import PortalCGI
        from nb.NewsBruiserCGIs import PortalCGI
        from nb.CoreCGIs import LogoutCGI
        for (link, text) in self.navLinks:
            s = s + self.getPanelLink(link, text) + ' '
        if self.preAuth:
            s = s + self.getPanelLink(self.notebook.getURL(LogoutCGI.NAME), _('Logout'))
        return s

    def passwordField(self, name=const.PASSWORD_CGI_KEY, value='', size=20,
                      maxlength=20, force=0):
        """Prints a standard password field. Only does this if the NewsBruiser
        user is not currently authenticated via a cookie."""
        print self.getPasswordField(name, value, size, maxlength, force)

    def getPasswordField(self, name=const.PASSWORD_CGI_KEY, value='', size=20,
                         maxlength=20, force=0):
        s = ''
        if force or not hasattr(self, 'preAuth') or not getattr(self,
                                                                'preAuth'):
            s = '<strong>' + _('Password:') + '</strong> '
            s = s + self.getPasswordEntryField(name, value, size, maxlength)
        return s

    def masterPasswordField(self):        
        print '<b>' + _('Master NewsBruiser password:') + '</b>'
        self.passwordEntryField(const.MASTER_PASSWORD_CGI_KEY)
        print ''

    def getReferer(self):
        return os.environ.get('HTTP_REFERER', '')

    #### PATH_INFO processor methods

    def nullPathInfoProcessor(self):
        pass

    def processPathInfoAsCategoryPathOrEntryID(self):
        """Try to process the given PATH_INFO as a category ID. If
        this fails, try to process it as an entry ID."""
        self.processPathInfoAsCategoryPath(0)
        if not self.category:
            self.processPathInfoAsEntryID()

    def processPathInfoAsCategoryPath(self, required=1):
        """Attempts to turn the value of PATH_INFO into a list of
        categories."""
        self.categories = []        
        self.categoryPath = self.pathInfo
        prevCat = None
        for part in string.split(self.pathInfo, '/'):
            if prevCat:
                map = prevCat.getChildren()
            else:
                map = self.notebook.getCategories()
            if part:
                if map.has_key(part):
                    prevCat = map[part]
                    self.categories.append(prevCat)
                else:
                    if required:
                        self.printError(_('No such category: %s') % self.pathInfo)
                    break
        if self.categories:
            self.category = self.categories[-1]
        else:
            self.category = None

    def processPathInfoAsEntryID(self):
        """Attempts to turn the value of PATH_INFO into self.notebook
        and self.id."""

        #We already have self.notebook; the other member obtained from
        #PATH_INFO is self.id
        draft = 0
        mightBeDraft = self.shiftPathInfo()
        if mightBeDraft == 'drafts':
            if not self.preAuth:
                self.redirectToLogin()
            draft = 1
        else:
            self.unshiftPathInfo(mightBeDraft)

        if draft:
            defaultYearMonth = [const.ALL, const.ALL]
        else:
            defaultYearMonth = self.notebook.today()[:2]

        if self.pathInfo:
            idDescription = [defaultYearMonth[0], const.ALL, const.ALL,
                             const.ALL]
        else:
            idDescription = [defaultYearMonth[0], defaultYearMonth[1],
                             const.ALL, const.ALL]
            self.usedDefaultEntryID = 1

        parts = string.split(self.pathInfo, '/')
        l = len(parts)-1
        for i in range(0,len(idDescription)):
            if l >= i and parts[0]:
                if i == 0 and parts[0]=="lastmonth":

                    #Special case: cgi/notebook/lastmonth means "previous
                    #month". This is so a generic "archives link" will
                    #always work
                    (idDescription[0], idDescription[1]) = DateArithmetic.previousMonth(defaultYearMonth)
                    i = 2
                else:
                    idDescription[i] = parts[0]
            elif self.var(const.DATE_CGI_KEYS[i]):
                idDescription[i] = self.var(const.DATE_CGI_KEYS[i])
            parts = parts[1:]
        
        #Handle "day-ordinal" even though "day/ordinal" is the
        #canonical form for URLs.  "day-ordinal" is the way the
        #filesystem does it, so we should accept it just to be
        #consistent.
        if idDescription[1] <> const.ALL and type(idDescription[1]) != types.IntType and string.find(idDescription[1], '-') <> -1:
            idDescription[1:] = re.split('-',idDescription[1])

        idDescription.insert(0, self.notebook)
        idDescription.append(draft)
        self.id = apply(EntryID, idDescription)
        if len(parts) > 0:
            self.id.filename = parts[0]
        else:
            self.id.filename = None
        try:
            self.entries = self.id.getEntries()
        except:
            error = _('No such entry: %s.') % self.id.asPathInfo()
            if self.id.draft:
                error = error + ' ' + _("If it existed before, it may have been published already, or deleted.")
            else:
                error = error + ' ' +_("If it existed before, it may have been deleted.")
            self.printError(error)

    ### Attachment helper methods.

    def getNumberOfUploadBoxes(self):
        default = self.notebook.getDefaultNumberOfUploadBoxes()
        numBoxes = self.var(const.NUMBER_OF_ATTACHMENTS_CGI_KEY, default)
        try:
            numBoxes = int(numBoxes)
        except ValueError:
            numBoxes = default
        return numBoxes

    def printAttachmentUploadBoxes(self):
        numBoxes = self.getNumberOfUploadBoxes()
        for i in range(0, numBoxes):
            if not i % 2:
                print '<p>'
            self.uploadBox(const.ATTACHMENT_CGI_KEY_PREFIX + str(i), 30)
            print '&nbsp;'

    def getUploadedAttachments(self):
        """Returns a map of valid uploaded files keyed by filename.
        Print an error if any of the files are too large or of the
        wrong type."""
        files = self.getFormValuesWithPrefix(const.ATTACHMENT_CGI_KEY_PREFIX)
        goodFiles = {}
        badFiles = []
        bigFiles = []
        max = self.notebook.getMaximumAttachmentSize() * 1024
        for file in files.values():
            if hasattr(file, 'filename') and file.filename:
                name = file.filename
                contents = file.value
                if len(contents) > max:
                    bigFiles.append(name)
                elif not self.notebook.isAcceptableAttachmentFilename(name):
                    badFiles.append(name)
                else:
                    goodFiles[name] = contents
        if bigFiles:
            error = _("The following file(s) are too large to be uploaded to this notebook:") + " " + string.join(bigFiles, ', ')
            error = error + '<p>' + (_("The maximum upload size is %s kilobytes.") % (max/1024)) + '</p>' 
            self.printError(error)
        if badFiles:
            self.printError(_("The following file(s) are of the wrong type to be uploaded to this notebook:") + " " + string.join(badFiles, ', '))
        return goodFiles

    ### Errors, help, and debug

    def printError(self, exception, note=_('Error: ')):
        "Prints an error and stops execution of the script."
        self.startPage('Error')
        print '<h1 class="heading">%s %s</h1>' % (note,exception)
        ref = self.getReferer()
        if ref:
            self.addNavLink(ref, 'Back')
        self.endPage(0)
        sys.exit()

    def dumpEnvironment(self):
        "Dumps all environment variables."
        self.printHeader()
        print '<pre>'
        print _('Environment dump:')
        for (key, val) in os.environ.items():
            print '%s: %s' % (key, val)
        print '</pre>'

class EntryCGI(NBCGI):

    def titleField(self, value='', heading="Title"):
        "Prints the form field for an entry title."
        if self.notebook.allowEntryTitles():
            print '<b>%s:</b>' % heading
            self.textField(const.TITLE_CGI_KEY, value, 40, None)        

    def authorControl(self, notebook, defaultAuthor=None, defaultEmail=None,
                      authorHeading=None, emailHeading=None):
        """Prints a dropdown selection of authors, a hidden field if there's
        only one, a text box if the author is freeform, or two text
        boxes if the author is freeform with freeform email."""
        authors = notebook.getAuthors()
        if len(authors) > 1 or self.notebook.freeformAuthor() :
            heading = authorHeading
            if not heading:
                if self.notebook.collectAuthorEmail():
                    heading = _('Author name')
                else:
                    heading = _('Author')
            print '<p><b>%s:</b>' % heading                
        if self.notebook.freeformAuthor():
            self.textField(const.AUTHOR_CGI_KEY, defaultAuthor, 30, 50)
        else:
            if len(authors) == 1:
                self.hiddenField(const.AUTHOR_CGI_KEY, authors[0])
            else:
                self.listBox(const.AUTHOR_CGI_KEY, authors, authors,
                             defaultAuthor)

        if self.notebook.collectAuthorEmail():
            heading = emailHeading
            if not heading:
                heading = _('Author email')
            print '<br/><b>%s:</b>' % heading
            self.textField(const.AUTHOR_EMAIL_CGI_KEY, defaultEmail, 30, 50)
        if len(authors) > 1 or self.notebook.freeformAuthor() :
            print '</p>'

    def _makeInsertJavascript(self, text):
        text = string.replace(text, '"', '&quot;')
        return "javascript:insertText('%s')" % (text)

    def printEntryInterpolationHelp(self, entry=None, textarea=None):
        """Prints help for the interpolation of attachment URLs into
        an entry."""
        if textarea and textarea.find('document') != 0:
            textarea = 'document.' + textarea
        names = None
        example = 'image.jpg'
        if entry:            
            names = entry.getAttachmentNames()
            if names:
                example = names[0]
        showHelp = self.notebook.showAttachmentHelp()
        if not names and not showHelp:
            return
        what = self.notebook.getAttachmentDesignation()
        if names:
            if textarea:
                print """<script language="javascript">function insertText(text) {
                    input = %s; if (input.value) text = ' ' + text;
                    input.value = input.value + text; input.focus(); }</script>""" % textarea

            requiresJavascript = _('This link requires that you enable JavaScript. ')
            print _('Available %s:') % (self.notebook.getPluralAttachmentDesignation())
            print '<ul>'
            for name in names:            
                print '<li>%s' % name
                if textarea:
                    url = '%' + name + '%'
                    if self.notebook.isImage(name):
                        js = self._makeInsertJavascript(self.getImg(url, ''))
                        print _('(%s)') % self.getLink(js, _('Insert image'),
                                                       title=requiresJavascript)
                    js = self._makeInsertJavascript(self.getLink(url, ''))
                    print _('(%s)') % self.getLink(js, _('Insert link'),
                                                   title=requiresJavascript)
                print '</li>'
            print '</ul>'
        print '</p>'
        if showHelp:
            if (names or not entry):
                print _('Put <code>%%<i>name</i>%%</code> in the entry text to get the URL to the %s <i>name</i>.') % what
            print '<p>'
            if self.notebook.isImage(example):
                exampleURL = cgi.escape('<img src="%%%s%%">' % example)
                args = {'url' : exampleURL, 'filename' : example}
                print _('Example: <code>%(url)s</code> will make the entry display the attached image named "%(filename)s".') % args
            else:
                exampleURL = cgi.escape('<a href="%%%s%%">' % example
                                        + _('text') + '</a>' )
                args = {'url' : exampleURL, 'filename' : exampleURL}
                print _('Example: <code>%(url)s</code> will make the entry link to the attached file named "%(filename)s".') % args
            print '</p>'
