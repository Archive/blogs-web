import copy

import const

"""A module containing standard maps for the templating system."""

BASIC_COMMAND_MAP = { '_' : '_',
                      'n' : 'getName',
                      'system-baseContentURL' : 'getBaseContentURL',
                      'notebook-yearSpan' : 'getYearSpan',
                      'notebook-name' : 'getName',
                      'notebook-title' : 'getUnlinkedTitle',
                      'notebook-titleLink' : 'getLinkedTitle',
                      'notebook-contactLink' : 'getContactLink',
                      'notebook-description' : 'getDescription',
                      'links-reader' : 'getReaderLinks',
                      'links-admin' : 'getAdminLinks',
                      'searchBox' : 'getSearchBox',
                      'newsBruiser-version' : 'getNewsBruiserVersion',
                      'newsBruiser-link' : 'getNewsBruiserLink',
                      'notebook-calendar' : 'getCurrentTimespanCalendar',
                      'notebook-blogroll' : 'getBlogrollLinks',
                      'notebook-archive-monthly' : 'getMonthlyArchive',
                      'panel-blogroll' : 'getBlogrollPanel',
                      'entryCount-currentMonth' : 'getCurrentMonthEntryCount'
                      }

MASTER_HEADER_TEMPLATE_COMMAND_MAP = copy.copy(BASIC_COMMAND_MAP)
MASTER_HEADER_TEMPLATE_COMMAND_MAP['page-docType'] = 'getDoctype'
MASTER_HEADER_TEMPLATE_COMMAND_MAP['page-standardHeadContents'] = 'getHeadContents'
MASTER_HEADER_TEMPLATE_COMMAND_MAP['page-bodyColors'] = 'getBodyColors'
MASTER_HEADER_TEMPLATE_COMMAND_MAP['page-title'] = 'getTitle'

MASTER_FOOTER_TEMPLATE_COMMAND_MAP = copy.copy(BASIC_COMMAND_MAP)
MASTER_FOOTER_TEMPLATE_COMMAND_MAP['links-contextual'] = 'getContextualLinks'
MASTER_FOOTER_TEMPLATE_COMMAND_MAP['panel-readerLinks'] = 'getReaderLinksPanel'
MASTER_FOOTER_TEMPLATE_COMMAND_MAP['panel-searchBox'] = 'getSearchBoxPanel'

ENTRY_LIST_COMMAND_MAP = copy.copy(BASIC_COMMAND_MAP)
ENTRY_LIST_COMMAND_MAP['Y'] = 'interpolateTimeframeTemplate'
ENTRY_LIST_COMMAND_MAP['M'] = 'interpolateTimeframeTemplate'
ENTRY_LIST_COMMAND_MAP['D'] = 'interpolateTimeframeTemplate'
ENTRY_LIST_COMMAND_MAP['E'] = 'interpolateEntryTemplate'

ENTRY_LIST_COMMAND_MAP['panel-readerLinks'] = 'getReaderLinksPanel'
ENTRY_LIST_COMMAND_MAP['panel-searchBox'] = 'getSearchBoxPanel'

YEAR_SPAN_COMMAND_MAP = copy.copy(ENTRY_LIST_COMMAND_MAP)
for i in ('y', 'Y'):
    YEAR_SPAN_COMMAND_MAP[i] = 'getCurrentEntryTime'
del(YEAR_SPAN_COMMAND_MAP['D'])
del(YEAR_SPAN_COMMAND_MAP['E'])
YEAR_SPAN_COMMAND_MAP['url-currentYear'] = 'getCurrentYearURL'

MONTH_SPAN_COMMAND_MAP = copy.copy(YEAR_SPAN_COMMAND_MAP)
del(MONTH_SPAN_COMMAND_MAP['M'])
MONTH_SPAN_COMMAND_MAP['D'] = 'interpolateTimeframeTemplate'
for i in ('b', 'B', 'm'):
    MONTH_SPAN_COMMAND_MAP[i] = 'getCurrentEntryTime'
MONTH_SPAN_COMMAND_MAP['url-currentMonth'] = 'getCurrentMonthURL'

DAY_SPAN_COMMAND_MAP = copy.copy(MONTH_SPAN_COMMAND_MAP)
del(DAY_SPAN_COMMAND_MAP['D'])
DAY_SPAN_COMMAND_MAP['E'] = 'interpolateEntryTemplate'
    
for i in ('o', 'O'):
    DAY_SPAN_COMMAND_MAP[i] = 'getCurrentEntryDayName'
DAY_SPAN_COMMAND_MAP['h'] = 'getCurrentEntryDayOrdinal'
for i in ('a', 'A', 'd', 'e', 'w'):
    DAY_SPAN_COMMAND_MAP[i] = 'getCurrentEntryTime'        
DAY_SPAN_COMMAND_MAP['url-currentDay'] = 'getCurrentDayURL'

BASIC_ENTRY_COMMAND_MAP = copy.copy(DAY_SPAN_COMMAND_MAP)
del(BASIC_ENTRY_COMMAND_MAP['E'])
for i in ('H', 'M', 'S'):
    BASIC_ENTRY_COMMAND_MAP[i] = 'getCurrentEntryTime'        
BASIC_ENTRY_COMMAND_MAP['2'] = 'getAlternatorValue'
BASIC_ENTRY_COMMAND_MAP['b'] = 'getSizeBytes'
BASIC_ENTRY_COMMAND_MAP['i'] = 'getAnchorID'
BASIC_ENTRY_COMMAND_MAP['v'] = 'getAbsoluteURL'
BASIC_ENTRY_COMMAND_MAP['url-currentEntry'] = 'getAbsoluteURL'
TITLE_COMMAND_MAP = BASIC_ENTRY_COMMAND_MAP
TITLE_COMMAND_MAP['t'] = 'getTitle'
    
AUTHOR_COMMAND_MAP = TITLE_COMMAND_MAP
AUTHOR_COMMAND_MAP['a'] = 'getAuthorHTML'
    
ENTRY_COMMAND_MAP = copy.copy(BASIC_ENTRY_COMMAND_MAP)
ENTRY_COMMAND_MAP['d'] = 'getFormattedDate'
ENTRY_COMMAND_MAP['e'] = 'getEntryText'
ENTRY_COMMAND_MAP['s'] = 'getSummary'
ENTRY_COMMAND_MAP['p'] = 'getPermalink'        
ENTRY_COMMAND_MAP['A'] = 'interpolateAuthorTemplate'
ENTRY_COMMAND_MAP['T'] = 'interpolateTitleTemplate'
ENTRY_COMMAND_MAP['sample'] = 'sample'
    
TIMESPAN_COMMAND_MAPS = { const.YEAR : YEAR_SPAN_COMMAND_MAP,
                          const.MONTH : MONTH_SPAN_COMMAND_MAP,
                          const.DAY : DAY_SPAN_COMMAND_MAP }    

ARCHIVE_LINK_COMMAND_MAP = {'link' : 'getViewLink' }
