#A EntryID object identifies a particular notebook entry or span of
#notebook entries.

#An EntryID is described by a year, a month, a day, and an ordinal.
#Any of these can be a number, or ALL. If one of them is ALL, then all
#rightward ones will be set to ALL. An EntryID with one or more ALL
#fields represents multiple entries on disk.

import copy
import os.path

import lib.DateArithmetic
from Entry import Entry
import const
from const import *
from util import *
import util

class EntryID:
    def __init__(self, notebook, year=None, month=ALL, day=ALL, ordinal=ALL,
                 draft=0):
        """A constructor which either takes portions of a calendar
        interval, or figures out what values a new ID would have."""
        self.notebook = notebook
        self.draft = draft
        if year==None:
            #New entry: find the directory into which this entry will go.  
            (self.year,self.month,self.day) = self.notebook.today()
            self.ordinal = ALL

            #Find the smallest ordinal for today not already associated
            #with an entry.
            counter = 0
            base_filename = self.asPath() + '-'
            filename = base_filename + str(counter)
            while os.path.exists(filename):			
                counter = counter+1
                filename = base_filename + str(counter)
            self.ordinal = counter
        else:
            #Make sure the identifier is normalized.
            self.year = year
            if year == ALL:
                self.month = ALL
                self.day = ALL
                self.ordinal = ALL
            else:
                self.year = int(year)
                self.month = month

            if month == ALL:
                self.day = ALL
                self.ordinal = ALL
            else:
                self.month = int(self.month)
                self.day = day
                
            if day == ALL:
                self.ordinal = ALL
            else:
                self.day = int(self.day)
                self.ordinal = ordinal

            if self.ordinal != ALL:
                self.ordinal = int(self.ordinal)

    def asDate(self):
        "Returns this ID as (year, month, day)"
        return (self.year, self.month, self.day)

    def asLinkedDescription(self, cgi):
        return self.asDescription(cgi)

    def getMonthName(self):
        """Returns the localized name of this entry's month."""
        try:
            monthName = _(lib.DateArithmetic.MONTHS[int(self.month)-1])
        except IndexError:
            raise _("No such month: %s") % self.month
        return monthName
    
    def asDescription(self, cgi=None):
        "Returns the description of this ID, with each component linked to the given CGI except the one which uniquely identifies this entry or span."
        description = ""
        if self.draft:
            description = _("Drafts: ")
        else:
            description = ""

        if cgi:
            if self.draft:
                url = self.notebook.getURL(cgi, '', 'drafts')
            else:
                url = self.notebook.getURL(cgi)

            notebook = '<a href="%s">%s</a>' % (url, self.notebook.getTitle())
        else:
            notebook = self.notebook.getTitle()
        if self.year <> ALL:
            args = { 'notebook' : notebook,
                     'entryNum' : '',
                     'entry' : ''}
            yearID = EntryID(self.notebook, self.year, draft=self.draft)
            if self.month == ALL or not cgi:
                date = str(yearID.year)
            else:
                date = '<a href="%s">%s</a> ' % (yearID.asLink(cgi),
                                                 yearID.year)
            if self.month <> ALL:
                monthID = EntryID(self.notebook, self.year, self.month,
                                  draft=self.draft)
                monthName = monthID.getMonthName()
                if self.day == ALL or not cgi:        
                    date = date + ' ' + monthName
                else:
                    date = date + '<a href="%s">%s</a> ' % (monthID.asLink(cgi),
                                                            monthName)
                if self.day <> ALL:
                    dayID = EntryID(self.notebook, self.year, self.month,
                                    self.day, draft=self.draft)
                    if (self.ordinal == ALL) or not cgi:
                        date = date + ' ' + str(self.day)
                    else:
                        date = date + '<a href="%s">%s</a>' % (dayID.asLink(cgi),
                                                               self.day)
                    if self.ordinal <> ALL:
                        if self.draft:
                            entryNum = _('(draft %s)')
                        else:
                            entryNum = _('(entry %s)')
                        args['entry'] = entryNum % self.ordinal
            args['date'] = date
            if args['entry']:
                description = description + _('%(notebook)s for %(date)s %(entry)s') % args
            else:
                description = description + _('%(notebook)s for %(date)s') % args
        return description

    def getViewLink(self):
        """Used in archive templates to print out an archive link.
        Currently only a monthly archive link is supported, and
        the date format is hard-coded."""
        s = ''
        if self.granularity() == const.MONTH:
            from CoreCGIs import ViewCGI
            monthName = self.getMonthName()
            url = self.asLink(ViewCGI.NAME)
            s = '<a href="%s">%s %s</a>' % (url, monthName, self.year)
        return s

    def asLink(self, cgi=None, args='', pathInfo='', rewrite=1):
        "Renders this ID as a PATH_INFO argument to the given CGI."
        if not cgi:
            from CoreCGIs import ViewCGI
            cgi = ViewCGI.NAME
        url = util.urljoin(cgi, self.asPathInfo())
        if pathInfo:
            url = util.urljoin(url, pathInfo)
        if args:
            url = url + '?' + args
        if rewrite:
            url = self.notebook.nbconfig.urlRewriter.rewrite(url)
        return url

    def asPath(self, includeFile=1):
        "Renders this ID as a that portion of the notebook filesystem namespace which the ID unambiguously identifies."
        return os.path.join(self.notebook.getSystemWorkingDirectory(),
                            self.describe(1, includeFile, connector=os.sep))

    def asPathInfo(self):
        "Renders this ID as a URL fragment which NewsBruiser knows how to parse as part of PATH_INFO."   
        return self.describe(1,1,'/')

    def asTuple(self):
        "Returns a tuple describing this entry."
        ints = map(int, (self.year, self.month, self.day, self.ordinal))  
        return (ints[0], ints[1], ints[2], ints[3])

    def denotesMultipleEntries(self):
        "Returns true iff the object denotes a span of entries in some notebook, as opposed to one particular entry."
        return (self.ordinal == ALL)

    def denotesSingleEntry(self):
        "Returns true iff object denotes a particular entry in some notebook, as opposed to a span of such entries."
        return not self.denotesMultipleEntries()

    def containedWithin(self, id):
        "Returns true iff this entry ID is a subset of the given entry ID."
        ok = 1
        if id.year != ALL and self.year != id.year:
            ok = 0
        elif id.month != ALL and self.month != id.month:
            ok = 0
        elif id.day != ALL and self.day != id.day:
            ok = 0
        elif id.ordinal != ALL and self.ordinal != id.ordinal:
            ok = 0
        return ok

    def describe(self, includeName=1, includeFile=1, ordinalConnector='-',
                 connector='/'):
        "Returns a canonical representation of this entry, subject to including or not including the notebook name or the file on disk, and which connector to use between the day and the ordinal."
        if includeName:
            d = self.notebook.name + connector
        else:
            d = ""
        if self.draft:
            d = d + 'drafts' + connector
        if self.year <> ALL:
            d = d + str(self.year)
            if self.month <> ALL:
                d = d + connector
                if self.month < 10:
                    d = d + '0'
                d = d + str(self.month)
                if includeFile and self.day <> ALL:                    
                    d = d + connector
                    if self.day < 10:
                        d = d + '0'
                    d = d + str(self.day)
                    if self.ordinal <> ALL:
                        d = d + ordinalConnector + str(self.ordinal)
        return d

    def equals(self, other):
        "Returns true iff this EntryID represents the same span of entries as the one provided."
        thisday = (self.year, self.month, self.day)
        otherday = (other.year, other.month, other.day)
        if self.draft != other.draft or lib.DateArithmetic.compareDates(thisday, otherday) <> 0 or self.ordinal <> other.ordinal:
            return 0
        else:
            return 1   

    def exists(self):
        "Returns true iff the specified entry exists on disk."
        import os
        if not self.denotesSingleEntry(): raise AmbiguousIDError
        return os.path.exists(self.asPath())

    def filenamesToEntryIDs(self, filenames, limit=None):
        """Translates a list of relative or absolute filenames into a
        chronologically sorted list of EntryID objects."""
        import re
        path = self.notebook.getDir(self.draft)
        filenames.sort(cmpFilename)
        #We have the filenames, now turn them into EntryIDs.
        entryIDs = []
        for file in filenames:
            if limit == None or len(entryIDs) < limit:
                if string.find(file, path) == 0:
                    file = file[len(path)+1:]
                (y,m,do) = string.split(file, os.sep)[-3:]
                (d,o) = string.split(do, '-')
                entryIDs.append(EntryID(self.notebook,y,m,d,o,self.draft))
        return entryIDs

    def getEntries(self, limit=None):
        "Returns all the entries specified by this ID."
        entries = []
        for id in self.getIDs(limit):
            entries.append(Entry(id))
        return entries

    def getIDs(self, limit=None):
        "Returns all the unique IDs specified by this span of IDs."
        from glob import glob
        if self.denotesSingleEntry():
            entryIDs=[self]
        else:
            (y,m,d,o) = (self.year,self.month,self.day,self.ordinal)
            path = self.notebook.getDir(self.draft)
            fileglob = os.path.join(path, str(y), util.zeroPad(m),
                                    util.zeroPad(d) + '-' + o)
            files = glob(fileglob)            
            entryIDs = self.filenamesToEntryIDs(files, limit)
        return entryIDs

    def granularity(self):
        "Returns the granularity of this ID."
        if (self.year == ALL):
            return NOTEBOOK
        elif (self.month == ALL):
            return YEAR 
        elif (self.day == ALL):
            return MONTH
        elif (self.ordinal == ALL):
            return DAY
        else:
            return ORDINAL

    def getDayID(self):
        """Returns an EntryID which is just like this one except it covers all
        entries for this day."""
        id = copy.copy(self)
        id.ordinal = const.ALL
        return id

    def getMonthID(self):
        """Returns an EntryID which is just like this one except it covers all
        entries for this month."""
        id = self.getDayID()
        id.day = const.ALL
        return id

    def getYearID(self):
        """Returns an EntryID which is just like this one except it covers all
        entries for this year."""
        id = self.getMonthID()
        id.month = const.ALL
        return id

    #### Date/entry manipulation functions

    def dayCrement(self, dayMethod):
        "Calls methods on this ID's day and makes sure the result is within this notebook's parameters."
        if self.granularity() == DAY:
            return self.notebook.verifyDayFit(self.draft,
                                              dayMethod((self.year, self.month, self.day)))
        return None

    def monthCrement(self, monthMethod, dayMethod):
        "Calls methods on this ID's month and day, and makes sure the result is within this notebook's parameters."
        g = self.granularity()
        if g == MONTH:
            return self.notebook.verifyMonthFit(self.draft, monthMethod((self.year, self.month)))
        elif g == DAY:
            return self.notebook.verifyDayFit(self.draft, dayMethod((self.year, self.month, self.day)))
        return None

    def yearCrement(self, yearMethod, monthMethod, dayMethod):
        "Calls methods on this ID's year, month, and day, and makes sure the result is within this notebook's parameters."
        g = self.granularity()
        if g == YEAR:
            return self.notebook.verifyYearFit(self.draft,
                                               yearMethod(self.year))
        elif g == MONTH:
            return self.notebook.verifyMonthFit(self.draft,
                                                monthMethod((self.year,
                                                             self.month)))
        elif g == DAY:
            return self.notebook.verifyDayFit(self.draft,
                                              dayMethod((self.year, self.month,
                                                         self.day)))
        return None

    def nextYear(self):
        "Returns the next year, the same month in the next year, or the same day and month in the next year (for some value of 'same'). Will return None if this date goes past today."
        return self.yearCrement(lib.DateArithmetic.nextYear,
                                lib.DateArithmetic.thisMonthNextYear,
                                lib.DateArithmetic.thisDayNextYear)

    def previousYear(self):
        "Returns the previous year, the same month in the previous year, or the same day and month in the previous year (for some value of 'same'). Will return None if this date is before the first entry in this notebook."
        return self.yearCrement(lib.DateArithmetic.previousYear,
                                lib.DateArithmetic.thisMonthLastYear,
                                lib.DateArithmetic.thisDayLastYear)

    def nextMonth(self):
        "Returns the next month or the same day in the next month (for some value of 'same')."
        return self.monthCrement(lib.DateArithmetic.nextMonth,
                                 lib.DateArithmetic.thisDayNextMonth)

    def previousMonth(self):
        "Returns the previous month or the same day in the previous month (for some value of 'same')."
        return self.monthCrement(lib.DateArithmetic.previousMonth,
                                 lib.DateArithmetic.thisDayLastMonth)

    def nextDay(self):
        "Returns the day after the one designated by this entry, or None if that's after today."
        return self.dayCrement(lib.DateArithmetic.nextDay)

    def previousDay(self):
        "Returns the day before the one designated by this entry, or None if that's before this notebook's first entry."
        return self.dayCrement(lib.DateArithmetic.previousDay)

    def nextEntryID(self):
        "Returns the ID of the entry after this one, or None if this is the notebook's most recent entry."
        entryID = None
        if self.granularity() <> ORDINAL:
            return entryID
        mostRecent = self.notebook.getMostRecentEntry(self.draft)
        if mostRecent and not self.equals(mostRecent.id):
            day = (self.year, self.month, self.day)
            args = list(day)
            args.extend([ALL, self.draft])
            entries = apply(self.notebook.getEntries, args)
            ordinal = int(self.ordinal)
            if len(entries) > ordinal+1:
                entryID = entries[ordinal+1].id
            while entryID == None:     
                day = lib.DateArithmetic.nextDay(day)
                args = list(day)
                args.extend([ALL, self.draft])
                entries = apply(self.notebook.getEntries, args)
                if len(entries) > 0:
                    entryID = entries[0].id
        if entryID == None:
            return entryID
        else:
            return entryID.asTuple()

    def previousEntryID(self):
        "Returns the ID of the entry before this one, or None if this is this notebook's first entry."  
        entryID = None
        if self.granularity() <> ORDINAL:
            return entryID
        firstEntry = self.notebook.getFirstEntry(self.draft)
        if firstEntry and not self.equals(firstEntry.id):
            myOrdinal = int(self.ordinal)
            if myOrdinal > 0:
                entryID = EntryID(self.notebook, self.year, self.month, self.day, myOrdinal-1, self.draft)
            else:
                day = lib.DateArithmetic.previousDay((self.year, self.month, self.day))
                while entryID == None:
                    args = list(day)
                    args.extend([ALL, self.draft])
                    entries = apply(self.notebook.getEntries, args)
                    if len(entries) > 0:
                        entryID = entries[len(entries)-1].id
                    day = lib.DateArithmetic.previousDay(day)
        if entryID == None:
            return entryID
        else:
            return entryID.asTuple()

    #### Self-test
    def selfTest(self):
        "Describes the features of a hypothetical new entryID."
        print 'If I were to create an entry right now, its id would be %s' % self.describe()
        print 'It would be put into %s' % self.asPath()

if __name__=='__main__':
    from NBConfig import NBConfig
    EntryID(notebook=NBConfig().getDefaultNotebook()).selfTest()
