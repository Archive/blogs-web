#Stupid library to generate RDF description of the capabilities of
#various CC licenses.

import string

class License:

    def __init__(self, name=''):
        self.reproduction = 1 #True for all CC
        self.distribution = 1 #True for all CC
        self.notice = 1 #True for all non-public domain

        self.attribution = 0
        self.derivativeWorks = 1
        self.commercialUse = 1
        self.sharealike = 0

        self.name = name

        if name:           
            self._parseName(name)

    def _parseName(self, name):
        urlStart = string.find(name, 'licenses/') + len('licenses/')
        if urlStart != -1 and urlStart < len(name):
            name = name[urlStart:]
        urlEnd = string.find(name, '/')
        if urlEnd != -1:
            name = name[:urlEnd]
        self.knownLicense = 1
        for i in string.split(name, '-'):
            self.knownLicense = self.knownLicense and self._handleNamePart(i)

    def _handleNamePart(self, part):
        found = 1
        if part == 'by':
            self.attribution = 1
        elif part == 'nd':
            self.derivativeWorks = 0
        elif part == 'nc':
            self.commercialUse = 0
        elif part == 'sa':
            self.sharealike = 1
        elif part == 'publicdomain':
            self.notice = 0
        else:
            found = 0
        return found

    REQUIRES = 'requires'
    PERMITS = 'permits'
    PROHIBITS = 'prohibits'

    def getDetailsRDF(self):
        s = ''
        if self.knownLicense:
            if self.reproduction:
                s = s + self._getRDFPart(self.PERMITS, 'Reproduction')
            if self.derivativeWorks:
                s = s + self._getRDFPart(self.PERMITS, 'Distribution')
            what = self.PROHIBITS
            if self.derivativeWorks:
                s = s + self._getRDFPart(self.PERMITS, 'DerivativeWorks')

            if not self.commercialUse:
                s = s + self._getRDFPart(self.PROHIBITS, 'CommercialUse')

            if self.notice:
                s = s + self._getRDFPart(self.REQUIRES, 'Notice')
            if self.attribution:
                s = s + self._getRDFPart(self.REQUIRES, 'Attribution')
            if self.sharealike:
                s = s + self._getRDFPart(self.REQUIRES, 'ShareAlike')

        return s

    def _getRDFPart(self, whatHappens, toWhat):
        return '<%s rdf:resource="http://web.resource.org/cc/%s" />\n' % (whatHappens, toWhat)
        
