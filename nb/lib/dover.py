#step 1: download and write stub
from BeautifulSoup import BeautifulSoup
import urllib
s = BeautifulSoup()
s.feed(open("dover.html").read())

#Step 2: examine the file, experiment
import re
import types
NUMBERED = re.compile("[0-9]*.html")
found = {}
#for i in s.fetch('font', {'size': '2', 'color' : '#000000'}):
for i in s.fetch('a', {'href': NUMBERED}):
    href = i['href']
    if not found.get(href):
        found[href] = 1
        url = 'http://store.doverpublications.com/' + href
        title = i.next
        image = i.parent.parent.parent.first('img')
        #Reeeeally bad hack here. We need __delitem__ on Tags.
        if image.get('ismap'):
            bad = []
            for i in range(0, len(image.attrs)):
                if image.attrs[i][0] in ('ismap', 'usemap'):
                    bad.append(i)
            for i in range(0, len(bad)):
                del(image.attrs[bad[i]-i])
        print image
        body = i.parent.first('font', {'size' : '2', 'color' : '#000000'}).contents[0]
        author = title.next.contents[1].contents[1]
        if author:
            author = str(author)
            f = author.find('by ')
            if f > -1:
                author = author[f+3:]
            else:
                author = None
                
#for i in s.fetch("a", {"href": NUMBERED}):
#    if not found.get(i):
#        print i
#        found[i] = i

