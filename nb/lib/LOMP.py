"""Leonard's Obsequious Markup Producer

A very simple library for generating bits of HTML, especially HTML
form fields.

This library has no external dependencies. It defines the following classes:

* HtmlGenerator: Make this class a superclass of your class, and your
  class will magically be able to print out cool CSS-themed text and
  HTML forms like nobody's business.

"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 1999-2004 Leonard Richardson"
__license__ = "Python"

import types
import cgi

class HtmlGenerator:

    """This is a utility superclass for pieces of code which generate
    HTML."""

    def __init__(self):
        raise "This is an abstract class."

    def line(self):
        "Prints a standard HTML line."
        print '<hr/>'

    def panelLink(self, url, text, elementClass=''):
        """Prints a 'control panel' type link, with the link enclosed
        in square brackets."""
        self.linkTo(url, text, elementClass, '[', ']')

    def getPanelLink(self, url, text, elementClass=''):
        """Returns a 'control panel' type link, with the link enclosed
        in square brackets."""
        return '[' + self.getLink(url, text, elementClass) + ']'

    def linkTo(self, url, text, elementClass="", prefix='', suffix='',
               style='', title=''):
        print prefix + self.getLink(url, text, elementClass, style, title) + suffix

    def getLink(self, url, text, elementClass='', style='', title=''):
        classText = ""
        if title:
            title = ' title="%s"' % title
        if elementClass:
            classText = ' class="%s"' % elementClass
        if style:
            classText = ' style="%s"' % style
        return '<a%s href="%s"%s>%s</a>' % (title, url, classText, text)

    def getImg(self, src, alt=None, title=None):
        altText = ''
        titleText = ''
        if alt != None:
            altText = ' alt="%s"' % alt
        if title != None:
            titleText = ' title="%s"' % title
        return '<img src="%s"%s%s>' % (src, altText, titleText)

    def printSpan(self, cssClass, contents):
        "Prints an HTML span of the given CSS class with the given contents"
        print '<span class="%s">%s</span>' % (cssClass, contents)

    def printEmailLink(self, email, name):
        """Prints a mailto link to the given email with the given name
        as link text."""
        if name:
            text = name
        else:
            text = email
        url = 'mailto:%s' % email
        self.linkTo(url, text)

    def printHeading(self, msg, size=1):
        "Prints a heading of the given size."
        print '<h%s class="heading">%s</h%s>' % (size, msg, size)

    def printStatus(self,msg):
        "Prints a status message."
        print '<h3 class="status">%s</h3>' % msg

    def getRobotExclusionTag(self, doIndex=0, doFollow=0):
        if not doIndex:
            index = 'noindex'
        else:
            index = 'index'

        if not doFollow:
            follow = 'nofollow'
        else:
            follow = 'follow'

        return '<meta name="robots" content="%s,%s" />' % (index, follow)

    #### Generation of form fields.

    def startForm(self, action, method="post", encType=None, name=''):
        print self.getStartForm(action, method, encType, name)

    def getStartForm(self, action, method="post", encType=None, name=''):
        enc = ''
        if encType:
            enc = ' enctype="%s"' % encType
        if method:
            method = 'method="%s"' % method
        if name:
            name = ' name="%s"' % name
        return '<form action="%s" %s%s%s>' % (action, name, method, enc)

    def submitButton(self, submitText, submitName=_("Submit")):
        print self.getSubmitButton(submitText, submitName)

    def getSubmitButton(self, submitText, submitName):
        
        return '<input type="submit" name="%s" value="%s" />' % (submitName,
                                                               submitText) 

    def endForm(self, submitText=_('Submit'), submitName=_('submit')):
        print self.getEndForm(submitText, submitName)

    def getEndForm(self, submitText=_('Submit'), submitName=_('submit')):
        s = ''
        if submitText:
            s = s + self.getSubmitButton(submitText, submitName)
        return s + '</form>'

    def hiddenField(self,name,value):
        print self.getHiddenField(name, value)

    def getHiddenField(self, name, value):
        return '<input type="hidden" name="%s" value="%s" />' % (name,
                                                               cgi.escape(str(value)))

    def radioButton(self, name, value, description, selectedValue=None,
                    linebreak=1):
        """Print a radio button."""        
        if value==selectedValue:
            selected=' checked="checked"'
        else:
            selected = ''
        if linebreak:
            linebreak = '<br/>'
        else:
            linebreak = ''
        print '<input type="radio" name="%s" value="%s"%s> %s%s' % \
              (name, value, selected, description, linebreak)

    def checkbox(self, name, checked='', suppressHidden=0):
        """Print a checkbox and (optionally) a hidden field which lets
        a CGI know that the user saw the checkbox and left it
        unchecked, rather than not having seen the checkbox."""
        if not suppressHidden:
            print '<input type="hidden" name="%s" value="true">' % name
            name = '%s-checkbox' % name
        print '<input type="checkbox" name="%s"' % name,
        if checked:
            print 'checked="checked"',
        print '>'

    def passwordEntryField(self, name, value='', size=20, maxlength=20):
        print self.getPasswordEntryField(name, value, size, maxlength)

    def getPasswordEntryField(self, name, value='', size=20, maxlength=20):
        return '<input type="password" name="%s" size="%s" maxlength="%s" value="%s" />' % (name, size, maxlength, value)
    
    def textField(self,name,value,size=10,maxlength=10):        
        "Prints a standard text field."
        if not value:
            value = ''
        if maxlength:
            if size and int(size) > int(maxlength):
                size = maxlength
            maxlength = ' maxlength="%s"' % maxlength
        else:
            maxlength = ''
        print '<input type="text" name="%s" value="%s" size="%s"%s />' % (name, cgi.escape(str(value), 1), size, maxlength)

    def textArea(self, rows, cols, name, content=''):
        "Prints a standard multiline text box."        
        print '<p><textarea wrap="virtual" rows="%s" cols="%s" name="%s">%s</textarea></p>' % (rows, cols, name, cgi.escape(str(content)))

    def listBox(self, boxName, names, values, defaults=None, size=1,
                allowMultiple=0):
        """Prints a list box or (if size is 1) a drop-down box with the
        given names and values, and the given values selected."""
        multipleDefaults = (type(defaults) == types.ListType)

        if len(names) < len(values):
            use = names
        else:
            use = values

        if not len(use):
            return

        if len(use) < size and allowMultiple:
            size = len(use)

        print '<select',
        if allowMultiple:
            print 'multiple',
        print 'name="%s" size="%s">' % (boxName, size)

        for i in range(0,len(use)):
            default = 0
            val = str(values[i])
            if multipleDefaults:
                try:
                    defaults.index(val)
                    default = 1
                except:
                    pass
            elif val == str(defaults):
                default = 1
            print ' <option value="%s"' % values[i],
            if default:
                print 'selected="selected"',
            print '>%s</option>' % names[i]

        print '</select>'

    def uploadBox(self, name, size=60):
        "Prints a standard upload box."
        print '<input type="file" name="%s" size="%s">' % (name, size)
