import xmllib
import string

# Copyright (c) 2002 by Fredrik Lundh
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and its
# associated documentation for any purpose and without fee is hereby
# granted, provided that the above copyright notice appears in all
# copies, and that both that copyright notice and this permission notice
# appear in supporting documentation, and that the name of Secret Labs
# AB or the author not be used in advertising or publicity pertaining to
# distribution of the software without specific, written prior
# permission.
#
# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
# THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# namespaces used for standard elements by different RSS formats
RSS_NAMESPACES = (
    "http://my.netscape.com/rdf/simple/0.9/", # RSS 0.9
    "http://purl.org/rss/1.0/", # RSS 1.0
    "http://backend.userland.com/rss2", # RSS 2.0 (sometimes)
    )

class ParseError(Exception):
    pass

class RSSParser(xmllib.XMLParser):

    def __init__(self):
        xmllib.XMLParser.__init__(self)
        self.rss_version = None
        self.channel = None
        self.current = None
        self.data_tag = None
        self.data = None
        self.items = []

    def _gethandlers(self, tag):
        # check if the tag lives in a known RSS namespace
        if tag == "http://www.w3.org/1999/02/22-rdf-syntax-ns# RDF":
            # this appears to be an RDF file.  to simplify processing,
            # map this element to an "rss" element
            return self.elements.get("rss")
        if tag == "http://my.netscape.com/rdf/simple/0.9/ channel":
            # this appears to be a my.netscape.com 0.9 file
            self.rss_version = "0.9"
        try:
            namespace, tag = string.split(tag)
        except ValueError:
            pass # ignore this element
        else:
            if namespace in RSS_NAMESPACES:
                methods = self.elements.get(tag)
                if methods:
                    return methods
        return None, None

    def unknown_starttag(self, tag, attrib):
        start, end = self._gethandlers(tag)
        if start:
            start(attrib)

    def unknown_endtag(self, tag):
        start, end = self._gethandlers(tag)
        if end:
            end()

    # stuff to deal with text elements.

    def _start_data(self, tag):
        if self.current is None:
            raise ParseError("%s tag not in channel or item element" % tag)
        self.data_tag = tag
        self.data = ""

    def handle_data(self, data):
        if self.data is not None:
            self.data = self.data + data

    handle_cdata = handle_data

    def _end_data(self):
        if self.data_tag:
            self.current[self.data_tag] = self.data or ""

    # main rss structure

    def start_rss(self, attr):
        self.rss_version = attr.get("version")
        if self.rss_version is None:
            # no undecorated version attribute.  as a work-around,
            # just look at the local names
            for k, v in attr.items():
                if k.endswith(" version"):
                    self.rss_version = v
                    break

    def start_channel(self, attr):
        if self.rss_version is None:
            # no version attribute; it might still be an RSS 1.0 file.
            # check if this element has an rdf:about attribute
            if attr.get("http://www.w3.org/1999/02/22-rdf-syntax-ns# about"):
                self.rss_version = "1.0"
            else:
                raise ParseError("cannot read this RSS file")
        self.current = {}
        self.channel = self.current

    def start_item(self, attr):
        if self.rss_version is None:
            raise ParseError("cannot read this RSS file")
        self.current = {}
        self.items.append(self.current)

    # content elements

    def start_title(self, attr):
        self._start_data("title")
    end_title = _end_data

    def start_link(self, attr):
        self._start_data("link")
    end_link = _end_data

    def start_guid(self, attr):
        self.current['guid.isPermalink'] = attr.get('isPermalink')
        self._start_data("guid")
    end_guid = _end_data

    def start_description(self, attr):
        self._start_data("description")
    end_description = _end_data

    def start_dc_date(self, attr):
        self._start_data("pubDate")
    end_dc_date = _end_data

    def start_pubDate(self, attr):
        self._start_data("pubDate")
    end_pubDate = _end_data
