"""Priority Ouch

Tiny classes to keep a map or list whose values stay sorted according
to the priorities you specify.

This library has no external dependencies. It defines the following
classes:

 * PriorityMap: A map whose values stay sorted.
 * PriorityList: A list that stays sorted.

To use, set the value and priority as a tuple: (value, priority),
rather than just setting the value. Eg. map['foo'] = (objref, 2)

Insort algorithm taken from Simo Salminen's Priority Queue recipe:

 http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/87369

"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2003 Leonard Richardson"
__license__ = "Python"

class PriorityMap:

    def __init__(self):
        self.queue = []
        self.map = {}

    def get(self, key, default=None):
        return self.map.get(key, default)

    def __getitem__(self, key):
        return self.map[key]

    def __setitem__(self, key, valuePriority):
        self.map[key] = valuePriority[0]
        self._insort_right((key, valuePriority[0], valuePriority[1]))

    def keys(self):
        "Ideally this would return the keys in sorted order, but it doesn't."
        return map(lambda x: x[0], self.queue)

    def values(self):
        return map(lambda x: x[1], self.queue)

    def items(self):
        return map(lambda x: x[0:2], self.queue)

    def has_key(self, key):
        return self.map.has_key(key)

    def _insort_right(self, x):
        """Insert item x in list, and keep it sorted assuming a is sorted.
        
        If x is already in list, insert it to the right of the rightmost x.       
        """
        a = self.queue
        lo = 0        
        hi = len(a)

        while lo < hi:
            mid = (lo+hi)/2
            if x[2] < a[mid][2]: hi = mid
            else: lo = mid+1
        a.insert(lo, x)

class PriorityList:

    def __init__(self):
        self.queue = []

    def add(self, item):
        data, priority = item
        self._insort_right((data, priority))

    def __getitem__(self, i):
        return self.queue[i][0]

    def __len__(self):
        return len(self.queue)

    def _insort_right(self, x):
        """Insert item x in list, and keep it sorted assuming a is sorted.
        
        If x is already in list, insert it to the right of the rightmost x.       
        """
        a = self.queue
        lo = 0        
        hi = len(a)

        while lo < hi:
            mid = (lo+hi)/2
            if x[1] < a[mid][1]: hi = mid
            else: lo = mid+1
        a.insert(lo, x)
        
if __name__ == '__main__':
    a = PriorityMap()
    a['lastLetter'] = ('z', 26)
    a['firstLetter'] = ('a', 1)
    a['middleLetter'] = ('m', 13)
    a['theOtherFirstLetter'] = ('A', 1)

    print "Keys:", a.keys()
    print "Values:", a.values()
    print "Items:", a.items()

    print 'First letter: ', a['firstLetter']

    a = PriorityList()
    a.add(('z', 26))
    a.add(('a', 1))
    a.add(('m', 13))
    a.add(('A', 1))

    for i in a:
        print i


