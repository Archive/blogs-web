#!/usr/bin/python

"""Transfusion

Parses Moveable Type export files into intuitively understandable data
structures. A little hacky but it works.

The result of read() is an array of dictionaries.

A dictionary key is the name of a field defined by the Moveable type
export format.

A value is the corresponding value of the field.

Values of which there can be more than one for an entry (comments and
pings), are kept in a list.

Values that have attributes (comments and pings again) are stored as
hashes. The text of the thing is kept as 'TEXT'.

Run this as a script on some sample text to see how the format works.
"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2004 Leonard Richardson"
__license__ = "Python"

import re
import string
import sys

class Transfusion:

    ONE_LINE_FIELDS = ['AUTHOR', 'TITLE', 'DATE', 'CATEGORY', 'STATUS',
                       'ALLOW COMMENTS', 'ALLOW PINGS', 'CONVERT BREAKS',
                       'NO ENTRY']

    MULTI_LINE_FIELDS = ['BODY', 'EXTENDED BODY', 'EXCERPT', 'COMMENT', 'PING']

    POTENTIALLY_MULTIPLE = ['COMMENT', 'PING']

    MULTI_LINE_SUBFIELDS = { 'COMMENT' : ['AUTHOR', 'EMAIL', 'URL', 'IP',
                                          'DATE'],
                             'PING' :  ['TITLE', 'URL', 'IP', 'BLOG NAME',
                                        'DATE']
                             }

    FIELD_TEXT = 'TEXT'

    ENTRY_DELIMITER = ('-' * 8) + '\n'
    FIELD_DELIMITER = '-' * 5

    FIELD_NAME_RE = re.compile('^[A-Z ]*:')

    def read(self, s):
        entries = []
        for i in string.split(s, self.ENTRY_DELIMITER):
            if i:
                entries.append(self.readEntry(i))
        return entries

    def readEntry(self, s):
        entry = {}
        errors = []
        workingFieldName = ''
        workingFieldValue = None
        workingSubfieldValue = None
        for line in string.split(s, '\n'):
            inMulti = workingFieldName in self.MULTI_LINE_FIELDS
            if (inMulti and line == self.FIELD_DELIMITER) or \
               (not inMulti and self.FIELD_NAME_RE.match(line)):
                #We have reached the end of the previous field,
                #whether it be a single-line or multi-line field
                if workingFieldName and workingFieldValue != None:
                    #Store the previous field in the entry object.
                    if inMulti and workingFieldName in self.POTENTIALLY_MULTIPLE:
                        values = entry.get(workingFieldName)
                        if values:
                            values.append(workingFieldValue)
                        else:
                            entry[workingFieldName] = [workingFieldValue]
                    else:
                        entry[workingFieldName] = workingFieldValue
                #Set our working objects to the new field.
                if inMulti:
                    workingFieldName = None
                    workingFieldValue = None
                    workingSubfieldValue = None
                else:
                    if self.FIELD_NAME_RE.match(line):
                        nameValue = string.split(line, ':', 1)
                        (workingFieldName, workingFieldValue) = nameValue
                        if workingFieldName in self.MULTI_LINE_FIELDS and self.MULTI_LINE_SUBFIELDS.get(workingFieldName):
                            workingFieldValue = {}
                        elif workingFieldValue and workingFieldValue[0] == ' ':
                            workingFieldValue = workingFieldValue[1:]
            elif inMulti:
                isText = 1
                if self.FIELD_NAME_RE.match(line):
                    nameValue = string.split(line, ':', 1)                
                    (testSubfieldName, testSubfieldValue) = nameValue
                    if testSubfieldValue and testSubfieldValue[0] == ' ':
                        testSubfieldValue = testSubfieldValue[1:]
                    if testSubfieldName in self.MULTI_LINE_SUBFIELDS.get(workingFieldName, []):
                        isText = 0
                        workingSubfieldName = testSubfieldName
                        workingSubfieldValue = testSubfieldValue

                if isText:
                    line = line + '\n'
                    if workingSubfieldValue and workingSubfieldName == self.FIELD_TEXT:
                        workingSubfieldValue = workingSubfieldValue + line
                    else:
                        workingSubfieldName = self.FIELD_TEXT
                        workingSubfieldValue = line
                if type(workingFieldValue) == type({}):
                    workingFieldValue[workingSubfieldName] = workingSubfieldValue
                else:
                    workingFieldValue = workingSubfieldValue
            elif line and line != self.FIELD_DELIMITER:
                #Nothing else makes sense
                errors.append("Unknown line: %s" % line)
        return entry, errors

if __name__ == '__main__':
    for i in Transfusion().read(sys.stdin.read()):
        print i
        print
