import string
import types

DEFAULT_LOCALE = 'en-us'

class LanguageNegotiator:

    """Ultra-liberal HTTP Accept-Language negotiator."""

    def findAcceptableLocale(self, header, locales,
                             defaultLocale=DEFAULT_LOCALE):
        """Given an Accept-Language header and a list of supported locales,
        tries to find the locale that provides the best fit."""
        locale = None
        if not defaultLocale in locales:
            defaultLocale = locales[0]
        for i in self.parseAcceptLanguageHeader(header):
            for j in self.getVariants(i):
                if j == '*':
                    #They will take anything. The default will do fine.
                    locale = defaultLocale
                    break                 
                elif j[-2:] == '-*':
                    #They are looking for any variant of a specific
                    #language.
                    for l in locales:
                        if string.find(l, j[:-2]) == 0:
                            locale = l
                            break
                else:
                    #They are looking for a specific variant of
                    #a specific language.
                    if j in locales:
                        locale = j
                        break
                if locale:
                    break
            if locale:
                break            
        if not locale:
            locale = defaultLocale
        return locale                        
    
    def getVariants(self, locale):
        """Returns all acceptable variants of the given locale code,
        in order of preference."""
        t = string.split(locale, '-')
        if len(t) > 1:
            #There was a locale code. Prefer the locale, but
            #accept the language, or any other locale,
            return [locale, t[0], t[0] + '-*']
        else:
            #There was no locale code. Accept the language or any locale.
            t.append(t[0] + '-*')
            return t

    def parseAcceptLanguageHeader(self, header):
        """Parses an Accept-Language header and returns the locale
        codes in order of preference."""    
        languages = string.split(header, ',')
        for i in range(0, len(languages)):
            l = string.split(string.strip(languages[i]), ';q=')
            priority = 1.0
            if len(l) > 1:
                priority = l[1]
            languages[i] = (float(priority), l[0])
        languages.sort()
        ret = []
        for i in languages:
            ret.insert(0, i[1])
        return ret

#Some simple tests for LanguageNegotiator
#a = LanguageNegotiator()
#print a.findAcceptableLocale("da, en-gb;q=0.8, en;q=0.7", ['en-us'])
#print a.findAcceptableLocale("da, en-gb;q=0.8, en;q=0.7", ['en-us', 'en-gb'])
#print a.findAcceptableLocale("da, en-gb;q=0.8, en;q=0.7", ['en-us', 'da-ff'])
#print a.findAcceptableLocale("da-*, en-gb;q=0.8, en;q=0.7", ['en-us', 'da'])
#print a.findAcceptableLocale("*, da-*", ['ja', 'fr'])
#print a.findAcceptableLocale("", ['ja', 'fr'])

def uq(str):
    return string.replace(str, '\\"', '"')

class I18NBundle:

    """Cheap-ass I18N bundle parser for use until I can figure out how
    to use gettext to do what I want. Parses .po files into an
    enormous hashtable which you can then use or pickle."""

    def __init__(self, files=[]):
        self.bundle = {}
        if type(files) == types.StringType:
            files = [files]
        for file in files:
            self.parse(file)
    
    def read(self, fh):
        if type(fh) == types.StringType:
            fh = open(fh, 'r')
        key = ''
        val = ''
        multiLine = 0
        inValue = 0
        for i in fh.readlines():                        
            if len(i) > 6 and string.find(i, 'msgid "') == 0 and i[-2] == '"':
                key = uq(i[7:-2])
                multiline = not key
                inValue = 0
            elif key and len(i) > 9 and string.find(i, 'msgstr "') == 0 and i[-2] == '"':
                val = uq(i[8:-2])
                inValue = 1
                if val:
                    self.bundle[key] = val
                    key = ''
                    val = ''
                else:
                    multiLine = 1
            elif multiLine and len(i) > 3 and i[0] == '"' and i[-2] == '"':
                if inValue:
                    val = self.bundle.get(key, '')
                    if val:
                        val = val + '\n'
                    self.bundle[key] = val + uq(i[1:-2])
                else:
                    key = key + uq(i[1:-2])
            else:
                key = ''
                value = ''
            
    def __getitem__(self, str):        
        return self.bundle.get(str, str)

    def format(self, key, args):
        """args is either a single value or a list of values. If
        there's only one value, the key is assumed to contain a single
        %s, %d, etc.  Otherwise the key is assumed to contain multiple
        variables like %(0)s, %(1)d, etc."""
        if type(args) in (types.TupleType, types.ListType):            
            map = {}
            for i in range(0, len(args)):
                map[i] = args[i]
            args = map
        val = self[map]
        if args:
            val = val % args
        return val
