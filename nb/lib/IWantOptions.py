"""I Want Options

A library for managing an application's configuration, user
preferences, and/or other miscellaneous options through the application
itself. You provide the application, it provides the configuration
management.

This library has no external dependencies. It defines the following
classes:

* OptionConfig: reads from the option definition store. You can
  extend this class to read from a different format (e.g. from an XML
  file or a database).

* OptionGroup: A container for options. You probably don't need to
  extend this class.

* Option: Defines the semantics for an option (e.g. a boolean or a
  string option). You can extend this class to create options with
  application-specific semantics (see TimeZoneOption for a
  complex but generic example).

* Context: Defines the unit of configuration (e.g. for a user
  preference engine, this is your application's notion of "user"). You
  should make some existing class in your application extend this. A
  simple FileBackedContext, which simply writes key-value pairs to a
  file, is provided.

* OptionWrapper: Defines display semantics for your application.
  If you don't extend this, your option screen will look very boring
  and not like the rest of your application.

* ConfigurationCGI: Very simple logic for an option-setting CGI. You
  probably want to write your own CGI that fits in with whatever Web
  framework you're using, but this will get you started (and is great
  for demos).

* NullOption, StringOption, LongStringOption, BooleanOption,
  SelectionOption, IntegerOption, EmailOption, HTMLColorOption,
  LanguageOption, TimeZoneOption, etc: Extensions of Option which
  implement different display, gathering, and validation interfaces.

To use, initialize an OptionConfig object with the path to your option
definition store and (optionally) an instance of OptionWrapper. Pass
an instance of Context to all OptionConfig methods which require one.

The option definition store looks like this:

group=OptionGroup1
This is the first option group.

name=option1
displayName=This is the first option in option group 1.
property=Options can have arbitrary properties, but the first property must be "name".
#Comments are allowed as well.

name=option2
displayName=This is the second option in option group 1.

group=OptionGroup2
This is the second option group.

name=option1
displayName=This is the first option in option group 2.

...

All user-visible English text in this file is run through a '_' method
for easy integration into whatever internationalization scheme you
might be using. See IWantOptions.messages.pot in this directory for a
messages.pot file, and eg. IWantOptions.messages.fr-fr.po for
translations.

"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2002-2003 Leonard Richardson"
__license__ = "Python"

import base64
import cgi
import math
import os
import re
import string
import sys
import time
import types

DEFAULT_OPTION_TYPE = 'string'

#The _ function is, by convention, used for I18N. This library will
#plug into whatever I18N framework you have in place, and will set up
#a null framework if you don't have one.
import __builtin__
if not __builtin__.__dict__.get('_'):
    __builtin__.__dict__['_'] = lambda(x): x

class OptionConfig:

    """This class reads from the option definition store. It instantiates
    OptionGroup objects, which contain Option objects."""

    #Regular expressions used when parsing option config files.
    COMMENT_RE = re.compile('^\s*#.*$', re.M)
    GROUP_SEPARATOR_RE = re.compile('^group=([^\n]*)\n', re.M)
    OPTION_SEPARATOR_RE = re.compile('^option=([^\n]*)\n', re.M)

    def __init__(self, filename, wrapper=None):
        """Initialize variables; the data structures are populated
        lazily."""

        self.filename = filename
        if not wrapper:
            wrapper = OptionWrapper()
        self.wrapper = wrapper
        self.wrapper.config = self

        #A list of OptionGroup objects in definition order.
        self.optionGroupList = None

        #A mapping of option group names to OptionGroup objects.
        self.optionGroupMap = None

        #A mapping of option names to Option objects. If multiple option
        #groups define options with the same name, the contents of this
        #map are undefined.
        self.optionMap = None

    def getOptionMap(self):
        """Returns a mapping of option names to Option objects. If multiple
        option groups define options with the same name, the contents of this
        map are undefined; use the option group interface instead."""
        if not self.optionMap:
            self.getOptionDefinitions()
        return self.optionMap

    def getOptionGroupList(self):
        """Returns a list of OptionGroup objects, in the order in which they
        were defined."""
        if not self.optionGroupList:
            self.getOptionDefinitions()
        return self.optionGroupList

    def getOptionGroupMap(self):
        """Returns a mapping of names of option groups to their corresponding
        OptionGroup objects."""
        if not self.optionGroupMap:
            self.getOptionDefinitions()
        return self.optionGroupMap

    def getOption(self, name):
        """Convenience method to retrieve an option by name. This
        method's behavior is undefined if multiple option groups
        define options with the same name."""
        return self.getOptionMap().get(name)

    def getOptionValue(self, name, context):
        """Convenience method to retrieve the value of a named option
        in the given context. This method's behavior is undefined if
        multiple option groups define options with the same name."""
        return self.getOptionMap()[name].getValue(context)

    def getOptionDefault(self, name, context):
        """Convenience method to obtain the default value of the named
        option in the given context. This method's behavior is undefined if
        multiple option groups define options with the same name."""
        return self.getOptionMap()[name].getDefault(context)

    def getRelevantGroups(self, context):
        """Returns only those option groups which contain options
        relevant to the given context."""
        groups = []
        for group in self.getOptionGroupList():
            if group.containsRelevantOptions(context):
                groups.append(group)
        return groups

    def getOptionDefinitions(self):
        """Loads option definitions into the data structures. If the
        option definitions have already been loaded, does nothing."""
        if not self.optionGroupList:
            self.optionGroupList = []
            self.optionMap = {}
            self.optionGroupMap = {}
            self.loadOptionDefinitions(self.filename)

    def loadOptionDefinitions(self, filename):
        """Called by the internal initialization on the file you
        specify in the OptionConfig constructor. You can call this
        manually on additional files to dynamically add options to the
        option configuration."""
        file = open(filename)

        groups = self.GROUP_SEPARATOR_RE.split\
                 (self.COMMENT_RE.sub('', file.read()))
        for i in range(1, len(groups), 2):
            groupName = groups[i]
            groupText = groups[i+1]
            currentGroup = self.optionGroupMap.get(groupName)
            if not currentGroup:
                currentGroup = OptionGroup(groupName, self.wrapper)
                self.optionGroupList.append(currentGroup)
                self.optionGroupMap[currentGroup.name] = currentGroup
            options = self.OPTION_SEPARATOR_RE.split(groupText)
            if options[0]:
                currentGroup.setText(string.strip(options[0]))
            for j in range(1, len(options), 2):
                optionName = options[j]
                optionSettings = options[j+1]
                optionProperties = {'name' : optionName}
                for setting in string.split(optionSettings, '\n'):
                    if setting:
                        key, value = string.split(setting, '=', 1)
                        optionProperties[key] = value            
                self.__createOption(optionProperties, currentGroup)
        file.close()

    def __createOption(self, properties, defaultGroup):
        """Transforms a set of option properties and an OptionGroup object
        into an Option object, and registers the Option object with its
        group and the map of options by name."""

        #First, see if the option itself specifies an (already defined)
        #option group, which would override any current group.
        groupName = properties.get('optionGroup', None)
        group = self.optionGroupMap.get(groupName, defaultGroup)
        if not group:
            raise "Option %s is not in any group!" % properties['name']

        optionType = properties.get('type', DEFAULT_OPTION_TYPE)
        optionClass = self.wrapper.optionClasses.get(optionType, None)
        if not optionClass:
            optionClass = sys.modules[self.wrapper.__module__].__dict__[optionType]
        option = optionClass(self.wrapper, properties)
        self.optionMap[option.name] = option
        group.addOption(option)
        properties.clear()

class OptionGroup:    
    def __init__(self, name, wrapper):
        self.name = name
        self.options = []
        self.text = ''
        self.wrapper = wrapper

        #Whether or not this option group contains settable options for
        #various contexts.
        self.settable = {}

    def containsRelevantOptions(self, context):
        """Returns true iff this option group contains at least one option
        which can be set in the given context."""
        settable = self.settable.get(context, None)
        if settable == None:
            settable = 0
            for option in self.options:
                if option.isRelevant(context):
                    settable = 1
                    break
            self.settable[context] = settable
        return settable

    def validate(self, context, formVariables):
        """Validate all the options in this group against the given
        set of form variables. Returns a list of errors."""
        errors = []
        for option in self.options:
            if option.isRelevant(context):
                newErrors = option.validate \
                            (context, option.processFormValue(formVariables),
                             formVariables)
                if newErrors:
                    if type(newErrors) == types.ListType:
                        errors.extend(newErrors)
                    elif type(newErrors) == types.StringType:
                        errors.append(newErrors)
        return errors
    
    def setText(self, text):
        """Sets the descriptive text for this option group."""
        self.text = text

    def getText(self, context):
        """Returns the descriptive text for this option group."""
        return self.wrapper.getGroupDescription(self, context)

    def getName(self, context):
        """A hook method to retrieve and possibly transform a group's
        description."""
        return self.wrapper.getGroupName(self, context)

    def addOption(self, option):
        """Adds an option to the end of the list of options defined in
        this group."""
        self.options.append(option)

    def getOptions(self):
        """Returns the list of options defined in this group."""
        return self.options

    def render(self, context, presetValues):
        """Renders this option group as HTML."""
        self.wrapper.renderGroup(self, context, presetValues)

class Option:

    UNESCAPED_COMMA_RE = re.compile('([^\\\]),')

    def splitOnUnescapedCommas(self, s):
        """A helper method to split a list on commas that aren't
        preceded by a slash."""
        l = self.UNESCAPED_COMMA_RE.split(s)
        l2 = []
        for i in range(0, len(l), 2):
            if i != len(l)-1:
                l[i] = l[i] + l[i+1]
            l2.append(string.replace(l[i], '\,', ','))
        return l2            

    def __init__(self, wrapper, properties):
        self.wrapper = wrapper
        for (key, value) in properties.items():
            setattr(self, key, value)

    ### Some methods are specific to the implementation, and delegate
    ### to the OptionWrapper (defined below). Subclasses may add logic
    ### of their own in addition to or instead of delegating to the
    ### wrapper.

    def isRelevant(self, context):
        """Returns true iff this option can be set in the given context."""
        return self.wrapper.isRelevant(self, context)

    def renderControl(self, context, presetValue=None):
        """Renders the control to edit this option, as well as running
        any application-specific UI wrapper code."""
        self.wrapper.renderControl(self, context, presetValue)

    def getControllingOption(self, context):
        """Returns the boolean or text option, if any, whose value controls
        the appearance of this option. This gives you an easy way of
        making one option depend on another without having to define a
        special isRelevant() method for it."""
        return self.wrapper.getControllingOption(self, context)

    def getControllingOptionValue(self, context):
        """Returns, for the non-boolean option that is this option's
        controlling option, the value that option must have for this
        option to be relevant. For instance, if this option depends on
        another option's value being set to 'foo', this method should
        return 'foo'."""
        return self.wrapper.getControllingOptionValue(self, context)

    def getDisplayName(self, context):
        """Convenience method to return the display name, warped in
        whatever way the wrapper deems neccessary."""
        return self.wrapper.getOptionDisplayName(self, context)

    ### Some methods are specific to the type of option.

    def getValue(self, context, override=None):
        """Returns the value of this option as set in the given context."""
        val = override        
        if not val:
            val = context.get(self.name, None)
        if val == None:
            val = self.getDefault(context)
        return val

    def processValueForStorage(self, context, value):
        """Turns the value as entered into a storable value. Return None to
        cancel this value's storage."""
        return value
        
    def renderEditControl(self, context, presetValue=None):
        pass

    def validate(self, context, value, formVariables):
        """Return a list of problems with the given value."""
        return []

    def getDefault(self, context):
        """Returns the default value of this attribute in the given context."""
        default = self.getStandardDefault(context)
        if hasattr(self, 'default'):
            default = self.wrapper.getOptionDefault(self, context)
        return default

    def getStandardDefault(self, context):
        """Returns the default value of this attribute as defined in its
        definition (before it's given to wrapper.getOptionDefault)."""
        return ''

    def processFormValue(self, vars):
        """Obtain the value of this option from a list of CGI variables,
        perform any neccessary processing, and return the processed value."""
        return vars.get(self.name, '')

class Context:

    """The context is the unit of configuration. It represents some
    resource controlled by your application, such that each different
    instance of the resource can have its own configuration settings.
    In NewsBruiser this is the Notebook object (since each notebook
    has its own distinct settings). If you are using this library to
    implement user options, this would be your user object.

    The get(), set(), and save() methods implement the interface to
    your configuration store."""

    def get(self, key, default=None):
        self.__abstract()

    def set(self, key, value):
        self.__abstract()

    def save(self, authenticationToken=None):
        """A context which saves values on set() doesn't need a save(). You
        also might want to override this with a version of save() which takes
        an authentication token."""
        pass    

    def __abstract(self):
        raise "This is an abstract class."

    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, value):
        return self.set(key, value)

### Very simple sample implementations of various classes which you
### should implement yourself in a way the fits in with your
### application framework.

class OptionWrapper:

    """The Option class tree implements different types of options.
    Rather than making you subclass each Option subclass to get your
    application-specific behavior for each option, or change each
    Option subclass to subclass a superclass you define, implement
    your application-specific Option and OptionGroup behavior in an
    OptionWrapper subclass and pass in an instance of it to the
    OptionConfig constructor.

    From this class you can define new option classes, change the way
    options are rendered, and the like.

    External example: NewsBruiser extends this with the
    Options.NBOptionWrapper class."""

    def __init__(self):
        self.optionClasses = {}
        self.scanModuleForOptionClasses(sys.modules[OptionWrapper.__module__])

    def scanModuleForOptionClasses(self, module):
        """Scans the given module for classes that provide an
        OPTION_KEY member, and registers them in the optionClasses map
        under their OPTION_KEY. If you define additional option
        classes in OptionWrapper and you want to use key names for
        them rather than their full class names, you can define
        OPTION_KEY in each Option subclass in the module and call
        scanModuleForOptionClasses(sys.modules[self.__module__]) in
        the constructor of your OptionWrapper subclass."""
        for c in module.__dict__.values():
            if hasattr(c, 'OPTION_KEY'):
                self.optionClasses[c.OPTION_KEY] = c

    def renderGroup(self, group, context, presetValues={}):
        """Render a group of options. Does not print out the group
        text; if you want that, you need to do it separately."""

        for option in group.getOptions():
            if option.isRelevant(context):
                option.renderControl(context, presetValues.get(option.name))

    def renderControl(self, option, context, presetValue=None):
        """Render an HTML depiction of the given option."""
        print '<p>'
        if hasattr(option, 'displayName'):
            print '<b>%s</b>:' % self.getOptionDisplayName(option, context)
        option.renderEditControl(context, presetValue)
        print '<br/>'
        if hasattr(option, 'description'):
            print self.getOptionDescription(option, context)
        print '</p>'

    def isRelevant(self, option, context):
        """Returns true iff this option can be set in the given context."""
        relevant = 1
        controllingOption = option.getControllingOption(context)
        if controllingOption:
            controllingOptionValue = option.getControllingOptionValue(context)
            value = controllingOption.getValue(context)
            if controllingOptionValue:
                relevant = (value == controllingOptionValue)
            else:
                relevant = value
        return relevant

    def getControllingOption(self, option, none):
        """A hook method to retrieve any option whose value controls whether
        or not this option is relevant."""
        controllingOption = None
        optionName = getattr(option, 'controllingOption', None)
        if optionName:
            controllingOption = self.config.getOption(optionName)
        return controllingOption

    def getControllingOptionValue(self, option, none):
        """A hook method to retrieve the value the controlling option
        (q.v.) must have."""
        controllingOptionValue = None
        return getattr(option, 'controllingOptionValue', None)

    def getOptionDisplayName(self, option, context):
        """A hook method to retrieve and possibly transform an option's
        display name."""
        return _(option.displayName)

    def getOptionDescription(self, option, context):
        """A hook method to retrieve and possibly transform an option's
        description."""
        return _(option.description)

    def getOptionDefault(self, option, context):
        """A hook method to retrieve and possibly transform an option's
        default value."""
        return _(getattr(option, default, ''))

    def getGroupName(self, group, context):
        """A hook method to retrieve and possibly transform a group's
        description."""
        return _(group.name)

    def getGroupDescription(self, group, context):
        """A hook method to retrieve and possibly transform a group's
        description."""
        return _(group.text)

class FileBackedContext(Context):
    """For applications in which the configuration is global, affecting the
    entire application, you can use this simple context which keeps
    its configuration options in a file whose path you specify."""

    def __init__(self, config, path):
        self.path = path
        self.map = {}
        self.config = config
        if os.path.exists(self.path):
            file = open(self.path, 'r')
            for line in file.readlines():
                (key,value) = string.split(line[:-1], '=', 1)
                self.map[key] = value

    def get(self, name, default=None):
        if not default:
            default = self.config.getOptionDefault(name, self)
        return self.map.get(name, default)

    def set(self, name, value):
        self.map[name] = value

    def save(self):
        file = open(self.path, 'w')
        for (key, value) in self.map.items():
            file.write('%s=%s\n' % (key, string.replace(value, '\n', ' ')))
        file.close()

class ConfigurationCGI:

    """An ultra-simple configuration CGI class. Pass in an
    OptionConfig object and it will print out a form. Change items on
    the form, submit, and they're validated and propagated to the
    Context in your OptionConfig. You probably want to write your own
    CGI to make it fit in with your framework, but this is good for demos
    and quick starts.

    External example: see NewsBruiser's configure.cgi, which has
    similar logic."""
    
    import cgi

    SELECTED_GROUP = 'group'
    SUBMIT_BUTTON = 'submit'
    
    def __init__(self, config, context, formURL, title=''):
        self.config = config
        self.context = context
        self.formURL = formURL
        self.title = title

        if not hasattr(self, 'variables'):
            self.getVariables()

        self.groups = self.config.getOptionGroupList()
        groupName = self.variables.get(self.SELECTED_GROUP)
        self.selectedGroup = self.config.getOptionGroupMap().get \
                             (groupName, self.groups[0])

        print "Content-type: text/html\n"
        if self.variables.get(self.SUBMIT_BUTTON):
            errors = self.selectedGroup.validate(self.context, self.variables)
            if errors:
                print '<h1>Errors:</h1>'
                print '<ul>'
                for error in errors:
                    print '<li>%s</li>' % error
                print '</ul>'
            else:
                self.saveChanges()        
        self.startPage()
        self.printForm()
        self.endPage()

    def getVariables(self):
        self.variables = {}
        form = cgi.FieldStorage()
        if form.list:
            for key in form.keys():            
                if type(form[key]) == types.ListType:
                    #This is to handle one CGI field with multiple
                    #values. We want a list of the string values
                    #rather than a list of MiniFieldStorage objects.
                    value = []
                    for field in form[key]:
                        value.append(field.value)
                else:
                    value = form[key].value
                self.variables[key] = value
    
    def saveChanges(self):
        for option in self.selectedGroup.getOptions():
            val = option.processValueForStorage\
                  (option.processFormValue(self.variables))
            if val != None:
                self.context.set(option.name, val)
        self.context.save()

    def startPage(self):
        print '<html>'
        if self.title:
            print '<head><title>%s</title></head>' % self.title
        print '<body>'                

    def endPage(self):
        print '</body></html>'        

    def makeSelfURL(self, extraQueryString):
        addOn = '?'
        if addOn in self.formURL:
            addOn = '&'
        return self.formURL + addOn + extraQueryString            

    def startForm(self):
        print '<form action="%s" method="post">' % self.formURL

    def endForm(self):
        print '</form>'

    def printForm(self):
        self.startForm()
        print '<input type="hidden" name="%s" value="%s">' % \
              (self.SELECTED_GROUP, self.selectedGroup.name)
        if len(self.groups) > 1:
            print '['
            for i in range(0, len(self.groups)):
                group = self.groups[i]
                if group.name == self.selectedGroup.name:
                    print group.name
                else:
                    url = self.makeSelfURL\
                          (self.SELECTED_GROUP + '=' + group.name)
                    print '<a href="%s">%s</a>' % (url, group.name)
                if i < len(self.groups) - 1:
                    print ' | '
            print ']'
        text = self.selectedGroup.getText(self.context)
        if text:
            print '<p><i>%s</i></p>' % text
        self.selectedGroup.render(self.context, self.variables)
        print '<input type="submit" name="%s" value="Save changes">' \
              % self.SUBMIT_BUTTON
        self.endForm()

    def __getitem__(self, key):
        "Returns a value from the configuration."
        return self.config.getOptionValue(key, self.context)

#### Everything below this line is an Option implementation

### Some simple types of options

class NullOption(Option):

    OPTION_KEY = "null"

    """An option with no user interface, which is set only by the
    application itself."""
    def isRelevant(self, context):
        return 0

class StringOption(Option):

    OPTION_KEY = DEFAULT_OPTION_TYPE

    INPUT_TYPE = 'text'
    DEFAULT_SIZE = 50

    def renderEditControl(self, context, presetValue=None):
        """The edit control for this option is a simple text field."""
        if not hasattr(self, 'size'):
            self.size = self.DEFAULT_SIZE
        if not hasattr(self, 'maxLength'):
            self.maxLength = None
        if self.maxLength and int(self.size) > int(self.maxLength):
            self.size = self.maxLength
        print '<input type="%s" name="%s" value="%s" size="%s" maxlength="%s">' % (self.INPUT_TYPE, self.name, cgi.escape(self.getEditableValue(context, presetValue), 1), self.size, self.maxLength)

    def validate(self, context, value, formVariables):
        max = getattr(self, 'maxLength', None)
        if max and len(value) > int(max):
            args = { 'value' : value,
                     'option' : self.getDisplayName(context),
                     'max' : max }
            return _('"%(value)s" is too long to be a valid value for for "%(option)s" (maximum length is %(max)s).') % args

    def getEditableValue(self, context, presetValue):
        """Returns the value of this option, suitable for display in an HTML
        form text box."""
        value = str(self.getValue(context, presetValue))
        return value

class PasswordOption(StringOption):

    """Displays a password field and uses state-of-the-art base64 encryption to
    obscure the password from not-very-prying eyes."""

    OPTION_KEY = 'password'

    INPUT_TYPE = 'password'
    DEFAULT_SIZE=8

    def getValue(self, context, presetValue=None):
        v = StringOption.getValue(self, context, presetValue)
        return self.decrypt(v)
        
    def processValueForStorage(self, context, value):
        if value:
            value = self.encrypt(value)
        else:
            value = context[self.name]
        return value

        return self.encrypt(value)

    def encrypt(self, text):
        return base64.encodestring(text)[:-1]

    def getEditableValue(self, context, presetValue):
        return ''

    def decrypt(self, text):
        return base64.decodestring(text)

class LongStringOption(StringOption):

    OPTION_KEY = 'longString'

    def getValue(self, context, presetValue=None):
        v = StringOption.getValue(self, context, presetValue)
        return string.replace(v, '\n ', '\n')
        
    def processValueForStorage(self, context, value):
        return string.replace(value, '\n', '\n ')

    def renderEditControl(self, context, presetValue):
        """The edit control for this option is a text area."""
        if not hasattr(self, 'rows'):
            self.rows = 8
        if not hasattr(self, 'columns'):
            self.columns = 80
        print '<p><textarea wrap="virtual" rows="%s" cols="%s" name="%s">%s</textarea></p>' % (self.rows, self.columns, self.name, self.getEditableValue(context, presetValue))

class BooleanOption(Option):

    OPTION_KEY = 'boolean'

    def getStandardDefault(self, context):
        return 0

    def getValue(self, context, override=None):
        "Returns the value of this option as set for the given context."
        val = override
        if not val:
            val = Option.getValue(self, context)
        if type(val) == types.StringType:
            val = string.upper(val)
            if val == 'Y' or val == 'TRUE':
                val = 1
            else:
                val = 0
        return val

    def processFormValue(self, vars):
        val = 'false'
        if vars.has_key(self.name):
            checkbox = '%s-checkbox' % self.name
            if vars.has_key(checkbox):
                if vars[self.name]:
                    val = 'true'
        return val

    def renderEditControl(self, context, presetValue):
        print '<input type="hidden" name="%s" value="true">' % self.name
        print '<input type="checkbox" name="%s-checkbox"' % self.name,
        if self.getValue(context):
            print 'checked="checked"',
        print '>'

class SelectionOption(StringOption):

    OPTION_KEY = 'selection'

    DROPDOWN = 'dropdown'
    RADIO = 'radio'

    def getManifestation(self):
        "manifestation can be 'dropdown' or 'radio'"
        return getattr(self, 'manifestation', self.DROPDOWN)

    def isMultiple(self):
        return hasattr(self, 'multiple')

    def isSelected(self, value, context, presetValue=None):
        selected = 0
        if presetValue:
            multiple = self.isMultiple()
            if multiple:
                selected = value in presetValue
            else:
                selected = value == presetValue
        else:
            selected = value in self.getSelectedValues(context)
        return selected
    
    def getSelectionValues(self):
        return self.splitOnUnescapedCommas(self.values)

    def getSelectionValueDescriptions(self):
        return self.splitOnUnescapedCommas(self.valueDescriptions)

    def getSelectedValues(self, context):
        "Returns all selected values."
        values = self.getValue(context)
        if values:
            if self.isMultiple():
                values = self.splitOnUnescapedCommas(values)
            else:
                values = [values]
        else:
            values = []
        return values

    def getDescriptionForValue(self, value):
        "Returns the description for a given value."
        descriptions = self.getSelectionValueDescriptions()
        values = self.getSelectionValues()
        description = None
        for i in range(0, len(values)):
            if values[i] == value:
                description = descriptions[i]
                break
        return description

    def processFormValue(self, vars):
        if self.isMultiple():
            value = vars.get(self.name, '')
            if type(value) == types.StringType:
                value = value
            else:
                value = string.join(value, ',')
        else:
            value = StringOption.processFormValue(self, vars)
        return value

    def validate(self, context, value, formVariables):
        "Throw an exception if this value is invalid."
        errors = []
        values = [value]
        if self.isMultiple() and ',' in value:
            values = self.splitOnUnescapedCommas(value)
        for value in values:
            if value and not value in self.getSelectionValues():
                args = { 'value' : value,
                         'option' : self.getDisplayName(context) }
                errors.append(_('"%(value)s" is not a valid selection value for the "%(option)s" option.') % args)
        return errors

    def getSize(self):
        if self.isMultiple():
            size = len(self.getSelectionValues())
            if size > 10:
                size = 10
            size = getattr(self, 'size', size)
        else:        
            size = 1
        return size

    def renderOption(self, context, description, value, presetValue):
        if self.getManifestation() == self.DROPDOWN:
            print '<option value="%s"' % value,
            if self.isSelected(value, context, presetValue):
                print ' selected="selected"',
            print '>%s</option>' % description
        else:
            selected = ''
            if self.isSelected(value, context, presetValue):
                selected = ' checked="checked"'
            print '<tr><td valign="top"><input type="radio" name="%s" value="%s"%s></td>' % (self.name, value, selected)
            print '<td valign="top">%s</td></tr>' % description


    def renderEditControl(self, context, presetValue):
        selectedValue = self.getValue(context, presetValue)
        [descriptions, values] = [self.getSelectionValueDescriptions(),
                                  self.getSelectionValues()]
        multiple = ''
        if self.isMultiple():
            multiple = 'multiple="multiple"'

        if self.getManifestation() == self.DROPDOWN:
            print '<select name="%s" size="%s"%s>' % (self.name,
                                                      self.getSize(),
                                                      multiple)
        else:
            print '<table>'
        for i in range(0, len(descriptions)):
            self.renderOption(context, descriptions[i], values[i], presetValue)

        if self.getManifestation() == self.DROPDOWN:
            print '</select>'
        else:
            print '</table>'
            
    def isRelevant(self, context):
        return Option.isRelevant(self, context) and len(self.getSelectionValues()) > 1

class SelectionOptionWithTextFields(SelectionOption):

    OPTION_KEY = 'selectionWithTextFields'

    def isMultiple(self):
        """This should be made workable in multiple selection mode,
        but I don't need it."""
        return 0

    def getManifestation(self):
        return self.RADIO

    def getValuesWithAssociatedTextField(self):
        return string.split(getattr(self, 'valuesWithTextField'), ',')

    def getTextFieldName(self, optionName):
        return '%s-%s-text' % (self.name, optionName)

    def renderOption(self, context, description, value, presetValue):
        if value in self.getValuesWithAssociatedTextField():
            size = getattr(self, '%s-size' % value, 50)
            maxLength = getattr(self, '%s-maxLength' % value, 50)
            if maxLength < size:
                maxLength = size
            textValue = ''
            if self.getValue(context, presetValue) == value:
                textValue = self.getTextBoxValue(context, presetValue)
            textbox = '<input type="text" name="%s" size="%s" maxlength="%s" value="%s">' % (self.getTextFieldName(value), size, maxLength, textValue)
            description = '%s<br />%s' % (textbox, description)
        SelectionOption.renderOption(self, context, description, value,
                                     presetValue)

    def processFormValue(self, vars):
        val = vars.get(self.name)
        if val in self.getValuesWithAssociatedTextField():
            val = val + ' ' + vars.get(self.getTextFieldName(val), '')
        return val

    def getValue(self, context, presetValue=None):
        """Returns the name of the selected member of the radio button
        group."""
        return self._parseValue(context, presetValue)[0]

    def getTextBoxValue(self, context, presetValue=None):
        """Returns the value of the text box
        associated with the selected radio button group, assuming
        there is one."""
        val = ''
        l = self._parseValue(context, presetValue)
        if len(l) > 1:
            val = l[1]
        return val

    def validate(self, context, value, formVariables):
        "Throw an exception if this value is invalid."
        errors = []
        if not self.getValue(context, value) in self.getSelectionValues():
            args = { 'value' : value,
                     'option' : self.getDisplayName(context) }
            errors.append(_('"%(value)s" is not a valid selection value for the "%(option)s" option.') % args)
        return errors        

    def _parseValue(self, context, presetValue=None):
        """Returns a list which will contain either one or two items.
        The first item is the value of the selection radio button
        group.  The second item is the value of the text box
        associated with the selected radio button group, assuming
        there is one."""
        value = SelectionOption.getValue(self, context, presetValue)
        return string.split(value, ' ', 1)

### Some fancy options which do validation

class IntegerOption(StringOption):

    OPTION_KEY = 'integer'

    def __init__(self, wrapper, properties):
        StringOption.__init__(self, wrapper, properties)

        if hasattr(self, 'maxValue'):
            self.maxLength = 1 + math.log10(int(self.maxValue))
            self.size = self.maxLength

    def validate(self, context, value, formVariables):
        try:
            value = int(value)
        except:
            return _('"%s" is not an integer.') % value
        if hasattr(self, 'maxValue') and value > int(self.maxValue):
            args = { 'value' : value,
                     'option' : self.getDisplayName(context),
                     'maxValue' : self.maxValue }
            return _('%(value)s is greater than the maximum "%(option)s" value of %(maxValue)s.') % args

class EmailOption(StringOption):

    OPTION_KEY = 'email'

    """An option whose value must be an email address."""

    def validate(self, context, value, formVariables):
        if not re.match("^.+@[^.]+(\..{2,})?$", value):
            return _('"%s" is not a well-formed email address.') % value

class HTMLColorOption(StringOption):

    OPTION_KEY = 'htmlColor'

    """An option whose value must be an HTML color or color constant."""

    COLOR_CONSTANTS = string.split("aliceblue antiquewhite aqua aquamarine azure beige bisque black blanchedalmond blue blueviolet brown burlywood cadetblue chartreuse chocolate coral cornflowerblue cornsilk crimson cyan darkblue darkcyan darkgoldenrod darkgray darkgreen darkkhaki darkmagenta darkolivegreen darkorange darkorchid darkred darksalmon darkseagreen darkslateblue darkslategray darkturquoise darkviolet deeppink deepskyblue dimgray dodgerblue firebrick floralwhite forestgreen fuchsia gainsboro ghostwhite gold goldenrod gray green greenyellow honeydew hotpink indianred indigo ivory khaki lavender lavenderblush lawngreen lemonchiffon lightblue lightcoral lightcyan lightgoldenrodyellow lightgreen lightgrey lightpink lightsalmon lightseagreen lightskyblue lightslategray lightsteelblue lightyellow lime limegreen linen magenta maroon mediumaquamarine mediumblue mediumorchid mediumpurple mediumseagreen mediumslateblue mediumspringgreen mediumturquoise mediumvioletred midnightblue mintcream mistyrose mistyrose navajowhite navy oldlace olive olivedrab orange orangered orchid palegoldenrod palegreen paleturquoise palevioletred papayawhip peachpuff peru pink plum powderblue purple red rosybrown royalblue saddlebrown salmon sandybrown seagreen seashell sienna silver skyblue slateblue slategray snow springgreen steelblue tan teal thistle tomato turquoise violet wheat white whitesmoke yellow yellowgreen")
    def __init__(self, wrapper, properties):
        StringOption.__init__(self, wrapper, properties)
        self.maxLength = max(map(len, self.COLOR_CONSTANTS))
        self.size = self.maxLength

    def validate(self, context, value, formVariables):
        if not re.match("#?[A-Fa-f0-9]{6}$", value) and value not in self.COLOR_CONSTANTS:
            return _('"%s" is not an HTML color code or color constant.') % value

class LanguageOption(SelectionOption):

    """A selection drop-down for language/locale, which yields values
    in the "language[-locale]" format used by RSS, and is restricted
    those languages acceptable to RSS:
    http://backend.userland.com/stories/storyReader$16"""

    OPTION_KEY = 'language'

    LANGUAGES = { _('Afrikaans') : 'af',
                  _('Albanian') : 'sq ',
                  _('Basque') : 'eu',
                  _('Belarusian') : 'be',
                  _('Bulgarian') : 'bg',
                  _('Catalan') : 'ca',
                  _('Chinese (Simplified)') : 'zh-cn',
                  _('Chinese (Traditional)') : 'zh-tw',
                  _('Croatian') : 'hr',
                  _('Czech') : 'cs',
                  _('Danish') : 'da',
                  _('Dutch') : 'nl',
                  _('Dutch (Belgium)') : 'nl-be',
                  _('Dutch (Netherlands)') : 'nl-nl',
                  _('English') : 'en',
                  _('English (Australia)') : 'en-au',
                  _('English (Belize)') : 'en-bz',
                  _('English (Canada)') : 'en-ca',
                  _('English (Ireland)') : 'en-ie',
                  _('English (Jamaica)') : 'en-jm',
                  _('English (New Zealand)') : 'en-nz',
                  _('English (Phillipines)') : 'en-ph',
                  _('English (South Africa)') : 'en-za',
                  _('English (Trinidad)') : 'en-tt',
                  _('English (United Kingdom)') : 'en-gb',
                  _('English (United States)') : 'en-us',
                  _('English (Zimbabwe)') : 'en-zw',
                  _('Estonian') : ' et',
                  _('Faeroese') : 'fo',
                  _('Finnish') : 'fi',
                  _('French') : 'fr',
                  _('French (Belgium)') : 'fr-be',
                  _('French (Canada)') : 'fr-ca',
                  _('French (France)') : 'fr-fr',
                  _('French (Luxembourg)') : 'fr-lu',
                  _('French (Monaco)') : 'fr-mc',
                  _('French (Switzerland)') : 'fr-ch',
                  _('Galician') : 'gl',
                  _('Gaelic') : 'gd',
                  _('German') : 'de',
                  _('German (Austria)') : 'de-at',
                  _('German (Germany)') : 'de-de',
                  _('German (Liechtenstein)') : 'de-li',
                  _('German (Luxembourg)') : 'de-lu',
                  _('German (Switzerland)') : 'de-ch',
                  _('Greek') : 'el',
                  _('Hawaiian') : 'haw',
                  _('Hungarian') : 'hu',
                  _('Icelandic') : 'is',
                  _('Indonesian') : 'in',
                  _('Irish') : 'ga',
                  _('Italian') : 'it',
                  _('Italian (Italy)') : 'it-it',
                  _('Italian (Switzerland)') : 'it-ch',
                  _('Japanese') : 'ja',
                  _('Korean') : 'ko',
                  _('Macedonian') : 'mk',
                  _('Norwegian') : 'no',
                  _('Polish') : 'pl',
                  _('Portuguese') : 'pt',
                  _('Portuguese (Brazil)') : 'pt-br',
                  _('Portuguese (Portugal)') : 'pt-pt',
                  _('Romanian') : 'ro',
                  _('Romanian (Moldova)') : 'ro-mo',
                  _('Romanian (Romania)') : 'ro-ro',
                  _('Russian') : 'ru',
                  _('Russian (Moldova)') : 'ru-mo',
                  _('Russian (Russia)') : 'ru-ru',
                  _('Serbian') : 'sr',
                  _('Slovak') : 'sk',
                  _('Slovenian') : 'sl',
                  _('Spanish') : 'es',
                  _('Spanish (Argentina)') : 'es-ar',
                  _('Spanish (Bolivia)') : 'es-bo',
                  _('Spanish (Chile)') : 'es-cl',
                  _('Spanish (Colombia)') : 'es-co',
                  _('Spanish (Costa Rica)') : 'es-cr',
                  _('Spanish (Dominican Republic)') : 'es-do',
                  _('Spanish (Ecuador)') : 'es-ec',
                  _('Spanish (El Salvador)') : 'es-sv',
                  _('Spanish (Guatemala)') : 'es-gt',
                  _('Spanish (Honduras)') : 'es-hn',
                  _('Spanish (Mexico)') : 'es-mx',
                  _('Spanish (Nicaragua)') : 'es-ni',
                  _('Spanish (Panama)') : 'es-pa',
                  _('Spanish (Paraguay)') : 'es-py',
                  _('Spanish (Peru)') : 'es-pe',
                  _('Spanish (Puerto Rico)') : 'es-pr',
                  _('Spanish (Spain)') : 'es-es',
                  _('Spanish (Uruguay)') : 'es-uy',
                  _('Spanish (Venezuela)') : 'es-ve',
                  _('Swedish') : 'sv',
                  _('Swedish (Finland)') : 'sv-fi',
                  _('Swedish (Sweden)') : 'sv-se',
                  _('Turkish') : 'tr',
                  _('Ukranian') : 'uk'}

    def getSelectionValues(self):
        if not hasattr(self, 'values'):
            self.values = []
            for i in self.getSelectionValueDescriptions():
                self.values.append(self.LANGUAGES[i])
        return self.values

    def getSelectionValueDescriptions(self):
        if not hasattr(self, 'descriptions'):
            self.descriptions = self.LANGUAGES.keys()
            self.descriptions.sort()
        return self.descriptions

class TimeZones:

    DEFAULT_TIMEZONE_ABBR = 'AAA'

    systemTimezone = "GMT" + str(-time.timezone/60/60) + ' '
    if len(time.tzname) == 1:
        systemTimezone = time.tzname[0] + systemTimezone
    elif len(time.tzname) == 2:
        systemTimezone = systemTimezone + time.tzname[0] + '/' + time.tzname[1]

    TIME_ZONES = [ ('',      _('Use system time zone (%s)') % systemTimezone),
                   ('-12',   _('Eniwetok, Kwajalein')),
                   ('-11',   _('Alofi, Apia')),
                   ('-10',   _('Avura, Ulupalakua'), 'HST'),
                   ('-9',    _('Bora Bora, Nome'), 'AKST', 'AKDT'),
                   ('-8',    _('Pacific (Berkeley, Vancouver)'), 'PST', 'PDT'),
                   ('-7',    _('Mountain (Helena, Provo)'), 'MST', 'MDT'),
                   ('-6',    _('Central (Decatur, Repulse Bay)'), 'CST', 'CDT'),
                   ('-5',    _('Eastern (Northampton, Seacoast)'), 'EST', 'EDT'),
                   ('-4',    _("Atlantic (Bishop's Falls, Halifax)"), 'AST', 'ADT'),
                   ('-3:30', _("Newfoundland (St. John's, Cow Head)"), 'NST', 'NDT'),
                   ('-3',    _('Montevideo, New Amsterdam')),
                   ('-2',    _('Belgrano, Fernando de Noronha')),
                   ('-1',    _('Azores, Scoresbysund')),
                   ( '0',    _('Bath, Hekla'), 'GMT', 'BST'),
                   ('+1',    _('Belgrade, Szeged'), 'CET'),
                   ('+2',    _('Minsk, Thessaloniki'), 'EET'),
                   ('+3',    _('Manama, Nakuru'), 'MSK'),
                   ('+3:30', _('Chabahar, Qom')),
                   ('+4',    _('Abu Dabhi, Sevastopol')),
                   ('+4:30', _('Kandahar, Mazar-e-Sharif')),
                   ('+5',    _('Hyderabad, Turkmenbasy')),
                   ('+5:30', _('Chennai, Kathmandu')),
                   ('+6',    _('Petropavlovsk, Thimphu')),
                   ('+7',    _('Irkutsk, Xiangkhoang')),
                   ('+8',    _('Bandar Seri Begawan, Ulaanbaatar'), 'AWST'),
                   ('+9',    _('Kyoto, Maluku'), 'JST'),
                   ('+9:30', _('Darwin'), 'ACST', 'ACDT'),
                   ('+10',   _('Saipan, Vladivostok'), 'AEST', 'AEDT'),
                   ('+11',   _('Luganville, Noumea')),
                   ('+12',   _('Suva, Dunedin')),
                   ]    

    def getAbbreviationFromName(self, abbreviation, daylight=0):
        """Turns a timezone name timezone name eg ('PST8PDT') into a
        timezone abbreviation (eg. 'PDT')."""
        tup = self._getTimezoneHash().get(abbreviation)        
        if tup:
            if not daylight or len(tup) == 1:
                return tup[0]
            else:
                return tup[1]
        else:
            #They probably gave us 'GMT+8' or something that's already
            #a valid timezone descriptor.
            return abbreviation

    def getTimezoneName(self, offset, normalAbbr, daylightAbbr):
        #The TZ var treats positive offsets as west of GMT,
        #which is the opposite of what you'd expect and of
        #what we display. We strip off '-' and change '+' to
        #'-'.
        if offset:
            if offset[0] == '-':
                offset = offset[1:]
            elif offset[0] == '+':
                offset = '-' + offset[1:]
        return normalAbbr + offset + daylightAbbr

    def _getTimezoneHash(self):
        if not hasattr(self, 'timezoneAbbreviationsByName'):
            self.timezoneAbbreviationsByName = {}
            for i in self.TIME_ZONES:
                (offset, cities, normal, daylight) = self._parseTuple(i)
                if offset:
                    name = self.getTimezoneName(offset, normal, daylight)
                    if normal == self.DEFAULT_TIMEZONE_ABBR:
                        normal = 'GMT' + i[0]
                    if not daylight:
                        daylight = normal                
                    self.timezoneAbbreviationsByName[name] = (normal, daylight)
        return self.timezoneAbbreviationsByName
        
    def _parseTuple(self, tup):
        offset = tup[0]
        cities = tup[1]
        if offset:
            normal = self.DEFAULT_TIMEZONE_ABBR
        else:
            normal = ''
        daylight = ''
        if len(tup) > 2:
            normal = tup[2]
        if len(tup) > 3:
            daylight = tup[3]
        return (offset, cities, normal, daylight)

    def getSelectionValues(self):
        if not hasattr(self, 'values'):
            self.values = []
            for i in self.TIME_ZONES:
                (offset, cities, normal, daylight) = self._parseTuple(i)
                self.values.append(self.getTimezoneName(offset, normal,
                                                        daylight))
        return self.values

    def getSelectionValueDescriptions(self):
        if not hasattr(self, 'descriptions'):
            self.descriptions = [self.TIME_ZONES[0][1]]            
            for i in self.TIME_ZONES[1:]:                
                offset, cities = i[:2]
                if offset == '0':
                    offset = '+' + offset
                if ':' not in offset:
                    offset = offset + ':00'
                if '(' not in cities:
                    cities = '(%s)' % cities
                self.descriptions.append('GMT %s %s' % (offset, cities))
        return self.descriptions

timeZones = TimeZones()

class TimeZoneOption(TimeZones, SelectionOption):

    """A selection drop-down for time zone selection, which yields
    values in the format accepted by the Unix TZ environment variable.

    I've tried to choose time zone-appropriate cities to illustrate
    each time zone, but since I've been to very few of these places I
    may have made some mistakes. I also don't have abbreviations for
    most of the time zones.    

    The abbreviation stuff is a mess. I think the best way to go might
    be to put a "Daylight savings?" checkbox next to the selection box
    and, if it's set, generate a bogus daylight savings time zone
    abbreviation to go along with the bogus regular time zone
    abbreviation. This would allow you to use the TZ environment variable
    as a simple GMT offset, and if you needed the name of the timezone you
    could define the TZ format in a text box."""

    OPTION_KEY = 'timezone'
    

class PEcrypt:
    """
    PEcrypt - very, very simple word key encryption system
              uses cyclic XOR between the keyword character
              bytes and the string to be encrypted/decrypted.
              Therefore, the same function and keyword will
              encrypt the string the first time and decrypt
              it if called on the encrypted string.
    """

    def __init__(self, aKey):
        """
        Initialise the class with the key that
        is used to encrypt/decrypt strings
        """
        self.key = aKey

        # CRC can be used to validate a key (very roughly)
        # if you store the CRC from a previous keyword
        # and then compare with a newly generated one and
        # they are the same then chances are the keyword
        # is correct - only a single byte so not that reliable
        self.crc = 0    
        for x in self.key:
            intX = ord(x)
            self.crc = self.crc ^ intX


    def Crypt(self, aString):
        """
        Encrypt/Decrypt the passed string object and return
        the encrypted string
        """
        kIdx = 0
        cryptStr = ""   # empty 'crypted string to be returned

        # loop through the string and XOR each byte with the keyword
        # to get the 'crypted byte. Add the 'crypted byte to the
        # 'crypted string
        for x in range(len(aString)):
            cryptStr = cryptStr + \
                       chr( ord(aString[x]) ^ ord(self.key[kIdx]))
            # use the mod operator - % - to cyclically loop through
            # the keyword
            kIdx = (kIdx + 1) % len(self.key)

        return cryptStr
