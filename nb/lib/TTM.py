"""Template! The Musical

A simple, fast templating system.

This library has no external dependencies. It defines the following
classes:

* TemplatingSystem: Contains information about which strings denote
  template directives. Implements a simple templating system.

* BracketedTemplatingSystem: An alternate templating system in which the
  directives are more clearly denoted.

* Template: A wrapper for a string which enables template directives
  embedded in the string to be interpolated.

To use, create a dictionary which maps directive names to method
names, and an object which implements the methods named in the
directive map. Initialize an Template object with the string you want
to interpolate and the TemplatingSystem class which defines the format
of the template, Call interpolate() on the Template object, passing in
the directive map and the object which implements the directives. See
the self=test at the end for some simple examples.

This system performs about 95% as well as simply calling the methods
yourself and printing out the intervening text.

"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2002-2003 Leonard Richardson"
__license__ = "Python"

import re
import string
import types
import StringIO
import time

TIMING = 0

class TemplatingSystem:

    """Override this class to define different variables for your
    templating system. DIRECTIVE_BEGIN and DIRECTIVE_END can be either
    strings or regular expression objects. Set CONSUME_DIRECTIVE_END
    to true if DIRECTIVE_END is not something you want showing up in
    the interpolated text (like brackets), false if the end of a
    directive is delimited by, eg. whitespace, which you do want to
    show up in the interpolated text.

    In the default templating system where the directives are enclosed
    in curly brackets.  For instance, in "foo %{bar}. %{baz}", there
    are two directives: "bar", and "baz"."""

    DIRECTIVE_BEGIN = '%{'
    DIRECTIVE_END_NO_GROUPS = '}'
    CONSUME_DIRECTIVE_END = 1

class WhitespaceTemplatingSystem(TemplatingSystem):

    """In this templating system, directives start with a % and end
    with whitespace or a non-alphanumeric character. For instance, in
    "foo %bar. %baz %bar", there are three directives: "bar", "baz",
    and "bar" again. "%%" will yield a literal % character."""

    DIRECTIVE_BEGIN = '%'
    DIRECTIVE_END_NO_GROUPS = re.compile('[^A-Za-z0-9\-_]')
    CONSUME_DIRECTIVE_END = 0

class ParenthesizedArgumentsTemplatingSystem(TemplatingSystem):

    """This system supports directives %like-this_one and
    %like-this(one). In the latter case, the method corresponding to
    "like-this" will be called with "one" as a string argument, passed
    in after the context."""

    DIRECTIVE_BEGIN = re.compile('%([A-Za-z0-9\-_]*\()?')
    DIRECTIVE_END_NO_GROUPS = re.compile('[^A-Za-z0-9\-_]')
    DIRECTIVE_END_WITH_GROUPS = re.compile('(?<!\\\)\)')

    def extractDirectiveAndArgument(self, groups, text):        
        return [groups[0][:-1], string.replace(text, '\)', ')')]

    def CONSUME_DIRECTIVE_END(self, groups):
        return groups and groups[0]

class Template:

    def __init__(self, text, templatingSystem=TemplatingSystem):
        if type(templatingSystem) == types.InstanceType:
            self.system = templatingSystem
        else:
            self.system = templatingSystem()
        self.text = text
        self.analysis = None

    def __str__(self):
        return self.text

    def interpolate(self, directiveMaps, object, backupSources=[],
                    context=None, writeTo=None):

        """Interpolates this template on the given object using the
        given map of directives to method names. Methods are called on
        the provided object and the provided context is used as an
        argument to the method (if it takes an argument). If the named
        method is not a method on the given object, then the objects
        listed in backupSources are tried in turn before TTM gives up.

        Pass in a filehandle as writeTo to have Python write template
        fragments to that filehandle as it figures them out, rather
        than building a big string and returning it. This saves time
        and memory when you're pumping a lot of data direct to, e.g.,
        sys.stdout.

        The context object, if provided, will have its TTM_directive
        set to the name of the current directive. You can use this to
        distinguish between multiple directives which map to the same
        method."""

        #TODO: we could keep a dictionary mapping directive names to
        #object methods, for use when a template contained the same
        #directive multiple times. Usually a template only contains a
        #given directive once, though.

        writtenToString = not writeTo
        if not writeTo:
            writeTo = StringIO.StringIO()

        templateAnalysis = self.analyze()
        #print "Interpolating %s" % self.text
        if TIMING:
            before = time.clock()
        curPos = 0
        c = object.__class__
        for (startPos, endPos, directive, argument) in templateAnalysis:
            if context:
                context.TTM_directive = directive
            if curPos < startPos:
                s = self.text[curPos:startPos]
                #print "Gobbling up", s
                writeTo.write(s)
            if type(directiveMaps) in (types.ListType, types.TupleType):
                for m in directiveMaps:
                    directiveMethodName = m.get(directive)
                    if directiveMethodName:
                        break
            else:
                directiveMethodName = directiveMaps.get(directive)
            #print "Directive lookup: %s=%s" % (directive, directiveMethodName)
            if directiveMethodName:
                method = getattr(c, directiveMethodName, None)
                if method:
                    o = object
                else:
                    for backupObject in backupSources:
                        method = getattr(backupObject.__class__,
                                         directiveMethodName, None)
                        if method:
                            o = backupObject
                            break
                res = None
                if not method:
                    raise 'Bad directive map: could not find a method called "%s"! (Corresponding to directive "%s")' % (directiveMethodName, directive)
                #print "Calling ", method.__name__, " on ", o, ", ", context
                if context:
                    try:
                        if argument:
                            res = method(o, context, argument)
                        else:
                            res = method(o, context)
                    except TypeError, e:
                        #Sometimes a template method can be called but
                        #calling it throws a TypeError due to a bug. If
                        #this happens, you'll get an unhelpful "takes
                        #exactly 2 arguments" error when method(o) is
                        #called. Uncomment this line to find out what
                        #the actual error is while debugging.
                        #
                        #print e
                        if argument:
                            res = method(o, argument)
                        else:
                            res = method(o)
                else:
                    if argument:
                        res = method(o, argument)
                    else:
                        res = method(o)
                if res:
                    writeTo.write(res)
            elif directive == self.system.DIRECTIVE_BEGIN:
                writeTo.write(directive)
            else:
                s = self.text[startPos:endPos+1]
                writeTo.write(s)
            curPos = endPos+1
        s = self.text[curPos:]
        writeTo.write(s)
        if writtenToString:
            s = writeTo.getvalue()
            writeTo.close()
        else:
            s = None
        if TIMING:
            after = time.clock()
            print "Template interpolation took %s sec" %(after-before)
        return s

    def analyze(self):
        """Derives from the template text a list of string offsets
        containing directives."""
        if not self.analysis:
            if TIMING:
                before = time.clock()
            templateAnalysis = []
            done = 0
            pos = 0
            while not done:
                #print "Looking for directive in", self.text[pos:]
                (beginS, beginE, beginG) = self.__find(self.system.DIRECTIVE_BEGIN,
                                                       pos)
                if beginS < 0 or beginS == None:
                    done = 1
                else:
                    if beginG and beginG[0]:
                        use = self.system.DIRECTIVE_END_WITH_GROUPS
                    else:
                        use = self.system.DIRECTIVE_END_NO_GROUPS
                    (endS, endE, endG) = self.__find(use, beginE)
                    if endS == -1 or endS == None:
                        endS = len(self.text)
                        done=1
                    directive = self.text[beginE:endS]
                    argument = None                        
                    if beginG and beginG[0] and hasattr(self.system, 'extractDirectiveAndArgument'):
                        directive, argument = self.system.extractDirectiveAndArgument(beginG, directive)
                    #print endE
                    if endE:                        
                        directiveEnd = endS-1
                    else:
                        directiveEnd = len(self.text)
                    try:
                        consume = self.system.CONSUME_DIRECTIVE_END(beginG)
                    except:
                        consume = self.system.CONSUME_DIRECTIVE_END
                    if consume:
                        directiveEnd = endS
                    andArgument = ''
                    #if argument:
                    #    andArgument = " and argument %s" % argument
                    #print "Found directive %s%s" % (directive, andArgument)
                    templateAnalysis.append((beginS, directiveEnd, directive,
                                             argument))
                    if endE:                        
                        pos = endE-1
                    else:
                        pos = len(self.text)
            self.analysis = templateAnalysis
            if TIMING:
                after = time.clock()
                print "Template analysis took %s sec" %(after-before)
        return self.analysis

        
    def __find(self, exp, startAt):

        """ "Find expression in template string." If the given
        expression is a string, finds a substring starting at the
        given offset. If the given expression is a regular expression
        object, finds the first match starting at the given
        offset. Returns the position in the string of the first and
        last characters of the expression. If there were groups for
        the regular expression, returns the groups."""

        #print "Trying to find", exp.pattern, " in '%s', starting at %s" % \
        #      (self.text, startAt)
        if type(exp) == types.StringType:
            first = string.find(self.text, exp, startAt)
            found = (first, first+len(exp), None)
        else:
            match = exp.search(self.text, startAt)
            if match:
                found = (match.start(), match.end(), match.groups())
            else:
                found = (None, None, None)
        #print " Found", found
        return found
            
        
if __name__ == '__main__':

    #Perform simple self-test

    import random

    class TestObject:
        def getClassState(self):
            return random.choice(["full", "mostly full", "half full",
                                 "half empty", "mostly empty", "empty"])

        def whatISay(self):
            return random.choice(["bah!", "good for them.",
                                 "it is %s." % self.getClassState()])

    map = { 'classState': 'getClassState',
            'whatISay' : 'whatISay' }

    object = TestObject()

    template = Template("%% %a Some say the class is %classState. I say %whatISay")
    print template.interpolate(map, object)

    template = Template("%{a} Now those same people say the class is %{classState}. Myself, I say %{whatISay}", TemplatingSystem)
    print template.interpolate(map, object)


    class TestObject2:
        def foo(self):
            return 'I feel happy'

        def bar(self, arg):
            return '-!%s!-' % arg

    map = {'a_b-c' : 'foo',
           'a' : 'bar',
           'e' : 'foo'}
    
    system = ParenthesizedArgumentsTemplatingSystem
    template = Template("% %a_b-c %a((When I put things in parentheses\)) %e%e %", system)
    print template.interpolate(map, TestObject2())

