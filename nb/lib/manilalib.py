"""Interface for Manila servers"""

__author__ = "Mark Pilgrim (f8dy@diveintopython.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2001 Mark Pilgrim"
__license__ = "Python"

# Requires Pythonware's XML-RPC library
# http://www.pythonware.com/products/xmlrpc/
import xmlrpclib
from xml.dom import minidom
import string
import types

class NotConnectedError(Exception): pass
class NotSupportedError(Exception): pass

def _unbool(value):
    return value and 1 or 0

def _undate(date):
    v = date.value
    return (int(v[:4]), int(v[4:6]), int(v[6:8]), int(v[9:11]), int(v[12:14]), int(v[15:17]), 0, 0, 0)

def _un(value):
    if isinstance(value, xmlrpclib.Boolean):
        value = _unbool(value)
    if isinstance(value, xmlrpclib.DateTime):
        value = _undate(value)
    if isinstance(value, types.DictionaryType):
        if value.has_key("body"):
            value = value["body"]
    return value

class RefreshList:
    def __init__(self):
        self.data = []
        self.dirty = 1
    
    def forceRefresh(self):
        self.dirty = 1
        self.refresh()
        
    def refresh(self):
        pass
    
    def __repr__(self):
        self.refresh()
        return repr(self.data)
    
    def __contains__(self, item):
        self.refresh()
        return item in self.data
    
    def __len__(self):
        self.refresh()
        return len(self.data)
    
    def __getitem__(self, i):
        self.refresh()
        return self.data[i]
    
    def __getslice__(self, i, j):
        self.refresh()
        i = max(i, 0); j = max(j, 0)
        return self.__class__(self.data[i:j])
    
    def __add__(self, other):
        self.refresh()
        for item in other:
            self.append(item)
    __radd__ = __add__
    extend = __add__

    def __setitem__(self, i, item):
        raise NotSupportedError, 'editing not supported'
    
    def __delitem__(self, i):
        raise NotSupportedError, 'deleting not supported'
    
    def append(self, item):
        raise NotSupportedError, 'append not supported'
    
    def remove(self, item):
        raise NotSupportedError, 'remove not supported'
    
    def count(self, item):
        raise NotSupportedError, 'count not supported'
    
    def index(self, item):
        raise NotSupportedError, 'index not supported'
    
class Site:
    """a Manila site"""
    def __init__(self, serverURL, username=None, password=None, pingWeblogsComOnChange=0):
        self.url = serverURL
        serverURL = self._mungeURL(serverURL)
        self.pingWeblogsComOnChange = pingWeblogsComOnChange
        self.remote = xmlrpclib.Server(serverURL)
        self.siteName = self.remote.manila.getSiteName(self.url)
        self.login(username, password)
        self.members = MemberList(self)
        self.editors = EditorList(self)
        self.shortcuts = ShortcutList(self)
        self.preferences = Preferences(self)
        self.messages = MessageList(self)
        self.stories = StoryList(self)
        self.pictures = PictureList(self)
        self.calendar = Calendar(self)
        self.homepage = HomePage(self)
    
    def _mungeURL(self, url):
        if url[-5:] <> '/RPC2':
            if url[-1:] <> '/':
                url = url + '/'
            url = url + 'RPC2'
        return url
    
    def login(self, username, password):
        self.username = username
        self.password = password

    def loggedIn(self):
        return (self.username and self.password) and 1 or 0

    def pingWeblogsCom(self):
        if self.pingWeblogsComOnChange:
            weblogsCom = xmlrpclib.Server("http://rpc.weblogs.com/RPC2")
            data = weblogsCom.weblogUpdates.ping(self.siteName, self.url)
            rc = _unbool(data["flerror"])
            rc = 1 - rc # weirdness, server returns false for success
            return (rc, data["message"])
        else:
            return (0, "Ping disabled, set site.pingWeblogsOnChange=1 to enable")
    
class MemberRole:
    def __init__(self, role, parent):
        self.member = parent
        self.role = role
        if not self.role:
            self.dirty = 1
        else:
            self.dirty = 0
        
    def get(self):
        """get member role from server"""
        if self.dirty:
            site = self.member.parent.site
            self.role = site.remote.manila.member.getRole(self.member.email, site.siteName)
            self.dirty = 0
        return self.role
    
    def __str__(self):
        return str(self.get())
    
class Member:
    NotConnectedMessage = "member is not connected to a site"
    
    def __init__(self, name, email, role=None, password=None, parent=None):
        self.name = name
        self.email = email
        self.password = password
        self.parent = parent
        self.role = MemberRole(role, self)
    
    def block(self):
        """block a member"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        return _unbool(site.remote.manila.member.block(site.username, site.password, site.siteName, self.email))
    
    def unblock(self):
        """unblock a member"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        return _unbool(site.remote.manila.member.unblock(site.username, site.password, site.siteName, self.email))
    
class MemberList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1

    def refresh(self):
        """re-read member list from server"""
        if self.dirty:
            dict = self.site.remote.manila.member.listAll( \
                self.site.username, self.site.password, self.site.siteName)
            items = dict.items()
            items.sort()
            self.data = []
            for k, v in items:
                self.data.append(Member(v["name"], v["email"], None, None,
                                        self))
            self.dirty = 0

    def append(self, member):
        """add member to site (argument must be Member instance)"""
        self.refresh()
        if not isinstance(member, Member):
            raise TypeError, 'value must be Member instance'
        rc = _unbool(self.site.remote.manila.member.create( \
            self.site.username, self.site.password, self.site.siteName,
            member.email, member.name, member.password))
        if rc:
            member.parent = self
            self.data.append(member)
        return rc

    def index(self, email):
        """get index of member by email address"""
        self.refresh()
        emailList = []
        for m in self:
            emailList.append(m.email)
        return emailList.index(email)

    def count(self, email):
        """count members by email address
        
        since email addresses are unique, this will only ever return 1 or 0"""
        self.refresh()
        emailList = []
        for m in self:
            emailList.append(m.email)
        return emailList.count(email)

class Editor(Member):
    def listStories(self):
        """list stories posted by this editor"""
        if not self.parent:
            raise RuntimeError, self.NotConnectedMessage
        return self.parent.site.stories.listByEditor(self)

class EditorList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1

    def refresh(self):
        """re-read editor list from server"""
        if self.dirty:
            managingEditorsDict = self.site.remote.manila.editors.getTable( \
                self.site.siteName, 'managingEditors', self.site.username, self.site.password)
            contributingEditorsDict = self.site.remote.manila.editors.getTable( \
                self.site.siteName, 'contributingEditors', self.site.username, self.site.password)
            self.data = []
            for unused, data in managingEditorsDict.items():
                self.data.append(Editor(data["name"], data["email"], 1, None,
                                        self))
            for unused, data in contributingEditorsDict.items():
                self.data.append(Editor(data["name"], data["email"], 1, None,
                                        self))
            self.dirty = 0

    def index(self, email):
        """get index of editor by email address"""
        self.refresh()
        emailList = []
        for e in self:
            emailList.append(e.email)
        return emailList.index(email)

    def count(self, email):
        """count editors by email address
        
        since email addresses are unique, this will only ever return 1 or 0"""
        self.refresh()
        emailList = []
        for e in self:
            emailList.append(e.email)
        return emailList.count(email)

class Shortcut:
    NotConnectedMessage = "shortcut is not connected to a site"
    
    def __init__(self, name, value, parent=None):
        self.name = name
        self.value = value
        self.parent = parent

    def __setitem__(self, key, value):
        if self.parent:
            if key == "value":
                self.set(value)
        self.__dict__[key] = value
    
    def set(self, value):
        """edit existing shortcut"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        rc = _unbool(site.remote.manila.shortcut.set( \
            site.username, site.password, site.siteName,
            self.name, value))
        self.__dict__["value"] = value
        return rc
        
    def delete(self):
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        rc = _unbool(site.remote.manila.shortcut.delete( \
            site.username, site.password, site.siteName, self.name))
        if rc:
            self.parent.dirty = 1
        return rc

class ShortcutList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1
    
    def refresh(self):
        """re-read shortcut list from server"""
        if self.dirty:
            dict = self.site.remote.manila.shortcut.getTable( \
                self.site.username, self.site.password, self.site.siteName)
            items = dict.items()
            items.sort()
            self.data = []
            for k, v in items:
                self.data.append(Shortcut(v["name"], v["value"], self))
            self.dirty = 0

    def append(self, shortcut):
        """create new shortcut (argument must be Shortcut instance)"""
        self.refresh()
        if not isinstance(shortcut, Shortcut):
            raise TypeError, 'value must be Shortcut instance'
        shortcut.parent = self
        self.data.append(shortcut)
        rc = shortcut.set(shortcut.value)
        return rc

    def __delitem__(self, i):
        self.refresh()
        self.data[i].delete()

    def remove(self, name):
        """remove shortcut by name"""
        self.refresh()
        found = None
        for s in self:
            if s.name == name:
                found = s
                break
        if found:
            return found.delete()
        else:
            raise ValueError, '%s not in list' % name

    def index(self, name):
        """get index of shortcut by name"""
        self.refresh()
        nameList = []
        for s in self:
            nameList.append(s.name)
        return nameList.index(name)

    def count(self, name):
        """count shortcuts by name
        
        since shortcut names are unique, this will only ever return 1 or 0"""
        self.refresh()
        nameList = []
        for s in self:
            nameList.append(s.name)
        return nameList.count(name)
        
def _setDirtyRecurse(node):
    if node and isinstance(node, Preference):
        node.dirty = 1
        _setDirtyRecurse(node.parent)

class Preference:
    remotePath = "manila.sitePref"
    
    def __init__(self, prefName, parent):
        if isinstance(parent, Preference):
            prefName = "%s.%s" % (parent.prefName, prefName)
        self.prefName = prefName
        self.parent = parent
        self.site = self.parent.site
        self.dirty = 1
        self.__value = None

    def _remote(self):
        return getattr(self.parent.site.remote, self.remotePath)
    
    def get(self):
        """get preference value, from server if necessary"""
        if self.dirty:
            self.__value = _un(self._remote().get( \
                self.site.username, self.site.password,
                self.site.siteName, self.prefName))
            self.dirty = 0
        return self.__value

    def __str__(self):
        return str(self.get())
    
    def _set(self, value):
        return _unbool(self._remote().set( \
            self.site.username, self.site.password,
            self.site.siteName, self.prefName, value))

    def set(self, value):
        """set preference"""
        rc = self._set(value)
        if rc:
            self.__value = value
            _setDirtyRecurse(self.parent)
        return rc

class AdvancedPreference(Preference):
    remotePath = "manila.advancedPref"

    def checkOut(self):
        """check out preference item"""
        return _unbool(self._remote().checkOut( \
            self.site.username, self.site.password,
            self.site.siteName, self.prefName))

    def checkIn(self):
        """check in preference item"""
        return _unbool(self._remote().checkIn( \
            self.site.username, self.site.password,
            self.site.siteName, self.prefName))

    def restoreDefault(self):
        """restore preference item to default value (determined by server)"""
        rc = _unbool(self._remote().restoreDefault( \
            self.site.username, self.site.password,
            self.site.siteName, self.prefName))
        if rc:
            self.dirty = 1
        return rc

    def _set(self, value):
        return _unbool(self._remote().set( \
            self.site.username, self.site.password,
            self.site.siteName, self.prefName, value, {}))
    
class Preferences:
    def __init__(self, site):
        self.site = site
        self.css = AdvancedPreference("css", self)
        self.customPrefs = AdvancedPreference("customPrefs", self)
        self.homePageTemplate = AdvancedPreference("homepageTemplate", self)
        self.javaScript = AdvancedPreference("javaScript", self)
        self.joinNowMembersBox = AdvancedPreference("joinNowMembersBox", self)
        self.navigation = AdvancedPreference("navigation", self)
        self.newsDayTemplate = AdvancedPreference("newsDayTemplate", self)
        self.newsItemTemplate = AdvancedPreference("newsItemTemplate", self)
        self.signOutMembersBox = AdvancedPreference("signOutMembersBox", self)
        self.siteStructure = AdvancedPreference("siteStructure", self)
        self.template = AdvancedPreference("template", self)
        # the rest is auto-generated from http://static.userland.com/theTwoWayWebXml/sitePrefs.xml
        self.autoShortcuts = Preference('autoShortcuts', self)
        self.backgroundPicture = Preference('backgroundPicture', self)
        self.bulletins = Preference('bulletins', self)
        self.bulletins.lastSubject = Preference('lastSubject', self.bulletins)
        self.bulletins.lastText = Preference('lastText', self.bulletins)
        self.bulletins.senderEmail = Preference('senderEmail', self.bulletins)
        self.bulletinsPrefName = Preference('bulletinsPrefName', self)
        self.bylineFormat = Preference('bylineFormat', self)
        self.calendarTemplates = Preference('calendarTemplates', self)
        self.calendarTemplates.dayNameTemplate = Preference('dayNameTemplate', self.calendarTemplates)
        self.calendarTemplates.dayTemplate = Preference('dayTemplate', self.calendarTemplates)
        self.calendarTemplates.monthYearTemplate = Preference('monthYearTemplate', self.calendarTemplates)
        self.colors = Preference('colors', self)
        self.colors.adminBoxBackground = Preference('adminBoxBackground', self.colors)
        self.colors.adminBoxBackgroundText = Preference('adminBoxBackgroundText', self.colors)
        self.colors.adminBoxHeader = Preference('adminBoxHeader', self.colors)
        self.colors.adminBoxHeaderText = Preference('adminBoxHeaderText', self.colors)
        self.colors.discussListings = Preference('discussListings', self.colors)
        self.colors.membersBox = Preference('membersBox', self.colors)
        self.colors.membersBoxHeader = Preference('membersBoxHeader', self.colors)
        self.colors.membersBoxHeaderText = Preference('membersBoxHeaderText', self.colors)
        self.colors.separator = Preference('separator', self.colors)
        self.colors.separatorText = Preference('separatorText', self.colors)
        self.contributingEditors = Preference('contributingEditors', self)
        self.cowSkullImage = Preference('cowSkullImage', self)
        self.custody = Preference('custody', self)
        self.custody.timeOutMinutes = Preference('timeOutMinutes', self.custody)
        self.directory = Preference('directory', self)
        self.directory.commentTemplate = Preference('commentTemplate', self.directory)
        self.directory.flDisplayTitle = Preference('flDisplayTitle', self.directory)
        self.directory.foldericon = Preference('foldericon', self.directory)
        self.directory.linkicon = Preference('linkicon', self.directory)
        self.directory.suggestALinkTemplate = Preference('suggestALinkTemplate', self.directory)
        self.directory.template = Preference('template', self.directory)
        self.discussionGroup = Preference('discussionGroup', self)
        self.discussionGroup.activeTopicIcon = Preference('activeTopicIcon', self.discussionGroup)
        self.discussionGroup.dayListFooter = Preference('dayListFooter', self.discussionGroup)
        self.discussionGroup.dayListHeader = Preference('dayListHeader', self.discussionGroup)
        self.discussionGroup.dayListItemTemplate = Preference('dayListItemTemplate', self.discussionGroup)
        self.discussionGroup.dayListTemplate = Preference('dayListTemplate', self.discussionGroup)
        self.discussionGroup.dayListTemplateFooter = Preference('dayListTemplateFooter', self.discussionGroup)
        self.discussionGroup.dayListTemplateHeader = Preference('dayListTemplateHeader', self.discussionGroup)
        self.discussionGroup.daysTopicsActive = Preference('daysTopicsActive', self.discussionGroup)
        self.discussionGroup.daysTopicsLive = Preference('daysTopicsLive', self.discussionGroup)
        self.discussionGroup.inactiveTopicIcon = Preference('inactiveTopicIcon', self.discussionGroup)
        self.discussionGroup.msgTemplate = Preference('msgTemplate', self.discussionGroup)
        self.discussionGroup.responseIndentTemplate = Preference('responseIndentTemplate', self.discussionGroup)
        self.discussionGroup.responseItemTemplate = Preference('responseItemTemplate', self.discussionGroup)
        self.discussionGroup.responseListFooter = Preference('responseListFooter', self.discussionGroup)
        self.discussionGroup.responseListHeader = Preference('responseListHeader', self.discussionGroup)
        self.discussionGroup.responseSubThreadEndTemplate = Preference('responseSubThreadEndTemplate', self.discussionGroup)
        self.discussionGroup.responseSubThreadStartTemplate = Preference('responseSubThreadStartTemplate', self.discussionGroup)
        self.discussionGroup.topicBased = Preference('topicBased', self.discussionGroup)
        self.discussionGroup.topicListFooter = Preference('topicListFooter', self.discussionGroup)
        self.discussionGroup.topicListHeader = Preference('topicListHeader', self.discussionGroup)
        self.discussionGroup.topicListItemTemplate = Preference('topicListItemTemplate', self.discussionGroup)
        self.discussionGroup.topicListTemplate = Preference('topicListTemplate', self.discussionGroup)
        self.discussionGroup.topicMsgTemplate = Preference('topicMsgTemplate', self.discussionGroup)
        self.discussionGroup.topicTemplate = Preference('topicTemplate', self.discussionGroup)
        self.discussionGroup.userIcon = Preference('userIcon', self.discussionGroup)
        self.discussionRoot = Preference('discussionRoot', self)
        self.discussLinks = Preference('discussLinks', self)
        self.discussLinks.channelGraphicMsgNum = Preference('channelGraphicMsgNum', self.discussLinks)
        self.discussLinks.emailMsgNum = Preference('emailMsgNum', self.discussLinks)
        self.discussLinks.faqMsgNum = Preference('faqMsgNum', self.discussLinks)
        self.discussLinks.homePageMsgnum = Preference('homePageMsgnum', self.discussLinks)
        self.discussTheme = Preference('discussTheme', self)
        self.discussTheme.authorEmail = Preference('authorEmail', self.discussTheme)
        self.discussTheme.authorName = Preference('authorName', self.discussTheme)
        self.discussTheme.description = Preference('description', self.discussTheme)
        self.discussTheme.flCloneable = Preference('flCloneable', self.discussTheme)
        self.discussTheme.name = Preference('name', self.discussTheme)
        self.editorsOnlyMessage = Preference('editorsOnlyMessage', self)
        self.editorsOnlyMessageTitle = Preference('editorsOnlyMessageTitle', self)
        self.emailConfirmationSender = Preference('emailConfirmationSender', self)
        self.emailConfirmationSubject = Preference('emailConfirmationSubject', self)
        self.flBackIssueDisplayTitle = Preference('flBackIssueDisplayTitle', self)
        self.flBylinesOnHomePage = Preference('flBylinesOnHomePage', self)
        self.flBylinesOnStories = Preference('flBylinesOnStories', self)
        self.flCalendarOnHomePage = Preference('flCalendarOnHomePage', self)
        self.flDiscussListByDay = Preference('flDiscussListByDay', self)
        self.flEditFormOnHomePage = Preference('flEditFormOnHomePage', self)
        self.flEditorsOnlyAccessToSite = Preference('flEditorsOnlyAccessToSite', self)
        self.flHomePageDisplayTitle = Preference('flHomePageDisplayTitle', self)
        self.flMembership = Preference('flMembership', self)
        self.flMembersListPublic = Preference('flMembersListPublic', self)
        self.flNewsItemsByNewsDay = Preference('flNewsItemsByNewsDay', self)
        self.flNewsItemSite = Preference('flNewsItemSite', self)
        self.flNewThreadOnDiscussHome = Preference('flNewThreadOnDiscussHome', self)
        self.flNotifyChangedMessages = Preference('flNotifyChangedMessages', self)
        self.flNotifyChangedStories = Preference('flNotifyChangedStories', self)
        self.flNotifyEditedNewsItems = Preference('flNotifyEditedNewsItems', self)
        self.flNotifyNewMessages = Preference('flNotifyNewMessages', self)
        self.flNotifyNewNewsItems = Preference('flNotifyNewNewsItems', self)
        self.flNotifyNewStories = Preference('flNotifyNewStories', self)
        self.flNotifyPostedNewsItems = Preference('flNotifyPostedNewsItems', self)
        self.flNotifyReleasedNewsItems = Preference('flNotifyReleasedNewsItems', self)
        self.flPublicMembersDgAccess = Preference('flPublicMembersDgAccess', self)
        self.flSearchable = Preference('flSearchable', self)
        self.flShowMemberBookmarksOnHomePage = Preference('flShowMemberBookmarksOnHomePage', self)
        self.flStaticRenderingAlwaysAvailable = Preference('flStaticRenderingAlwaysAvailable', self)
        self.flStaticRenderingEnabled = Preference('flStaticRenderingEnabled', self)
        self.flSyndicate = Preference('flSyndicate', self)
        self.flUseHomePageTemplate = Preference('flUseHomePageTemplate', self)
        self.homePagesListedIn = Preference('homePagesListedIn', self)
        #self.javaScript = Preference('javaScript', self)
        self.legalTags = Preference('legalTags', self)
        self.legalTags.a = Preference('a', self.legalTags)
        self.legalTags.applet = Preference('applet', self.legalTags)
        self.legalTags.area = Preference('area', self.legalTags)
        self.legalTags.b = Preference('b', self.legalTags)
        self.legalTags.big = Preference('big', self.legalTags)
        self.legalTags.blockquote = Preference('blockquote', self.legalTags)
        self.legalTags.br = Preference('br', self.legalTags)
        self.legalTags.br.flClose = Preference('flClose', self.legalTags.br)
        self.legalTags.br.flLegal = Preference('flLegal', self.legalTags.br)
        self.legalTags.button = Preference('button', self.legalTags)
        self.legalTags.center = Preference('center', self.legalTags)
        self.legalTags.cite = Preference('cite', self.legalTags)
        self.legalTags.code = Preference('code', self.legalTags)
        self.legalTags.dd = Preference('dd', self.legalTags)
        self.legalTags.dd.flClose = Preference('flClose', self.legalTags.dd)
        self.legalTags.dd.flLegal = Preference('flLegal', self.legalTags.dd)
        self.legalTags.department = Preference('department', self.legalTags)
        self.legalTags.description = Preference('description', self.legalTags)
        self.legalTags.div = Preference('div', self.legalTags)
        self.legalTags.dl = Preference('dl', self.legalTags)
        self.legalTags.dl.flClose = Preference('flClose', self.legalTags.dl)
        self.legalTags.dl.flLegal = Preference('flLegal', self.legalTags.dl)
        self.legalTags.dt = Preference('dt', self.legalTags)
        self.legalTags.dt.flClose = Preference('flClose', self.legalTags.dt)
        self.legalTags.dt.flLegal = Preference('flLegal', self.legalTags.dt)
        self.legalTags.em = Preference('em', self.legalTags)
        self.legalTags.embed = Preference('embed', self.legalTags)
        self.legalTags.font = Preference('font', self.legalTags)
        self.legalTags.form = Preference('form', self.legalTags)
        self.legalTags.h1 = Preference('h1', self.legalTags)
        self.legalTags.h2 = Preference('h2', self.legalTags)
        self.legalTags.h3 = Preference('h3', self.legalTags)
        self.legalTags.h4 = Preference('h4', self.legalTags)
        self.legalTags.h5 = Preference('h5', self.legalTags)
        self.legalTags.h6 = Preference('h6', self.legalTags)
        self.legalTags.hr = Preference('hr', self.legalTags)
        self.legalTags.hr.flClose = Preference('flClose', self.legalTags.hr)
        self.legalTags.hr.flLegal = Preference('flLegal', self.legalTags.hr)
        self.legalTags.i = Preference('i', self.legalTags)
        self.legalTags.iframe = Preference('iframe', self.legalTags)
        self.legalTags.img = Preference('img', self.legalTags)
        self.legalTags.img.flClose = Preference('flClose', self.legalTags.img)
        self.legalTags.img.flLegal = Preference('flLegal', self.legalTags.img)
        self.legalTags.input = Preference('input', self.legalTags)
        self.legalTags.input.flClose = Preference('flClose', self.legalTags.input)
        self.legalTags.input.flLegal = Preference('flLegal', self.legalTags.input)
        self.legalTags.label = Preference('label', self.legalTags)
        self.legalTags.li = Preference('li', self.legalTags)
        self.legalTags.li.flClose = Preference('flClose', self.legalTags.li)
        self.legalTags.li.flLegal = Preference('flLegal', self.legalTags.li)
        self.legalTags.map = Preference('map', self.legalTags)
        self.legalTags.newsitem = Preference('newsitem', self.legalTags)
        self.legalTags.object = Preference('object', self.legalTags)
        self.legalTags.ol = Preference('ol', self.legalTags)
        self.legalTags.option = Preference('option', self.legalTags)
        self.legalTags.p = Preference('p', self.legalTags)
        self.legalTags.p.flClose = Preference('flClose', self.legalTags.p)
        self.legalTags.p.flLegal = Preference('flLegal', self.legalTags.p)
        self.legalTags.param = Preference('param', self.legalTags)
        self.legalTags.param.flClose = Preference('flClose', self.legalTags.param)
        self.legalTags.param.flLegal = Preference('flLegal', self.legalTags.param)
        self.legalTags.pre = Preference('pre', self.legalTags)
        self.legalTags.script = Preference('script', self.legalTags)
        self.legalTags.select = Preference('select', self.legalTags)
        self.legalTags.small = Preference('small', self.legalTags)
        self.legalTags.span = Preference('span', self.legalTags)
        self.legalTags.strong = Preference('strong', self.legalTags)
        self.legalTags.style = Preference('style', self.legalTags)
        self.legalTags.sub = Preference('sub', self.legalTags)
        self.legalTags.sup = Preference('sup', self.legalTags)
        self.legalTags.table = Preference('table', self.legalTags)
        self.legalTags.tbody = Preference('tbody', self.legalTags)
        self.legalTags.td = Preference('td', self.legalTags)
        self.legalTags.textarea = Preference('textarea', self.legalTags)
        self.legalTags.th = Preference('th', self.legalTags)
        self.legalTags.thead = Preference('thead', self.legalTags)
        self.legalTags.tr = Preference('tr', self.legalTags)
        self.legalTags.u = Preference('u', self.legalTags)
        self.legalTags.ul = Preference('ul', self.legalTags)
        self.legalTags.url = Preference('url', self.legalTags)
        #self.navigation = Preference('navigation', self)
        self.navigation.itemFormat = Preference('itemFormat', self.navigation)
        self.navigation.separatorFormat = Preference('separatorFormat', self.navigation)
        self.newsItems = Preference('newsItems', self)
        self.newsItems.access = Preference('access', self.newsItems)
        self.newsItems.access.flContributingEditorsCreate = Preference('flContributingEditorsCreate', self.newsItems.access)
        self.newsItems.access.flContributingEditorsPost = Preference('flContributingEditorsPost', self.newsItems.access)
        self.newsItems.access.flMembersCreate = Preference('flMembersCreate', self.newsItems.access)
        self.newsItems.access.flMembersPost = Preference('flMembersPost', self.newsItems.access)
        self.newsItems.departments = Preference('departments', self.newsItems)
        self.newsItemsHomePageTitle = Preference('newsItemsHomePageTitle', self)
        self.notifyList = Preference('notifyList', self)
        self.notifySender = Preference('notifySender', self)
        self.numMessagesInDiscussListing = Preference('numMessagesInDiscussListing', self)
        self.numNewsDaysToRender = Preference('numNewsDaysToRender', self)
        self.numNewsItemsToRender = Preference('numNewsItemsToRender', self)
        self.picturesListedIn = Preference('picturesListedIn', self)
        self.prefs = Preference('prefs', self)
        self.prefs.addLinefeeds = Preference('addLinefeeds', self.prefs)
        self.prefs.alink = Preference('alink', self.prefs)
        self.prefs.autoparagraphs = Preference('autoparagraphs', self.prefs)
        self.prefs.background = Preference('background', self.prefs)
        self.prefs.bgcolor = Preference('bgcolor', self.prefs)
        self.prefs.clayCompatibility = Preference('clayCompatibility', self.prefs)
        self.prefs.defaultMembershipGroup = Preference('defaultMembershipGroup', self.prefs)
        self.prefs.discussionRoot = Preference('discussionRoot', self.prefs)
        self.prefs.editingToolName = Preference('editingToolName', self.prefs)
        self.prefs.enableSafeMacros = Preference('enableSafeMacros', self.prefs)
        self.prefs.fileextension = Preference('fileextension', self.prefs)
        self.prefs.flDiscussStoreImages = Preference('flDiscussStoreImages', self.prefs)
        self.prefs.flrender = Preference('flrender', self.prefs)
        self.prefs.imageextensions = Preference('imageextensions', self.prefs)
        self.prefs.language = Preference('language', self.prefs)
        self.prefs.link = Preference('link', self.prefs)
        self.prefs.renderDiscussOutlinesWith = Preference('renderDiscussOutlinesWith', self.prefs)
        self.prefs.renderOutlineWith = Preference('renderOutlineWith', self.prefs)
        self.prefs.renderTableWith = Preference('renderTableWith', self.prefs)
        self.prefs.spaceGif = Preference('spaceGif', self.prefs)
        self.prefs.text = Preference('text', self.prefs)
        self.prefs.vlink = Preference('vlink', self.prefs)
        self.readNewsUrl = Preference('readNewsUrl', self)
        self.rssInfo = Preference('rssInfo', self)
        self.rssInfo.channelDescription = Preference('channelDescription', self.rssInfo)
        self.rssInfo.channelLink = Preference('channelLink', self.rssInfo)
        self.rssInfo.channelTitle = Preference('channelTitle', self.rssInfo)
        self.rssInfo.imageLink = Preference('imageLink', self.rssInfo)
        self.rssInfo.imageTitle = Preference('imageTitle', self.rssInfo)
        self.rssInfo.imageUrl = Preference('imageUrl', self.rssInfo)
        self.rssInfo.userLandExtensions = Preference('userLandExtensions', self.rssInfo)
        self.rssInfo.userLandExtensions.imageCaption = Preference('imageCaption', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.imageHeight = Preference('imageHeight', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.imageWidth = Preference('imageWidth', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.language = Preference('language', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.managingEditor = Preference('managingEditor', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.skipDays = Preference('skipDays', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.skipHours = Preference('skipHours', self.rssInfo.userLandExtensions)
        self.rssInfo.userLandExtensions.webmaster = Preference('webmaster', self.rssInfo.userLandExtensions)
        self.search = Preference('search', self)
        self.search.flIndex = Preference('flIndex', self.search)
        self.search.flIndexDgMessages = Preference('flIndexDgMessages', self.search)
        self.search.serverDomain = Preference('serverDomain', self.search)
        self.search.serverPort = Preference('serverPort', self.search)
        self.search.serverProcedureName = Preference('serverProcedureName', self.search)
        self.search.serverRpcPath = Preference('serverRpcPath', self.search)
        self.search.siteName = Preference('siteName', self.search)
        self.search.siteUrl = Preference('siteUrl', self.search)
        self.siteName = Preference('siteName', self)
        self.siteShellVersion = Preference('siteShellVersion', self)
        self.siteUrl = Preference('siteUrl', self)
        self.slides = Preference('slides', self)
        self.slides.indentString = Preference('indentString', self.slides)
        self.slides.outdentString = Preference('outdentString', self.slides)
        self.staticSite = Preference('staticSite', self)
        self.staticSite.filters = Preference('filters', self.staticSite)
        self.staticSite.ftpSite = Preference('ftpSite', self.staticSite)
        self.staticSite.ftpSite.folder = Preference('folder', self.staticSite.ftpSite)
        self.staticSite.ftpSite.isLocal = Preference('isLocal', self.staticSite.ftpSite)
        self.staticSite.ftpSite.method = Preference('method', self.staticSite.ftpSite)
        self.staticSite.ftpSite.url = Preference('url', self.staticSite.ftpSite)
        self.staticSite.images = Preference('images', self.staticSite)
        self.staticSite.images.fatpage = Preference('fatpage', self.staticSite.images)
        self.staticSite.images.skull = Preference('skull', self.staticSite.images)
        self.staticSite.images.space = Preference('space', self.staticSite.images)
        self.staticSite.prefs = Preference('prefs', self.staticSite)
        self.staticSite.prefs.fileExtension = Preference('fileExtension', self.staticSite.prefs)
        self.staticSite.prefs.imageExtensions = Preference('imageExtensions', self.staticSite.prefs)
        self.staticSite.prefs.lowerCaseFileNames = Preference('lowerCaseFileNames', self.staticSite.prefs)
        self.staticSite.prefs.spaceGif = Preference('spaceGif', self.staticSite.prefs)
        self.staticSite.template = Preference('template', self.staticSite)
        self.staticSite.tools = Preference('tools', self.staticSite)
        self.storiesListedIn = Preference('storiesListedIn', self)
        self.sysopMail = Preference('sysopMail', self)
        self.sysopMemberOfGroup = Preference('sysopMemberOfGroup', self)
        self.tagLine = Preference('tagLine', self)
        self.theme = Preference('theme', self)
        self.theme.authorEmail = Preference('authorEmail', self.theme)
        self.theme.authorName = Preference('authorName', self.theme)
        self.theme.description = Preference('description', self.theme)
        self.theme.flCloneable = Preference('flCloneable', self.theme)
        self.theme.name = Preference('name', self.theme)
        self.urls = Preference('urls', self)
        self.urls.discussEditInBrowser = Preference('discussEditInBrowser', self.urls)
        self.urls.discussEditInFrontier = Preference('discussEditInFrontier', self.urls)
        self.urls.discussEnclosureDownloader = Preference('discussEnclosureDownloader', self.urls)
        self.urls.discussEnclosureRPCer = Preference('discussEnclosureRPCer', self.urls)
        self.urls.discussEnclosureViewer = Preference('discussEnclosureViewer', self.urls)
        self.urls.discussGetChangesFromWorkstation = Preference('discussGetChangesFromWorkstation', self.urls)
        self.urls.discussHomePage = Preference('discussHomePage', self.urls)
        self.urls.discussMsgReader = Preference('discussMsgReader', self.urls)
        self.urls.discussNewThread = Preference('discussNewThread', self.urls)
        self.urls.discussPostEditedMessage = Preference('discussPostEditedMessage', self.urls)
        self.urls.discussPostMessage = Preference('discussPostMessage', self.urls)
        self.urls.discussTopics = Preference('discussTopics', self.urls)
        self.urls.editInPike = Preference('editInPike', self.urls)
        self.urls.editorialAdmin = Preference('editorialAdmin', self.urls)
        self.urls.editorialBulletins = Preference('editorialBulletins', self.urls)
        self.urls.editorialGems = Preference('editorialGems', self.urls)
        self.urls.editorialHelp = Preference('editorialHelp', self.urls)
        self.urls.editorialNewPicture = Preference('editorialNewPicture', self.urls)
        self.urls.editorialNewsItems = Preference('editorialNewsItems', self.urls)
        self.urls.editorialNewStory = Preference('editorialNewStory', self.urls)
        self.urls.editorialPictures = Preference('editorialPictures', self.urls)
        self.urls.editorialPrefs = Preference('editorialPrefs', self.urls)
        self.urls.editorialShortcuts = Preference('editorialShortcuts', self.urls)
        self.urls.editorialStories = Preference('editorialStories', self.urls)
        self.urls.editorsOnlyMessage = Preference('editorsOnlyMessage', self.urls)
        self.urls.flipHomePage = Preference('flipHomePage', self.urls)
        self.urls.frontierLogo = Preference('frontierLogo', self.urls)
        self.urls.gemNewMessage = Preference('gemNewMessage', self.urls)
        self.urls.gemPostMessage = Preference('gemPostMessage', self.urls)
        self.urls.imageCowSkull = Preference('imageCowSkull', self.urls)
        self.urls.imageViewer = Preference('imageViewer', self.urls)
        self.urls.memberCheckMail = Preference('memberCheckMail', self.urls)
        self.urls.memberHome = Preference('memberHome', self.urls)
        self.urls.memberLogoff = Preference('memberLogoff', self.urls)
        self.urls.memberLogon = Preference('memberLogon', self.urls)
        self.urls.memberSignUp = Preference('memberSignUp', self.urls)
        self.urls.newsArchive = Preference('newsArchive', self.urls)
        self.urls.newsHome = Preference('newsHome', self.urls)
        self.urls.newsItemEditInBrowser = Preference('newsItemEditInBrowser', self.urls)
        self.urls.newsItemNew = Preference('newsItemNew', self.urls)
        self.urls.newsItemPostItem = Preference('newsItemPostItem', self.urls)
        self.urls.newsItemViewDepartment = Preference('newsItemViewDepartment', self.urls)
        self.urls.pictureEditInBrowser = Preference('pictureEditInBrowser', self.urls)
        self.urls.pictureHomePage = Preference('pictureHomePage', self.urls)
        self.urls.pictureNewMessage = Preference('pictureNewMessage', self.urls)
        self.urls.picturePostEditedMessage = Preference('picturePostEditedMessage', self.urls)
        self.urls.picturePostMessage = Preference('picturePostMessage', self.urls)
        self.urls.pictureReader = Preference('pictureReader', self.urls)
        self.urls.prefsHome = Preference('prefsHome', self.urls)
        self.urls.profilesHome = Preference('profilesHome', self.urls)
        self.urls.searchHome = Preference('searchHome', self.urls)
        self.urls.storyReader = Preference('storyReader', self.urls)

class Message:
    NotConnectedMessage = 'message is not connected to a site'
    
    def __init__(self, subject=None, body=None, bodyType="text/plain", inResponseTo=0, msgNum=None, parent=None):
        self.subject = subject
        self.body = body
        self.bodyType = bodyType
        self.inResponseTo = inResponseTo
        self.msgNum = msgNum
        self.parent = parent
        self.dirty = 1
        self.flServerAcceptsOpml = None
        self.postTime = None
        self.alsoListedIn = None
        self.member = None
        self.windowInfo = 0
        self.rendererInfo = {}
        self.flNewPostNotificationSent = None
        self.memberName = None
        self.url = None
        self.responses = None
        self.ctReads = None
        self.lastUpdate = None

    def _refresh(self, data):
        for key, value in data.items():
            if key == "msgnum":
                key = "msgNum"
            self.__dict__[key] = _un(value)
        self.dirty = 0
        if self.body:
            if self.body[:10] == '<newsItem>':
                self.__class__ = NewsItem
                self._get()
        
    def get(self):
        """get message from server"""
        if self.dirty:
            site = self.parent.site
            data = site.remote.manila.message.get( \
                site.username, site.password,
                site.siteName, self.msgNum)
            self._refresh(data)
        return self.body

    def __str__(self):
        return str(self.get())

    def delete(self):
        """delete message"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        rc = _unbool(site.remote.manila.message.delete( \
            site.username, site.password, site.siteName, self.msgNum))
        if rc:
            self.parent.dirty = 1
        return rc

    def set(self, subject=None, body=None, bodyType=None):
        """edit message"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        subject = subject or self.subject
        body = body or self.body
        bodyType = bodyType or self.bodyType
        windowInfo = getattr(self, "windowInfo", 0)
        rendererInfo = getattr(self, "rendererInfo", {})
        site = self.parent.site
        data = site.remote.manila.message.set( \
            site.username, site.password, site.siteName,
            self.msgNum, subject, body, bodyType,
            windowInfo, rendererInfo)
#        site.pingWeblogsCom()
        self._refresh(data)

    def checkIn(self):
        """check in message"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        return _unbool(site.remote.manila.message.checkIn( \
            site.username, site.password, site.siteName,
            self.msgNum))

    def checkOut(self):
        """check out message"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        return _unbool(site.remote.manila.message.checkOut( \
            site.username, site.password, site.siteName,
            self.msgNum))

    def isStory(self):
        """see if this message is a story"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        return self in self.parent.site.stories

    def isPicture(self):
        """see if this message is a picture"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        return self in self.parent.site.pictures

    def setIsStory(self, value):
        """set whether this message is a story"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        if value:
            self.parent.site.stories.append(self)
        else:
            self.parent.site.stories.remove(self.msgNum)

    def setIsPicture(self, value):
        """set whether this message is a picture"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        if value:
            self.parent.site.pictures.append(self)
        else:
            self.parent.site.pictures.remove(self.msgNum)

    def isHomePage(self):
        """see if this message is or has ever been a home page"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        return _unbool(site.remote.manila.message.isHomePage( \
            site.username, site.password, site.siteName,
            self.msgNum))
    
    def listResponses(self):
        """get list of messages that are responses to this message"""
        if not self.parent:
            raise NotConnectedError, self.NotConnectedMessage
        site = self.parent.site
        headers = site.remote.manila.message.getResponsesHeaders( \
            site.siteName, site.username, site.password,
            self.msgNum)
        msgNumList = []
        for v in headers.values():
            msgNumList.append(v["msgNum"])
        responses = []
        for m in self.parent:
            if m.msgNum in msgNumList:
                responses.append(m)
        return responses

    def attachPicture(self, source, mimeType="image/jpeg"):
        """attach picture to message
        
        source can be filename or any file-like object"""
        if hasattr(source, "read"):
            bits = source.read()
        else:
            fsock = open(source, 'rb')
            bits = fsock.read()
            fsock.close()
        site = self.parent.site
        url = site.remote.manila.message.attachPicture( \
            site.username, site.password, site.siteName,
            self.msgNum, mimeType, bits)
        if url:
            return self.setIsPicture(1)
        else:
            return 0

class xElement(minidom.Element):
    pass

class xSimple(xElement):
    def __init__(self, name, data):
        xElement.__init__(self, name)
        self.appendChild(minidom.Text(data))

class xNewsItem(xElement):
    def __init__(self, url, description, department):
        xElement.__init__(self, "newsItem")
        self.appendChild(xSimple("department", department))
        self.appendChild(xSimple("description", description))
        self.appendChild(xSimple("url", url))
        
class NewsItem(Message):
    def __init__(self, subject, url, description, department):
        Message.__init__(self)
        self.subject = subject
        self.newsItemURL = url
        self.description = description
        self.department = department
        self._set()
    
    def _gettext(self, doc, tag):

        nodes = []
        for node in doc.getElementsByTagName(tag)[0].childNodes:
            nodes.append(node.data)
        return str(string.join(nodes, ""))
    
    def _get(self):
        self.body = string.replace(self.body, "&", "&amp;")
        #print "Parsing: ", self.body
        xmldoc = minidom.parseString(self.body)
        self.department = self._unescape(self._gettext(xmldoc, "department"))
        self.description = self._unescape(self._gettext(xmldoc, "description"))
        self.newsItemURL = self._unescape(self._gettext(xmldoc, "url"))

    def _unescape(self, text):
        text = string.replace(text, '&lt;', '<')
        text = string.replace(text, '&gt;', '>')
        return text

    def _set(self):
        self.body = xNewsItem(self.newsItemURL, self.description, self.department).toxml()
        self.bodyType = "text/plain"

    def set(self, subject=None, url=None, description=None, department=None):
        if subject:
            self.subject = subject
        if url:
            self.newsItemURL = url
        if description:
            self.description = description
        if department:
            self.department = department
        self._set()
        site = self.parent.site
        rc = _unbool(site.remote.manila.newsItem.edit( \
            site.username, site.password, site.siteName,
            self.msgNum, self.subject, self.newsItemURL, self.description,
            self.department, 1))
#        if rc:
#            site.pingWeblogsCom()
        return rc

class MessageList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1

    def refresh(self):
        """re-read message list from server if necessary"""
        if self.dirty:
            count = self.site.remote.manila.message.count( \
                self.site.username, self.site.password, self.site.siteName)
            self.data = []
            for i in range(1, count+1):                
                self.data.append(Message(msgNum=i, parent=self))
            self.dirty = 0
    
    def append(self, message):
        """create new message (argument must be Message instance)"""
        if not isinstance(message, Message):
            raise TypeError, 'value must be Message instance'
        if isinstance(message, NewsItem):
            message.msgNum = self.site.remote.manila.newsItem.create( \
                self.site.username, self.site.password, self.site.siteName,
                message.subject, message.newsItemURL, message.description,
                message.department, 1)
        else:
            data = self.site.remote.manila.message.create( \
                self.site.username, self.site.password, self.site.siteName,
                message.subject, message.body, message.bodyType,
                message.inResponseTo, message.windowInfo, message.rendererInfo)
            message._refresh(data)
        message.parent = self
        self.refresh()
        self.data.append(message)
        if isinstance(message, NewsItem):
            message.dirty = 1
            message.get()
#        self.site.pingWeblogsCom()
        
    def __delitem__(self, i):
        self.refresh()
        self.data[i].delete()

    def remove(self, msgNum):
        """remove message by msgNum"""
        self.refresh()
        found = None
        for s in self:
            if s.msgNum == msgNum:
                found = s
                break
        if found:
            return found[0].delete()
#            self.site.pingWeblogsCom()
        else:
            raise ValueError, '%s not in list' % msgNum

    def index(self, msgNum):
        """get index of message by message number"""
        self.refresh()
        msgNumList = []
        for m in self:
            msgNumList.append(m.msgNum)
        return msgNumList.index(msgNum)

    def count(self, msgNum):
        """count occurrences of message number in messages list

        Since msgNum is unique within the message list, this is not that useful;
        it will always return 1 or 0"""
        self.refresh()
        msgNumList = []
        for m in self:
            msgNumList.append(m.msgNum)
        return msgNumList.count(msgNum)

    def listByDate(self, date):
        """get all messages from a certain date, as 9-tuple (YYYY, MM, DD, etc.)"""
        dateStr = "%s/%s/%s" % (date[1], date[2], date[0])
        headers = self.site.remote.manila.message.getHeaders( \
            self.site.siteName, self.site.username, self.site.password, dateStr)
        msgNums = []
        for v in headers.values():
            msgNums.append(v["msgNum"])
        returnList = []
        for m in self:
            if m.msgNum in msgNums:
                returnList.append(m)
        return returnList

    def listToday(self):
        """get today's messages"""
        headers = self.site.remote.manila.message.getCurrentHeaders( \
            self.site.siteName, self.site.username, self.site.password)
        for v in headers.values():
            msgNums.append(v["msgNum"])
        returnList = []
        for m in self:
            if m.msgNum in msgNums:
                returnList.append(m)
        return returnList

    def listThreads(self):
        """get current top-level threads in discussion forums"""
        headers = self.site.remote.manila.message.getCurrentThreads( \
            self.site.siteName, self.site.username, self.site.password)
        for v in headers.values():
            msgNums.append(v["msgNum"])
        returnList = []
        for m in self:
            if m.msgNum in msgNums:
                returnList.append(m)
        return returnList
    
    def undelete(self, msgNum):
        """undelete a message"""
        rc = _unbool(self.site.remote.manila.message.undelete( \
            self.site.username, self.site.password,
            self.site.siteName, msgNum))
        if rc:
            self.dirty = 1
#            self.site.pingWeblogsCom()
        return rc

class StoryList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1

    def refresh(self):
        """re-read stories list from server"""
        if self.dirty:
            rawdata = self.site.remote.manila.message.listStories( \
                self.site.username, self.site.password, self.site.siteName)
            msgNumList = []
            self.data = []
            for k in rawdata.keys():
                msgNumList.append(int(k))
            for m in self.site.messages:
                if m.msgNum in msgNumList:
                    self.data.append(m)
            self.dirty = 0

    def append(self, message):
        """make message a story"""
        if not isinstance(message, Message):
            raise TypeError, 'argument must be Message instance'
        self.refresh()
        if message in self:
            return 1
        rc = _unbool(self.site.remote.manila.message.addToStoriesList( \
            self.site.username, self.site.password, self.site.siteName,
            message.msgNum))
        if rc:
            self.data.append(message)
        return rc

    def remove(self, msgNum):
        """turn a story into a regular message"""
        self.refresh()
        msgNumList = []
        for m in self:
            msgNumList.append(m.msgNum)
        index = msgNumList.index(msgNum)
        rc = _unbool(self.site.remote.manila.message.removeFromStoriesList( \
            self.site.username, self.site.password, self.site.siteName,
            msgNum))
        if rc:
            del self.data[index]
        return rc
    
    def listByEditor(self, editor):
        """list stories written by an editor (argument can be Editor or email address)"""
        if isinstance(editor, Member):
            email = editor.email
        else:
            email = editor
        rawdata = self.site.remote.manila.editors.getStoriesHeaders( \
            self.site.siteName, email, self.site.username, self.site.password)
        msgNumList = []
        for v in rawdata.values():
            msgNumList.append(v["msgNum"])
        returnList = []
        for m in self:
            if m.msgNum in msgNumList:
                returnList.append(m)
        return returnList

class PictureList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.site = parent
        self.dirty = 1

    def refresh(self):
        """re-read picture list from server if necessary"""
        if self.dirty:
            rawdata = self.site.remote.manila.message.listPictures( \
                self.site.username, self.site.password, self.site.siteName)
            msgNumList = []
            for k in rawdata.keys():
                msgNumList.append(int(k))
            self.data = []
            for m in self.site.messages:
                if m.msgNum in msgNumList:
                    self.data.append(m)
            self.dirty = 0

    def append(self, message):
        """make message a picture"""
        if not isinstance(message, Message):
            raise TypeError, 'argument must be Message instance'
        self.refresh()
        msgNumList = []
        for m in self:
            msgNumList.append(m.msgNum)
        if message.msgNum in msgNumList:
            return 1
        rc = _unbool(self.site.remote.manila.message.addToPicturesList( \
            self.site.username, self.site.password, self.site.siteName,
            message.msgNum))
        if rc:
            self.data.append(message)
        return rc

    def remove(self, msgNum):
        """turn a picture into a regular message"""
        self.refresh()
        msgNumList = []
        for m in self:
            msgNumList.append(m.msgNum)
        index = msgNumList.index(msgNum)
        rc = _unbool(self.site.remote.manila.message.removeFromPicturesList( \
            self.site.username, self.site.password, self.site.siteName,
            msgNum))
        if rc:
            del self.data[index]
        return rc

class Day:
    def __init__(self, day, parent):
        self.value = day
        self.parent = parent
        self.month = parent.month
        self.year = parent.year
        self.site = parent.site

    def listMessages(self):
        """get list of messages from this day"""
        return self.site.messages.listByDate((self.year, self.month, self.value, 0, 0, 0, 0, 0, 0))

class DayList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.parent = parent
        self.site = parent.site
        self.month = parent.value
        self.year = parent.year
        self.dirty = 1

    def refresh(self):
        """re-read day list from server if necessary"""
        if self.dirty:
            rawdata = self.site.remote.manila.message.calendar.getDaysInMonth( \
                self.site.username, self.site.password, self.site.siteName,
                self.year, self.month)
            self.data = []
            for d in rawdata:
                self.data.append(Day(int(d), self))
            self.dirty = 0
        
class Month:
    def __init__(self, month, parent):
        self.value = month
        self.parent = parent
        self.year = parent.year
        self.site = parent.site
        self.days = DayList(self)

    def listMessages(self):
        """get list of messages from this month"""
        rc = []
        for d in self.days:
            rc.extend(d.listMessages())
        return rc

class MonthList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.parent = parent
        self.site = parent.site
        self.year = parent.value
        self.dirty = 1

    def refresh(self):
        """re-read month list from server if necessary"""
        if self.dirty:
            rawdata = self.site.remote.manila.message.calendar.getMonthsInYear( \
                self.site.username, self.site.password, self.site.siteName,
                self.year)
            self.data = []
            for m in rawdata:
                self.data.append(Month(int(m), self))
            self.dirty = 0

class Year:
    def __init__(self, year, parent):
        self.value = year
        self.parent = parent
        self.site = parent.site
        self.months = MonthList(self)
        
    def listMessages(self):
        """get list of messages from this year"""
        rc = []
        for m in self.months:
            rc.extend(m.listMessages())
        return rc

class YearList(RefreshList):
    def __init__(self, parent):
        RefreshList.__init__(self)
        self.parent = parent
        self.site = parent.site
        self.dirty = 1

    def refresh(self):
        """re-read year list from server if necessary"""
        if self.dirty:
            first = _undate(self.site.remote.manila.message.calendar.getFirstDate( \
                self.site.username, self.site.password, self.site.siteName))
            last = _undate(self.site.remote.manila.message.calendar.getLastDate( \
                self.site.username, self.site.password, self.site.siteName))
            self.data = []
            for y in range(first[0], last[0]+1):
                self.data.append(Year(y, self))
            self.dirty = 0

class Calendar:
    def __init__(self, parent):
        self.site = parent
        self.years = YearList(self)

class HomePage:
    def __init__(self, parent):
        self.site = parent

    def flip(self):
        """flip home page"""
        rc = _unbool(self.site.remote.manila.homepage.flip( \
            self.site.username, self.site.password, self.site.siteName))
        if rc:
            self.site.pingWeblogsCom()
        return rc

    def _getCurrentMessage(self):
        msgNum = self.site.remote.manila.homepage.getMsgNum( \
            self.site.username, self.site.password, self.site.siteName)
        return self.site.messages[self.site.messages.index(msgNum)]

    def get(self):
        """get message that is the current home page"""
        data = self.site.remote.manila.homepage.getCurrentContent( \
            self.site.siteName)
        if self.site.loggedIn():
            message = self.site.messages[self.site.messages.index(data["msgnum"])]
        else:
            message = Message()
        message._refresh(data)
        message.dirty = 0
        return message

    def addText(self, text):
        """add text to the message that is the current home page"""
        rc = _unbool(self.site.remote.manila.homepage.addToHomePage( \
            self.site.username, self.site.password, self.site.url,
            text))
        if rc:
            message = self._getCurrentMessage()
            message.dirty = 1
            self.site.pingWeblogsCom()
        return rc

    def set(self, message):
        """set home page to an existing message (can be Message instance or message number)"""
        if isinstance(message, Message):
            msgNum = message.msgNum
        else:
            msgNum = message
        rc = _unbool(self.site.remote.manila.homepage.setMsgNum( \
            self.site.username, self.site.password, self.site.siteName,
            msgNum))
        if rc:
            self.site.pingWeblogsCom()
        return rc
    
