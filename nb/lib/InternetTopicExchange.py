"""
Internet Topic Exchange Client

Python bindings to the Internet Topic Exchange XML-RPC and POST APIs.

Requires the xmlrpclib library.
"""

__author__ = "Leonard Richardson (leonardr@segfault.org)"
__version__ = "$Revision$"
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2003 Leonard Richardson"
__license__ = "Public Domain"

import xmlrpclib
import urllib

TOPIC_EXCHANGE_URL = 'http://topicexchange.com/RPC2'
TOPIC_EXCHANGE_NEW_CHANNEL_URL = 'http://topicexchange.com/new'

class Client:

    def __init__(self, url=TOPIC_EXCHANGE_URL,
                 newChannelURL=TOPIC_EXCHANGE_NEW_CHANNEL_URL):
        self.server = xmlrpclib.Server(url)
        self.newChannelURL = newChannelURL

    def newChannel(self, catname, email):
        """Creates a new ITE channel by making a POST request to the
        appropriate URL."""
        data = urllib.urlencode({'catname': catname, 'email': email})
        return urllib.urlopen(self.newChannelURL, data)

    def ping(self, topicName, blog_name, title, url, excerpt, autoMunge=0):
        """Pings an ITE topic, the required information being
        specified as arguments to the method."""
        details = { 'blog_name' : blog_name,
                    'title' : title,
                    'url' : url,
                    'excerpt' : excerpt,
                    'autoMunge' : autoMunge }
        self.pingStruct(topicName, details)

    def pingStruct(self, topicName, details):
        """Pings an ITE topic, the required information being provided
        in a dictionary."""
        return self.server.topicExchange.ping(topicName, details)

    def getChannels(self):
        """Returns a mapping of ITE channel names to structs
        containing Trackback/post display URLs."""
        return self.server.topicExchange.getChannels()

if __name__ == '__main__':
    client = Client()
    print client.getChannels()
    print client.newChannel('test', 'leonardr@segfault.org').read()
    print client.pingITE('test', 'ITE Python client',
                         'Python client self-test', 'N/A',
                         'This is just the self-test of the ITE Python client. Even by the standards of the "test" topic, it\'s not that exciting.')
