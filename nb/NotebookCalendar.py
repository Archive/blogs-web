import calendar

from HtmlGenerator import HtmlGenerator
from EntryID import EntryID
from const import ALL
import lib.DateArithmetic
import util

class NotebookCalendar(HtmlGenerator):
    """A class containing logic for printing out a calendar of links or
    summary of entries for a notebook."""

    COLOR_LINKS = 0

    def __init__(self, notebook):
        self.notebook = notebook

    def printYear(self, year):
        print '<table>'
        for month in range(1,13):            
            if month % 3 == 1 and month > 1:
                print '</tr>'
            if month % 3 == 1:
                print '<tr>'
            print '<td>'
            self.printMonth(year, month)
            print '</td>'

    def printMonth(self, year, month, dayGuides=1, displayYear=0,
                   displayMonth=1, dayCGI=None, monthCGI=None, yearCGI=None,
                   monthLinks=0):
        """Prints out the calendar for one month, including links to the
         given CGI for each day that has entries."""    
        print self.getMonth(year, month, dayGuides, displayYear, displayMonth,
                       dayCGI, monthCGI, yearCGI, monthLinks)        

    def getMonth(self, year, month, dayGuides=1, displayYear=0,
                 displayMonth=1, dayCGI=None, monthCGI=None, yearCGI=None,
                 monthLinks=0):
        if not dayCGI:
            from CoreCGIs import ViewCGI
            dayCGI = ViewCGI.NAME
        if not monthCGI:
            from CoreCGIs import ViewCGI
            monthCGI = ViewCGI.NAME
        if not yearCGI:
            from CoreCGIs import CalendarCGI
            yearCGI = CalendarCGI.NAME            
        (year, month) = map(int, (year, month))
        if dayGuides:
            imgURL = self.notebook.getDayGuideImageURL()
        weeks = calendar.monthcalendar(year, month)
        entryID = EntryID(self.notebook, year, month)
        ids = entryID.getIDs()
        numIds = len(ids)
        i = 0        
        s = '<table>'
        s = s + '<tr><td colspan="7" class="calendarMonthName">'
        if displayYear and displayMonth:
            s = s + self.getLink(self.notebook.getURL(yearCGI, pathInfo=year), year) + ' '
        if displayMonth:
            monthPathInfo = '%s/%s' % (year,month)
            s = s + self.getLink(self.notebook.getURL(monthCGI,
                                                      pathInfo=monthPathInfo),
                                 calendar.month_name[month])
        s = s + '</td></tr><tr>'
        for abbr in lib.DateArithmetic.DAY_ABBR:
            s = s + '<th class="calendarDayOfWeek">%s</th>' % abbr
        s = s + '</tr>'
        for week in weeks:
            s = s + '<tr>'
            for day in week:
                s = s + '<td class="calendarDate">'

                if day == 0:
                    s = s + '&nbsp;'
                else:
                    itemsForDay = 1
                    while i < numIds and int(ids[i].day) < day:
                        i = i + 1
                    if i < numIds and int(ids[i].day) == day:
                        url = ids[i].getDayID().asLink(dayCGI)
                        ids[i].ordinal = ALL
                        while i <numIds and int(ids[i].day) == day:
                            i = i + 1                            
                            itemsForDay = itemsForDay + 1
                        style = ''
                        if self.COLOR_LINKS:
                            style = "color:%s" % self.getColor(itemsForDay)
                        s = s + self.getLink(url, day, style=style)
                        if dayGuides:
                            if itemsForDay > 10:
                                width = 2 + (itemsForDay / 5)
                                height = 10
                            else:
                                width = 2
                                height = itemsForDay
                            #I tried to do this with a <span>, but couldn't
                            #figure out how.
                            #s = s + '<span style="border-left-width:%spx; border-top-width:%spx; border-style:solid; border-color:ccccff"></span>' % (width, height)
                            s = s + '<img src="%s" width="%s" height="%s" alt="%s" />' % (imgURL, width, height, itemsForDay)
                            
                    else:
                        s = s + str(day)
                s = s + '</td>'
            s = s + '</tr>'
        if monthLinks:
            s = s + '<tr><td colspan="7">'
            prev = entryID.previousMonth()
            if prev is not None:
                dayPathInfo = '%s/%02d' % (prev[0], prev[1])
                s = s + self.getLink(self.notebook.getURL(monthCGI,
                                                          pathInfo=dayPathInfo),
                                     "&#171;" + calendar.month_name[prev[1]])
            else:
                s = s + calendar.month_name[
                    lib.DateArithmetic.previousMonth((year, month))[1]]
            next = entryID.nextMonth()
            if next is not None:
                dayPathInfo = '%s/%02d' % (next[0], next[1])
                s = s + self.getLink(self.notebook.getURL(monthCGI,
                                                          pathInfo=dayPathInfo),
                                     calendar.month_name[next[1]] + '&#187;')
            else:
                s = s + calendar.month_name[
                    lib.DateArithmetic.nextMonth((year, month))[1]]
            s = s + '</td></tr>'
        s = s + '</table>'
        return s

    def getColor(self, items):        
        baseColor = "d000%x0"
        if items > 7:
            items = 0
        else:
            items = 15-(items*2)
        return baseColor % items
    
