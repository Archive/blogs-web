import string
import sys
import types

import const
import CommandMaps
import lib.DateArithmetic

def registerPreEntryHook(hook, notebookMethod=None):
    Viewer.preEntryHooks.append((hook, notebookMethod))

class Viewer:

    preEntryHooks = []

    def __init__(self, notebook, entries, chronological=1, edit=0, detail=0,
                 entryHeader=''):
        """Creates a viewer container for a notebook with some
        entries, to be displayed chronologically or otherwise."""
        self.entryID = None
        self.notebook = notebook
        self.detail = detail
        if type(entries) == types.ListType or type(entries) == types.NoneType:
            self.entries = entries
        else:
            self.entries = [entries]
        self.edit = edit
        self.entryHeader = entryHeader
        self.entryCursor = 0
        if entries and not chronological:
            entries.reverse()        
        if self.notebook:
            gran = self.notebook.getMinimumTimespanGranularity()
        else:
            gran = const.ENTRY
        self.minimumGranularity = gran
        self.granularity = [const.ENTRY]
            
    def render(self, out=sys.stdout):
        """Renders this Viewer's entries using the options associated
        with this Viewer."""
        #time1 = time.time()
        self.out = out
        self.entryCursor = 0
        self.notebook.interpolateTemplate('entry-list',
                                          CommandMaps.ENTRY_LIST_COMMAND_MAP,
                                          self, self, out=out)
        #time2 = time.time()
        #print "That took %s ms" % (time2-time1)

    def interpolateEntryTemplate(self):
        """Interpolates the entry template for each of the entries associated
        with this viewer."""
        if not self.entries:
            self.out.write(self.notebook.getNoEntriesMessage())
        else:
            activePreEntryHooks = []
            for (method, notebookMethodName) in self.preEntryHooks:
                if not notebookMethodName or getattr(self.notebook, notebookMethodName)():
                    activePreEntryHooks.append(method)
                
            self.alternate = 0
            timespanBroken = 0
            numEntries = len(self.entries)
            if self.granularity[-1] == const.ENTRY:
                for i in self.entries:
                    for hook in activePreEntryHooks:
                        hook(self, i)
                    i.render(self, out=self.out)
                    self.entryCursor = self.entryCursor + 1
                    self.alternate = not self.alternate
            else:
                for i in range(self.entryCursor, numEntries):
                    for hook in activePreEntryHooks:
                        hook(self, self.entries[i])
                    self.entryCursor = i
                    self.entries[i].render(self, out=self.out)
                    if i < numEntries - 1:
                        this = self.entries[i]
                        next = self.entries[i+1]
                        if not self.entriesInSameTimespan(this, next):
                            recent = self.granularity.pop()
                            while not self.entriesInSameTimespan(this, next):
                                recent = self.granularity.pop()
                            self.granularity.append(recent)
                            self.entryCursor = i + 1
                            timespanBroken = 1                    
                            break;
                    self.alternate = not self.alternate            

            if not timespanBroken:
                self.entryCursor = len(self.entries)

    def interpolateTimeframeTemplate(self):
        """Interpolates the timeframe template corresponding to the given
        timeframe (which is the current command)"""
        if self.TTM_directive == 'Y':
            self.granularity.append(const.YEAR)
        elif self.TTM_directive == 'M':
            self.granularity.append(const.MONTH)
        elif self.TTM_directive == 'D':
            self.granularity.append(const.DAY)

        #See if we can get a smaller granularity
        if self.entryCursor == 0:
            smallerPossible = 1
            while smallerPossible and const.GRANULARITIES.index(self.granularity[-1]) > self.minimumGranularity:
                if self.entries and self.entriesInSameTimespan(self.entries[0], self.entries[-1]):
                    self.granularity.append(const.GRANULARITIES[const.GRANULARITIES.index(self.granularity[-1])-1])
                else:
                    smallerPossible = 0

        oldGranularity = self.granularity[-1]            
        if not self.entries:
            self.out.write(self.notebook.getNoEntriesMessage())
        while self.entryCursor < len(self.entries) and self.granularity[-1] == oldGranularity:
            if self.granularity[-1] == const.ENTRY:
                self.interpolateEntryTemplate()
            else:
                #TODO: check to make sure the template will bottom out.
                self.notebook.interpolateTemplate(self.granularity[-1] + '-span',
                                                  CommandMaps.TIMESPAN_COMMAND_MAPS[self.granularity[-1]],
                                                  self, self, out=self.out)

    def getCurrentEntryTime(self):
        return self.entries[self.entryCursor].getDateInFormat('%' + self.TTM_directive)

    def getCurrentEntryDayName(self):
        name = lib.DateArithmetic.DAY_ORDINAL_NAMES[int(self.entries[self.entryCursor].getDateInFormat('%d'))]
        if self.TTM_directive == 'o':
            name = string.lower(name)
        return name

    def getCurrentEntryDayOrdinal(self):
        return lib.DateArithmetic.dayOrdinal(int(self.entries[self.entryCursor].getDateInFormat('%d')))

    def getCurrentDayURL(self):
        return self._getCurrentEntryURL('getDayURL')

    def getCurrentMonthURL(self):
        return self._getCurrentEntryURL('getMonthURL')

    def getCurrentYearURL(self):
        return self._getCurrentEntryURL('getMonthURL')

    def _getCurrentEntryURL(self, entryMethod, cgi=None):
        if not cgi:
            from CoreCGIs import ViewCGI
            cgi = ViewCGI.NAME
        return getattr(self.entries[self.entryCursor], entryMethod)(cgi)

    def entriesInSameTimespan(self, entry1, entry2):
        if entry1 == entry2 or self.granularity[-1] == const.ENTRY:
            return 1
        format = const.GRANULARITY_DATE_FORMATS[self.granularity[-1]]
        return entry1.getDateInFormat(format) == entry2.getDateInFormat(format)
        
