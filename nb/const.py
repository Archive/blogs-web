#Granularity constants
NOTEBOOK='notebook'
YEAR='year'
MONTH='month'
DAY='day'
ENTRY='entry'
ORDINAL='entry'

GRANULARITIES = [ENTRY, DAY, MONTH, YEAR, NOTEBOOK]

GRANULARITY_DATE_FORMATS = { YEAR : '%Y',
                             MONTH : '%Y%m',
                             DAY : '%Y%m%d' }

#Filename constants
CONFIG_LOCATION = 'config.location'
DEFAULT_NEWSBRUISER_DATA_DIRECTORY = 'NewsBruiserData'
DEFAULT_NEWSBRUISER_CONFIG_FILE_NAME = '.nbrc'

#Configuration constants and defaults for notebook setup
NAME_CONFIG_KEY = 'name'
ORDINAL_CONFIG_KEY = 'ordinal'
ORDINAL_DEFAULT = '0'
PASSWORD_CONFIG_KEY = 'password'
WORKING_DIR_CONFIG_KEY = 'working-dir'

#CGI key constants
AUTHOR_CGI_KEY = 'author'
AUTHOR_EMAIL_CGI_KEY = 'email'
ENTRY_CGI_KEY = 'entry'
TITLE_CGI_KEY = 'title'
MAKE_CHANGE_CGI_KEY = 'make_change'
PASSWORD_CGI_KEY = 'password'
QUERY_CGI_KEY = 'q'

MASTER_PASSWORD_CGI_KEY = 'mp'
PASSWORD_VERIFY_CGI_KEY = 'pwVerify'

NOTEBOOK_NAME_CGI_KEY = 'notebookName'
NOTEBOOK_ORDINAL_CGI_KEY = 'ordinal'

NUMBER_OF_ATTACHMENTS_CGI_KEY = 'numUploads'
ATTACHMENT_CGI_KEY_PREFIX = 'attachment'

REFERER_CGI_KEY = 'referer'

NOTEBOOK_CGI_KEY = 'notebook'
YEAR_CGI_KEY = 'year'
MONTH_CGI_KEY = 'month'
DAY_CGI_KEY = 'day'
ORDINAL_CGI_KEY = 'ordinal'
DATE_CGI_KEYS = [YEAR_CGI_KEY, MONTH_CGI_KEY, DAY_CGI_KEY, ORDINAL_CGI_KEY]

ALL = '[0-9]*'

#Entry file constants
ENTRY_SECTION='entry'
ENTRY_AUTHOR_KEY='author'
ENTRY_AUTHOR_EMAIL_KEY='authorEmail'
ENTRY_TIME_KEY='time'
ENTRY_MODIFIED_KEY='modifiedTime'
ENTRY_TITLE_KEY='title'
ENTRY_TEXT_KEY='text'

#Sort order constants
CHRONOLOGICAL_SORT_ORDER = 'chronological'
REVERSE_CHRONOLOGICAL_SORT_ORDER = 'reverse-chronological'

#Error constants
AmbiguousIDError = "ID denotes multiple entries."

#Mode creation constants
DIR_MODE = 0775
FILE_MODE = 0664

#You should not change any of the constants below.

#This is the format in which data files are STORED. It has nothing to
#do with how the data files are displayed (that is viewer-dependant).
STORAGE_TIME_FORMAT = '%Y/%m/%d %H:%M:%S'

#This is the format in which the publication times of notebook entries
#are exported to RSS.
SHORT_W3CDTF_TIME_FORMAT = '%Y-%m-%d' 
RFC822_TIME_FORMAT_NO_TZ = '%a, %d %b %Y %H:%M:%S' 
RFC822_TIME_FORMAT = RFC822_TIME_FORMAT_NO_TZ + ' GMT' 
LONG_W3CDTF_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S+00:00'
