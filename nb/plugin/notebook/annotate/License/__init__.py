import copy
import re
import string
import urlparse

from nb import CommandMaps, Options
from nb.Notebook import Notebook
from nb.Entry import Entry
from nb.lib.IWantOptions import SelectionOptionWithTextFields
from nb.lib import cclib

DEFAULT_CC_LICENSES = ['by-nc-sa', 'by-nc-nd', 'by-sa', 'publicdomain']
CC_LICENSE_URL_PREFIX = 'http://creativecommons.org/licenses/'
CC_LICENSE_URL_SCHEMA = CC_LICENSE_URL_PREFIX + '%s/2.0/'
OTHER = 'other'
NONE = 'none'

def _getLicenseOption(self):
    return self.optionConfig.getOption('license')
Notebook._getLicenseOption = _getLicenseOption

def isLicensed(self):
    """Returns true iff this notebook has any specific license at all."""
    return self._getLicenseOption().getValue(self) != NONE
Notebook.isLicensed = isLicensed

def isCreativeCommonsLicensed(self):
    option = self._getLicenseOption()
    value = option.getValue(self)
    text = option.getTextBoxValue(self)
    return value in DEFAULT_CC_LICENSES or (value == OTHER and string.find(text, CC_LICENSE_URL_PREFIX) == 0)
Notebook.isCreativeCommonsLicensed = isCreativeCommonsLicensed

def getLicenseURL(self):
    url = None
    if self.isCreativeCommonsLicensed():
        option = self._getLicenseOption()
        value = option.getValue(self)
        if value in DEFAULT_CC_LICENSES:
            url = CC_LICENSE_URL_SCHEMA % value
    if not url:
        custom = self.getLicenseCustomText()
        check = urlparse.urlparse(custom)
        if check[0] and check[1]:
            url = custom
    return url
Notebook.getLicenseURL = getLicenseURL

def getLicenseCustomText(self):
    option = self.optionConfig.getOption('license') 
    return option.getTextBoxValue(self)
Notebook.getLicenseCustomText = getLicenseCustomText

def _getMetadata(self, notebook, url, title, authors,
                format='text/html', licenseURL=None, licenseRDF=None):
    """Returns RDF metadata for something that has a
    license. Taken from example Creative Commons code."""
    meta = ''
    if notebook.isCreativeCommonsLicensed():
        if not licenseURL:
            licenseURL = notebook.getLicenseURL()
        if not licenseRDF:
            licenseRDF = notebook.getLicenseDescriptionRDF()
        meta = '<rdf:RDF xmlns="http://web.resource.org/cc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">\n'
        meta = meta + '<Work rdf:about="%s">\n' % url
        meta = meta + '<dc:title>%s</dc:title>\n' % title
        if authors:
            meta = meta + '<dc:creator><Agent>'
            for author in authors:
                meta = meta + '<dc:title>%s</dc:title>' % author
            meta = meta + '</Agent></dc:creator>\n'
        meta = meta + '<dc:rights><Agent><dc:title>%s</dc:title></Agent></dc:rights>\n' % title
        meta = meta + '<dc:format>%s</dc:format>\n' % format
        meta = meta + '<license rdf:resource=%s/>\n' % licenseURL
        meta = meta + '</Work>\n'
        meta = meta + licenseRDF
        meta = meta + '</rdf:RDF>\n'
    return meta
Entry._getMetadata = _getMetadata
Notebook._getMetadata = _getMetadata

def getLicenseDescriptionRDF(self):
    rdf = ''
    if self.isCreativeCommonsLicensed():
        rdf = '<License rdf:about="%s">\n' % self.getLicenseURL()
        rdf = rdf + cclib.License(self.getLicenseURL()).getDetailsRDF()
        rdf = rdf + '</License>'
    return rdf
Notebook.getLicenseDescriptionRDF = getLicenseDescriptionRDF

def getNotebookLicenseMetadata(self):
    return self._getMetadata(self, self.getExportedSiteURL(),
                            self.getTitle(), self.getAuthors())
Notebook.getLicenseMetadata = getNotebookLicenseMetadata

def getEntryLicenseMetadata(self):
    return self._getMetadata(self.notebook, self.getAbsoluteURL(),
                            self.sample(), self.getAuthor())
Entry.getLicenseMetadata = getEntryLicenseMetadata

#Syndication hooks

from nb.plugin.entry.render import syndication
from nb.plugin.entry.render.syndication import Syndicator
from nb.plugin.entry.render.syndication.RSS20 import RSS20Syndicator

#An RSS 2.0 syndication hook.
key = str(RSS20Syndicator.VERSION)
def RSS20HeadHook(self, notebook, entries):
    if notebook.isCreativeCommonsLicensed():
        return 'xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule"'
syndication.registerHook(key, Syndicator.HEAD, RSS20HeadHook)

def RSS20ChannelHook(self, notebook, entries):
    if notebook.isCreativeCommonsLicensed():
        return '<creativeCommons:license>%s</creativeCommons:license>' % notebook.getLicenseURL()
syndication.registerHook(key, Syndicator.CHANNEL, RSS20ChannelHook)

#Some RSS 1.0 syndication hooks.
from nb.plugin.entry.render.syndication.RSS10 import RSS10Syndicator
key = str(RSS10Syndicator.VERSION)
def RSS10HeadHook(self, notebook, entries):
    if notebook.isCreativeCommonsLicensed():
        return 'xmlns:cc="http://web.resource.org/cc/"'
syndication.registerHook(key, Syndicator.HEAD, RSS10HeadHook)

def RSS10ChannelHook(self, notebook, entries):
    if notebook.isCreativeCommonsLicensed():
        return '<cc:license rdf:resource="%s" />' % notebook.getLicenseURL()
syndication.registerHook(key, Syndicator.CHANNEL, RSS10ChannelHook)

#TODO: License capabilities in the RSS 10 feed, once we support
#license capabilities.
    
#Template system hooks

def getLicenseURLOrData(self, viewer):
    """If this is a Creative Commons license, prints the
    URL. Otherwise, prints any custom license text or link."""
    if self.isCreativeCommonsLicensed():
        return self.getLicenseURL()
    else:
        url = self.getLicenseURL()        
        if url:
            text = _('(<a href="%s">License info</a>)') % url
        else:
            text = self.getLicenseCustomText()
        return text
Notebook.getLicenseURLOrData = getLicenseURLOrData
        
def getLicenseText(self, viewer):
    template = None
    cc = self.isCreativeCommonsLicensed()
    if self.isLicensed():
        template = 'cc-license-text'    
        if not cc:
            template = 'non-' + template
    if template:
        self.interpolateTemplate(template, LICENSE_TEXT_TEMPLATE_COMMAND_MAP,
                                 viewer, viewer, out=viewer.out)
        if cc:
            return '<!--%s-->' % self.getLicenseMetadata()

Notebook.getLicenseText = getLicenseText

def getLicenseInformation(self):
    t = ''
    if self.isCreativeCommonsLicensed():
        t = self.getCreativeCommonsLicenseInformation()
    else:
        t = self.getNonCreativeCommonsLicenseInformation()
Notebook.getLicenseInformation = getLicenseInformation

LICENSE_TEXT_TEMPLATE_COMMAND_MAP = copy.copy(CommandMaps.BASIC_COMMAND_MAP)
LICENSE_TEXT_TEMPLATE_COMMAND_MAP['notebook-licenseData'] = 'getLicenseURLOrData'
for map in CommandMaps.MASTER_HEADER_TEMPLATE_COMMAND_MAP, CommandMaps.MASTER_FOOTER_TEMPLATE_COMMAND_MAP:
    map['notebook-licenseText'] = 'getLicenseText'
