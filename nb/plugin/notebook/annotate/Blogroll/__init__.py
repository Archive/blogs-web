"""A plugin for keeping a blogroll or list of links for a notebook."""

import string

from nb.lib.IWantOptions import LongStringOption
from nb import CommandMaps
from nb import Options
from nb.Notebook import Notebook

#Some Notebook methods for grabbing the blogroll.
def getBlogroll(self):
    return self.optionConfig.getOption('blogroll').getEntries(self)
Notebook.getBlogroll = getBlogroll

def getBlogrollPanel(self, viewer):
    """Interpolates the blogroll panel, if there is anything on
    the blogroll."""
    if self.getBlogroll():
        self.interpolateTemplate('panel-blogroll',
                                 CommandMaps.BASIC_COMMAND_MAP,
                                 viewer, viewer, out=viewer.out)
Notebook.getBlogrollPanel = getBlogrollPanel

def getBlogrollLinks(self, viewer):
    """Interpolates the blogroll link panel for each blogroll link."""
    for i in self.getBlogroll():
        self.interpolateTemplate('blogroll-link',
                                 CommandMaps.ARCHIVE_LINK_COMMAND_MAP,
                                 i, viewer)
Notebook.getBlogrollLinks = getBlogrollLinks


class BlogrollEntry:

    """A simple class containing information about a blogroll entry."""

    def __init__(self, url, name, description, feed):
        self.url = url
        self.name = name
        self.description = description
        self.feed = feed

    def getViewLink(self):
        title = ''
        if self.description:
            title = ' title="%s"' % self.description
        feed = ''
        name = self.name
        if not name:
            name = self.url
        if self.feed:
            if self.name:
                buttonTitle = ' title="%s"' % (_("Syndication feed for %s") % self.name)
            feed = ' <a class="XMLSyndicationButton" %shref="%s">%s</a>' % (buttonTitle, self.feed, _('XML'))
        return '<a class="blogrollLink" href="%s"%s>%s</a> %s' % (self.url,
                                                                  title,
                                                                  name,
                                                                  feed)

class BlogrollOption(LongStringOption):

    """An option that controls a notebook's blogroll."""

    def validate(self, context, value, formValues):
        errors = []
        for i in string.split(value, '\n'):
            if i:
                weblog = self._parseLine(i)
                try:
                    self._validate(weblog)
                except Exception, msg:
                    args = {'value' : i,
                            'error' : msg}
                    errors.append(_('Invalid blogroll entry "%(value)s" (%(error)s)') % args)
        return errors

    def getEntries(self, context, override=None):
        weblogs = []
        value = LongStringOption.getValue(self, context, override)
        for line in string.split(value, '\n'):
            if line:
                weblogs.append(apply(BlogrollEntry, self._parseLine(line)))
        return weblogs

    def _validate(self, weblog):
        if not weblog[0]:
            raise Exception, _('No weblog address specified.')
    
    def _parseLine(self, line):
        url = None
        name = None
        description = None
        feed = None
        tup = string.split(line, '|')
        url = tup[0]
        if len(tup) > 1:
            name = tup[1]
        if len(tup) > 2:
            description = tup[2]
        if len(tup) > 3:
            feed = tup[3]
        return (url, name, description, feed)
Options.BlogrollOption = BlogrollOption
