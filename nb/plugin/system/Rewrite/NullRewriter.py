import URLRewriter
class NullRewriter(URLRewriter.URLRewriter):
  def __init__(self, baseURL):
      self.baseURL = self.baseCGIURL = baseURL
  def rewrite(self, url):
      return self.baseCGIURL + url
