class URLRewriter:

    """A class that rewrites NewsBruiser URLs."""

    def __init__(self, baseURL):
        self.baseCGIURL = baseURL + 'nb.cgi/'

    def rewrite(self, url):
        """Takes a URL and returns the rewritten URL."""
        return self.baseCGIURL + url

    def getCookiePath(self):
        return self.baseCGIURL

class URLShorteningRewriter(URLRewriter):

    """This URLRewriter chops off the base URL and, if applicable,
    view.cgi.  This allows you to access NewsBruiser entries as
    http://www.domain.com/[notebook]/[entry].

    To get this to work you need to set up the following Apache rewrite
    rules:

     RewriteRule ^/?([^/]*.cgi.*)$ [baseURL]$1 [L]
     RewriteRule ^/?([^/]+/([0-9]+/*)+)$ [baseURL]view.cgi/$1 [L]
    """

    def __init__(self, baseURL):
        self.baseURL=baseURL
        self.vars = []
        self.lens = {}
        self.vars.append(self.baseURL[:-1])
        self.vars.append('/view.cgi')
        for i in self.vars:
            self.lens[i] = len(i)

    def rewrite(self, url):
        for i in self.vars:
            if url[:self.lens[i]] == i:
                url = url[self.lens[i]:]
        if not url:
            url = '/'
        return url


