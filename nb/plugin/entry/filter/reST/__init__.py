"""This plugin adds an entry text filter which turns reST-formatted
text into HTML. It only operates if the plugin-entry-filter-rest
option is selected, and if the text currently contains no HTML tags."""

import re
import string
from nb import Entry

try:
    from docutils.core import publish_string
    from docutils.writers import html4css1
    installed = 1
except ImportError:
    installed = 0

if installed:
    class EntryHTMLTranslator(html4css1.HTMLTranslator):
        def astext(self):
            return string.join(self.body, "")

        def visit_document(self, node):
            pass

        def depart_document(self, node):
            pass

    reST_htmlTagRE = re.compile('<[^>]*>')

    def reST_filter(self, text):
        import sys
        if self.notebook.getOptionValue('plugin-entry-filter-rest') and not reST_htmlTagRE.search(text):
            writer = html4css1.Writer()
            writer.translator_class = EntryHTMLTranslator        
            text = publish_string(text, writer=writer)
        return text

    Entry.registerFilter('text', reST_filter)
