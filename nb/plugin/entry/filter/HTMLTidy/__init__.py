"""This plugin adds an entry text filter which runs HTMLTidy on entry
text to turn it into valid HTML. It operates only if the
plugin-entry-filter-htmltidy option is selected."""

from nb import Entry
from nb.lib import HTMLTidy

def HTMLTidy_filter(self, text):
    if self.notebook.getOptionValue('plugin-entry-filter-htmltidy'):
        text = HTMLTidy.htmlTidy(text)
    return text

Entry.registerFilter('text', HTMLTidy_filter)
