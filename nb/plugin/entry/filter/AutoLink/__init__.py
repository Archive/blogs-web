"""This plugin adds an entry text filter which turns URLs into links
if they are bare URLs. It only operates if the
plugin-entry-filter-autolink option is selected.

NOTE: The given RE can get "maximum recursion limit exceeded" when run
on a very long string which contains URLs. Help needed. Split it up
into smaller strings?"""

import re
from nb import Entry

# This regular expression taken from ViewCVS: see ViewCVS license at bottom.
AutoLink_common = "-a-zA-Z0-9%.~:_"
AutoLink_urlRE = "((https?|ftp|gopher|file)://[%s/]+([?&][%s]+(=[%s]+)?)*(#[%s]+)?)" % ((AutoLink_common,) * 4)
AutoLink_urlRE = re.compile(r"^(.*?)%s(.*)$" % AutoLink_urlRE, re.S)


# This one wasn't
AutoLink_hrefRE = re.compile(r"""\s*href\s*=\s*["']$""", re.S) # ' doh emacs!

def AutoLink_link(text):
    """Automatically link unlinked URLs in HTMLish text.  Multiple
    invocations should not change the result at all."""
    out = ""
    while text:
        m = AutoLink_urlRE.search(text)
        if m:
            before, url, after = m.group(1, 2, len(m.groups()))
            if AutoLink_hrefRE.search(before) or after[:4] == "</a>":
                out = out + before + url
            else:
                out = out + before + '<a href="%s">%s</a>' % (url, url)
            text = after
        else:
            out = out + text
            text = ""
    return out
            
def AutoLink_filter(self, text):
    if self.notebook.getOptionValue('plugin-entry-filter-autolink'):
        text = AutoLink_link(text)
    return text

Entry.registerFilter('text', AutoLink_filter)
Entry.registerFilter('title', AutoLink_filter)


# Copyright (C) 1999-2002 The ViewCVS Group. All rights reserved.
#
# By using ViewCVS, you agree to the terms and conditions set forth below:
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  1. Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  2. Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
