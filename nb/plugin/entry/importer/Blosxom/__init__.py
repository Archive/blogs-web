import os
import string
import sys
import time
from nb.plugin.entry.importer import Importer

__author__ = "Zack Weinberg (zackw@panix.com) and Leonard Richardson (leonardr@segfault.org)"

class BlosxomImporter(Importer):

    """This will import a directory full of Blosxom entry fragments into
    NewsBruiser. Note that it will not import any data stored by a
    Blosxom plugin; if you use any plugins, you'll need to modify this
    importer to take them into consideration."""

    def getKey(self):
        return "blosxom"

    def getPriority(self):
        return 30

    def getTitle(self):
        return _("Import from a Blosxom weblog on this server")

    def getDescription(self):
        return self.__doc__

    def getLocalFileImportLabel(self):
        return _("Enter the path to the directory on this server where you keep your Bloxsom blog entries:")

    def _isTextFile(self, file):
        return string.rfind(file, '.txt') == len(file)-4

    def gatherFromFile(self, dir):
        errors = []
        entries = []
        # The file is actually a directory, or should be.
        files = []
        if os.path.isdir(dir):
            files = os.listdir(dir)
        else:
            if self._isTextFile(dir):
                #Well, if they want us to just import one entry we can do that.
                files.append(dir)
            else:
                #Something's wrong.
                if not os.path.exists(dir):
                    errors.append(_("Sorry, but the directory you gave me (%s) doesn't exist on this server.") % dir)
                else:
                    errors.append(_("Sorry, but %s is a regular file and I need a directory (specifically, the one containing your blosxom entries).") % dir)
            
        try:
            for e in files:
                if self._isTextFile(e):
                    entries.append(self.gatherOneEntry(os.path.join(dir, e)))
        except:
            return (entries, sys.exc_info())
        else:
            return (entries, errors)

    def gatherOneEntry(self, path):
        # The publication date is given by the file modification timestamp.
        date = time.localtime(os.stat(path)[8])
        f = open(path)
        title = f.readline()[:-1]
        body = f.read()
        return { 'text' : body,
                 'title' : title,
                 'creationTime' : date }
