import string
import time
import urllib
import urlparse

from nb.lib.transfusion import Transfusion
from nb.plugin.entry.importer import Importer
from nb.plugin.entry.annotate import Comments
from nb.plugin.entry.annotate.Comments import CommentScreens
from nb.plugin.entry.annotate import Trackback
from nb.plugin.entry.annotate.Trackback.TrackbackScreens import TrackbackPingScreen

class MovableTypeImporter(Importer):

    def getKey(self):
        return "movabletype"

    def getPriority(self):
        return 2

    def getTitle(self):
        return _("Import from a Movable Type weblog")

    def getDescription(self):

        commentsEnabled = self.notebook.commentsEnabled()
        trackbackEnabled = self.notebook.incomingTrackbackEnabled()

        warning = ''
        what = ''
        it = 'that feature'
        if not commentsEnabled and not trackbackEnabled:
            what = 'comments or incoming trackbacks'
            it = 'those features'
        elif not commentsEnabled:
            what = 'comments'
        elif not trackbackEnabled:
            what = 'incoming trackbacks'
        if what:
            warning = "However, since this notebook doesn't currently have %s enabled, you won't be able to see the %s until you turn %s on." % (what, what, it)
        
        return """This will import entries previously posted to a
        Movable Type weblog. You can do this by entering the address
        to the export screen for the weblog in question. If it's
        easier for you, you can also do this by exporting the data to
        a file on the server, and entering the path to the
        file.<p>Your entries will be imported along with all their
        comments and incoming trackbacks. %s""" % warning

    def getURLImportLabel(self):
        return """Here you should enter the export URL for your weblog. To find this URL, first go to your Moveable Type weblog. Click the "Import/Export" button and look at the bottom of the page, where it says "Export Entries From [Weblog Name]". The address behind that link is the URL you want to put in here (it should look something like the default).<br />"""

    def getURLImportDefault(self):
        return "http://www.example.com/cgi-bin/mt.cgi?__mode=export&blog_id=1"

    def getUploadedFileImportLabel(self):
        return "Use Moveable Type's export feature (see above) to export your weblog to a file. Then upload the file here.<br />"
    
    def getForeignSiteUsernameLabel(self):
        return "Your Movable Type username"

    def getForeignSitePasswordLabel(self):
        return "Your Movable Type password"

    def gatherFromURL(self, url):
        name = urllib.quote(self.foreignUsername)
        password = urllib.quote(self.foreignPassword)
        if urlparse.urlparse(url)[0][:4] == 'http':
            url = url + '&username=%s&password=%s' % (name, password)
        try:
            opened = urllib.urlopen(url)
        except IOError:
            return [], [_("Sorry, I couldn't connect to %s. The site might be down, or you might have made a typo.") % url]    
        entries, errors = self.gatherFromFilehandle(opened)
        if not entries and opened.info().getheader('content-type') != 'text/plain':
            more = ''
            if errors:
                more = " More errors follow below, which should give you a better idea of what is going on."
            errors.insert(0, "The page I was able to access really doesn't look like a Moveable Type export file. It might be a Moveable Type error, a Moveable Type login form (indicating that you entered the wrong login or password), or a 404 error or other random page (indicating that you entered the wrong URL).%s" % more)
        return entries, errors

    def gatherFromFilehandle(self, fh):
        entries, errors = [], []
        done = 0
        text = ''
        self.parser = Transfusion()
        while not done:
            block = fh.read(4096)
            if not block:
                done = 1
            text = text + block
            
            entryTexts = string.split(text, self.parser.ENTRY_DELIMITER)
            if len(entryTexts) > 0:
                if done:
                   (toImport, text) = entryTexts, ''
                else:
                    (toImport, text) = entryTexts[:-1], entryTexts[-1]
                for i in toImport:
                    if i:
                        self.gatherEntry(i, entries, errors)
        return entries, errors

    DATE_FORMAT = '%m/%d/%Y %H:%M:%S %p'

    def _parseDate(self, date, seconds=1):
        "Turns a Movable Type date string into tuple or seconds since epoch."
        if date:
            date = time.strptime(date, self.DATE_FORMAT)
            if seconds:
                date = time.mktime(date)
        return date

    import re
    TWO_LINE_BREAKS = re.compile('\n\n')

    def gatherEntry(self, s, entries, errors):        
        bare, newErrors = self.parser.read(s)[0]
        errors.extend(map(lambda(x): string.replace(string.replace(string.replace(x, '&', '&amp;'), '<', '&lt;'), '>', '&gt;',), newErrors))
        if not bare:
            return
        date = bare.get('DATE')
        if date:
            date = self._parseDate(date, 0)
        category = bare.get('CATEGORY')
        if category:
            category = [category]
        else:
            category = []
        entry = { 'authorName' : bare.get('AUTHOR'),
                  'text' : bare.get('EXTENDED_BODY',
                                   bare.get('BODY',
                                            bare.get('EXCERPT'))),
                  'title' : bare.get('TITLE'),
                  'categories' : category,
                  'draft' : not string.lower(bare.get('STATUS', '')) == 'publish',
                  'creationTime' : date }
        if bare.get('CONVERT BREAKS', 0):
            entry['text'] = string.replace(entry['text'], '\n\n', '\n\n<p>')
        entry[Comments.COMMENTS] = []
        for comment in bare.get('COMMENT', []):
            c ={ CommentScreens.COMMENT_IP : comment.get('IP'),
                 CommentScreens.COMMENT_NAME : comment.get('AUTHOR'),
                 CommentScreens.COMMENT_EMAIL : comment.get('EMAIL'),
                 CommentScreens.COMMENT_URL : comment.get('URL'),
                 CommentScreens.COMMENT_TEXT : comment.get('TEXT'),
                 CommentScreens.COMMENT_TIME : self._parseDate(comment.get('DATE'))
                 }
            entry[Comments.COMMENTS].append(c)

        entry[Trackback.INCOMING_TRACKBACK] = []
        for trackback in bare.get('PING', []):
            t ={ TrackbackPingScreen.IP : trackback.get('IP'),
                 TrackbackPingScreen.URL : trackback.get('URL'),
                 TrackbackPingScreen.TITLE : trackback.get('TITLE'),
                 TrackbackPingScreen.BLOGNAME: trackback.get('BLOG NAME'),
                 TrackbackPingScreen.EXCERPT : trackback.get('TEXT'),
                 TrackbackPingScreen.TIME: self._parseDate(trackback.get('DATE'))
                 }
            entry[Trackback.INCOMING_TRACKBACK].append(t)

        #TODO: Propagate support for 'allow comments'
        #TODO: Support multiple categories. (requires transfusion change)
        entries.append(entry)
