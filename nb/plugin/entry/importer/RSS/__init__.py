import cgi
import re
import string
import sys
import time
import traceback

from nb import const
from nb import util
try:
    from nb.lib import feedparser
    class RSSParser:
        def __init__ (self):
            pass
    useFeedParser=1
except:
    #For Python 1.5; has less namespace support.
    from nb.lib import RSSParser
    useFeedParser=0
from nb.plugin.entry.importer import Importer

def _makeTitleAndBody(title, body):
    """Returns the given title, or nothing if it turns out to be
    the same as the first part of the given body."""
    title = string.strip(title)
    body = string.strip(body)
    sample = util.sample(body, 50)
    sample = string.replace(sample, '\n', '')

    title = string.replace(title, '\n', '')
    if sample == title:
        title = ''
    return title, body

class RSSImporter(Importer):

    """Turns an RSS feed into a delicious list of arguments to
    Notebook.addEntry. Just add password!"""

    SUPPORTED_VERSIONS = [0.92, 1.0, 2.0, 3.0]

    def getKey(self):
        return "rss"

    def getPriority(self):
        return 0
    
    def getTitle(self):
        return "Import from an RSS feed" 

    def getDescription(self):
        return "Import to NewsBruiser from your old weblog's RSS feed (supported versions: %s)" % self.getSupportedVersionString()

    def getURLImportLabel(self):
        return "Enter the address of the RSS feed you want to import. If you leave this blank, NewsBruiser will import from this notebook's blogroll.<br />"

    def getUploadedFileImportLabel(self):
        return "Download an RSS feed to your computer and upload it here.<br />"

    def getLocalFileImportLabel(self):
        return "If your old RSS feed is on the same computer you installed NewsBruiser on, enter its path here.<br />"

    def gather(self, source):
        if source:
            return Importer.gather(self, source)
        else:
            return self.gatherFromBlogroll()

    def gatherFromBlogroll(self):        
        blogroll = self.notebook.getBlogroll()
        errors = []
        entries = []
        self.includeSource = 1
        for i in blogroll:
            if i.feed:                
                thisEntries, thisErrors = self.gatherFromURL(i.feed)
                entries.extend(thisEntries)
                errors.extend(thisErrors)
        return entries, errors

    def gatherFromString(self, s):
        entries = []
        errors = []
        if s:
            for ver in self.SUPPORTED_VERSIONS:
                methodName = 'import' + str(ver)
                methodName = string.replace(methodName, '.', '_')
                method = getattr(self, methodName, None)
                if not method:
                    raise _("RSSImporter claims to support RSS version %s.\nBut it doesn't even define a method called '%s'.\nIf you can't even trust RSSImporter to support RSS %s,\n how can you trust it in the governor's office?\n\nPaid for by Citizens For An Alternative To RSSImporter.") % (ver, methodName, ver)
                else:
                    try:
                        thisEntries, thisErrors = method(s)
                        entries.extend(thisEntries)
                        errors.extend(thisErrors)
                    except:                        
                        #Add traceback to parseErrors and try parsing as
                        #another version                        
                        errors.append("Tried to import as RSS %s: <pre>%s</pre>" % (ver,
                                                                                    cgi.escape(string.join(traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback), ''))))
            if not entries:            
                errors.insert(0, "I tried and I tried, but I couldn't import what you gave me as any of the following RSS versions: %s. Check to make sure you actually gave me an RSS feed and not a 404 error or something. <p>Version-specific errors follow." % self.getSupportedVersionString())
        return entries, errors

    def import3_0(self, s):
        """Imports from a string containing an RSS 3.0 feed."""
        entries = []
        exp = re.compile('(?s)([^\n:]+): (.*?)(?=\n[^ \t]|\Z)')
        ok = 0        
        for unparsed in string.split(s, '\n\n'):
            parsed = exp.findall(unparsed)
            if not ok: #The feed definition, a good place to ask:
                       #is this valid RSS 3.0?
                if parsed:
                    ok = 1
                else:
                    raise "This isn't RSS 3.0!"
            elif parsed:
                #Subsequent files contain actual entries, which we
                #should import.
                item = {}
                for (key, value) in parsed:
                    item[key] = value
                title = item.get('title', '')
                text = item.get('description', '')
                authorName = item.get('creator', '')
                authorEmail = None
                if authorName and ' ' in author:
                    (authorEmail, authorName) = string.split(authorName,
                                                                 ' ', 2)
                categories = item.get('subject', None)
                creationTime = None
                created = item.get('created', None)
                if created:
                    for format in (const.SHORT_W3CDTF_TIME_FORMAT,
                                   const.LONG_W3CDTF_TIME_FORMAT):
                        if not creationTime:
                            try:
                                creationTime = time.strptime(created,
                                                             format)
                                creationTime = creationTime - self.notebook.getGMTOffset()
                            except:
                                pass
                
                data = { 'authorName' : authorName,
                         'authorEmail' : authorEmail,
                         'text' : text,
                         'title' : title,
                         'creationTime' : creationTime }
                entries.append(data)
        return entries, []

    def import2_0(self, s):
        entries = []
        errors = []
        if useFeedParser:
            parsed = feedparser.parse(s)
            self.processParsedFeed(parsed, entries, errors)
        else:
            parser = ImportingRSSParser(entries, errors)
            parser.importer = self
            parser.feed(s)
            parser.close()
        if not entries and not errors:
            errors.append("This is valid RSS 2.0, but there are no items to import.")
        return entries, errors

    def import1_0(self, s):
        #import2_0 method should parse any valid 1.0 feed.
        return [], []

    def import0_92(self, s):
        #import2_0 method should parse any valid 0.92 feed.
        return [], []

    def getSupportedVersionString(self):
        "Returns a string listing the supported RSS versions."
        return string.join(map(str, self.SUPPORTED_VERSIONS), ', ')

    def processParsedFeed(self, parsed, entries, errors):
        """The Python 2.1 and up way to import from an RSS feed, using
        feedparser."""    
        defaultAuthorName = None
        defaultAuthorEmail = None
        authorDetail = getattr(parsed.feed, 'author_detail', None)
        fromGMT = 0
        atom = parsed.version.find('atom') == 0
        if authorDetail:
            defaultAuthorName = authorDetail.get('name')
            defaultAuthorEmail = authorDetail.get('email')
        for e in parsed.entries:
            created = getattr(e, 'created_parsed', None)
            if not created:
                created = getattr(e, 'modified_parsed', None)
            #feedparser gives us times in GMT.
            fromGMT = self.notebook.getGMTOffset(created[-1])
            created = time.localtime(time.mktime(created)-fromGMT)

            authorName = defaultAuthorName
            authorEmail = defaultAuthorEmail
            authorDetail = getattr(e, 'author_detail', None)
            if authorDetail:
                authorName = authorDetail.get('name', authorName)
                authorEmail = authorDetail.get('email', authorName)
            else:
                authorName = getattr(e, 'author', authorName)
        
            title = getattr(e, 'title', '')
            body = None
            for i in 'content', 'summary_detail':
                bodyContainer = getattr(e, i, None)
                if bodyContainer:                                
                    if hasattr(bodyContainer, 'value'):
                        body = bodyContainer['value']
                    elif hasattr(bodyContainer, '__getitem__'):
                        body = bodyContainer[0]['value']
                if body:
                    break
            title, body = _makeTitleAndBody(title, body)

            originalURL = getattr(e, 'link', '')

            categories = getattr(e, 'categories', [])
            if hasattr(categories, '__len__'):
                for i in range(0, len(categories)):
                    if hasattr(categories[i], '__len__'):
                        categories[i] = [categories[i][1]]
            elif categories:
                categories = [categories]

            data = { 'text' : body,
                     'authorName' : authorName,
                     'authorEmail' : authorEmail,
                     'title' : title,
                     'categories' : categories,
                     'creationTime' : created,
                     'originalURL' : originalURL }
            entries.append(data)

class ImportingRSSParser(RSSParser):

    """The Python 1.5 way to import from an RSS feed. A class which
    parses an RSS feed and puts each entry into import format. Does
    not currently recognize authors or categories."""

    def __init__(self, entries, errors):
        self.entries = entries
        self.errors = errors
        RSSParser.__init__(self)

    def deriveEntryFromItem(self, item):
        body = item.get("description", '')
        title = item.get("title", '')
        title, body = _makeTitleAndBody(title, body)

        #Parse the date into a tuple.
        tuple = None
        pubDate = item.get("pubDate")
        if pubDate:
            datesToTry = [pubDate, pubDate[:-4]]
            for date in datesToTry:
                try:
                    tuple = time.strptime(date, const.RFC822_TIME_FORMAT_NO_TZ)
                except:
                    pass

            if tuple:
                tuple = time.localtime(time.mktime(tuple) - self.importer.notebook.getGMTOffset())

        originalURL = item.get('link', None)
        if not originalURL and item.get('guid.isPermalink') != 'false':
            originalURL = item.get('guid')

        return (body, title, tuple, originalURL)

    def end_item(self):
        item = self.items[-1]        
        (body, title, date, originalURL) = self.deriveEntryFromItem(item)
        author = ''
        if getattr(self.importer, 'includeSource', None):
            author = self.channel.get('title')
            sourceURL = self.channel.get('link')
            if sourceURL:
                sourceTitle = self.channel.get('description', '')
                if sourceTitle:
                    sourceTitle = 'title="%s" ' % string.replace(sourceTitle,
                                                                 '"', '&quot;')
                author = '<a %shref="%s">%s</a>' % (sourceTitle, sourceURL,
                                                    author)
        data = { 'text' : body,
                 'authorName' : author,
                 'title' : title,
                 'creationTime' : date,
                 'originalURL' : originalURL }

        self.entries.append(data)
