import time

from nb.lib.xmlrpclib import ResponseError
from nb.plugin.entry.importer import Importer

class VirguleImporter(Importer):

    def getKey(self):
        return "virgule"

    def getPriority(self):
        return 5

    def getTitle(self):
        return "Import from a mod_virgule (Advogato) diary"

    def getDescription(self):
        return """This will import entries previously posted to a
        mod_virgule diary (eg. one on Advogato)."""

    def getURLImportLabel(self):
        return "The URL of the XML-RPC interface to the mod_virgule site"

    def getURLImportDefault(self):
        return "http://www.advogato.org/XMLRPC"

    def getForeignSiteUsernameLabel(self):
        return "Your mod_virgule username"

    def gatherFromURL(self, url):
        entries = []
        errors = []
        user = self.foreignUsername

        PARSE_ERROR = "Sorry, but it looks like %s is not really an Advogato server; it gave me some weird content I couldn't parse when I tried to call the %s method."

        from nb.lib import xmlrpclib
        server = xmlrpclib.Server(url)
        try:
            length = server.diary.len(user)
        except ResponseError: 
            errors.append(PARSE_ERROR % (url, 'diary.len'))
        except:
            errors.append(_("Sorry, I couldn't connect to %s.") % url)

        if errors:
            length = 0

        for i in range(0, length):
            createdTime = None
            text = None
            try:                
                createdTime = server.diary.getDates(user, i)[0]
            except ResponseError:
                errors.append(PARSE_ERROR % (url, 'diary.getDates'))
            try:
                text = server.diary.get(user, i)
            except ResponseError:
                errors.append(PARSE_ERROR % (url, 'diary.get'))
            if createdTime and text:
                createdTuple = time.strptime(createdTime.value, "%Y%m%dT%H:%M:%S")
                data = {'authorName' : user,
                        'text' : text,
                        'creationTime' : createdTuple}
                entries.append(data)

        return entries, errors
