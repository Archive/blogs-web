import string
import os
import time
import types
import urllib
from StringIO import StringIO
from nb.lib import AsciiDammit

try:
    import codecs
except:
    codecs = 0

from nb import const
from nb.plugin.entry.annotate.Categories.Category import Category
from nb.NewsBruiserCGIs import DispatchCGI
from nb.NBCGI import NBCGI
import nb

DispatchCGI.cgiModules.append(__name__)

userDefinedImporters = []

def registerImporter(importer):

    """This can be called from cfg.py to register a user-defined
    importer. Eg.:

    from nb.plugin.entry import importer
    from nb.MyImporter import MyImporter
    importer.registerImporter(MyImporter())

    MyImporter will show up below all the importers that come with
    NewsBruiser.
    """
    userDefinedImporters.append(importer)

def instantiateOriginalURLFromInput(vars, entry):
    url = vars.get(Importer.ORIGINAL_URL)
    if url:
        setattr(entry, Importer.ORIGINAL_URL, url)
nb.Entry.registerInstantiationFromInputHook(instantiateOriginalURLFromInput)

def originalURLAsOutputString(entry):
    """Exports to the entry file the orignal URL of this entry."""
    original = getattr(entry, Importer.ORIGINAL_URL, None)
    if original:
        return '%s: %s\n' % (Importer.ORIGINAL_URL, original)
nb.Entry.registerAsOutputStringHook(originalURLAsOutputString)

def importTimeAsOutputString(entry):
    """Exports to the entry file the time this entry was imported."""
    importTime = getattr(entry, Importer.IMPORT_TIME, None)
    if importTime:
        return '%s: %s\n' % (Importer.IMPORT_TIME, importTime)
nb.Entry.registerAsOutputStringHook(importTimeAsOutputString)

def getImportTimeMilliseconds(self):
    """Returns the import time of this entry in milliseconds (local time)."""
    return int(getattr(self, Importer.IMPORT_TIME),
               self.getTimeMilliseconds(1))
nb.Entry.getImportTimeMilliseconds = getImportTimeMilliseconds

def syndicationViewOnly(entry):
    return self.getOptionValue('syndication-only')
nb.Entry.syndicationViewOnly = syndicationViewOnly

class Importer:

    """An abstract class which all importers should subclass so that
    the import action will know how to deal with them."""

    ORIGINAL_URL = 'originalURL'
    IMPORT_TIME = 'importedTime'

    def getKey(self):
        "Something like '[sourcefoo]'"
        pass

    def getPriority(self):
        "A unique value to use when sorting the importers."
        pass

    def getTitle(self):
        "Something like 'Import from [source foo]'"
        pass        

    def getDescription(self):
        "Something like 'This imports from [source foo]'"
        pass

    def getURLImportLabel(self):
        """Return the label to display by the URL input for this importer, or
        None if a URL import doesn't make sense for this importer."""
        return None

    def getURLImportDefault(self):
        """Return the default value of the URL import box, or None if there
        should be no default."""
        return None

    def getLocalFileImportLabel(self):
        """Return the label to display by the on-server file/directory
        specification control for this importer, or None if that
        doesn't make sense for this importer."""
        return None

    def getUploadedFileImportLabel(self):
        """Return the label to display by the file upload control for
        this importer, or None if a file upload doesn't make sense for
        this importer."""
        return None

    def getForeignSiteUsernameLabel(self):
        """Return the label to display by the foreign site username
        for this importer, or None if this importer doesn't go to
        another site that requires a username."""

    def getForeignSitePasswordLabel(self):
        """Return the label to display by the foreign site password
        for this importer, or None if this importer doesn't go to
        another site that requires a password."""
        return None

    #TODO: StringImportLabel

    def canImportRecursively(self):
        """Return true if this importer can take the file/URL you give
        it and derive other files/URLs from it which contain more
        entries.  A user will be given the chance to do a recursive
        import by importing a URL and all the URLs derivable from it,
        recursively."""
        return 0

    def gather(self, descriptor):
        method = self.gatherFromURL
        if type(descriptor) == types.ListType:
            what, descriptor = descriptor
            if what == 'handle':
                method = self.gatherFromFilehandle
            elif what == 'file':
                method = self.gatherFromFile
        return method(descriptor)

    def gatherFromURL(self, url):
        try:
            opened = urllib.urlopen(url)
        except IOError:
            return [], [_("Sorry, I couldn't open the URL %s") % url]
        return self.gatherFromFilehandle(opened)

    def gatherFromFile(self, file, gatherEmbeddedURLs=0, errors=[]):        
        if not os.path.exists(file):
            errors.append((_("Sorry, I couldn't find the file <tt>%s</tt> on this server.") % file))
        elif not os.access(file, os.R_OK):
            errors.append((_("Sorry; The file <tt>%s</tt> exists but I can't read it. Check the permissions.") % file))
        if errors:
            return [], errors
        else:
            if os.path.isdir(file):
                entries = []
                otherURLs = []
                for i in os.listdir(file):
                    if not errors:
                        en, er, oth = self.gatherFromFile(os.path.join(file,
                                                                       i),
                                                          gatherEmbeddedURLs,
                                                          errors)
                    entries.extend(en)
                    errors.extend(er)
                    otherURLs.extend(otherURLs)
                return entries, errors, otherURLs
            else:
                return self.gatherFromFilehandle(open(file))

    def gatherFromFilehandle(self, fh):
        return self.gatherFromString(fh.read())

    def gatherFromString(self, s):
        raise "Abstract method called!"

    def newEntry(self):
        now = time.localtime(time.time())
        return { 'authorName' : '',
                 'authorEmail' : '',
                 'text' : '',
                 'title' : '',
                 'categories' : [],
                 'draft' : 0,                 
                 'creationTime' :  now }

    def fixText(self, t):
        if t:
            if codecs:
                t = codecs.utf_8_encode(t)[0]
            t = AsciiDammit.htmlDammit(t)
        return t

    ### Don't override these methods.

    def hasInterface(self):
        """Returns true if this importer exposes any methods to the import
        cgi."""
        return self.getURLImportLabel() or self.getLocalFileImportLabel() \
               or self.getUploadedFileImportLabel() or hasattr(self, 'gather')

    def setForeignUsername(self, username):
        self.foreignUsername = username

    def setForeignPassword(self, password):
        self.foreignPassword = password

class ImportCGI(NBCGI):

    NAME = 'import'
    DATE_FORMAT = '%Y/%m/%d %H:%M:%S'

    IMPORT_KEY = 'import'

    RECURSION_DEPTH_CGI_KEY = 'maxRecursionDepth'
    SAFETY_CGI_KEY = 'safety'
    IMPORTER_KEY_CGI_KEY = 'importer'
    URL_CGI_KEY = 'url'
    UPLOADED_FILE_CGI_KEY = 'file'
    LOCAL_PATH_CGI_KEY = 'localPath'
    
    FOREIGN_SITE_PASSWORD_CGI_KEY = 'foreignPassword'
    FOREIGN_SITE_USERNAME_CGI_KEY = 'foreignUsername'

    def __init__(self, pathInfo=''):
        self.importersByKey = {}
        self.importersByPriority = {}
        NBCGI.__init__(self, pathInfo=pathInfo)

    def run(self):
        self.printHeader()
        self.__gatherImporters()
        importerKey = self.var(self.IMPORTER_KEY_CGI_KEY)
        if importerKey:
            givenPassword = self.var(const.PASSWORD_CGI_KEY)
            if not self.notebook.validatePassword(givenPassword):
                self.printError('Incorrect password.')
                
            importer = self.importersByKey.get(importerKey, None)
            importer.notebook = self.notebook
            if not importer:
                self.printError(_("I don't know which importer you want."))
            
            importer.setForeignUsername(self.var(self.FOREIGN_SITE_USERNAME_CGI_KEY))
            importer.setForeignPassword(self.var(self.FOREIGN_SITE_PASSWORD_CGI_KEY))
            
            recursionDepth = int(self.var(self.RECURSION_DEPTH_CGI_KEY, 0))
            if self.var(self.IMPORT_KEY):
                errors = []
                sources = []

                file = self.var(self.UPLOADED_FILE_CGI_KEY)
                if file != None and hasattr(file, 'filename'):
                    sources.append(['handle', StringIO(file.value)])
                elif self.var(self.LOCAL_PATH_CGI_KEY):
                    file = self.var(self.LOCAL_PATH_CGI_KEY)
                    sources.append(['file', file])
                else:
                    url = self.var(self.URL_CGI_KEY, importer.getURLImportDefault())
                    if url:
                        sources.append(url)

                safety = self.var(self.SAFETY_CGI_KEY)
                if safety:
                    self.printTestHeader(importer)
                else:
                    self.printImportHeader(importer)
                beenImported = {}
                if not sources:
                    sources.append('')
                while sources and not errors:
                    importer.findMoreURLs = recursionDepth != 0
                    for source in sources:
                        isString = type(source) == types.StringType
                        if not isString or not beenImported.get(source):
                            ret = importer.gather(source)
                            l = len(ret)
                            newURLs = []
                            if l == 1:
                                entries = ret
                            elif l == 2:
                                entries, errors = ret
                            else:
                                entries, errors, newURLs = ret
                            if isString:
                                beenImported[source] = 1
                            sources.remove(source)
                            for e in entries:
                                for i in 'title', 'text':
                                    t = e.get(i)
                                    if t:
                                        e[i] = importer.fixText(t)
                                if safety:
                                    self.describe(e, importer)
                                else:
                                    self.importEntry(e, importer)
                    if recursionDepth == 0:
                        break

                    #We're going to recurse further down.
                    sources.extend(newURLs)
                    if recursionDepth > 0: #ie. not -1
                        recursionDepth = recursionDepth - 1
                if safety:
                    self.printTestFooter(errors, importer)
                else:
                    self.printImportFooter(errors, importer)
                self.endPage()
            else:
                self.printForm(importer)
        else:
            self.printForm()

    def printForm(self, importer=None):
        self.startPage(_('Import entries into %s') % self.notebook.getTitle())
        from nb.CoreCGIs import AddCGI
        manualImport = self.notebook.getURL(AddCGI.NAME, 'Import=Y')

        if importer:
            self.printImporterForm(importer)
            self.addNavLink(self.notebook.getURL(self.NAME),
                            _('Back to the list of ways to import'))
        else:
            print '<p>%s</p>' % (_("""NewsBruiser can import entries from your old weblog's RSS feed, as well as being able to talk directly to several other weblog systems. If you want to import entries from a  weblog system you don't see listed here, you can try to write a custom importer (if you know some Python), or send mail to <a href="mailto:dev@newsbruiser.tigris.org">dev@newsbruiser.tigris.org</a>, asking for help. If all else fails, you can <a href="%s">import entries manually</a>, one at a time.""") % manualImport)
            print '<p>%s</p>' % _('Here are all the ways you can import entries into this version of NewsBruiser:')
            print '<ul>'
            keys = self.importersByPriority.keys()
            keys.sort()
            for key in keys:
                print '<li>'
                self.printSummaryForm(self.importersByPriority[key])
                print '</li>'
            print '</ul>'
        self.addNavLink(manualImport, _('Import an entry manually'))
        self.endPage()

    def printSummaryForm(self, importer):
        self.linkTo(self.notebook.getURL(self.NAME,
                                         self.IMPORTER_KEY_CGI_KEY + '=' + importer.getKey()),
                    importer.getTitle())

    def printImporterForm(self, importer):
        if importer.hasInterface():
            self.startForm(self.notebook.getURL(self.NAME),
                           encType='multipart/form-data')
            self.hiddenField(self.IMPORTER_KEY_CGI_KEY, importer.getKey());
            self.printHeading(importer.getTitle())
            if importer.getDescription():
                print '<p>%s</p>' % importer.getDescription()
            self.passwordField()

            urlLabel = importer.getURLImportLabel()
            localFileLabel = importer.getLocalFileImportLabel()
            uploadedFileLabel = importer.getUploadedFileImportLabel()

            totalLabels = 0
            for i in urlLabel, localFileLabel, uploadedFileLabel:
                if i:
                    totalLabels = totalLabels + 1

            if urlLabel and totalLabels > 1:
                self.printHeading(_('Import from the web...'), 3)
                print '<blockquote>'

            if importer.getForeignSiteUsernameLabel():
                print '<p>%s:' % importer.getForeignSiteUsernameLabel()
                self.textField(self.FOREIGN_SITE_USERNAME_CGI_KEY,
                               self.var(self.FOREIGN_SITE_USERNAME_CGI_KEY),
                               30, None)
                print '</p>'

            if importer.getForeignSitePasswordLabel():
                print '<p>%s:' % importer.getForeignSitePasswordLabel()
                self.passwordEntryField(self.FOREIGN_SITE_PASSWORD_CGI_KEY,
                                        self.var(self.FOREIGN_SITE_PASSWORD_CGI_KEY),
                                        20, None)
                print '</p>'
                
            if urlLabel:
                print '<p>%s' % urlLabel
                default = importer.getURLImportDefault()
                if not default:
                    default = ''
                length = max(len(default), 60)
                self.textField(self.URL_CGI_KEY,
                               self.var(self.URL_CGI_KEY, default),
                               length, None)
                print '</p>' 

            if urlLabel and totalLabels > 1:
                print '</blockquote>'

            if uploadedFileLabel and totalLabels > 1:
                dots = '.'
                if totalLabels > 2:
                    dots = '...'
                self.printHeading(_('Or import from a file you upload%s') % dots,
                                  3)
                print '<blockquote>'
                
            if uploadedFileLabel:
                print '<p>%s' % uploadedFileLabel
                self.uploadBox(self.UPLOADED_FILE_CGI_KEY)
                print '</p>' 
                
            if uploadedFileLabel and totalLabels > 1:
                print '</blockquote>'

            if localFileLabel and totalLabels > 1:
                self.printHeading(_('Or import from a path on this server.'), 3)
                print '<blockquote>'
                
            if localFileLabel:
                print '<p>%s' % localFileLabel
                self.textField(self.LOCAL_PATH_CGI_KEY,
                               self.var(self.LOCAL_PATH_CGI_KEY),
                               60, None)
                print '</p>' 
                
            if localFileLabel and totalLabels > 1:
                print '</blockquote>'

            if importer.canImportRecursively():
                print '<p>', _("Archive depth:")
                self.textField(self.RECURSION_DEPTH_CGI_KEY, self.var(self.RECURSION_DEPTH_CGI_KEY, '0'), 2, 2)
                print "<br />", _("If you want, I can follow archive links on the page you give me to find other pages of entries that should be imported. Enter -1 here to get all the archived pages I can find, or enter an upper limit on how far back I should go. If you enter 0, I'll import the page you specify and no other pages.")
            print '<p>', _('Safety switch:')
            self.checkbox(self.SAFETY_CGI_KEY, 'Y', 1)
            print '<br />', _('Leaving this checked will do a test run which will show you what would be imported. This will not actually import anything; uncheck this box to do a real import.'), '</p>'
            self.endForm(_('Import'), self.IMPORT_KEY)

    def printErrors(self, errors):
        if errors:
            self.printHeading(_('Import errors'))
            print '<ul>'
            for error in errors:                
                if error:
                    print '<li>%s</li>' % error
            print '</ul>'
            self.line()

    def entryAsArgList(self, entry):
        """Turns the entry map into a list of argument suitable for the
        Entry constructor."""
        authorName = entry.get('authorName', '')
        authorEmail = entry.get('authorEmail')
        text = entry.get('text', '')
        title = entry.get('title', '')
        creationTime = entry.get('creationTime')
        draft = entry.get('draft', 0)
        return [authorName, authorEmail, text, title, draft,
                creationTime, entry]

    def printTestHeader(self, importer):
        self.collectedEntries = []
        
        self.startPage(_('Import test for %s') % self.notebook.getTitle())
        self.printHeading(_('Performing import test run...'), 2)
        print '<ul>'
        print '<li><a href="#form">%s</a></li>' % _('See the import form.')
        print '<li><a href="#summary">%s</a></li>' % _('See a summary of the test run.')
        print '<li><a href="#errors">%s</a>' % _('See any errors.')
        print '<li><a href="#detail">%s</a>' % _('See details of the test run.')
        print '</ul>'
        self.line()
        print '<a name="#form"></a>'
        self.printImporterForm(importer)
        self.line()

        print '<a name="summary"></a>'

        self.printHeading('Summary view')
        print '<table border="1"><tr>'
        for i in [_('Body size (bytes)'),
                  _('Title'),
                  _('Import time'),
                  _('Already been imported?'),
                  _('Author name'),
                  _('Author email'),
                  _('Categories')]:
            print ' <th>%s</th>' % i
        print '</tr>'        

    def describe(self, entry, importer):
        (authorName, authorEmail, text, title, draft, creationTime, vars) = self.entryAsArgList(entry)
        categories = entry.get('categories', [])
        print '<tr>'
        formattedTime = None
        if creationTime:
            formattedTime = time.strftime(self.DATE_FORMAT, creationTime)
        duplicate = _('No')
        if not creationTime:
            duplicate = _('?')
        elif self.notebook.entryAlreadyExists(creationTime, title, text):
            duplicate = _('Yes, will be ignored')
        if not text:
            text = ''
                
        catStrings = []
        for i in categories:
            if type(i) in (types.StringType, types.UnicodeType):
                catStrings.append(i)
            else:
                catStrings.append(string.join(i, '/'))
        for i in (len(text), title, formattedTime, duplicate, authorName,
                  authorEmail, string.join(catStrings, ', ')):
            if not i:
                i = _('[None]')
            print ' <td>%s</td>' % i
        print '</tr>'
        self.collectedEntries.append(entry)

    def printTestFooter(self, errors, importer):
        print '</table>'
        self.line()
        print '<a name="errors"></a>'
        self.printHeading(_("Errors"))
        if errors:
            self.printErrors(errors)
            print _("Due to the preceding error(s), not all of the available entries may have been imported. Please fix the problem that caused the preceding error(s) and try importing again.")
        else:
            print _("No errors.")
        self.line()
        print '<a name="detail"></a>'
        self.printHeading(_('Detail view'))
        for entry in self.collectedEntries:
            (authorName, authorEmail, text, title, draft, creationTime, vars) = self.entryAsArgList(entry)
            print '<p>', _('Title: %s') % title
            if creationTime:
                print '<br>', _('<b>Post time:</b> %s') % time.strftime(self.DATE_FORMAT, creationTime)
            print '<br>', _('<b>Body:</b> %s') % text
            print '<p><hr width="50%%"></p>'
        self.addNavLink(self.notebook.getURL(self.NAME), _('Back to main import screen'))

    def printImportHeader(self, importer):
        self.startPage(_('Importing entries into %s') % self.notebook.getTitle())

    def importEntry(self, entry, importer):
        time = entry.get('creationTime')
        text = entry.get('text', '')
        title = entry.get('title', '')
        entryExists = self.notebook.entryAlreadyExists(time, title, text)
        duplicate = 1
        if not entryExists:
            duplicate = 0
            #Create any new categories defined in this entry.
            newCategories = entry.get('categories', [])
            for category in newCategories:
                parent = None
                parentsChildren = self.notebook.getCategories()
                if type(category) == types.StringType:
                    category = [category]
                for part in category:
                    newParent = parentsChildren.get(part)
                    if not newParent:
                        newParent = Category(self.notebook, part, parent)
                        newParent.write()
                    parent = newParent
                    parentsChildren = parent.getChildren()
            
            argList = self.entryAsArgList(entry)
            argList.insert(0, self.var(const.PASSWORD_CGI_KEY))
            entry = apply(self.notebook.addEntry, argList);
        else:
            entry = entryExists
        self.panelLink(entry.getAbsoluteURL(), _('Here'))
        what = 'entry from %s' % entry.getFormattedDate()
        if entry.getTitle():
            what = what + ' ("%s")' % entry.getTitle()
        if duplicate:
            print _("The %s had already been imported, so I didn't import it again.") % what
        else:
            print (_('Successfully imported %s') % what)
        print '<br />'

    def printImportFooter(self, errors, importer):
        print '<a name="errors"></a>'
        self.printHeading(_("Errors"))
        if errors:
            self.printErrors(errors)
        else:
            print _("No errors.")
        self.addNavLink(self.notebook.getURL(self.NAME), _('Import some more'))

    def __gatherImporters(self):
        """Retrieves all importers from subplugins, and puts them in
        the appropriate mappings, along with any user-defined importers"""
        max = 0
        for (name, module) in self.config.enabledPlugins.items():
            if string.find(name, self.__module__) == 0 and name != self.__module__:
                for c in module.__dict__.values():
                    if hasattr(c, 'getKey') and type(c.getKey) == types.MethodType and c != Importer:
                        o = c()
                        p = o.getPriority()
                        if not p:
                            p = 0
                        while self.importersByPriority.has_key(p):
                            p = p + 1
                        if p > max:
                            max = p
                        self.importersByPriority[p] = o
                        self.importersByKey[o.getKey()] = o
        for i in userDefinedImporters:            
            max = max + 1
            self.importersByPriority[max] = i
            self.importersByKey[i.getKey()] = i
