import cgi
import string
import sys
import traceback

from nb.plugin.entry.importer import Importer

class ManilaImporter(Importer):

    def getKey(self):
        return "manila"

    def getPriority(self):
        return 10

    def getTitle(self):
        return "Import from a Manila weblog"

    def getDescription(self):
        return """This will import all the entries that have been
        posted to a Manila weblog."""

    def getURLImportLabel(self):
        return "URL to homepage of Manila site"

    def getURLImportDefault(self):
        return 'http://mysite.editthispage.com/'

    def getForeignSiteUsernameLabel(self):
        return "Your email address as registered on the Manila site"

    def getForeignSitePasswordLabel(self):
        return "Your password on the Manila site"

    def gatherFromURL(self, url):
        entries = []
        errors = []
        
        from nb.lib import manilalib
        site = manilalib.Site(url, self.foreignUsername, self.foreignPassword)

        site.messages.refresh()
        for message in site.messages:            
            try:
                message.get()
                if message.inResponseTo == 0:
                    categories = []
                    title = message.subject
                    if hasattr(message, 'description'):
                        #It's a news item.
                        if message.newsItemURL:
                            title = '<a href="%s">%s</a>' % (message.newsItemURL,
                                                             title)
                        text = message.description
                        categories = [message.department]
                    else:                    
                        text = message.body
                    authorName = message.memberName
                    authorEmail = message.member
                    creationTime = message.postTime
                    data = { 'authorName' : authorName,
                             'authorEmail' : authorEmail,
                             'text' : text,
                             'title' : title,
                             'creationTime' : creationTime }
                    entries.append(data)
            except:
                errors.append("Couldn't parse message: %s/%s <pre>%s</pre>" % (message.msgNum, message.subject, cgi.escape(string.join(traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback), ''))))
        return entries, errors
