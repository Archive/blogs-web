import urllib
import urlparse
import string
import time
import types
from nb.lib.BeautifulSoup import BeautifulSoup
from nb.lib.BeautifulSoup import NavigableText
from nb.plugin.entry.importer import Importer

class ScreenscraperImporter(Importer):

    """This is a class for you to extend if your old weblog was
    written using some tool NewsBruiser can't import, or if you just
    wrote it on HTML pages. You need to subclass this class, and you
    need to implement getKey(), getPriority(), and parse(). Your goal
    is to fill up self.entries and (if there are any errors)
    self.errors.

    An entry is represented as a hash with the following keys:
      'authorName'
      'authorEmail',
      'text',
      'title',
      'categories',
      'draft',
      'publicationDate'
      'originalURL'
      'timezone'
      
    Most of these are strings.
    
    'draft' is a 0 if the entry has been published and a 1 if
    it hasn't.

    'publicationDate' is a tuple (local time).

    'categories' is a list of category names. A category name can itself
    be a list, of a category with its parents:

     ['category', ['supercategory', 'subcategory']]

    'timezone' should be in Unix format, like 'PST8PDT'

    """

    def __init__(self):
        self.alreadyImported = {}
        self.entries = []
        self.errors = []

    def getKey(self):
        pass

    def getPriority(self):
        return 99

    def getTitle(self):
        return "Import via screen-scraper."

    def getDescription(self):
        return """This will download a web page and process it in some
        way to derive notebook entries from it."""

    def getURLImportLabel(self):
        return "The URL to download and process"

    def getURLImportDefault(self):
        return ""

    def setBaseURL(self, url):
        self.baseURL = url
        i = string.find(url, ':')+3
        lastSlash = string.find(url, '/', i)
        if lastSlash == -1:
            self.baseURL = url + '/'
        else:
            self.baseURL = url[:lastSlash+1]

    def gatherFromURL(self, url):
        self.url = url
        return Importer.gatherFromURL(self, url)

    def gatherFromString(self, s):
        if not hasattr(self, 'url'):
            self.url = ''
        self.setBaseURL(self.url)
        self.soup = BeautifulSoup()
        self.soup.feed(s)
        #Change relative links to absolute links.
        for i in self.soup('a'):
            i['href'] = self.absoluteizeLink(i.get('href'))
        self.gatherEntries()
        otherURLs = []
        if self.findMoreURLs:
            otherURLs = self.gatherOtherEntryURLs()
        entries, errors = self.entries, self.errors
        self.entries = []
        self.errors = []
        return entries, errors, otherURLs

    def gatherEntries(self):
        self.startEntry()
        self.currentEntry['text'] = str(self.soup)        
        self.endEntry()

    def startEntry(self):
        self.currentEntry = { 'authorName' : '',
                              'authorEmail' : '',
                              'text' : '',
                              'title' : '',
                              'categories' : [],
                              'draft' : 0,
                              'creationTime' : time.localtime(time.time()),
                              'originalURL' : self.url }

    def endEntry(self):        
        self.entries.append(self.currentEntry)
        self.startEntry()

    def absoluteizeLink(self, url):
        """Change a relative link to an absolute link."""
        if url == '/':
            url = ''
        url = urlparse.urljoin(self.baseURL, url)
        return url

class SethDavidSchoenImporter(ScreenscraperImporter):

    """An example screenscraper that imports the latest entry in Seth David
    Schoen's 'vitanuova' diary."""

    def getTitle(self):
        return 'Import from vitanuova'

    def getDescription(self):
        return """This will import an entry from Seth David
        Schoen's web diary."""

    def getKey(self):
        return 'sethdavidschoen'

    def getURLImportDefault(self):
        return 'http://vitanuova.loyalty.org/latest.html'

    def canImportRecursively(self):
        return 1

    def gatherOtherEntryURLs(self):
        """Finds the previous and next entries."""
        urls = []
        check = self.soup('hr')[-1]
        found = 0
        while check.next and found < 2:
            if isinstance(check, NavigableText):
                for i in ('Prev:'):
                    if string.find(check.string, i) != -1:
                        found = found + 1
                        urls.append(check.next['href'])
            check = check.next
        return urls

    def gatherEntries(self):
        self.startEntry()
        self.currentEntry['authorName'] = 'Seth David Schoen'
        self.currentEntry['authorEmail'] = 'schoen@loyalty.org'        

        date = string.strip(self.soup.first('big').contents[0])
        try:
            published = time.strptime(date, '%A, %B %d, %Y')
        except ValueError:
            published = time.localtime(time.time())
        self.currentEntry['creationTime'] = published
        
        headings = self.soup('strong')
        headings = map(lambda x: str(x.contents[0]), headings)
        self.currentEntry['categories'] = headings
        if headings:
            title = string.join(headings, ', ')
        else:
            title = date
        self.currentEntry['title'] = title
        body = self.soup.first('body')
        if body:
            body.hideTag = 1
        else:
            body = self.soup
        self.currentEntry['text'] = str(body)        
        self.currentEntry[self.ORIGINAL_URL] = self.soup.first('a', contents='Permanent link to this entry').get('href')
        self.endEntry()
