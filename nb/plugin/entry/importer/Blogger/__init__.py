# from nb.plugin.entry.importer import Importer

# class BloggerImporter(Importer):

#     def getKey(self):
#         return "blogger"

#     def getPriority(self):
#         return 15

#     def getTitle(self):
#         return "Import from a Blogger site"

#     def getDescription(self):
#         return """This will import the 20 most recent entries
#         posted to a Blogger site."""

#     def getURLImportLabel(self):
#         return "The ID of the weblog to import from (leave this blank to import from your first weblog)"

#     def getForeignSiteUsernameLabel(self):
#         return "Your username as registered on blogger.com"

#     def getForeignSitePasswordLabel(self):
#         return "Your blogger.com password"

#     def gatherFromURL(self, url):
#         #Not really a URL, but...
#         entries = []
#         name = self.foreignUsername
#         passwd = self.foreignPassword

#         from nb.lib import blogger
#         blogs = blogger.listBlogs(name, passwd)
#         blog = None
#         if url:
#             for b in blogs:
#                 if b['blogid'] == url:
#                     blog = b
#                     break
#         else:
#             blog = blogs[0]
#         if not blog:
#             raise "User %s doesn't own blog with id %s!" % (name, url)
#         blogID = blog["blogid"]
#         recent = blogger.getRecentPosts(blogID, name, passwd)

#         #Note: we assume that you posted all these entries.
#         user = blogger.getUserInfo(name, passwd)
#         authorName = user['nickname']
#         authorEmail = user['email']

#         for message in recent:
#             text = message['content']
#             data = { 'authorName' : authorName,
#                      'authorEmail' : authorEmail,
#                      'text' : text }
#             entries.append(data)

#         return entries, []
