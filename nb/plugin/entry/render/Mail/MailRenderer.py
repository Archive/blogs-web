from nb import util

class MailRenderer:

    def __init__(self, subjectPrefix=""):
        self.subjectPrefix = subjectPrefix

    def renderEntry(self, entry):        
        return self.getHeaders(entry), self.getBody(entry)

    def getSubjectPrefix(self, entry):

        """Returns the prefix for the subject of the putative
        email. Should somehow indicate the notebook/weblog that
        spawned the entry, unless you are sending to a mailing list
        that will add on its own prefix.""" 
        if self.subjectPrefix == "":
            prefix = '[%s]' % entry.notebook.getName()
        elif self.subjectPrefix == None:
            prefix = ''
        else:
            prefix = self.subjectPrefix
        return prefix

    def getSubject(self, entry):
        "Returns the subject of the putative email."
        title = entry.getTitle()
        if not title:
            title = 'New entry as of %s' % entry.getFormattedDate()
        subject = self.getSubjectPrefix(entry) + ' ' + title
        return subject

    def getBody(self, entry):
        return '%s\r\n\r\n---\r\nPosted at %s\r\n%s' % (util.formatForRSS30(entry.getProcessedText()),
                                                        entry.getFormattedDate(),
                                                        entry.getAbsoluteURL())

    def getHeaders(self, entry):
        """Returns the headers for an email message representing this
        entry, not counting 'To' and 'From'."""
        return ['Subject: ' + self.getSubject(entry)]
