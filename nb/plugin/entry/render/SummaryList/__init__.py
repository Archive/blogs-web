"""A NewsBruiser plugin to generate a summary of recent entries.
Timo Virkkala 2004 <timo.virkkala@cs.helsinki.fi>
Dean Kusler 2004 <deank@unt.edu>
NewsBruiser by Leonard Richardson, more info at
http://newsbruiser.tigris.org
"""

__author__= "Timo Virkkala (timo.virkkala@cs.helsinki.fi) and Dean Kusler (deank@unt.edu)"
__version__ = "2.0" # TODO: Replace this to CVS revision string?
__date__ = "$Date$"
__copyright__ = "Copyright (c) 2002-2003 Timo Virkkala. Copyright (c) 2004 Dean Kusler."
__license__ = "Python"

import copy

import nb
from nb import CommandMaps
from nb import Notebook
from nb.Entry import Entry
from nb.plugin.entry.annotate.Comments import COMMENT_COMMAND_MAP, Comment
from nb.plugin.entry.annotate.Trackback import TRACKBACK_COMMAND_MAP, IncomingTrackback
import time

CommandMaps.BASIC_COMMAND_MAP["summaryList"] =  "getSummaryList"
CommandMaps.MASTER_HEADER_TEMPLATE_COMMAND_MAP["summaryList"] = "getSummaryList"
CommandMaps.MASTER_FOOTER_TEMPLATE_COMMAND_MAP["summaryList"] = "getSummaryList"
CommandMaps.ENTRY_LIST_COMMAND_MAP["summaryList"] = "getSummaryList"

SUMMARYLIST_COMMAND_MAP = copy.copy(CommandMaps.ENTRY_COMMAND_MAP)
SUMMARYLIST_COMMAND_MAP["summaryList-date"] = "getSummaryListFormattedDate"
SUMMARYLIST_COMMAND_MAP["summaryList-title"] = "getSummaryListTitle"

def getSummaryList(self, viewer):
    """Prints a summary of recent entries to the notebook."""
    dateformat = self.getOptionValue('summaryList-date-format')
    if not dateformat:
        dateformat = self.getOptionValue('display-time-format')

    tb = self.getOptionValue('summaryList-include-trackbacks')
    com = self.getOptionValue('summaryList-include-comments')
    limit = int(self.getOptionValue('summaryList-item-count'))
    items = []
    if tb or com:
        daysback = int(self.getOptionValue('summaryList-days-back'))
	if daysback == 0:
	    entries = self.GetEntries()
	else:
	    entries = self.getPreviousDaysEntries(daysback)
	if tb:
	    trackbacks = []
	    for entry in entries:
	        trackbacks.extend(entry.getIncomingTrackbacks())
	    items.extend([(float(t.date), t) for t in trackbacks])
	    items.sort()
	    items.reverse()
	    if len(items) > limit:
	        items = items[:limit]
	if com:
	    comments = []
	    for entry in entries:
	        comments.extend(entry.getComments())
	    items.extend([(float(c.date), c) for c in comments])
	    items.sort()
	    items.reverse()
	    if len(items) > limit:
	        items = items[:limit]
    if self.getOptionValue('summaryList-include-entries'):
        entries = self.getPreviousEntries(limit)
	items.extend([(time.mktime(e.getTimeTuple()),e) for e in entries])
	items.sort()
	items.reverse()
	if len(items) > limit:
	    items = items[:limit]
    items = [i[1] for i in items]

    for item in items:
        if item.__class__.__name__ == 'Entry':
            self.interpolateTemplate('summaryList-entry', 
                                     SUMMARYLIST_COMMAND_MAP, item, self)
	elif item.__class__.__name__ == 'Comment':
	    self.interpolateTemplate('summaryList-comment',
	                             [SUMMARYLIST_COMMAND_MAP,COMMENT_COMMAND_MAP],
				     item, self, backupObjects=[item.entry])
	elif item.__class__.__name__ == 'IncomingTrackback':
	    self.interpolateTemplate('summaryList-trackback',
	                             SUMMARYLIST_COMMAND_MAP+TRACKBACK_COMMAND_MAP,
				     item, self, backupObjects=[item.entry])
	else:
	    pass #ACK!

Notebook.Notebook.getSummaryList = getSummaryList


def getSummaryListFormattedDate(self, notebook):
    """Prints the date of the entry in the format specified
    by the summary list."""
    dateformat = notebook.getOptionValue('summaryList-date-format')
    if not dateformat:
        dateformat = notebook.getOptionValue('display-time-format')
    return self.getDateInFormat(dateformat)

Entry.getSummaryListFormattedDate = getSummaryListFormattedDate

def getSummaryListFormattedDateComment(self, notebook):
    """Prints the date of the comment in the format specified
    by the summary list."""
    dateformat = notebook.getOptionValue('summaryList-date-format')
    if not dateformat:
        dateformat = notebook.getOptionValue('display-time-format')
    return time.strftime(dateformat,time.localtime(float(self.date)))

Comment.getSummaryListFormattedDate = getSummaryListFormattedDateComment
IncomingTrackback.getSummaryListFormattedDate = getSummaryListFormattedDateComment

def getSummaryListTitle(self):
    """Prints the title of the entry which the comment is a part of."""
    return self.entry.getTitle()

Comment.getSummaryListTitle = getSummaryListTitle
    
