group=Summary List
Here you can configure the way your list of summaries is formatted and 
displayed. If you're not using the summary list, you can ignore these options.

option=summaryList-item-count
type=integer
maxLength=4
default=10
displayName=How many items?
description=Choose how many of the latest items (entries, comments, and/or trackbacks) should be included in your summary list.

option=summaryList-include-entries
type=boolean
default=Y
displayName=Include entries in summary list?
description=If you check this box, entries will be included in your summary list.

option=summaryList-include-comments
type=boolean
default=Y
displayName=Include comments in summary list?
description=If you check this box, comments will be included in your summary list.

option=summaryList-include-trackbacks
type=boolean
default=Y
displayName=Include trackbacks in summary list?
description=If you check this box, trackbacks will be included in your summary list.

option=summaryList-date-format
type=string
allowempty=yes
themed=Y
displayName=Date format
description=Date format for the summary list dates. If you leave this empty, the notebook's default date format is used.

option=summaryList-days-back
type=integer
maxLength=4
displayName=Days back to check for new comments and/or trackbacks
description=Only check back this many days of entries to find new comments and/or trackbacks. This can drastically improve performance for blogs with lots of entries. Set this to 0 to check all entries.
default=90

option=summaryList-entry-template
type=longString
controllingOption=summaryList-include-entries
default=<span class="entryDate">%summaryList-date</span><p class="summaryListEntry"><a href="%v">%sample</a></p>
themed=Y
displayName=Summary list entry template
description=This template controls the display of entries in your summary list. It is interpolated once for every entry in your summary list. You can use the same directives as in the entry templates, plus "summarylist-date" which prints the entry date in the summary list date format.

option=summaryList-comment-template
type=longString
controllingOption=summaryList-include-comments
default=<span class="entryDate">%summaryList-date</span><p class="summaryListEntry">%comment-authorContact commented on<br /><a href="%v#comments">%sample</a></p>
themed=Y
displayName=Summary list comment template
description=This template controls the display of comments in your summary list. It is interpolated once for every comment in your summary list. You can use the same directives as in the entry templates (for the comment's parent entry) or the comment templates, plus "summarylist-date", which prints the comment date in the summary list date format, and "summaryList-title", which prints the title of the comment's parent (there's a function overlap that requires this).

option=summaryList-trackback-template
type=longString
controllingOption=summaryList-include-trackbacks
default=<span class="entryDate">%summaryList-date</span><p class="summaryListEntry">%trackback-blogname commented on<br /><a href="%v#trackback">%sample</a><br />in the post <a href="%trackback-url">%trackback-title</a></p>
themed=Y
displayName=Summary list trackback template
description=This template controls the display of trackbacks in your summary list. It is interpolated once for every trackback in your summary list. You can use the same directives as in the entry templates (for the trackback's parent entry) or the trackback templates, plus "summarylist-date", which prints the trackback date in the summary list date format, and "summaryList-title", which prints the title of the trackback's parent (there's a function overlap that requires this).
