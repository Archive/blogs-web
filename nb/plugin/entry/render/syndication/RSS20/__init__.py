import time

from nb import const
from nb.plugin.entry.render.syndication import UserlandRSSSyndicator, registerSyndicator

class RSS20Syndicator(UserlandRSSSyndicator):

    VERSION = '2.0'
    PRIORITY = 1

    def __asRSS20Item(self, entry):
        item = {}
        item['guid'] = self.getGUID(entry)
        title = entry.getTitle()
        if title:
            item['title'] = entry.getTitle()
        author = entry.getAuthorDescription()
        if author:
            item['author'] = author
        text = self.getExportSafeText()
        if text:
            item['description'] = text
        
        try:
            publicationTuple = entry.getTimeTuple(0)
        except AttributeError:
            publicationTuple = None
        if publicationTuple:
            date = time.strftime(const.RFC822_TIME_FORMAT, publicationTuple)
            item['pubDate'] = date
        return item

    def renderHeader(self, out):
        UserlandRSSSyndicator.renderHeader(self, out)
        if self.entries:
            lastBuildDate = time.strftime(const.RFC822_TIME_FORMAT,
                                          self.entries[0].getTimeTuple(0))
            out.write('  <lastBuildDate>%s</lastBuildDate>\n' % lastBuildDate)

    def getGUID(self, entry):
        if (entry.notebook.syndicationViewOnly() and not entry.notebook.permalinksOnForeignSite()):
            #We can't trust the foreign URL to be unique. We need to
            #create a new GUID and provide a link to the foreign URL
            #instead of using it as a guid.
            guid = entry.getTagURI()
        else:
            guid = entry.getAbsoluteURL()
        return guid

    def renderEntryFields(self, entry, out):
        if (entry.notebook.syndicationViewOnly() and not entry.notebook.permalinksOnForeignSite()):
            out.write(' <link>%s#%s</link>\n' % (entry.getAbsoluteURL(), entry.getTagURI()))
        out.write(' <guid isPermaLink="true">%s</guid>\n' % self.getGUID(entry))
        author = entry.getAuthorDescription()
        if author:
            out.write(' <author>%s</author>\n' % author)

registerSyndicator(str(RSS20Syndicator.VERSION), RSS20Syndicator)
