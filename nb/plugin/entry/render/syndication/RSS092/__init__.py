from nb.plugin.entry.render.syndication import UserlandRSSSyndicator, registerSyndicator

class RSS092Syndicator(UserlandRSSSyndicator):

    VERSION = '0.92'
    PRIORITY = 3

    def getVersionURLAppend(self):
        return '092'
    
    def renderEntryFields(self, entry, out):
        out.write(' <link>%s</link>\n' % entry.getAbsoluteURL())

registerSyndicator(str(RSS092Syndicator.VERSION), RSS092Syndicator)
