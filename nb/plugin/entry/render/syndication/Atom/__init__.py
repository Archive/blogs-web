import cgi
import time
import types

from nb import const
from nb import version
from nb.plugin.entry.render.syndication import Syndicator, registerSyndicator

class AtomSyndicator(Syndicator):

    VERSION = '1.0'
    PRIORITY = 2
    NAME = 'Atom'

    def getDefaultFilename(self):
        return 'atom.xml'

    def getMimeType(self):
        return 'application/atom+xml'

    def _renderTags(self, out, dict):
        for (tag, value) in dict.items():
            if value:
                out.write('<%s>%s</%s>\n' % (tag, value, tag))

    def _renderAtomPerson(self, out, tagName, email, name, url=None):
        
        if email and not name:
            name = email
        dict = { 'name' : name,
                 'email' : email,
                 'uri' : url }
        if name:
            out.write(' <%s>\n' % tagName)
            self._renderTags(out, dict)
            out.write(' </%s>\n' % tagName)

    def _renderAtomContent(self, out, tagName, content, type='text'):
        if type in ('text', 'html'):
            content = cgi.escape(content)
        out.write('<%s type="%s">%s</%s>\n' % (tagName, type,
                                               content, tagName))

    def renderHeader(self, out):
        mostRecentModifiedEntry = None
        mostRecentModifiedDate = None
        if self.entries:
            for i in self.entries:
                modified = i.getModifiedDate()
                if not mostRecentModifiedEntry or modified > mostRecentModifiedDate:
                    mostRecentModifiedDate = modified
                    mostRecentModifiedEntry = i
            modified = None
        if mostRecentModifiedEntry:
            modified = mostRecentModifiedEntry.getModifiedDate(const.LONG_W3CDTF_TIME_FORMAT)
        else:
            modified = time.strftime(const.LONG_W3CDTF_TIME_FORMAT, time.gmtime(time.time()))
        notebookInfo = {
            'title' : self.notebook.getExportedSiteName(),
            'subtitle' : self.notebook.getDescription(),
            'generator' : 'NewsBruiser v%s' % version.version,
            'updated' : modified
            }
        lang = self.notebook.getLanguage()
        if lang:
            lang = ' xml:lang="%s"' % lang
        else:
            lang = ''
        out.write('<?xml version="1.0" encoding="utf-8"?>\n')
        out.write('<feed xmlns="http://www.w3.org/2005/Atom"%s>\n' % lang)
        out.write(' <link rel="alternate" type="text/html" href="%s"/>\n' % self.notebook.getExportedSiteURL())
        out.write(' <link rel="self" type="application/atom+xml" href="%s"/>\n' % self.syndicatorURL)
        self._renderTags(out, notebookInfo)
        self._renderAtomPerson(out, 'author', self.notebook.getContactEmail(),
                               self.notebook.getContactName())
        out.write('\n')

    def renderEntry(self, entry, out):
        item = {}
        item['author'] = cgi.escape(entry.getAuthor())
        item['updated'] = entry.getModifiedDate(const.LONG_W3CDTF_TIME_FORMAT)
        item['published'] = entry.getDateInFormat(const.LONG_W3CDTF_TIME_FORMAT, 0)
        item['id'] = entry.getTagURI()
        #item['created'] = Do this after fixing time zone conversion
        out.write('<entry>\n')
        out.write('<link rel="alternate" type="text/html" href="%s" />\n' % entry.getAbsoluteURL())
        self._renderAtomContent(out, 'title', entry.sample(), 'html')
        self._renderTags(out, item)
        if self.lofi:
            text = entry.sample()
        else:
            text = entry.getProcessedText()
        self._renderAtomContent(out, 'content', text, 'html')
        out.write('</entry>\n\n')

    def renderFooter(self, out):
        out.write('</feed>\n')

registerSyndicator('Atom', AtomSyndicator)
