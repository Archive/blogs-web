import time

from nb.plugin.entry.render.syndication import Syndicator, registerSyndicator

class ESFSyndicator(Syndicator):

    NAME = 'ESF'
    PRIORITY=5

    def getMimeType(self):
        return 'application/esf+text'

    def getContentType(self):
        return 'text/plain'

    def getDefaultFilename(self):
        return 'esf.txt'

    def getIconText(self):
        return self.NAME
    
    def renderHeader(self, out):
        contact = self.notebook.getContactEmail()
        if contact:
            contactName = self.notebook.getContactName()
            if contactName:
                contact = contact + ' (%s)' % contactName                

        out.write('title:%s\n' % self.getSyndicationName(self.category))
        if contact:
            out.write('contact:%s\n' % contact)
        out.write('link:%s\n' % self.getSyndicationURL(self.category))
        out.write('\n')

    def renderEntry(self, entry, out):
        "Render an entry as part of an ESF document."
        out.write('%d\t%s\t%s\n' % (time.mktime(entry.getTimeTuple(0)),
                                    entry.sample(), entry.getAbsoluteURL()))

registerSyndicator(ESFSyndicator.NAME, ESFSyndicator)
