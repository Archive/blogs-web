import cgi
import re
import string
import sys
import time
import types

from nb import const
from nb import util
from nb.EntryID import EntryID
from nb import Options
from nb.lib.IWantOptions import SelectionOption
from nb.lib.PriorityOuch import PriorityMap
from nb.Notebook import Notebook
from nb import NBCGI
from nb.NewsBruiserCGIs import DispatchCGI

DispatchCGI.cgiModules.append(__name__)

SYNDICATORS = PriorityMap()

SYNDICATOR_HOOKS = {}

def registerSyndicator(key, klass):
    SYNDICATORS[key] = (klass, klass.PRIORITY)

#You can register syndication hooks to have syndication code print out
#special text specific to a plugin you write, without having to change
#the syndication code. Your hook methods should take a Syndicator
#object, a Notebook object, and a list of Entry objects.
#
#Currently only RSS 1.0 and 2.0 support syndication hooks. You can
#add a hook to the head of the RSS document or to the channel
#descriptor.
def registerHook(syndicator, position, method):
    hooksForSyndicator = SYNDICATOR_HOOKS.get(syndicator, {})
    if not hooksForSyndicator:
        SYNDICATOR_HOOKS[syndicator] = hooksForSyndicator
    hooksForPosition = hooksForSyndicator.get(position, [])
    if not hooksForPosition:
        hooksForSyndicator[position] = hooksForPosition
    hooksForPosition.append(method)

class SyndicationFormatOption(SelectionOption):
    """A way of selecting a particular syndication format."""

    def getSelectionValues(self):
        return SYNDICATORS.keys()
    
    def getSelectionValueDescriptions(self):
        return map(lambda(x): x().getHumanReadableName(), SYNDICATORS.values())
Options.SyndicationFormatOption = SyndicationFormatOption

def getDefaultSyndicator(self):
    syndicator = SYNDICATORS.get(self.getOptionValue('default-syndication-format'))
    if not syndicator:
        syndicator = SYNDICATORS.values()[0]
    return syndicator()
Notebook.getDefaultSyndicator = getDefaultSyndicator

def permalinksOnForeignSite(self):
    """For a notebook that is a feed generator for some other site,
    returns whether or not that site has permalinks."""
    return self.getOptionValue('permalinks-on-foreign-site')
Notebook.permalinksOnForeignSite = permalinksOnForeignSite

def skipDays(self):
    """Returns a list of days of the week during which aggregators
    should not fetch this feed."""
    return self.getOption('syndication-skip-days').getSelectedValues(self)
Notebook.syndicationSkipDays = skipDays

def skipHours(self):
    """Returns a list of hours (translated from notebook local time to GMT)
    during which aggregators should not fetch this feed."""
    hoursOffset = self.getGMTOffset() / 60 / 60    
    skipHours = []
    for i in self.getOption('syndication-skip-hours').getSelectedValues(self):
        i = (int(i) + hoursOffset) % 24
        if i < 0:
            i = 24 - i
        skipHours.append(i)
    return skipHours
Notebook.syndicationSkipHours = skipHours

class Syndicator:

    NAME = 'Unknown syndication format'
    PRIORITY = 100

    #Types of syndication hook.
    HEAD = 'head'
    CHANNEL = 'channel'

    def __init__(self):
        self.exportSafetyREs = []

    def getHooks(self, position):
        return SYNDICATOR_HOOKS.get(self.getKey(), {}).get(position, [])

    def getKey(self):
        return self.NAME

    def getHumanReadableName(self):
        return self.NAME

    def getShortHumanReadableName(self):
        return self.getHumanReadableName()

    def getDefaultFilename(self):
        return 'rss.xml'

    def getMimeType(self):
        return 'application/rss+xml'

    def getContentType(self):
        return 'text/xml'

    def getIconText(self):
        return 'XML'

    def render(self, notebook, entries, category=None, out=sys.stdout):
        self.notebook = notebook
        self.entries = entries
        self.category = category
        self.notebook.setTimeZone()
        self.renderHeader(out)
        if self.entries:
            for entry in self.entries:
                self.renderEntry(entry, out)
        self.renderFooter(out)
        self.notebook.unsetTimeZone()

    def renderHeader(self, out):
        pass
        
    def renderFooter(self, out):
        pass

    def renderEntry(self, entry, out):
        pass

    def getSyndicationName(self, category=None):
        exportedName = self.notebook.getExportedSiteName()
        if category:
            exportedName = exportedName + ': Category ' + category.asLinkedParentage()
        return exportedName

    def getSyndicationURL(self, category=None):
        if category:
            from nb.plugin.entry.annotate.Categories.CategoryScreens import CategoryListCGI
            exportedURL = self.notebook.getExportedHost() + category.getURL(CategoryListCGI.NAME)
        else:
            exportedURL = self.notebook.getExportedSiteURL()
        return exportedURL    

    def getExportSafeText(self, entry):
        "Escapes HTML and makes links and references safe for export."
        if type(entry) == types.StringType:
            text = entry
        else:
            if hasattr(self, 'lofi') and self.lofi:
                text = entry.getSummary()
            elif hasattr(self, 'titlesOnly') and self.titlesOnly:
                text = ''
            else:
                text = entry.getProcessedText()            
        for (pattern, replace) in self.exportSafetyREs:
            text = pattern.sub(replace, text)
        text = cgi.escape(text)
        return text

class RSSSyndicator(Syndicator):

    VERSION = 0

    def getKey(self):
        return str(self.VERSION)

    def getHumanReadableName(self):
        return self.getShortHumanReadableName() + ' ' + str(self.VERSION)

    def getShortHumanReadableName(self):
        return 'RSS'

    def render(self, notebook, entries, category=None, out=sys.stdout):
        replace = '\\1%s/' % notebook.getExportedHost()
        for i in ('href', 'img\s+src'):
            s = '(<[^>]*%s\s*=\s*"\s*)/' % i
            self.exportSafetyREs.append((re.compile(s, re.I), replace))
        self.notebook = notebook
        self.exportedName = self.getSyndicationName(category)
        self.exportedURL = self.getSyndicationURL(category)
        Syndicator.render(self, notebook, entries, category, out)

    def renderHeader(self, out):
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    def printBaseRSSItemTags(self, entry, out):
        title = entry.getTitle()
        if title:
            out.write(' <title>%s</title>\n' % cgi.escape(title))
        text = self.getExportSafeText(entry)
        out.write(' <description>%s</description>\n' % text)

    def printRSSImageTag(self, include='', out=sys.stdout):
        """Factored out code to print the RSS image tag for all
        RSS versions."""
        out.write('  <image%s>\n' % include)
        out.write('   <url>%s</url>\n' % self.notebook.getExportedSiteImageURL())
        out.write('   <title>%s</title>\n' % self.notebook.getTitle())
        out.write('   <link>%s</link>\n' % self.notebook.getExportedSiteURL())
        out.write('  </image>\n')

class UserlandRSSSyndicator(RSSSyndicator):

    "Common superclass for the Userland-style dialects of RSS."

    def getVersionURLAppend(self):
        return ''

    def renderHeader(self, out):
        if hasattr(self, 'stylesheet') and self.stylesheet:
            out.write('<?xml-stylesheet href="%s" type="text/xsl"?>\n' % \
                      util.urljoin(self.notebook.nbconfig.baseContentURL,
                                   '/resources/xsl/rss2.xsl'))
        out.write('<rss version="%s"' % self.VERSION)
        for hook in self.getHooks(self.HEAD):
            res = hook(self, self.notebook, self.entries)
            if res:
                out.write('\n' + res)
        out.write('>\n')
        out.write(' <channel>\n')
        out.write('  <title>%s</title>\n' % self.getExportSafeText(self.exportedName))
        out.write('  <link>%s</link>\n' % self.exportedURL)
        out.write('  <description>%s</description>\n' % self.getExportSafeText(self.notebook.getDescription()))
        days = self.notebook.syndicationSkipDays()
        if days:
            out.write('  <skipDays>\n')
            for i in days:
                out.write('   <day>%s</day>\n' % i)
            out.write('  </skipDays>\n')
        hours = self.notebook.syndicationSkipHours()
        if hours:
            out.write('  <skipHours>\n')
            for i in hours:
                out.write('   <hour>%s</hour>\n' % i)
            out.write('  </skipHours>\n')
            
        for hook in self.getHooks(self.CHANNEL):
            res = hook(self, self.notebook, self.entries)
            if res:
                out.write(res + '\n')
        self.printRSSImageTag(out=out)
        email = self.notebook.getContactEmail()
        if email:
            name = self.notebook.getContactName()
            if name:
                email = '%s (%s)' % (email, name)
            out.write('  <managingEditor>%s</managingEditor>\n' % email)
        language = self.notebook.getLanguage()
        if language:
            out.write('  <language>%s</language>\n' % language)
        out.write('  <docs>http://backend.userland.com/rss%s</docs>\n' % self.getVersionURLAppend())

    def renderEntry(self, entry, out):
        out.write('<item>\n')
        self.printBaseRSSItemTags(entry, out)
        publicationTuple = entry.getTimeTuple(0)
        if publicationTuple:
            date = time.strftime(const.RFC822_TIME_FORMAT, publicationTuple)
            out.write(' <pubDate>%s</pubDate>\n' % date)
        for category in entry.getCategories():
            self.renderCategory(category, out)
        self.renderEntryFields(entry, out)
        out.write('</item>\n')

    def renderCategory(self, category, out):
        from nb.plugin.entry.annotate.Categories.CategoryScreens import CategoryListCGI
        if category.notebook.syndicationViewOnly():
            #We don't know the URL to the view of this category, or even
            #if there is one. Just punt.
            domain = ""
        else:
            domain = 'domain="%s%s/"' % (category.notebook.getExportedHost(),
                                         category.notebook.getURL(CategoryListCGI.NAME))
        out.write(' <category %s>%s</category>\n' %  (domain,
                                                      category.asPathInfo()))

    def renderFooter(self, out):
        out.write(' </channel>\n')
        out.write('</rss>\n')

class SyndicationCGI(NBCGI.NBCGI):

    """View recent notebook entries in any of a number of syndication
    formats. Syndication format classes are located in SYNDICATORS."""

    NAME = 'syndicate'
    CGI_ALIASES = ['rss']

    DEFAULT_NUM_ENTRIES = 15
    
    VERSION_CGI_KEY = 'version'
    ENTRIES_CGI_KEY = 'entries'
    CONTENT_CGI_KEY = 'content'

    SUPPRESS_STYLESHEET = 'noStylesheet'

    def processPathInfoForSyndication(self):
        """This is just like processPathInfoAsCategoryPathOrEntryID
        except that if you're logged in, you can specify * as the path
        and export the entire contents of the notebook in a syndication
        format."""
        all = self.shiftPathInfo()
        self.category = None
        if not all or all != '*':
            #Oh well. Try it the regular way.
            self.unshiftPathInfo(all)
            self.processPathInfoAsCategoryPathOrEntryID()
        elif not self.preAuth:
            #They want everything but are not logged in.
            self.redirectToLogin()
        else:
            #They legitimately want everything.
            self.id = EntryID(self.notebook, year=const.ALL)
            #They sky's the limit
            setattr(self, self.ENTRIES_CGI_KEY, None)

    def __init__(self, pathInfo=''):
        NBCGI.NBCGI.__init__(self, self.processPathInfoForSyndication,
                             pathInfo=pathInfo)

    def run(self):
        version = self.variables.get('version', self.notebook.getDefaultSyndicator().getKey())
        content = self.var(self.CONTENT_CGI_KEY)
        useStylesheet = not self.var(self.SUPPRESS_STYLESHEET)
        if hasattr(self, self.ENTRIES_CGI_KEY):
            numEntries = getattr(self, self.ENTRIES_CGI_KEY)
        elif self.variables.has_key(self.ENTRIES_CGI_KEY):
            numEntries = int(self.variables.get(self.ENTRIES_CGI_KEY))
        else:
            if self.usedDefaultEntryID or self.category:
                numEntries = self.DEFAULT_NUM_ENTRIES
            else:
                numEntries = 0

        if content == 'form':
            self.printForm()
        else:
            if SYNDICATORS.has_key(version):
                syndicator = SYNDICATORS[version]()
                syndicator.lofi = (content == 'lofi')
                syndicator.titlesOnly = (content == 'titlesOnly')
                syndicator.stylesheet = useStylesheet
                syndicator.syndicatorURL = (
                    self.notebook.getExportedHost() +
                    self.getURL(args='version=%s' % version,
                                pathInfo=self.pathInfo))
                self.printHeader(syndicator.getContentType())
                # HACK: Force 15 entries
                syndicator.render(self.notebook, self.__getEntries(15),
                                  self.category, sys.stdout)
            else:
                self.printError('Only the following syndication formats are supported: ' + string.join(SYNDICATORS.keys(), ', '))

    def __getEntries(self, numberOfEntries):
        "Retrieves the entries that should be rendered."
        if self.category:
            entryMap = self.category.getEntries()
            ids = entryMap.keys()
            ids.sort(util.cmpFilename)
            ids.reverse()            
            self.entries = []
            if numberOfEntries > len(ids):
                numberOfEntries = len(ids)
            for i in range(0, numberOfEntries):
                self.entries.append(entryMap[ids[i]])
        else:
            if self.entries and numberOfEntries and len(self.entries) > numberOfEntries:
                self.entries = self.entries[-numberOfEntries:]
            elif numberOfEntries:
                self.entries = self.notebook.getPreviousEntries(numberOfEntries)
            elif self.id:
                self.entries = self.id.getEntries(numberOfEntries)
            if self.entries:
                self.entries.reverse()        
        return self.entries

    def printForm(self):
        self.startPage('Syndication control panel')
        self.startForm(self.notebook.getURL(SyndicationCGI.NAME), 'GET')
        print '<h1>Configure Syndication</h1>'
        keys = []
        descriptions = []
        for key, klass in SYNDICATORS.items():
            syndicator = klass()
            keys.append(key)
            descriptions.append(syndicator.getHumanReadableName())

        print '<p>Syndication format: '
        self.listBox(self.VERSION_CGI_KEY, descriptions, keys,
                     [self.notebook.getDefaultSyndicator().getKey()])
        print '</p>'
        print '<p>Number of entries:'
        self.textField(self.ENTRIES_CGI_KEY, self.DEFAULT_NUM_ENTRIES, 3, 3)
        print '</p>'
        print '<p>Suppress stylesheet? (RSS only): '
        self.checkbox(self.SUPPRESS_STYLESHEET, '', 1)
        print '<p>Verbosity (RSS only): '
        self.listBox(self.CONTENT_CGI_KEY, ['Full entries', 'Summaries only',
                                            'Titles only'],
                     ['hifi', 'lofi', 'titlesOnly'])
        print '</p>'
        self.endForm('View feed')
        self.endPage()


def printAlternateLink(cgi):
    s = ''
    if cgi.notebook and not cgi.usedDefaultNotebook:
        syn = cgi.notebook.getDefaultSyndicator()
        tag = '<link rel="alternate" type="%s" title="%s" href="%s">\n'
        s = tag % (syn.getMimeType(), syn.getHumanReadableName(),
                   cgi.notebook.getURL(SyndicationCGI.NAME))
    return s
                                                                      
NBCGI.registerDisplaySnippet(NBCGI.NBCGI, 'headData',
                             printAlternateLink, 1)
