import time

from nb.plugin.entry.render.syndication import RSSSyndicator, registerSyndicator

class RSS10Syndicator(RSSSyndicator):

    VERSION = 1.0
    PRIORITY = 2

    def renderHeader(self, out):
        RSSSyndicator.renderHeader(self, out)
        out.write('<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"\n')
        out.write(' xmlns:dc="http://purl.org/dc/elements/1.1/"\n')
        out.write(' xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"\n')
        out.write(' xmlns="http://purl.org/rss/1.0/"\n')
        for hook in self.getHooks(self.HEAD):
            res = hook(self, self.notebook, self.entries)
            if res:
                out.write(res + '\n')
        out.write('>\n')
        out.write('<channel rdf:about="%s">\n' % self.exportedURL)
        out.write(' <link>%s</link>\n' % self.exportedURL)
        out.write(' <title>%s</title>\n' % self.getSyndicationName())
        out.write(' <description>%s</description>\n' % self.notebook.getDescription())
        for hook in self.getHooks(self.CHANNEL):
            res = hook(self, self.notebook, self.entries)
            if res:
                out.write(res + '\n')
        out.write(' <items>\n')
        out.write('  <rdf:Seq>\n')
        if self.entries:
            for entry in self.entries:
                out.write('  <rdf:li resource="%s" />\n'% (entry.getAbsoluteURL()))
        out.write('  </rdf:Seq>\n')
        out.write(' </items>\n')
        out.write('</channel>\n')

    def renderEntry(self, entry, out):
        try:
            publicationTuple = self.getTimeTuple(0)
        except AttributeError:
            publicationTuple = None

        out.write('<item rdf:about="%s">\n' % entry.getAbsoluteURL())
        self.printBaseRSSItemTags(entry, out)
        if publicationTuple:
            date = time.strftime(const.LONG_W3CDTF_TIME_FORMAT,
                                 publicationTuple)
            out.write(' <dc:date>%s</dc:date>\n' % date)
            
        if entry.getAuthor():
            out.write(' <dc:author>%s</dc:author>\n' % entry.author)
            
        out.write('</item>\n')

    def renderFooter(self, out):
        out.write('</rdf:RDF>\n')

registerSyndicator(str(RSS10Syndicator.VERSION), RSS10Syndicator)
