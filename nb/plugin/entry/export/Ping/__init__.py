import string
import urllib

from nb import util
from nb.plugin.entry.export import EntryExporter

"""The NewsBruiser administrator can set up and register (as
EntryExporters) any number of ForeignSitePingers in cfg.py. However,
there's also one accessible from the UI, which by default pings the
amazing Ping-O-Matic, which in turn pings all known weblog pinging
services."""

DEFAULT_NOTIFY_URLS = ['http://rpc.pingomatic.com/RPC2']

class ForeignSitePinger(EntryExporter):
    def __init__(self, notifyURL, notebookName=None):
        self.notifyURL = notifyURL
        self.notebookName = notebookName

    def publishEntry(self, entry):
        notebook = entry.notebook
        if self.notebookName and self.notebookName != notebook.name:
            return
        url = self.notifyURL
        weblogName = notebook.getExportedSiteName()
        weblogURL = notebook.getExportedSiteURL()
        if string.find(url, '%s') > -1:
            #They want us to do string interpolation into the URL.
            siteName, siteURL = map(urllib.quote_plus, (weblogName, weblogURL))
            url = url % (siteName, siteURL)
            util.spawnURLAccessor(url)
        else:
            #It's a weblogs.com-compatible ping URL.
            util.spawnXMLRPCCall(url, 'weblogUpdates.ping',
                                 [weblogName, weblogURL])            
            
def notifyOnAdd(notebook):
    "Returns whether or not to notify another site after an add."
    return notebook.getOptionValue('notify-on-add')

def eventEntryPublished(entry):
    notebook = entry.notebook
    if notifyOnAdd(notebook):
        for url in DEFAULT_NOTIFY_URLS:
            ForeignSitePinger(url).publishEntry(entry)

