import string
import urllib

from nb.CoreCGIs import AddCGI
from nb.CoreCGIs import EditCGI
from nb import NBCGI
from nb import Entry
from nb import Notebook
from nb import util

OUTGOING_TRACKBACKS = 'outgoingTrackbacks'

#Create some new accessor methods to keep things OO.

def outgoingTrackbackEnabled(self):
    return self.getOptionValue('enable-outgoing-trackback')
Notebook.Notebook.outgoingTrackbackEnabled = outgoingTrackbackEnabled

def getOutgoingTrackbacks(self):
    s = getattr(self, 'outgoingTrackbacks', None)
    if s:
        l =  map(string.strip, string.split(s, '|'))
    else:
        l = []
    return l
Entry.Entry.getOutgoingTrackbacks = getOutgoingTrackbacks

def sendTrackbackPing(entry, url):
    title = entry.getTitle()
    entryURL = entry.getAbsoluteURL()
    excerpt = util.sample(entry.getProcessedText(), 255)
    blogName = entry.notebook.getTitle()
    data = urllib.urlencode({'title': title, 'url': entryURL, 'excerpt': excerpt, 'blog_name': blogName})
    util.spawnURLAccessor(url, data)

def getOutgoingTrackbackPingURLs(vars):
    return string.split(vars.get(OUTGOING_TRACKBACKS, ''), '\n')

def printOutgoingTrackbackURLBox(cgi, value=''):
    if cgi.notebook.outgoingTrackbackEnabled():
        print "<p><b>Trackback URL(s):</b></p>"
        cgi.textArea(3, 80, OUTGOING_TRACKBACKS, value)

def printOutgoingTrackbackURLBoxForEdit(cgi):
    if cgi.notebook.outgoingTrackbackEnabled():
        entry = cgi.entries[0]
        trackbacks = entry.getOutgoingTrackbacks()
        if entry.id.draft:
            value = string.join(trackbacks, '\n')
            printOutgoingTrackbackURLBox(cgi, value)

def listOutgoingTrackbackURLs(cgi):
    if cgi.notebook.outgoingTrackbackEnabled():
        entry = cgi.entries[0]
        if not entry.id.draft:
            trackbacks = entry.getOutgoingTrackbacks()
            if trackbacks:
                print '<h2>Outgoing Trackbacks</h2>'
                print '<p>'
                plural = ''
                if len(trackbacks) == 1:
                    if trackbacks[0]:
                        print 'This entry sent a trackback ping to <a href="%s">%s</a>.' % (trackbacks[0], trackbacks[0])
                else:
                    print 'This entry sent trackback pings to the following URLs: <ul>'
                    for i in trackbacks:
                        print '<li><a href="%s">%s</a>' % (i, i)
                    print '</ul>'
                print '</p>'

def gatherOutgoingTrackbacks(vars, entry):
    """Finds outgoing trackback URLs in the given CGI's data, and
    sets the given entry's URLs equal to them."""
    entry.outgoingTrackbacks = string.join(getOutgoingTrackbackPingURLs(vars), '|')

def outgoingTrackbacksAsOutputString(entry):
    """Turns the given entry's list of outgoing trackbacks into a line
    to be stored in that entry's representation on disk."""
    s = ''
    trackbacks = entry.getOutgoingTrackbacks()
    if trackbacks:
        s = OUTGOING_TRACKBACKS + ': ' + string.join(trackbacks, '|') + '\n'
    return s

NBCGI.registerDisplaySnippet(AddCGI, 'belowEntryText', printOutgoingTrackbackURLBox, 2)
NBCGI.registerDisplaySnippet(EditCGI, 'belowEntryText', printOutgoingTrackbackURLBoxForEdit, 2)
NBCGI.registerDisplaySnippet(EditCGI, 'belowSubmitButton', listOutgoingTrackbackURLs, 3)
Entry.registerAsOutputStringHook(outgoingTrackbacksAsOutputString)
Entry.registerInstantiationFromInputHook(gatherOutgoingTrackbacks)

def eventEntryPublished(entry):
    notebook = entry.notebook
    if notebook.outgoingTrackbackEnabled():
        for url in entry.getOutgoingTrackbacks():
            if url:
                sendTrackbackPing(entry, url)

