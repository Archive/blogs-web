import string
import sys
import smtplib

from nb.plugin.entry.export import EntryExporter

"""The NewsBruiser administrator can set up and register (as
EntryExporters) any number of MailExporters in cfg.py."""

class MailExporter(EntryExporter):
    def __init__(self, to, fromAddr, renderer, notebookName=None,
                 host='localhost'):
        if type(to) not in (type([]), type(())):
            to = [to]
        self.to = to
        self.fromAddr = fromAddr
        self.notebookName = notebookName
        self.host = host
        self.renderer = renderer

    def publishEntry(self, entry):
        notebook = entry.notebook
        if self.notebookName and self.notebookName != notebook.name:
            return

        headers, body = self.renderer.renderEntry(entry)
        headers.insert(0, 'To: ' + string.join(self.to, ', '))
        headers.insert(1, 'From: ' + self.fromAddr)

        msg = string.join(headers, '\r\n') + '\r\n\r\n' + body
        server = smtplib.SMTP(self.host)
        server.sendmail(self.fromAddr, self.to, msg)
        server.quit()
