# Open source, Python License
# a really quick del.icio.us APi wrapper:
# http://del.icio.us/doc/api
# influenced by pydelicious.py:
# http://dealmedia.net/blosxom/en/Programming/Python/delicious.html

__author__  = "Darryl VanDorp"
__version__ = "0.2"
__history__ = '''

v0.2
    - added inbox support

v0.1
    - initial release

'''

"""

Sample usage:

import pydel
delicious = pydel.Delicious('username','password')
posts = delicious.recentPosts()
for p in posts:
    print p['href'],p['description']

"""


import urllib2
import urllib

class Delicious:
    def __init__(self,user,password):
        self.user = user
        self.password = password
        self.baseuri = 'http://del.icio.us/api/'
        self.lib = urllib2
        a = self.lib.HTTPBasicAuthHandler()
        a.add_password('del.icio.us API', 'http://del.icio.us', self.user, self.password)
        opener = self.lib.build_opener(a)
        self.lib.install_opener(opener)

    def recentPosts(self, count=5, tag='', asXML=0):
        """ returns a list of dicts containing <count> recent posts filtered by <tag> """
        url = self.baseuri + "posts/recent?&count=%s&tag=%s" % (count,tag)
        posts = self._get_posts(url)
        return posts

    def getPosts(self, tag="", date="", asXML=0):
        """ returns a list of dicts containing posts for <date> filtered by <tag>
            date defaults to todays' date
        """
        url = self.baseuri + "posts/get?&tag=%s&dt=%s" % (tag,date)
        posts = self._get_posts(url,asXML)
        return posts
        
    def _get_posts(self, url, asXML=0):
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        if asXML: return xml
        xmlposts = xml.xpathEval("posts/post")
        xml.freeDoc()
        posts = []
        if xmlposts:
            for p in xmlposts:
                post = {}
                post['description'] = p.prop('description')
                post['href'] = p.prop('href')
                post['time'] = p.prop('time')
                post['extended'] = p.prop('extended')
                posts.append(post)
        return posts

    def getTags(self):
        """ returns a list of dicts containing tags """
        url = self.baseuri + "tags/get"
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        xmltags = xml.xpathEval("/tags/tag")
        xml.freeDoc()
        tags = []
        if xmltags:
            for t in xmltags:
                tag = {}
                tag['tag'] = t.prop('tag')
                tag['count'] = t.prop('count')
                tags.append(tag)
        return tags

    def addPost(self, url, description, extended="", tags="", dt=""):
        """ post a link to del.iciou.us. """
        params = urllib.urlencode({'url':url,
                                   'description':description,
                                   'extended':extended,
                                   'tags':tags,
                                   'dt':dt})
        url = self.baseuri + "posts/add?%s" % params
        result = self.lib.urlopen(url).read()
        return result

    def renameTag(self, old, new):
        """ rename <old> tag to <new> tag """
        params = urllib.urlencode({'old':old,
                                   'new':new})
        url = self.baseuri + "tags/rename?%s" % params
        result = self.lib.urlopen(url).read()
        return result

    def postDates(self, tag=""):
        """ return a list of dicts containg dates and and count of posts for
            the given date. can be filtered by <tag>
        """
        url = self.baseuri + "posts/dates?&tag=%s" % tag
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        xmldates = xml.xpathEval("/dates/date")
        xml.freeDoc()
        dates = []
        if xmldates:
            for d in xmldates:
               date = {}
               date['date'] = d.prop('date')
               date['count'] = d.prop('count')
               dates.append(date)
        return dates

    def getInbox(self, dt="", asXML=0):
        """ get contents of inbox filtered by dt (date) """
        url = self.baseuri + "inbox/get?&dt=%s" % dt
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        if asXML: return xml
        xmlinbox = xml.xpathEval("/inbox/post")
        xml.freeDoc()
        inbox_items = []
        if xmlinbox:
            for item in xmlinbox:
                inbox = {}
                inbox['href'] = item.prop('href')
                inbox['time'] = item.prop('time')
                inbox['user'] = item.prop('user')
                inbox['description'] = item.prop('description')
                inbox['tags'] = item.prop('tags')
                inbox_items.append(inbox)
        return inbox_items

    def inboxDates(self, asXML=0):
        url = self.baseuri + "inbox/dates"
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        if asXML: return xml
        xmldates = xml.xpathEval("/inbox/date")
        xml.freeDoc()
        dates = []
        if xmldates:
            for d in xmldates:
                date = {}
                date['date'] = d.prop('date')
                date['count'] = d.prop('count')
                dates.append(date)
        return dates

    def inboxSubs(self, asXML=0):
        url = self.baseuri + "inbox/subs"
        result = self.lib.urlopen(url).read()
        import libxml2
        xml = libxml2.parseDoc(result)
        if asXML: return xml
        xmlsubs = xml.xpathEval("/subs/sub")
        xml.freeDoc()
        subs = []
        if xmlsubs:
            for subscription in xmlsubs:
                sub = {}
                sub['user'] = subscription.prop('user')
                sub['tag'] = subscription.prop('tag')
                subs.append(sub)
        return subs

    def inboxSubscribe(self, user, tag=""):
        params = urllib.urlencode({'user':user,
                                   'tag':tag})
        url = self.baseuri + "inbox/sub?%s" % params
        result = self.lib.urlopen(url).read()
        return result

    def inboxUnsubscribe(self, user):
        params = urllib.urlencode({'user':user})
        url = self.baseuri + "inbox/unsub?%s" % params
        result = self.lib.urlopen(url).read()
        return result
    
    
        
