"""The del.icio.us exporter mirrors NewsBruiser entries to a
del.icio.us account if they look like they could be del.icio.us
entries (basically if they include a URL)."""

try:
    import urllib2
    installed = 1
except ImportError:
    installed = 0

if installed:
    import re
    import string

    from nb.lib import HTMLStripper
    from nb.lib.BeautifulSoup import BeautifulSoup
    from nb.Notebook import Notebook
    from nb.plugin.entry.export import EntryExporter
    from nb.plugin.entry.export.Delicious.pydel import Delicious

    def exportToDelicious(self):
        return self.getOptionValue('export-to-delicious')
    Notebook.exportToDelicious = exportToDelicious

    def deliciousUsername(self):
        return self.getOptionValue('delicious-username')
    Notebook.deliciousUsername = deliciousUsername

    def deliciousPassword(self):
        return self.getOptionValue('delicious-password')
    Notebook.deliciousPassword = deliciousPassword

    class DeliciousExporter:

        def sanitizeCategoryName(self, category):
            return string.replace(category.name, ' ', '-')

        def publishEntry(self, entry):
            notebook = entry.notebook
            if notebook.exportToDelicious():
                uname = notebook.deliciousUsername()
                password = notebook.deliciousPassword()
                url = entry.getLinkURL()
                if uname and password and url:
                    short = HTMLStripper.strip(getattr(entry, 'title',
                                                       entry.sample()),
                                               [])
                    if not short:
                        short = url
                    extended = HTMLStripper.strip(entry.getProcessedText(), [])
                    cats = map(self.sanitizeCategoryName,
                               entry.getCategories())
                    tags = string.join(cats, ' ')
                    d = Delicious(uname, password)
                    d.addPost(url, short, extended, tags)

        def renameCategory(self, category, oldName):
            notebook = category.notebook
            if notebook.exportToDelicious():
                uname = notebook.deliciousUsername()
                password = notebook.deliciousPassword()
                if uname and password:
                    d = Delicious(uname, password)
                    d.renameTag(oldName, self.sanitizeCategoryName(category))
            
    def eventEntrySaved(entry):
        if entry.notebook.exportToDelicious():
            DeliciousExporter().publishEntry(entry)

    def eventCategoryRenamed(categoryAndOldName):
        (category, oldName) = categoryAndOldName
        if category.notebook.exportToDelicious():
            DeliciousExporter().renameCategory(category, oldName)
