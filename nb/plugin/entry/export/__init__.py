userDefinedExporters = []

def registerExporter(exporter):
    userDefinedExporters.append(exporter)

def __propagateToExporters(message, arg=None):
    for exporter in userDefinedExporters:
        method = getattr(exporter, message, None)
        if method:
            method(arg)

def eventEntryPublished(entry):
    __propagateToExporters('publishEntry', entry)

def eventEntryDeleted(entry):
    __propagateToExporters('deleteEntry', entry)

def eventEntrySaved(entry):
    if not entry.id.draft:
        __propagateToExporters('saveEntry', entry)

def eventFullStaticRewrite(notebook):
    __propagateToExporters('exportNotebook', notebook)

class EntryExporter:

    """Exports an entry to some external source."""

    def publishEntry(self, entry):
        pass

    def deleteEntry(self, entry):
        pass

    def saveEntry(self, entry):
        """Override this if you want to know whenever an entry is saved."""
        pass

    def exportNotebook(self, notebook):
        """Override this if you want to hear about commands to export
        an entire notebook somewhere."""

        #TODO: There should be a way to avoid having to get all the
        #entries every time, yet also avoid getting the entries at all
        #when no enabled writers need to get the entries.

