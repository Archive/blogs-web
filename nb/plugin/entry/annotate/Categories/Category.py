#!/usr/bin/python

import os
import re
import string
import sys
import urllib

from nb import const
from nb import util
from nb import exceptions

CATEGORY_DATA_FILENAME = "data"
DESCRIPTION_KEY = "Description:"
TOPIC_EXCHANGE_TOPIC_KEY = "TopicExchangeTopic:"

class Category:

    """A Category represents a grouping of notebook entries. It is
    associated with a Notebook and has at most one parent Category,
    zero or more child Categories, and zero or more associated Entries
    (the notebook entries filed under the category). It may also have
    a description.

    Categories with no parent category are represented by a directory,
    named after the category, underneath the .categories subdirectory
    of the notebook's working directory. Categories with a parent
    category are represented by a similar directory underneath their
    parent's directory. If a category contains entries, their IDs are
    listed in the "data" file in the category's directory.

    A Category with no parent is unofficially considered to have the
    notebook itself as its parent."""

    def __init__(self, notebook, name, parent=None):
        """Constructor for a category. To create a new one, call this
        and then write() it."""
        self.notebook = notebook
        self.name = name
        self.parent = parent

        #These are obtained when requested to save on disk accesses.
        self.children = None
        self.entries = None
        self.description = None
        self.topicExchangeTopic = None

    #### Miscellaneous accessor methods

    def getName(self):
        return self.name

    def canBeDeleted(self):
        return not self.getChildren()

    def getDir(self):
        if self.parent:
            parentDir = self.parent.getDir()
        else:            
            parentDir = self.notebook.getCategoryDir()
        return os.path.join(parentDir, self.name)

    def getEntryListPath(self):
        return os.path.join(self.getDir(), CATEGORY_DATA_FILENAME)        

    def getURL(self, cgi, args=''):
        """Links to the given CGI to operate on this category with the
        given arguments"""
        return self.notebook.getURL(cgi, args, self.asPathInfo())

    def getDescription(self):
        if self.description == None:
            self.loadData()
        return self.description

    def getParent(self):
        return self.parent

    def getParentObject(self):
        "Returns the parent category or notebook."
        if self.parent:
            return self.parent
        else:
            return self.notebook

    def getChildren(self):
        if not self.children:
            self.children = {}
            dir = self.getDir()
            files = os.listdir(dir)
            files.sort()
            for name in files:
                if os.path.isdir(os.path.join(dir, name)):
                    self.children[name] = Category(self.notebook, name, self)
        return self.children

    def getEntries(self):
        "Returns the entries in this category."
        if self.entries == None:
            self.loadData()
        return self.entries

    #### Data loader method
    def loadData(self):
        if not self.entries or not self.description:
            from nb.Entry import Entry
            from nb.EntryID import EntryID
            self.entries = {}
            self.description = ""
            filename = self.getEntryListPath()
            if os.path.exists(filename):
                file = open(filename)
                lines = file.readlines()
                i = 0
                done = 0
                while not done and i < len(lines):
                    line = lines[i]
                    for varname, key in (('description', DESCRIPTION_KEY),
                                         ('topicExchangeTopic',
                                          TOPIC_EXCHANGE_TOPIC_KEY)):
                        if string.find(line, key) > -1:
                            setattr(self, varname, lines[i][len(key):-1])
                            i = i + 1
                            break
                    else:
                        done = 1                    
                lines = lines[i:]
                if lines:
                    thisIsAHack = EntryID(self.notebook)
                    entryIDs = thisIsAHack.filenamesToEntryIDs(lines)
                    for entryID in entryIDs:
                        try:
                            self.entries[entryID.asPath()] = Entry(entryID)    
                        except exceptions.DoesNotExist:
                            pass 
                            
    #### Methods dealing with examining and altering parentage.

    def asPathInfo(self, urlEscape=1):
        l = self.getParentage()
        if urlEscape:
            method = lambda x: x.escapeName()
        else:
            method = lambda x: x.name            
        return apply(os.path.join, map(method, l))

    def escapeName(self):
        return urllib.quote(self.name)

    def asLinkedParentage(self, cgi=None, args='', suppressLast=1,
                          includeNotebook=1):
        """Returns the parentage of this category, with every parent
        category linked to the given CGI."""
        parents = self.getParentage()
        linkedParents = []
        unlinkedParents = []
        notebookLink = ''
        notebookName = self.notebook.name + '/'
        if includeNotebook:
            if cgi:
                notebookLink = '<a href="%s">%s</a> | ' % (self.notebook.getURL(cgi, args), self.notebook.name)
            else:
                notebookLink = notebookName
        l = len(parents)-1
        for i in range(0, len(parents)):
            parent = parents[i]
            if cgi and (i < l or not suppressLast):
                link = '<a href="%s">%s</a>' % (parent.getURL(cgi, args),
                                                parent.name)
            else:
                link = parent.name
            linkedParents.append(link)
            unlinkedParents.append(parent.name)

        sep = self.notebook.getCategoryParentageSeparator()
        if cgi:
            return [notebookLink + string.join(linkedParents, sep),
                    notebookName + string.join(unlinkedParents, sep)]
                                               
        else:
            return string.join(unlinkedParents, sep)

    def getParentage(self):
        parents = [self]
        p = self.parent
        while p:
            parents.insert(0, p)
            p = p.parent
        return parents

    def getParentageAsString(self):
        p = self.getParentage()
        s = p[0].name
        sep = self.notebook.getCategoryParentageSeparator()
        if len(p) > 1:
            for cat in p[1:]:
                s = s + sep + cat.name
        return s

    def canHaveAsParent(self, proposedNewParent):
        "Returns true iff the given category can be a parent of this category."
        ok = 1
        try:
            self.assertValidParent(proposedNewParent)
        except:
            ok = 0
        return ok

    def assertValidParent(self, proposedNewParent):
        """Throws an exception if the given category cannot be a
        parent of this category."""

        if proposedNewParent == self:
            raise 'A category cannot be its own parent.'

        if proposedNewParent:
            circular = 0
            try:
                proposedNewParent.getParentage().index(self)
                circular = 1
            except:
                pass
            if circular:
                raise 'The %s category is a child of %s, and cannot also be its parent.' % (proposedNewParent.name, self.name)
            if self.getParent() != proposedNewParent and proposedNewParent.getChildren().has_key(self.name):
                raise 'The %s category already contains a child category named "%s"' % (proposedNewParent.name, self.name)
        else:
            if self.notebook.getCategories().has_key(self.name) and self.getParent():
                raise 'The %s notebook already contains a top-level category named "%s"' % (self.notebook.name, self.name)
    
    def changeParent(self, newParent):
        "Change this category's parent to the provided category."
        self.assertValidParent(newParent)

        if self.parent == newParent:
            return #Nothing to do

        if newParent:
            newPath = os.path.join(newParent.getDir(), self.name)
        else:
            newPath = os.path.join(self.notebook.getCategoryDir(), self.name)

        for entry in self.getEntries().values():
            entry.removeFromCategory(self, 0)

        os.rename(self.getDir(), newPath)
        self.denotifyParent()
        self.parent = newParent
        self.notifyParent()
        for entry in self.getEntries().values():
            entry.addToCategory(self, 0)
            entry.write()

    #### Mutator methods on the category's contents
    
    def addEntry(self, entry, propagate=1):        
        if self.entries == None:
            self.getEntries()
        if not entry.id.draft:
            path = entry.id.asPath()
            if not self.entries.has_key(path):
                self.entries[path] = entry
        if propagate:
            entry.addToCategory(self, 0)

    def removeEntry(self, entry, propagate=1):
        if not self.entries:
            self.getEntries()
        if not entry.id.draft:
            path = entry.id.asPath()
            if self.entries.has_key(path):
                del(self.entries[path])
        if propagate:
            entry.removeFromCategory(self, 0)

    #### Mutator methods on the category itself.

    def rename(self, newName):
        if newName == self.name:
            return #Nothing to do
        oldName = self.name

        if self.parent:
            if self.parent.getChildren().has_key(newName):
                raise 'The %s category already contains a child named "%s"' % (self.parent.name, newName)
            baseDir = self.parent.getDir()
        else:
            if self.notebook.getCategories().has_key(newName):
                raise 'The %s notebook already contains a top-level category named "%s"' % (self.notebook.name, newName)
            baseDir = self.notebook.getCategoryDir()
        
        newDir = os.path.join(baseDir, newName)
        entries = self.getEntries().values()

        self.denotifyParent()
        for entry in entries:
            entry.removeFromCategory(self,0)
        os.rename(self.getDir(), newDir)
        self.name = newName            
        for entry in entries:
            entry.addToCategory(self,0)
            entry.write()
        self.notifyParent()
        self.notebook.nbconfig.pluginEvent('CategoryRenamed', [self, oldName])

    def delete(self):
        """Deletes this category. Will not work if the category has
        subcategories."""
        if not self.canBeDeleted():
            raise "You can't delete the %s category until you delete its subcategories." % self.name
        for entry in self.getEntries().values():
            entry.removeFromCategory(self)
            entry.write()
        for file in os.listdir(self.getDir()):            
            path = os.path.join(self.getDir(), file)
            if os.path.isdir(path): #Unlikely, but try anyway
                os.rmdir(path)
            else:
                os.remove(path)
        os.rmdir(self.getDir())
        self.denotifyParent()

    def write(self):
        """Makes sure the directory for this category exists and that its
        data file is written."""
        if not os.path.exists(self.getDir()):
            os.makedirs(self.getDir(), const.DIR_MODE)
        entriesPath = self.getEntryListPath()
        if self.entries or self.description:
            file = open(entriesPath, 'w')
            if self.description:
                self.description = string.replace(self.description, "\n", " ")
                file.write(DESCRIPTION_KEY + self.description + "\n")
            if self.topicExchangeTopic:
                self.topicExchangeTopic = re.compile('[^a-z0-9]').sub('_',
                                                                         string.lower(self.topicExchangeTopic))
                file.write(TOPIC_EXCHANGE_TOPIC_KEY + self.topicExchangeTopic + "\n")
            if self.entries:
                paths = self.entries.keys()
                paths.sort(util.cmpFilename)
                paths.reverse()
                trim = self.notebook.getDir()
                for entryPath in paths:
                    entryPath = entryPath[len(trim):]
                    file.write(entryPath + '\n')
            file.close()
            os.chmod(entriesPath, const.FILE_MODE)

        elif os.path.exists(entriesPath):
            os.remove(entriesPath)
        self.notifyParent()

    #### Consistency methods

    def notifyParent(self):
        """Notifies this category's parent of this category, whether
        the parent is the notebook or another category."""
        self.getParentMap()[self.name] = self

    def denotifyParent(self):
        """Notifies this category's parent of the loss of this
        category, whether the parent is the notebook or another
        category."""

        map = self.getParentMap()
        if map.has_key(self.name):            
            del(map[self.name])

    def getParentMap(self):
        """Abstracts away the difference between the way categories
        store their lists of child categories and the way notebooks
        store their lists of top-level categories."""
        if self.parent:
            map = self.parent.getChildren()
        else:
            map = self.notebook.getCategories()
        return map

    #### Testing methods

    def log(self, str, indent):
        print ('' * range(0, indent)), str

    def selfTest(self, indent = 0):
        self.indent = indent
        self.log("Category %s" % self.getParentageAsString(),
                 indent)
        children = self.getChildren()
        indent = indent + 1
        entries = self.getEntries()
        self.log("Entries: %s" % len(entries), indent)
        for entryPath in entries.keys():
            self.log(entryPath, indent+1)
        if len(children.keys()):
            self.log("Children: %s" % len(children.keys()), indent)
        for child in children.values():
            child.selfTest(indent+1)

def validCategoryName(name):
    "Returns true iff the given string is an acceptable category name."
    return re.compile('[A-Za-z0-9+\- ,]+').match(name)

def parseCategoryPaths(notebook, paths, autocreate=0):
    """Turns a list of paths into a list of categories. Ignores
    nonexistent categories if autocreate is 0, creates them if
    autocreate is 1."""
    categories = []
    for path in paths:
        try:
            category = parseCategoryPath(notebook, path, autocreate)
            if category:
                categories.append(category)
        except:
            pass
    return categories

def parseCategoryPath(notebook, path, autocreate=0):
    "Turns a path into a Category object for the given notebook."
    char = '/'
    if autocreate:
        char = notebook.getCategoryParentageSeparator()
    if type(path) == type(''):
        parts = string.split(path, char)
    else:
        parts = path
    category = None
    if parts and parts[0]:
        category = notebook.getCategories().get(parts[0])
        if not category:
            category = Category(notebook, parts[0])
            category.write()
        if len(parts) > 1:
            for name in parts[1:]:
                newCategory = category.getChildren().get(name)
                if not newCategory:
                    newCategory = Category(notebook, name, category)
                    newCategory.write()
                category = newCategory
                    
    return category

if __name__=='__main__':
    from nb.NBConfig import NBConfig 
    config = NBConfig()
    notebook = config.getDefaultNotebook()

    entries = notebook.getPreviousEntries(6)
    entriesPerCategory = len(entries)/3

    print "TEST: Initial population."

    cat1 = Category(notebook, 'ST-topcat1')
    for entry in entries[:entriesPerCategory]:
        cat1.addEntry(entry)

    cat2 = Category(notebook, 'ST-topcat2')
    for entry in entries[entriesPerCategory+1:(2*entriesPerCategory)+1]:
        cat2.addEntry(entry)

    cat3= Category(notebook, 'ST-subcat', cat2)
    for entry in entries[2*entriesPerCategory:]:
        cat3.addEntry(entry)

    cats = [cat1, cat2, cat3]

    for i in cats:
        i.write()

    for i in cats[:2]: i.selfTest()
    print ''

    print "TEST: Changing %s parent from %s to %s" % (cat3.name, cat2.name,
                                                      cat1.name)
    cat3.changeParent(cat1)
    for i in cats[:2]: i.selfTest()
    print ''

    newName = 'ST-subsubcat'
    print "TEST: Renaming %s to %s." % (cat2.name, newName)
    cat2.rename(newName)
    cat2.write()
    for i in cats[:2]: i.selfTest()
    print ''

    print "TEST: Moving %s underneath %s." % (cat2.name, cat3.name)
    cat2.changeParent(cat3)
    cat1.selfTest()
    print ''

    print "TEST: Moving %s to top-level." % cat3.name
    cat3.changeParent(None)
    for i in [cat1, cat3]: i.selfTest()
    print ''

    print "TEST: Trying to delete %s, which has subcategory (should error)" % cat3.name
    try:
        cat3.delete()
        print "Oops, it done deleted."
    except:
        print "Good."
    print ''

    print "TEST: Trying to improperly rename %s (should error)" % cat3.name
    try:
        cat3.rename(cat1.name)
        print "Oops, it done renamed."
    except:
        print "Good."
    print ''

    #print "Now deleting categories properly (there should not be any errors)."
    #
    #cat1.delete()
    #cat2.delete()
    #cat3.delete()

    print "The .categories directory should now be clean."
