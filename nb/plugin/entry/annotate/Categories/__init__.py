import copy
import os
import string
import types
import urllib

import nb
from nb import CommandMaps
from nb import NBCGI
from nb import util
from nb import CoreCGIs
from nb.NewsBruiserCGIs import DispatchCGI
from nb.Entry import Entry
from nb.Notebook import Notebook

from nb.lib.InternetTopicExchange import Client

import Category
import CategoryScreens

CATEGORY_NAMES_KEY = 'categoryNames'

DispatchCGI.cgiModules.append(CategoryScreens.__name__)

# Some Notebook methods for determining things about this notebook's
# category support

def getCategoryDir(self):
    "Returns the directory in which this notebook's categories are stored."
    return os.path.join(self.getDir(), '.categories')
Notebook.getCategoryDir = getCategoryDir

def enableCategories(self):
    "Returns whether or not to enable category support for this notebook."
    return self.getOptionValue('enable-categories')
Notebook.enableCategories = enableCategories

def multipleCategories(self):
    """Returns whether or not to allow multiple categories per entry
    for this notebook."""
    return self.getOptionValue('multiple-categories')
Notebook.multipleCategories = multipleCategories

def allowCategoryEntry(self):
    """Returns whether or not to allow the user to type in category names
    for entries, rather than using the selection list."""
    return self.getOptionValue('allow-category-entry')
Notebook.allowCategoryEntry = allowCategoryEntry

def internetTopicExchangeIntegration(self):
    """Returns whether or not to allow integration of NewsBruiser categories
    with the Internet Topic Exchange."""
    return self.getOptionValue('internet-topic-exchange-integration')
Notebook.internetTopicExchangeIntegration = internetTopicExchangeIntegration

def getCategoryTaggingSeparator(self):
    """Returns the string to use to separate category names when
    parsing a manually entered list of names."""
    if self.allowCategoryEntry():
        return self.getOptionValue('category-tagging-separator')
    else:
        return ''
Notebook.getCategoryTaggingSeparator = getCategoryTaggingSeparator

def getCategoryParentageSeparator(self):
    """Returns the string to use to separate category names when
    printing out a category lineage."""
    return self.getOptionValue('category-parentage-separator')
Notebook.getCategoryParentageSeparator = getCategoryParentageSeparator

def getCategories(self):
    if not hasattr(self, 'categories'):
        self.categories = {}
        dir = self.getCategoryDir()
        if os.path.exists(dir):
            files = os.listdir(dir)
            files.sort()
            for file in files:
                category = Category.Category(self,file)
                self.categories[category.asLinkedParentage()] = category
    return self.categories
Notebook.getCategories = getCategories

# Some Entry methods for reading and manipulating an entry's categories

def getCategoryPaths(self):
    "Returns a list of the paths to this category's entries."
    return map(lambda x: x.asPathInfo(0), self.getCategories())
Entry.getCategoryPaths = getCategoryPaths

def getCategoryPathString(self):
    "Returns a pipe-delimited list of the paths to this category's entries"
    return string.join(self.getCategoryPaths(), '|')
Entry.getCategoryPathString = getCategoryPathString

def entryGetCategories(self):
    "Returns all the categories under which this entry is filed."
    if not hasattr(self, 'categories'):
        self.categories = []
        if hasattr(self, CATEGORY_NAMES_KEY):
            categoryNames = string.split(self.categoryNames, '|')
            for name in categoryNames:
                try:
                    if name:
                        category = Category.parseCategoryPath(self.notebook, name)
                        if category:
                            self.categories.append(category)
                except:
                    #Category error (ha ha). It was probably
                    #renamed, moved, or deleted and we weren't
                    #told. Act like it was not specified and if
                    #this entry gets written again it will go
                    #away.
                    pass
    return self.categories
Entry.getCategories = entryGetCategories

def inCategory(self, category):
    inCat = 0
    try:
        self.getCategories().index(category)
        inCat = 1
    except:
        pass
    return inCat
Entry.inCategory = inCategory

def addToCategory(self, category, propagate=1):
    "Adds this entry to the given category."
    if not self.inCategory(category):
        self.getCategories().append(category)
        if propagate:
            category.addEntry(self, 0)
Entry.addToCategory = addToCategory

def removeFromCategory(self, category, propagate=1):
    "Removes this entry from the given category."
    if self.inCategory(category):
        self.getCategories().remove(category)
        if propagate:
            category.removeEntry(self, 0)
Entry.removeFromCategory = removeFromCategory

def removeFromAllCategories(entry, write=1):
    "Removes the given entry from all its categories."
    for cat in copy.copy(entry.getCategories()):
        cat.removeEntry(entry)
        if write:
            cat.write()
nb.Entry.registerPreDeletionHook(removeFromAllCategories)

def preOrdinalChange(entry, newOrdinal):
    entry._TMP_ORDINAL_CHANGE_CATEGORIES = copy.copy(entry.getCategories())
    removeFromAllCategories(entry, 0)
Entry.preOrdinalChangeHooks.append(preOrdinalChange)

def postOrdinalChange(entry, newOrdinal):
    for cat in entry._TMP_ORDINAL_CHANGE_CATEGORIES:
        cat.addEntry(entry)
        cat.write()
        entry.write()
    del(entry._TMP_ORDINAL_CHANGE_CATEGORIES)
Entry.postOrdinalChangeHooks.append(postOrdinalChange)

# A hook method for manipulating an entry's categories

def gatherCategoryDelta(vars, entry):
    categoryNames = []
    wasDraft = getattr(vars, 'wasDraft', 0)
    if wasDraft:
        #We need to get the category names out of a member of vars,
        #because they're stored in a preexisting Entry object which
        #is vars.
        newCategories = vars.getCategories()
    else:
        categoryNames = vars.get(CategoryScreens.CATEGORIES_CGI_KEY, [])
        if categoryNames:        
            if type(categoryNames) == types.StringType:
                categoryNames = [categoryNames]
            if type(categoryNames[0]) == types.StringType:
                categoryNames = map(urllib.unquote_plus, categoryNames)
            categoryNames.extend(categoryNames)
        enteredCategoryNames = vars.get(CategoryScreens.ENTERED_CATEGORIES_CGI_KEY,
                                            '')
        if enteredCategoryNames:
            sep = entry.notebook.getCategoryTaggingSeparator()
            categoryNames.extend(map(string.strip, string.split(enteredCategoryNames, sep)))

        newCategories = Category.parseCategoryPaths(entry.notebook, categoryNames)

    if newCategories != None:
        if getattr(vars, 'wasDraft', 0):
            oldCategories = []
        else:
            oldCategories = copy.copy(entry.getCategories())
            if len(oldCategories) > len(newCategories):
                shorterList = newCategories
                otherList = oldCategories
            else:
                shorterList = oldCategories
                otherList = newCategories

            for category in copy.copy(shorterList):
                try:
                    otherList.remove(category)
                    shorterList.remove(category)
                except:
                    pass

        #Categories present in both lists are now gone. oldCategories
        #contains removed categories and newCategories contains added
        #categories.
        for category in oldCategories:
            category.removeEntry(entry)
            category.write()
        for category in newCategories:
            category.addEntry(entry)
            category.write()
nb.Entry.registerInstantiationFromInputHook(gatherCategoryDelta)

# A hook method for representing an entry's categories

def categoriesAsOutputString(entry):
    return '%s: %s\n' % (CATEGORY_NAMES_KEY, entry.getCategoryPathString())
nb.Entry.registerAsOutputStringHook(categoriesAsOutputString)

# UI hooks

def printEntryFilingControl(cgi, entry=None):
    if cgi.notebook.enableCategories():
        [names, values] = cgi.getCategoryNamesAndFormValues()
        defaults = None
        if entry:
            defaults = entry.getCategoryPaths()
        print '</td><td valign="top">'
        size = 12
        if cgi.notebook.allowCategoryEntry():
            size = 10
        cgi.listBox(CategoryScreens.CATEGORIES_CGI_KEY, names, values,
                    defaults, size, cgi.notebook.multipleCategories())
        if cgi.notebook.allowCategoryEntry():
            if not names:
                print '<b>%s</b>' % _('Categories:')
            print '<br />'
            if entry:
                print '<b>%s</b>' % _('Add:')
            cgi.textField(CategoryScreens.ENTERED_CATEGORIES_CGI_KEY,
                          '', 30, None)
            
def printEntryFilingControlForEdit(cgi):
    printEntryFilingControl(cgi, cgi.entries[0])
NBCGI.registerDisplaySnippet(CoreCGIs.AddCGI, 'rightOfEntryText',
                             printEntryFilingControl, 1)
NBCGI.registerDisplaySnippet(CoreCGIs.EditCGI, 'rightOfEntryText',
                             printEntryFilingControlForEdit, 1)

def getCategoryNamesAndFormValues(self, forCategory=None):
    names = []
    values = []
    categories = self.notebook.getCategories()
    categoryNames = categories.keys()
    categoryNames.sort()
    for name in categoryNames:
        category = categories[name]
        if not forCategory or forCategory.canHaveAsParent(category):
            names.append(category.asLinkedParentage(includeNotebook=0))
            values.append(category.asPathInfo(0))
            self._addSubcategories(forCategory, category, names, values)
    return [names, values]
NBCGI.NBCGI.getCategoryNamesAndFormValues = getCategoryNamesAndFormValues

def _addSubcategories(self, forCategory, category, names, values):
    children = category.getChildren()
    keys = children.keys()
    keys.sort()
    for key in keys:
        c = children[key]
        if not forCategory or forCategory.canHaveAsParent(c):
            names.append(c.asLinkedParentage(includeNotebook=0))
            values.append(c.asPathInfo(0))
            self._addSubcategories(forCategory, c, names, values)
NBCGI.NBCGI._addSubcategories = _addSubcategories

# Response to entries

def eventEntryPublished(entry):
    """Mirror the entry to one or more Internet Topic Exchange topics.
    TODO: Do this in background."""
    notebook = entry.notebook
    if notebook.internetTopicExchangeIntegration():
        title = entry.getTitle()
        entryURL = entry.getAbsoluteURL()
        excerpt = util.sample(entry.getProcessedText(), 255)
        blogName = entry.notebook.getTitle()

        client = Client()
        for category in entry.getCategories():
            mirror = category.topicExchangeTopic
            if mirror:
                client.ping(mirror, blogName, title, entryURL, excerpt, 1)

# Some hooks into the templating system

def interpolateCategoryListTemplate(self, viewer):
    """Interpolates the category list or category list detail template
    for this entry."""
    if len(self.getCategories()) and self.notebook.enableCategories():
        template = 'category-list'
        if viewer.detail:
            template = template + '-detail'
        self.notebook.interpolateTemplate(template,
                                          [CATEGORY_LIST_COMMAND_MAP,
                                           CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateCategoryListTemplate = interpolateCategoryListTemplate

def interpolateCategoryTemplateForEachCategory(self, viewer):
    """Interpolates the category template for each of this entry's
    categories."""
    cats = self.getCategories()
    if cats:
        viewer.iteratingOverFirstCategory = 1
        for category in cats:
            viewer.iteratedCategory = category
            self.notebook.interpolateTemplate('category',
                                              [CATEGORY_COMMAND_MAP,
                                               CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                              self, viewer, out=viewer.out)
            viewer.iteratingOverFirstCategory = 0
        viewer.iteratingOverFirstCategory = 0
        viewer.iteratedCategory = None
Entry.interpolateCategoryTemplateForEachCategory = interpolateCategoryTemplateForEachCategory

def getCommaIfNotFirstCategory(self, viewer):
    """Returns a comma unless we're iterating over the first category."""
    if viewer.iteratingOverFirstCategory:
        return ''
    else:
        return ','
Entry.getCommaIfNotFirstCategory = getCommaIfNotFirstCategory

def getLinkedCategoryName(self, viewer):
    """Returns the name of the viewer's iterated category."""
    link = ''
    if viewer.iteratedCategory:
        cat = viewer.iteratedCategory
        args = ''
        if viewer.edit:
            args = 'mode=edit'
        from CategoryScreens import CategoryListCGI
        link = self.getLink(cat.getURL(CategoryListCGI.NAME, args),
                            cat.asLinkedParentage(), 'entryCategory')
    return link
Entry.getLinkedCategoryName = getLinkedCategoryName

def getCategoryDescription(self, viewer):
    """Prints the description of the viewer's iterated category."""
    description = ''
    if viewer.iteratedCategory:
        description = viewer.iteratedCategory.getDescription()
    return description
Entry.getCategoryDescription = getCategoryDescription

def getNumberOfCategories(self):
    "Returns the number of categories under which this entry is filed."
    return str(len(self.getCategories()))
Entry.getNumberOfCategories = getNumberOfCategories

def getCategoryPlural(self):
    "Returns 'y' if this entry has one category, and 'ies' otherwise."
    return self._getPlural(len(self.getCategories()), 'y', 'ies')
Entry.getCategoryPlural = getCategoryPlural

CATEGORY_LIST_COMMAND_MAP = { 'category-numberOfCategories' : 'getNumberOfCategories',
                              'category-plural' : 'getCategoryPlural',
                              'category-each' : 'interpolateCategoryTemplateForEachCategory',
                              #Deprecated
                              'CP' : 'interpolateCategoryTemplateForEachCategory',
                              'cs' : 'getCategoryPlural',
                              }
    
CATEGORY_COMMAND_MAP = {'category-commaIfNotFirst' : 'getCommaIfNotFirstCategory',
                        'category-linkedName' : 'getLinkedCategoryName',
                        'category-description' : 'getCategoryDescription',

                        #Deprecated
                        'cn' : 'getLinkedCategoryName',
                        'cd' : 'getCategoryDescription'
                        }
CommandMaps.ENTRY_COMMAND_MAP['categories'] = 'interpolateCategoryListTemplate'
#Deprecated
CommandMaps.ENTRY_COMMAND_MAP['CL'] = 'interpolateCategoryListTemplate'
