import os

from nb import const
from nb import util
from nb.NBCGI import NBCGI
from nb.Viewer import Viewer
from nb.CoreCGIs import ViewCGI
from Category import Category, validCategoryName, parseCategoryPath

# Some CGI constants
CATEGORY_NAME_CGI_KEY = 'categoryName'
CATEGORY_DESCRIPTION_CGI_KEY = 'categoryDescription'
CATEGORY_PARENT_CGI_KEY = 'categoryParent'
CATEGORIES_CGI_KEY = 'categories'
ENTERED_CATEGORIES_CGI_KEY = 'enteredCategories'
INTERNET_TOPIC_EXCHANGE_TOPIC = 'internetTopicExchangeTopic'

class CategoryCGI(NBCGI):

    """Superclass of all category CGIs."""

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self,
                       pathInfoProcessor=self.processPathInfoAsCategoryPath,
                       pathInfo=pathInfo)

    def start(self, categoryPre, topLevelPre=None, suppressLast=0):
        cgi = CategoryListCGI.NAME
        args = 'mode=edit'
        if topLevelPre == None:
            topLevelPre = categoryPre        
        if self.category:
            usePre = categoryPre
            [linked, unlinked] = self.category.asLinkedParentage(cgi, args,
                                                                 suppressLast)
        else:
            usePre = topLevelPre
            [linked, unlinked] = ['<a href="%s">%s</a>' % (self.notebook.getURL(cgi, args), self.notebook.name),
                                  self.notebook.name]
        self.printHeader()
        self.startPage(usePre + unlinked)
        url = self.notebook.getURL(ViewCGI.NAME)
        notebook = '<a href="%s">%s</a>' % (url, self.notebook.getTitle())
        print '<div align="center"><h1 class="heading">%s</h1></div>' % notebook
        self.printHeading(usePre + linked)        

    def printCategoryNameControl(self, currentName=''):
        print '<p><b>'
        print _('Category name:')
        print '</b>'
        self.textField(CATEGORY_NAME_CGI_KEY, currentName, 50, 100)
        print '<br>'
        print _('Valid characters in category names: A-Z, a-z, 0-9, +-, comma, and space')
        print '</p>'

    def printCategoryParentSelection(self, forCategory=None,
                                     defaultCategory=None):
        (names, values) = self.getCategoryNamesAndFormValues(forCategory)
        names.insert(0, '[Top-level]')
        values.insert(0, '')
        default = ''
        if defaultCategory:
            default = defaultCategory.asPathInfo()
        print '<p><b>'
        print _('Parent:')
        print '</b>'
        self.listBox(CATEGORY_PARENT_CGI_KEY, names, values, default)
        print '</p>'

class CategoryAddCGI(CategoryCGI):

    """A CGI for adding a category."""

    NAME = 'category-add'

    def run(self):
        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.add()
        else:
            self.printForm()

    def add(self):
        self.assertVars([const.PASSWORD_CGI_KEY, CATEGORY_NAME_CGI_KEY])
        (givenPassword, categoryName, categoryDescription, topicExchangeTopic) = map(self.var, (const.PASSWORD_CGI_KEY, CATEGORY_NAME_CGI_KEY, CATEGORY_DESCRIPTION_CGI_KEY, INTERNET_TOPIC_EXCHANGE_TOPIC))
        if self.notebook.validatePassword(givenPassword):
            if not validCategoryName(categoryName):
                self.printError(_('%s is not a valid category name.') % self.categoryName)
            if self.category:                
                if self.category.getChildren().has_key(categoryName):
                    self.printError(_('The %s category already has a subcategory named "%s".') % (self.category.name, categoryName))
            else:
                if self.notebook.getCategories().has_key(categoryName):
                    self.printError('The %s notebook already has a top-level category named "%s".' % (self.notebook.name, categoryName))
            newCategory = Category(self.notebook, categoryName, self.category)
            newCategory.description = categoryDescription            
            if self.notebook.internetTopicExchangeIntegration():
                newCategory.topicExchangeTopic = topicExchangeTopic
            newCategory.write()
            parent = newCategory.getParent()
            if not parent:
                parent = newCategory.notebook                
            self.redirectTo(parent.getURL(CategoryListCGI.NAME, 'mode=edit'))

        else:
            self.printError(_('Incorrect password.'))

    def printForm(self):
        self.start(_('Adding subcategory to '), _('Adding category to notebook: '))
        if self.category:
            url = self.category.getURL(CategoryAddCGI.NAME)
        else:
            url = self.notebook.getURL(CategoryAddCGI.NAME)
        self.startForm(url)
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')

        print '<p>'
        self.passwordField()
        print '</p>'
        self.printCategoryNameControl()
        print '<p><b>'
        print _('Category description:')
        print '</b>'
        self.textField(CATEGORY_DESCRIPTION_CGI_KEY, '', 100, None)
        print '</p>'
        if self.notebook.internetTopicExchangeIntegration():
            print '<p><b>'
            print _('Internet Topic Exchange topic:')
            print '</b>'
            self.textField(INTERNET_TOPIC_EXCHANGE_TOPIC, '', 20, None)
            print '[<a href="http://topicexchange.com/topics">'
            print _('Topic list')
            print '</a>]'
            print '</p>'

        self.endForm()
        self.endPage()

class CategoryDeleteCGI(CategoryCGI):

    """A CGI for deleting a category."""

    NAME = 'category-delete'
        
    def run(self):
        if not self.category:
            self.printError(_('You did not specify a category to delete.'))
        if not self.category.canBeDeleted():
            self.printError(_("You can't delete the %s category until you delete its subcategories.") % self.name)

        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.delete()
        else:
            self.printForm()

    def delete(self):
        self.assertVars([const.PASSWORD_CGI_KEY])
        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        if self.notebook.validatePassword(givenPassword):
            parent = self.category.getParent()
            if not parent:
                parent = self.notebook
            self.category.delete()
            self.redirectTo(parent.getURL(CategoryListCGI.NAME, 'mode=edit'))
        else:
            self.printError(_('Incorrect password.'))

    def printForm(self):
        self.start('Deleting category: ')
        url = self.category.getURL(CategoryDeleteCGI.NAME)
        self.startForm(url)
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')

        print '<p>'
        self.passwordField()
        print '</p>'
        print '<p>'
        print _('Please confirm the deletion of this category.')
        print '</p>'

        self.endForm()
        self.endPage()

class CategoryEditCGI(CategoryCGI):

    """A CGI for editing a category."""

    NAME = 'category-edit'
        
    def run(self):
        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.edit()
        else:
            self.printForm()

    def edit(self):
        self.assertVars([const.PASSWORD_CGI_KEY, CATEGORY_NAME_CGI_KEY])
        (givenPassword, newParentPath, newCategoryName, categoryDescription, topicExchangeTopic) = map(self.var,(const.PASSWORD_CGI_KEY, CATEGORY_PARENT_CGI_KEY, CATEGORY_NAME_CGI_KEY, CATEGORY_DESCRIPTION_CGI_KEY, INTERNET_TOPIC_EXCHANGE_TOPIC))

        if not self.notebook.validatePassword(givenPassword):
            self.printError('Incorrect password.')

        #Possibly change the category's description or ITE topic.
        dirty = 0
        if categoryDescription != self.category.getDescription():
            self.category.description = categoryDescription
            dirty = 1
        if self.notebook.internetTopicExchangeIntegration():
            if topicExchangeTopic != self.category.topicExchangeTopic:
                self.category.topicExchangeTopic = topicExchangeTopic
                dirty = 1
        if dirty:
            self.category.write()

        #Possibly relocate the category.
        newParent = parseCategoryPath(self.notebook, newParentPath)
        if newParent != self.category.parent:
            self.category.changeParent(newParent)

        #Now, possibly rename the category.
        if not validCategoryName(newCategoryName):
            self.printError(_('%s is not a valid category name.') % self.newCategoryName)
        if self.category.name != newCategoryName:
            if self.category.getParentMap().has_key(newCategoryName):
                self.printError(_('There is already a category called "%s" at this level.') % newCategoryName)
            self.category.rename(newCategoryName)
        self.redirectTo(self.category.getURL(CategoryListCGI.NAME, 'mode=edit'))

    def printForm(self):
        self.start(_('Editing category: '))
        self.startForm(self.category.getURL(CategoryEditCGI.NAME))
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')

        print '<p>'
        self.passwordField()
        self.printCategoryParentSelection(self.category, self.category.parent)
        print '</p>'
        self.printCategoryNameControl(self.category.name)
        print '<p><b>'
        print _('Category description:')
        print '</b>'
        self.textField(CATEGORY_DESCRIPTION_CGI_KEY, self.category.getDescription(), 100, None)
        print '</p>'
        if self.notebook.internetTopicExchangeIntegration():
            print '<p><b>'
            print _('Internet Topic Exchange topic:')
            print '</b>'
            self.textField(INTERNET_TOPIC_EXCHANGE_TOPIC, self.category.topicExchangeTopic, 20, None)
            if self.category.topicExchangeTopic:
                print '[<a href="http://topicexchange.com/t/%s">'
                print _('View topic ')
                print '"%s"</a>]' % (self.category.topicExchangeTopic,
                self.category.topicExchangeTopic)
            print '[<a href="http://topicexchange.com/topics">'
            print _('Topic list')
            print '</a>]'
            print '</p>'

        self.endForm()
        self.endPage()

class CategoryListCGI(CategoryCGI):

    """A CGI for listing categories."""

    NAME = 'category'
    CGI_ALIASES = ['categories']
        
    def run(self):
        baseURL = os.path.join(CategoryListCGI.NAME, self.notebook.name)
        edit = 0
        if self.var('mode') == 'edit':
            edit = 1        
        notebookURL = baseURL
        args = ''
        if edit:
            args = 'mode=edit'
            notebookURL = notebookURL + args

        s = _('Categories:') + ' '
        self.start(s, s, 1)

        if edit:
            if self.category:
                url = self.category.getURL(CategoryAddCGI.NAME)
                text = _('Add new subcategory')
            else:
                url = self.notebook.getURL(CategoryAddCGI.NAME)
                text = _('Add new top-level category')
            self.panelLink(url, text)

            if self.category:
                self.panelLink(self.category.getURL(CategoryEditCGI.NAME), _('Edit'))
                if self.category.canBeDeleted():
                    self.panelLink(self.category.getURL(CategoryDeleteCGI.NAME), 'Delete')
        if self.category and self.category.getDescription():
            print '<p class="categoryDescription">%s</p>' % self.category.getDescription()

        self.displayCategories(edit, args)
        
        if self.category:
            self.displayEntries(edit, args)
        self.endPage()

    def displayCategories(self, edit, args):
        if self.category:
            categories = self.category.getChildren()
        else:
            categories = self.notebook.getCategories()

        if categories:
            url = CategoryListCGI.NAME + self.categoryPath
            print '<ul>'
            categoryNames = categories.keys()
            categoryNames.sort()
            for name in categoryNames:
                self.printCategory(categories[name], edit, url, args)
            print '</ul>'
        elif not self.category:
            print '<p class="alert">'
            print _('No categories yet.')
            print '</p>'

    def printCategory(self, category, edit, baseURL, args):
        url = os.path.join(baseURL, category.name)
        print '<li>'
        self.linkTo(category.getURL(CategoryListCGI.NAME, args), category.name)
        if edit:
            self.panelLink(category.getURL(CategoryAddCGI.NAME), _('Add sub'))
            self.panelLink(category.getURL(CategoryEditCGI.NAME), _('Edit'))
            if category.canBeDeleted():
                self.panelLink(category.getURL(CategoryDeleteCGI.NAME),
                               _('Delete'))
        children = category.getChildren()        
        if children:
            keys = children.keys()
            keys.sort()
            print '<ul>'
            for key in keys:
                self.printCategory(children[key], edit, url, args)
            print '</ul>'
    
    def displayEntries(self, edit, args):
        entries = self.category.getEntries()
        
        if entries:
            keys = entries.keys()
            keys.sort(util.cmpFilename)
            keys.reverse()
            print '<hr/>'
            newEntries = []
            for key in keys:
                newEntries.append(entries[key])
            Viewer(self.notebook, newEntries, edit=edit).render()
        else:
            print '<p class="alert">'
            print _('No filed entries.')
            print '</p>'
