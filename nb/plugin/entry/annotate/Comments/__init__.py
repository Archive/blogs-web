import os
import string
import time
import types

import nb
from nb import const
from nb import CommandMaps
from nb import NBConfig
from nb import Notebook
from nb import util
from nb.Entry import Entry
from nb.CoreCGIs import ViewCGI, EditCGI
from nb import NBCGI
from nb.lib import HTMLStripper
from nb.plugin.entry.annotate import ExternalAnnotation, makeStorable
from nb.CoreCGIs import DispatchCGI
from nb import CoreCGIs
import CommentScreens

DispatchCGI.cgiModules.append(CommentScreens.__name__)

def commentsEnabled(self):
    """Returns true iff comments are enabled, in general, for this
    notebook. Do not use this unless this is really what you
    mean--comments might be enabled in general but disabled for the
    entry you're looking at."""
    return self.getOptionValue('enable-comments')
Notebook.Notebook.commentsEnabled = commentsEnabled

def commentsEnabledOnPerEntryBasis(self):
    """Returns true iff comments are to be enabled on a per-entry basis."""
    return self.commentsEnabled() and self.getOptionValue('comments-enabled-on-selected-entries-only')
Notebook.Notebook.commentsEnabledOnPerEntryBasis = commentsEnabledOnPerEntryBasis

def commentCutoffTime(self):
    """Returns the earliest entry date for an entry that can have a
    comment posted to it right now."""
    cutoffDays = int(self.getOptionValue('comment-cutoff-days'))
    cutoff = None
    if cutoffDays > 0:
        cutoff = time.localtime(time.time() - (60*60*24*cutoffDays))
    return cutoff
Notebook.Notebook.commentCutoffTime = commentCutoffTime

def commentsEnabledForEntry(self):
    notebook = self.notebook
    enabled = self.notebook.commentsEnabled() 
    if enabled:
        enabled = (not self.notebook.commentsEnabledOnPerEntryBasis() or getattr(self, ENABLE_COMMENTS, None) == 'Y') 
    if enabled:
        cutoffTime = self.notebook.commentCutoffTime()
        if cutoffTime:
            enabled = (cutoffTime < self.getTimeTuple())
    return enabled
Entry.commentsEnabled = commentsEnabledForEntry

#Add a hook to index the text and authors of this entry's comments.
def indexComments(self, gatherer):
    """Indexes this entry's comments."""
    for comment in self.getComments():
        if comment.quality < comment.UNCONFIRMED_BAD:
            gatherer.feed(comment.authorName, CommentScreens.COMMENT_NAME)
            gatherer.feed(comment.text, CommentScreens.COMMENT_TEXT)
nb.Entry.registerIndexHook(indexComments)

COMMENTS = 'comment'

class Comment(ExternalAnnotation):
    """A simple data structure enabling the data from comments to be
    accessed from templates."""
    
    def __init__(self, entry, date, ip, authorName, authorEmail, url, title,
                 text, quality=None):
        self.entry = entry
        self.date = date
        self.ip = ip
        self.authorName = authorName
        self.authorEmail = authorEmail
        self.url = url
        self.title = title
        self.text = text
        if quality is None:
            self.quality = self.UNKNOWN_STATUS
            self.calculateQuality()
        else:
            self.quality = int(quality)

    def getIPAddress(self, viewer):
        return self.ip

    def getAuthorName(self):
        return self.authorName

    def getAuthorEmail(self):
        return self.authorEmail

    def getAuthorDesignant(self):
        name = self.authorName
        if not name:
            if self.authorEmail:
                name = self.authorEmail
            else:
                name = 'anonymous@' + self.ip
        return name

    def getAuthorContactLink(self):
        name = self.getAuthorDesignant()
        email = self.authorEmail
        linkText = self.url
        if not linkText:
            if email:
                linkText = 'mailto:' + email
        if linkText:
            return '<a class="commentAuthor" href="%s">%s</a>' % (linkText, name)
        else:
            return name

    def asOutputString(self):
        "Formats this object as an output string fragment."
        return string.join((self.date, self.ip, self.authorName,
                            self.authorEmail, self.url, self.title,
                            self.text, str(self.quality)), '|')

    def getBayesianClassificationTokens(self):
        tokens = ExternalAnnotation.getBayesianClassificationTokens(self)
        self._appendTokens(tokens, self.authorName, '_noauthorName')
        if self.authorEmail:
            tokens.append(self.authorEmail)
        else:
            tokens.append('_noauthorEmail')
        return tokens

#Now we tack some methods on to the Entry class.

def getComments(self):
    if not hasattr(self, 'comments'):
        self.comments = []
    return self.comments
Entry.getComments = getComments

def addComment(self, ip, authorName, authorEmail, url, title, text,
               postTime=None, authed=0):

    """Adds a comment to this entry from an untrusted outside
    source. Returns 1 iff the comment was accepted; if otherwise
    (because it appears to be spam), returns 0."""
    if not postTime:
        postTime = time.time()
    authorName = makeStorable(authorName)
    authorEmail = makeStorable(authorEmail)
    url = makeStorable(url)
    title = makeStorable(title)
    text = makeStorable(text, 1)
    comment = Comment(self, str(postTime), ip, authorName, authorEmail, url,
                      title, text)
    ret = 0
    badProbability = comment.badProbabilityPercentage()
    if authed or badProbability < int(self.notebook.spamCutoffPercentage()):
        if authed:
            #They know the password, so if they're a spammer we've got bigger
            #problems than complaining that their spam violates our Bayesian
            #sensibilities. And if they're not a spammer, it's good to build
            #up the known good corpus.
            comment.quality = comment.CONFIRMED_GOOD
        self.getComments().append(comment)
        self.notebook.nbconfig.pluginEvent('CommentPublished',
                                           [comment, authed])
        ret = 1
    return ret
Entry.addComment = addComment

def countComments(self):
    "Returns the number of non-bad comments for this entry."
    i = 0
    for comment in self.getComments():        
        if comment.quality < comment.UNCONFIRMED_BAD:
            i = i + 1
    return str(i)
Entry.countComments = countComments

## Some Entry methods used by the template system.

def getCommentsPlural(self):
    "Returns the appropriate plural suffix for 'comment'"
    return self._getPlural(self.countComments())
Entry.getCommentsPlural = getCommentsPlural

def getMostRecentCommentIPAddress(self):
    """Returns the IP address of the most recent comment, or
    the empty string if there are no comments"""
    return getMostRecentCommentField('ip')

def getMostRecentCommentDate(self):
    """Returns the date of the most recent comment, or
    the empty string if there are no comments"""
    return getMostRecentCommentField('date')

def getMostRecentCommentAuthorContactLink(self):
    """Returns the author contact link for the most recent
    comment, or the empty string if there are no comments"""
    comments = self.getComments()
    if comments:
        return comments[0].getAuthorContactLink()
    else:
        return ''

def getMostRecentCommentTitle(self):
    """Returns the title of the most recent comment, or the empty
    string if there are no comments"""
    return getMostRecentCommentField('title')

def getMostRecentCommentText(self):
    """Returns the description of the most recent comment,
    or the empty string if there are no comments"""
    return getMostRecentCommentField('text')

def _getMostRecentCommentField(self, field):
    """Returns the field value of the most recent comment, or
    the empty string if there are no comments"""
    comments = self.getComments()
    if comments:
        return getattr(comments[0], field)
    else:
        return ""
Entry._getMostRecentCommentField=_getMostRecentCommentField

def interpolateCommentTemplate(self, viewer):
    "Interpolates the comment template for this entry."
    numberOfComments = int(self.countComments())
    if numberOfComments > 0 or self.commentsEnabled():
        template = 'comment-list'
        if viewer.detail:
            template = template + '-detail'
        if not numberOfComments:
            template = 'empty-' + template
        self.notebook.interpolateTemplate(template,
                                          [COMMENT_LIST_COMMAND_MAP,
                                           CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateCommentTemplate = interpolateCommentTemplate

def interpolateCommentSummaryTemplate(self, viewer):
    "Interpolates the comment summary template for this entry."
    numberOfComments = int(self.countComments())
    if numberOfComments > 0 or self.commentsEnabled():
        template = 'comment-summary'
        if viewer.detail:
            template = template + '-detail'
        if not numberOfComments:
            template = 'empty-' + template
        self.notebook.interpolateTemplate(template,
                                          [COMMENT_SUMMARY_COMMAND_MAP,
                                          COMMENT_LIST_COMMAND_MAP,
                                          CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateCommentSummaryTemplate = interpolateCommentSummaryTemplate

def interpolateCommentTemplateForEachComment(self, viewer):
    """Interpolates the comment template for each of this entry's
    comments."""
    comments = self.getComments()
    if comments:
        viewer.iteratingOverFirstComment = 1
        template = 'comment'
        if viewer.detail:
            template = template + '-detail'
        for comment in comments:
            if comment.calculateQuality() < comment.UNCONFIRMED_BAD:
                viewer.iteratedComment = comment
                self.notebook.interpolateTemplate(template,
                                                  [COMMENT_COMMAND_MAP,
                                                   COMMENT_LIST_COMMAND_MAP,
                                                   CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                                  comment, viewer,
                                                  out=viewer.out)
                viewer.iteratingOverFirstComment = 0
        viewer.iteratingOverFirstComment = 0
        viewer.iteratedComment = None
    else:
        template = 'empty-comment-list'
        self.notebook.interpolateTemplate(template,
                                          [COMMENT_LIST_COMMAND_MAP,
                                           CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateCommentTemplateForEachComment = interpolateCommentTemplateForEachComment

COMMENT_LIST_COMMAND_MAP = {'comment-numberOfComments' : 'countComments',
                            'comment-plural' : 'getCommentsPlural',
                            'comment-each' : 'interpolateCommentTemplateForEachComment',
                            'comment-form' : 'printCommentAddForm'}

COMMENT_SUMMARY_COMMAND_MAP = {'comment-most-recent-ip' : 'getMostRecentIncmoingCommentIPAddress',
                               'comment-most-recent-date' : 'getMostRecentCommentDate',
                               'comment-most-recent-authorContact' : 'getMostRecentCommentAuthorContactLink',
                               'comment-most-recent-text' : 'getMostRecentCommentText'
                               }

COMMENT_COMMAND_MAP = { 'comment-ip' : 'getIPAddress',
                        'comment-date' : 'getDate',
                        'comment-authorContact' : 'getAuthorContactLink',
                        'comment-text' : 'getText'
                        }

CommandMaps.ENTRY_COMMAND_MAP['comments'] = 'interpolateCommentTemplate'
CommandMaps.ENTRY_COMMAND_MAP['comment-summary'] = 'interpolateCommentSummaryTemplate' 

#Now some hook methods.

# A hook method for representing an entry's categories

def parseComment(self, commentString):
    """Used to instantiate an comment from a NewsBruiser entry
    on disk."""
    l = string.split(commentString, '|')
    l.insert(0, self)
    self.getComments().append(apply(Comment, l))
nb.Entry.registerInstantiationFromFileHook(COMMENTS, parseComment)

def enableCommentsAsOutputString(entry):
    """Exports to the entry file whether or not comments are enabled
    for this entry."""
    enabled = ""
    if entry.notebook.commentsEnabledOnPerEntryBasis():        
        if entry.commentsEnabled():
            enabled = "Y"
        else:
            enabled = "N"
    enabled = getattr(entry, ENABLE_COMMENTS, enabled)
    return '%s: %s\n' % (ENABLE_COMMENTS, enabled)
nb.Entry.registerAsOutputStringHook(enableCommentsAsOutputString)

def commentsAsOutputString(self):
    """Formats an entry's comments as a series of lines suitable for
    storing in the given entry's disk representation."""
    return string.join(map(lambda(x): COMMENTS + ': ' + x.asOutputString(),
                           self.getComments()), '\n')
nb.Entry.registerAsOutputStringHook(commentsAsOutputString)

#A hook to give an interface to turning comments on.
ENABLE_COMMENTS = 'enableComments'
def printCommentEnablingControl(cgi, entry=None):
    if cgi.notebook.commentsEnabledOnPerEntryBasis():
        default = None
        if entry:
            default = entry.commentsEnabled()
        print '</td><td valign="top">'
        cgi.checkbox(ENABLE_COMMENTS, default, 1)
        print "Enable comments?"
def printCommentEnablingControlForEdit(cgi):
    printCommentEnablingControl(cgi, cgi.entries[0])
NBCGI.registerDisplaySnippet(CoreCGIs.AddCGI, 'belowEntryText',
                             printCommentEnablingControl, 1)
NBCGI.registerDisplaySnippet(CoreCGIs.EditCGI, 'belowEntryText',
                             printCommentEnablingControlForEdit, 1)

def gatherEnableComments(vars, entry):
    enabled = vars.get(ENABLE_COMMENTS)
    if enabled == 'Y':
        enabled = 'Y'
    else:
        enabled = 'N'
    setattr(entry, ENABLE_COMMENTS, enabled)    
nb.Entry.registerInstantiationFromInputHook(gatherEnableComments)

def gatherComments(vars, entry):
    "Comments can be part of the 'input' if we're importing."
    commentVars = vars.get(COMMENTS, [])
    for i in commentVars:
        if type(i) == types.DictionaryType:
            ip = i.get(CommentScreens.COMMENT_IP, os.environ.get('REMOTE_ADDR',
                                                                 '127.0.0.1'))
            name = i.get(CommentScreens.COMMENT_NAME, '')
            email = i.get(CommentScreens.COMMENT_EMAIL, '')
            url = i.get(CommentScreens.COMMENT_URL, '')
            text = i.get(CommentScreens.COMMENT_TEXT, '')
            commentTime = i.get(CommentScreens.COMMENT_TIME, time.time())
            entry.addComment(ip, name, email, url, '', text, commentTime)
nb.Entry.registerInstantiationFromInputHook(gatherComments)

#A hook to give an interface to adding comments

def printCommentAddForm(self):
    if not self.entries or not self.entries[0].commentsEnabled() or not self.viewer.detail:
        return
    else:
        print '<hr>'
        CommentScreens.printCommentAddForm(self, 2)

NBCGI.registerDisplaySnippet(ViewCGI, 'belowEntries', printCommentAddForm, 10)

#A hook to give an interface to deleting comments you
#don't like.

CLASSIFY_COMMENT_ACTION = 'classifyComment'
DELETE = CLASSIFY_COMMENT_ACTION + 'Delete'
IGNORE = CLASSIFY_COMMENT_ACTION + 'Ignore'
CONFIRM_BAD = CLASSIFY_COMMENT_ACTION + 'Confirmbad'
CONFIRM_GOOD = CLASSIFY_COMMENT_ACTION + 'Confirmgood'
CLASSIFY_COMMENTS = 'classifyComments'

def printCommentsForEdit(self):
    entry = self.entries[0]
    comments = entry.getComments()
    if comments:
        print '<h2>Comments</h2>'
        print '<table><tr class="optionNamePanel"><th>Date</th><th>Author</th><th>Comment</th><th>Spam/Abusive?</th><th>Action</th></tr>'
        classifier = self.notebook.getBayesianClassifier()
        for comment in comments:
            text = comment.text
            if len(text) > 252:
                text = text[:253] + '...'
            print '<tr><td>%s</td> <td>%s at %s</td> <td>%s</td>' % (comment.getDate(), comment.getAuthorContactLink(), comment.ip, HTMLStripper.strip(text))
            whatToDo = IGNORE
            t = "Don't know"
            comment.calculateQuality()
            if comment.unconfirmedBad():
                t = 'Probably (%.2f%%)' % comment.badProbabilityPercentage()
                whatToDo = CONFIRM_BAD
            elif comment.unconfirmedGood():
                t = 'Probably not (%.2f%%)' % comment.badProbabilityPercentage()
                whatToDo = CONFIRM_GOOD
            elif comment.confirmedGood():
                t = 'No'
                whatToDo = IGNORE
            print '<td>%s</td><td>' % t
            selectionOptions = [IGNORE, DELETE, CONFIRM_BAD]
            selectionTexts = ['Do nothing', 'Delete',
                              'Mark as bad']
            if not comment.confirmedGood():
                selectionOptions.insert(0, CONFIRM_GOOD)
                selectionTexts.insert(0, 'Mark as good')
            self.listBox(CLASSIFY_COMMENT_ACTION + ' ' + comment.date, selectionTexts, selectionOptions, whatToDo)
            print '</td></tr>'
        print '<tr><td colspan="4"></td><td>'
        self.submitButton('Perform actions', CLASSIFY_COMMENTS)
        print '</td></tr></table>'
NBCGI.registerDisplaySnippet(EditCGI, 'belowSubmitButton', printCommentsForEdit, 1)

def takeCommentActions(self):
    self.notebook.assertValidPassword(self.var(const.PASSWORD_CGI_KEY))
    entry = self.entries[0]
    comments = entry.getComments()
    entryDirty = 0
    classifierDirty = 0
    classifier = entry.notebook.getBayesianClassifier()
    for var in self.variables.keys():
        i = string.find(var, CLASSIFY_COMMENT_ACTION)
        if i <> -1:
            key = var[i+len(CLASSIFY_COMMENT_ACTION)+1:]
            value = self.variables[var]
            for comment in comments:
                if comment.date == key:
                    if value == DELETE:
                        comments.remove(comment)
                        entryDirty = 1
                    elif value == CONFIRM_BAD:
                        comment.trainBad()
                        comments.remove(comment)
                        entryDirty = 1
                        classifierDirty = 1
                    elif value == CONFIRM_GOOD:
                        comment.trainGood()
                        comment.quality = comment.CONFIRMED_GOOD
                        entryDirty = 1
                        classifierDirty = 1
    if entryDirty:
        for comment in entry.getComments():
            if comment.quality > comment.CONFIRMED_GOOD:
                comment.calculateQuality()
        entry.write()    
    if classifierDirty:
        classifier.save()
        
    self.printStatus('Action(s) completed.')
    self.printForm()

NBCGI.registerHookAction(EditCGI, CLASSIFY_COMMENTS, takeCommentActions)
