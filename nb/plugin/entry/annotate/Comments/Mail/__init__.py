from nb import util
from nb import CoreCGIs
from nb.plugin.entry.annotate import Comments
from nb.lib import HTMLStripper
import string
import smtplib
import urllib

MAIL_HOST = 'localhost'

def eventCommentPublished(commentAuthed):
    comment, authed = commentAuthed
    entry = comment.entry
    notebook = entry.notebook
    if not notebook.getOptionValue('email-comments'):
        return
    to = notebook.getOptionValue('contact-email')
    example = to.find('@example.com')
    if example != -1 and example == len(to) - len('@example.com'):
        return

    #If we can be certain that the user in charge of the weblog is the
    #one who posted the comment, we can forgo the mail.  This happens
    #if the user was authenticated, and either there's only one author
    #of the weblog, or the user's given email is the same as the
    #contact email.
    checkEmail = comment.authorEmail
    if authed:
        if checkEmail == to or (not notebook.freeformAuthor() and len(notebook.getAuthors()) < 2):
            return

    text = HTMLStripper.strip(comment.text, validTags=[])
    excerpt = text[:50]
    if excerpt != text:
        excerpt = excerpt + '...'
    subject = _("New comment: '%s'" % excerpt)

    poster = comment.getAuthorDesignant()
    url = comment.url
    if not url:
        url = comment.authorEmail
    if url:
        url = _(' (%s)' % url)
    poster = _('%(person)s%(url)s') % { 'person' : poster,
                                        'url' : url}

    query = "%s+%s=%s&classifyComments=%s" % (Comments.CLASSIFY_COMMENT_ACTION, comment.date, Comments.CONFIRM_BAD, urllib.quote_plus(_('Perform actions')))
    spamURL = entry.id.asLink(CoreCGIs.EditCGI.NAME, query)
    spamURL = util.urljoin(notebook.getExportedHost(), spamURL)
    body = [_("A comment has been posted to %s.")  % entry.getAbsoluteURL(),
            _("It was posted by %s. Here it is:") % poster,
            "",
            ' ' + string.replace(util.wrap(text), '\n', '\n '),
            "",
            "---",
            _("Comment posted from %(ip)s at %(date)s") % { 'ip' : comment.ip,
                                                            'date' : comment.getDate()},
            _("To mark this comment as spam, go to %s") % spamURL
            ]
            
    #Do not I18N these; they're email headers.
    headers = ['To: %s' % to,
               'From: %s <%s>' % (notebook.getTitle(), to),
               'Subject: %s' % subject]
    msg = string.join(headers, '\r\n') + '\r\n\r\n' + string.join(body, '\r\n')
    server = smtplib.SMTP(MAIL_HOST)
    server.sendmail('noreply@gnome.org', to, msg)
    server.quit()


