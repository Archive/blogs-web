import cgi
import os
import urllib

from nb.NBCGI import NBCGI
from nb.CoreCGIs import ViewCGI

ADD_COMMENT = 'addComment'
COMMENT_NAME = 'commentName'
COMMENT_EMAIL = 'commentEmail'
COMMENT_URL = 'commentURL'
COMMENT_TEXT = 'commentText'
REMEMBER_ME = 'commentRememberMe'
COMMENT_IP = 'commentIP' #Not part of the forms, but used when importing.
COMMENT_TIME = 'commentTime' #Ditto

COMMENT_INFO_COOKIE='NewsBruiserCommentInfo'

def printCommentAddForm(self, formLevel=1):
    if not self.id.draft:
        self.parseCookies()
        cookie = self.cookies.get(COMMENT_INFO_COOKIE)
        cookieMap = {}
        if cookie and not getattr(self, 'writingToFile', 0):
            if cookie == 'none':
                cookieMap['none'] = 1                
            else:            
                m = cgi.parse_qs(self.decodeCookie(cookie))
                for key, vals in m.items():
                    cookieMap[key] = vals[0]
        entry = self.entries[0]
        print _('<h%s>') % (formLevel)
        print _('Post a comment')
        print _('</h%s>') % (formLevel)
        self.startForm(entry.id.asLink(CommentAddScreen.NAME),
                       name='NBCommentForm')
        print '<table><tr><td>'
        print '<b>'
        print _('Your name:')
        print '</b></td><td>'
        self.textField(COMMENT_NAME, cookieMap.get('name'), 30, 50)
        print '</td><td colspan="3" valign="top">'
        print '</tr>'
        print '<tr><td><b>'
        print _('Your email address:')
        print '</b></td><td>'
        self.textField(COMMENT_EMAIL, cookieMap.get('email'), 30, 50)
        print '</td></tr><tr><td><b>'
        print _('Your home page:')
        print '</b></td><td>'
        self.textField(COMMENT_URL, cookieMap.get('url'), 30, 100)
        print '</td></tr><tr><td><b>'
        print _('Remember this information')
        print '</b></td><td>'
        self.checkbox(REMEMBER_ME, not cookieMap.has_key('none'))
        print '</td></tr></table><p><b>'
        print _('Comments:')
        print '</b>'
        self.textArea(10, 60, COMMENT_TEXT)
        self.submitButton(_('Post comment'), ADD_COMMENT)
        self.endForm(0)
        if getattr(self, 'writingToFile', 0):
            print """
        <script language="JavaScript">
function getCookie(name)
{
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1)
    {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1)
    {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

function setValueFromCookie(c, field, key)
{
 var s = c.indexOf(key + "=");
 if (s > -1)
 {
  s += key.length + 1;
  var e = c.indexOf("&",s);
  if (e == -1)
  {
   e = c.length;
  }
  field.value = unescape(c.substring(s,e).replace("+", " "));
 }
}

var c = getCookie("NewsBruiserCommentInfo");
setValueFromCookie(c, document.NBCommentForm.commentName, 'name');
setValueFromCookie(c, document.NBCommentForm.commentEmail, 'email');
setValueFromCookie(c, document.NBCommentForm.commentURL, 'url');

</script>
"""


class CommentAddScreen(NBCGI):

    NAME = 'comment-add'

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, pathInfo=pathInfo)
        if not self.notebook.commentsEnabled():
            self.printError(_("Comments are not enabled for this notebook."))
        if self.variables.has_key(ADD_COMMENT):            
            self.addComment()
        else:
            self.startPage(_('Add a comment to %s') % self.notebook.getTitle())
            printCommentAddForm(self)
            self.endPage()

    def addComment(self):
        if not self.id.draft:
            self.assertVars([COMMENT_TEXT])
            ip = os.environ.get('REMOTE_ADDR', '127.0.0.1')
            authorName = self.var(COMMENT_NAME, '')
            authorEmail = self.var(COMMENT_EMAIL, '')
            url = self.var(COMMENT_URL, '')
            text = self.var(COMMENT_TEXT, '')

            if self.variables.get(REMEMBER_ME + '-checkbox'):
                val = urllib.urlencode({'name': authorName,
                                        'email': authorEmail,
                                        'url': url})
                self.setCookie(COMMENT_INFO_COOKIE, val)
            elif self.variables.has_key(REMEMBER_ME):
                self.setCookie(COMMENT_INFO_COOKIE, 'none')

            entry = self.entries[0]
            if not entry.commentsEnabled():
                self.printError(_("Comments are not enabled for this entry."))
            spam = entry.addComment(ip, authorName, authorEmail, url, '', text, authed=getattr(self, 'preAuth', 0))
            if spam == 0:
                self.printError(_("I suspect that your comment is spam, abusive, or otherwise inappropriate. You might want to try rewording your comment."))
            else:
                entry.write()
            if entry.getComments()[-1].qualityPreventsDisplay():
                print 'Content-type: text/plain\n'
                print _('I suspect that your comment is abusive, spam, or otherwise inappropriate. It will not be visible until the site owner verifies that it is appropriate.')
            else:
                self.redirectTo(entry.id.asLink(ViewCGI.NAME))
