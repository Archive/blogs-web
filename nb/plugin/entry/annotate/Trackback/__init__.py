import string
import os
import time
import types
import urlparse

import nb
from nb import CommandMaps
from nb import NBConfig
from nb import Notebook
from nb import util
from nb.Entry import Entry
from nb.CoreCGIs import DispatchCGI, EditCGI
from nb import NBCGI
from nb.lib import HTMLStripper
from nb.plugin.entry.annotate import ExternalAnnotation, makeStorable
import TrackbackScreens
from TrackbackScreens import TrackbackPingScreen

DispatchCGI.cgiModules.append(TrackbackScreens.__name__)

def incomingTrackbackEnabled(self):
    return self.getOptionValue('enable-incoming-trackback')
Notebook.Notebook.incomingTrackbackEnabled = incomingTrackbackEnabled

INCOMING_TRACKBACKS = 'incomingTrackbacks'
INCOMING_TRACKBACK = 'incomingTrackback'

#Add a hook to index the text and authors of an entry's trackbacks.
def indexTrackbacks(self, gatherer):
    """Indexes this entry's trackbacks."""
    for trackback in self.getIncomingTrackbacks():
        if trackback.quality < trackback.UNCONFIRMED_BAD:
            gatherer.feed(trackback.blogname, 'trackback-blogname')
            gatherer.feed(trackback.title, 'trackback-entry-title')
            gatherer.feed(trackback.text, 'trackback-entry-text')
nb.Entry.registerIndexHook(indexTrackbacks)

class IncomingTrackback(ExternalAnnotation):
    """A simple data structure enabling the data from incoming
    trackbacks to be accessed from templates."""
    
    def __init__(self, entry, date, ip, blogname, url, title, text,
                 quality=None):
        self.entry = entry
        self.date = date
        self.ip = ip
        self.blogname = blogname
        self.url = url
        self.title = title
        self.text = text
        if quality is None:
            self.quality = self.UNKNOWN_STATUS
            self.calculateQuality()
        else:
            self.quality = int(quality)

    def getBlogname(self):
        return self.blogname

    def asOutputString(self):
        "Formats this object as an output string fragment."
        return string.join((self.date, self.ip, self.blogname, self.url,
                            self.title, self.text, str(self.quality)), '|')

    def getBayesianClassificationTokens(self):
        tokens = ExternalAnnotation.getBayesianClassificationTokens(self)
        self._appendTokens(tokens, self.blogname, '_noblogname')
        return tokens

#Now we tack some methods on to the Entry class.

def getIncomingTrackbacks(self):
    if not hasattr(self, INCOMING_TRACKBACKS):
        self.incomingTrackbacks = []
    return self.incomingTrackbacks
Entry.getIncomingTrackbacks = getIncomingTrackbacks

def addIncomingTrackback(self, ip, blogName, url, title, text, postTime=None):

    """Adds an incoming trackback to this entry from an untrusted
    outside source. Returns 1 iff the trackback was accepted; if
    otherwise (because it appears to be spam), returns 0."""

    if not postTime:
        postTime = time.time()
    postTime = str(postTime)

    blogName = makeStorable(blogName)
    url = makeStorable(url)
    title = makeStorable(title)
    text = makeStorable(text, 1)
    trackback = IncomingTrackback(self, postTime, ip, blogName, url,
                                  title, text)
    ret = 0
    badProbability = trackback.badProbabilityPercentage()
    if badProbability < int(self.notebook.spamCutoffPercentage()):
        self.getIncomingTrackbacks().append(trackback)
        ret = 1
    return ret
Entry.addIncomingTrackback = addIncomingTrackback

def countIncomingTrackbacks(self):
    "Returns the number of good incoming trackbacks for this entry."
    i = 0
    for trackback in self.getIncomingTrackbacks():
        if trackback.quality < trackback.UNCONFIRMED_BAD:
            i = i + 1
    return str(i)
Entry.countIncomingTrackbacks = countIncomingTrackbacks

def getTrackbackPingURL(self):
    return util.urljoin(self.notebook.getExportedHost(),
                        self.id.asLink(TrackbackScreens.TrackbackPingScreen.NAME))
Entry.getTrackbackPingURL = getTrackbackPingURL

## Some Entry methods used by the template system.

def getIncomingTrackbacksPlural(self):
    "Returns the appropriate plural suffix for 'trackback'"
    return self._getPlural(self.countIncomingTrackbacks())
Entry.getIncomingTrackbacksPlural = getIncomingTrackbacksPlural

def getMostRecentIncomingTrackbackIPAddress(self):
    """Returns the blog name of the most recent incoming trackback, or
    the empty string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('ip')

def getMostRecentIncomingTrackbackDate(self):
    """Returns the blog name of the most recent incoming trackback, or
    the empty string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('date')

def getMostRecentIncomingTrackbackBlogname(self):
    """Returns the blog name of the most recent incoming trackback, or
    the empty string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('blogname')

def getMostRecentIncomingTrackbackURL(self):
    """Returns the URL to the most recent incoming trackback,
    or the empty string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('url')

def getMostRecentIncomingTrackbackTitle(self):
    """Returns the title of the most recent incoming trackback, or the empty
    string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('title')

def getMostRecentIncomingTrackbackText(self):
    """Returns the description of the most recent incoming trackback,
    or the empty string if there are no trackbacks"""
    return getMostRecentIncomingTrackbackField('text')

def _getMostRecentIncomingTrackbackField(self, field):
    """Returns the field value of the most recent incoming trackback, or
    the empty string if there are no trackbacks"""
    trackbacks = self.getIncomingTrackbacks()
    if trackbacks:
        return getattr(trackbacks[0], field)
    else:
        return ""
Entry._getMostRecentIncomingTrackbackField=_getMostRecentIncomingTrackbackField

def interpolateIncomingTrackbackTemplate(self, viewer):
    "Interpolates the incoming trackback template for this entry."
    if self.notebook.incomingTrackbackEnabled():
        template = 'trackback-list'
        if viewer.detail:
            template = template + '-detail'
        if not int(self.countIncomingTrackbacks()):
            template = 'empty-' + template
        self.notebook.interpolateTemplate(template,
                                          [TRACKBACK_LIST_COMMAND_MAP,
                                           CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateIncomingTrackbackTemplate = interpolateIncomingTrackbackTemplate

def interpolateIncomingTrackbackSummaryTemplate(self, viewer):
    "Interpolates the incoming trackback summary template for this entry."    
    if self.notebook.incomingTrackbackEnabled():
        template = 'trackback-summary'
        if viewer.detail:
            template = template + '-detail'
        if not int(self.countIncomingTrackbacks()):
            template = 'empty-' + template
        self.notebook.interpolateTemplate(template,
                                          [TRACKBACK_SUMMARY_COMMAND_MAP,
                                          TRACKBACK_LIST_COMMAND_MAP,
                                          CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateIncomingTrackbackSummaryTemplate = interpolateIncomingTrackbackSummaryTemplate

def interpolateTrackbackTemplateForEachTrackback(self, viewer):
    """Interpolates the trackback template for each of this entry's
    incoming trackbacks."""
    trackbacks = self.getIncomingTrackbacks()
    if trackbacks:
        viewer.iteratingOverFirstIncomingTrackback = 1
        template = 'trackback'
        if viewer.detail:
            template = template + '-detail'
        for trackback in trackbacks:
            if trackback.calculateQuality() < trackback.UNCONFIRMED_BAD:
                viewer.iteratedIncomingTrackback = trackback
                self.notebook.interpolateTemplate(template,
                                                  [TRACKBACK_COMMAND_MAP,
                                                   TRACKBACK_LIST_COMMAND_MAP,
                                                   CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                                  trackback, viewer,
                                                  out=viewer.out)
                viewer.iteratingOverFirstIncomingTrackback = 0
        viewer.iteratingOverFirstIncomingTrackback = 0
        viewer.iteratedIncomingTrackback = None
    else:
        template = 'empty-trackback-list'
        self.notebook.interpolateTemplate(template,
                                          [TRACKBACK_LIST_COMMAND_MAP,
                                           CommandMaps.BASIC_ENTRY_COMMAND_MAP],
                                          self, viewer, out=viewer.out)
Entry.interpolateTrackbackTemplateForEachTrackback = interpolateTrackbackTemplateForEachTrackback

TRACKBACK_LIST_COMMAND_MAP = {'trackback-numberOfTrackbacks' : 'countIncomingTrackbacks',
                              'trackback-plural' : 'getIncomingTrackbacksPlural',
                              'trackback-each' : 'interpolateTrackbackTemplateForEachTrackback' }

TRACKBACK_SUMMARY_COMMAND_MAP = {'trackback-most-recent-ip' : 'getMostRecentIncmoingTrackbackIPAddress',
                                 'trackback-most-recent-date' : 'getMostRecentIncomingTrackbackDate',
                                 'trackback-most-recent-blogname' : 'getMostRecentIncomingTrackbackBlogname',
                                 'trackback-most-recent-title' : 'getMostRecentIncomingTrackbackTitle',
                                 'trackback-most-recent-description' : 'getMostRecentIncomingTrackbackText'
                                 }

TRACKBACK_COMMAND_MAP = { 'trackback-ip' : 'getIPAddress',
                          'trackback-date' : 'getDate',
                          'trackback-blogname' : 'getBlogname',
                          'trackback-url' : 'getURL',
                          'trackback-title' : 'getTitle',
                          'trackback-description' : 'getText'
                          }

CommandMaps.ENTRY_COMMAND_MAP['trackbacks'] = 'interpolateIncomingTrackbackTemplate'
CommandMaps.ENTRY_COMMAND_MAP['trackback-summary'] = 'interpolateIncomingTrackbackSummaryTemplate' 
CommandMaps.BASIC_ENTRY_COMMAND_MAP['entry-trackback-url'] = 'getTrackbackPingURL'
CommandMaps.ENTRY_COMMAND_MAP['entry-trackback-url'] = 'getTrackbackPingURL'

#Now some hook methods.

def parseOldStyleIncomingTrackbacks(self, trackbackString):
    """Used to instantiate a series of pre-2.1 incoming trackbacks
    from a NewsBruiser entry on disk."""
    incoming = self.getIncomingTrackbacks()
    l = string.split(trackbackString, '|')
    ip = '0.0.0.0'
    quality = IncomingTrackback.UNKNOWN_STATUS
    i = 0
    while i < len(l)-4:
        incoming.append(IncomingTrackback(self, l[i], ip, l[i+1], l[i+2], l[i+3],
                                          l[i+4], quality))
        i = i + 5
nb.Entry.registerInstantiationFromFileHook(INCOMING_TRACKBACKS,
                                           parseOldStyleIncomingTrackbacks)

def gatherIncomingTrackbacks(vars, entry):
    "Trackbacks can be part of the 'input' if we're importing."
    trackbackVars = vars.get(INCOMING_TRACKBACK, [])
    for i in trackbackVars:
        if type(i) == types.DictionaryType:
            ip = i.get(TrackbackPingScreen.IP, os.environ.get('REMOTE_ADDR',
                                                           '127.0.0.1'))
            url = i.get(TrackbackPingScreen.URL, '')
            title = i.get(TrackbackPingScreen.TITLE, '')
            text = i.get(TrackbackPingScreen.EXCERPT, '')
            blogName = i.get(TrackbackPingScreen.BLOGNAME, '')
            postTime = i.get(TrackbackPingScreen.TIME, time.time())
            entry.addIncomingTrackback(ip, blogName, url, title, text,
                                       postTime)
nb.Entry.registerInstantiationFromInputHook(gatherIncomingTrackbacks)

def parseIncomingTrackback(self, trackbackString):
    """Used to instantiate an incoming trackback from a NewsBruiser entry
    on disk."""
    l = string.split(trackbackString, '|')
    l.insert(0, self)
    self.getIncomingTrackbacks().append(apply(IncomingTrackback, l))
nb.Entry.registerInstantiationFromFileHook(INCOMING_TRACKBACK,
                                           parseIncomingTrackback)

def incomingTrackbacksAsOutputString(self):
    """Formats an entry's incoming trackbacks as a series of lines suitable for
    storing in the given entry's disk representation."""
    return string.join(map(lambda(x): INCOMING_TRACKBACK + ': ' + x.asOutputString(),
                           self.getIncomingTrackbacks()), '\n')
nb.Entry.registerAsOutputStringHook(incomingTrackbacksAsOutputString)

#A hook to print trackback autodiscovery code before every entry.
AUTODISCOVERY = '\n<!--<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/"> <rdf:Description rdf:about="%s" dc:identifer="%s" dc:title="%s" trackback:ping="%s"/></rdf:RDF>-->\n'
def printTrackbackAutodiscovery(viewer, entry):
    abs = entry.getAbsoluteURL()
    viewer.out.write(AUTODISCOVERY % (abs, abs, entry.getTitle(),
                                      entry.getTrackbackPingURL()))
nb.Viewer.registerPreEntryHook(printTrackbackAutodiscovery, 'incomingTrackbackEnabled')

#A hook to give an interface to deleting incoming trackbacks you
#don't like.

SPAM_ACTION = 'doAction'
DELETE = SPAM_ACTION + 'Delete'
IGNORE = SPAM_ACTION + 'Ignore'
CONFIRM_SPAM = SPAM_ACTION + 'ConfirmBad'
CONFIRM_NONSPAM = SPAM_ACTION + 'ConfirmGood'
TAKE_ACTIONS = 'takeActions'

def printIncomingTrackbacksForEdit(self):
    entry = self.entries[0]
    trackbacks = entry.getIncomingTrackbacks()
    if trackbacks:
        print '<h2>Incoming Trackbacks</h2>'
        print '<table><tr class="optionNamePanel"><th>Date</th><th>Where</th><th>Excerpt</th><th>Spam?</th><th>Action</th></tr>'
        classifier = self.notebook.getBayesianClassifier()
        for trackback in trackbacks:
            title = trackback.title
            if not title:
                title = '[no title]'
            print '<tr><td>%s</td><td><a href="%s">%s</a> from %s</td><td>%s</td>' % (trackback.getDate(), trackback.url, title, trackback.blogname, HTMLStripper.strip(trackback.text, []))
            whatToDo = IGNORE
            t = "Don't know"
            trackback.calculateQuality()
            if trackback.unconfirmedBad():
                t = 'Probably (%.2f%%)' % trackback.badProbabilityPercentage()
                whatToDo = CONFIRM_SPAM
            elif trackback.unconfirmedGood():
                t = 'Probably not (%.2f%%)' % trackback.badProbabilityPercentage()
                whatToDo = CONFIRM_NONSPAM
            elif trackback.confirmedGood():
                t = 'No'
                whatToDo = IGNORE
            print '<td>%s</td><td>' % t
            selectionOptions = [IGNORE, DELETE, CONFIRM_SPAM]
            selectionTexts = ['Do nothing', 'Delete',
                              'Mark as spam']
            if not trackback.confirmedGood():
                selectionOptions.insert(0, CONFIRM_NONSPAM)
                selectionTexts.insert(0, 'Mark as non-spam')
            self.listBox(SPAM_ACTION + ' ' + trackback.date, selectionTexts, selectionOptions, whatToDo)
            print '</td></tr>'
        print '<tr><td colspan="4"></td><td>'
        self.submitButton('Perform actions', TAKE_ACTIONS)
        print '</td></tr></table>'
NBCGI.registerDisplaySnippet(EditCGI, 'belowSubmitButton', printIncomingTrackbacksForEdit, 1)

def takeTrackbackActions(self):
    entry = self.entries[0]
    trackbacks = entry.getIncomingTrackbacks()
    entryDirty = 0
    classifier = self.notebook.getBayesianClassifier()
    for var in self.variables.keys():
        i = string.find(var, SPAM_ACTION)
        if i <> -1:
            key = var[i+len(SPAM_ACTION)+1:]
            value = self.variables[var]
            for trackback in trackbacks:
                if trackback.date == key:
                    if value == DELETE:
                        trackbacks.remove(trackback)
                        entryDirty = 1
                    elif value == CONFIRM_SPAM:
                        trackback.trainBad()
                        trackbacks.remove(trackback)
                        entryDirty = 1
                    elif value == CONFIRM_NONSPAM:
                        trackback.trainGood()
                        trackback.quality = trackback.CONFIRMED_GOOD
                        entryDirty = 1
    if entryDirty:
        for trackback in entry.getIncomingTrackbacks():
            if trackback.quality > trackback.CONFIRMED_GOOD:
                trackback.calculateQuality()
        entry.write()    
    if classifier.dirty:
        classifier.save()
        
    self.printStatus('Action(s) completed.')
    self.printForm()

NBCGI.registerHookAction(EditCGI, TAKE_ACTIONS, takeTrackbackActions)
