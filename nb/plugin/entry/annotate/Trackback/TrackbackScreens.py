import os
import string
import sys

from nb.NBCGI import NBCGI
from nb.lib import HTMLStripper

class TrackbackPingScreen(NBCGI):

    NAME = 'trackback'
    URL = 'url'
    TITLE = 'title'
    EXCERPT = 'excerpt'
    BLOGNAME = 'blog_name'    
    IP = 'ip' #Not part of the web API, but used when importing.
    TIME = 'time' #Ditto.

    RESPONSE = '<?xml version="1.0" encoding="utf-8"?>\n<response>\n%s\n</response>'

    SUCCESS = '<error>0</error>\n'

    ITEM = '<item>\n<link>%s</link>\n<title>%s</title>\n<description>%s</description>\n</item>\n\n'

    ITEMS = RESPONSE % (SUCCESS + '<rss version="0.91">\n<channel>\n<title>%s</title>\n<link>%s</link>\n<description>%s</description>\n<language>%s</language>\n\n%s\n</channel>\n</rss>')

    ERROR = RESPONSE % '<error>1</error>\n<message>%s</message>'

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, pathInfo=pathInfo)
        self.printHeader()
        if hasattr(self, 'error'):
            sys.exit()
        if not self.notebook.incomingTrackbackEnabled():
            self.printError('This notebook does not allow incoming trackback pings.')
        if not hasattr(self, 'entries') or len(self.entries) <> 1:
            self.printError('You must specify one and only one entry.')
        url = self.variables.get(self.URL)
        if url:
            self.registerTrackback()
        else:
            self.printTrackbacks()

    def printError(self, message):
        self.printHeader('text/xml')
        print self.ERROR % message
        self.error = 1
        sys.exit() #Don't know why this doesn't really exit...

    def registerTrackback(self):
        ip = os.environ.get('REMOTE_ADDR', '127.0.0.1')
        url = self.var(self.URL)
        blogName = self.var(self.BLOGNAME, '')
        title = self.var(self.TITLE, '')
        excerpt = HTMLStripper.strip(self.var(self.EXCERPT, ''), [])
        if len(excerpt) > 252:
            excerpt = excerpt[:253] + '...'
        self.printHeader('text/xml')
        entry = self.entries[0]
        if url:
            isValid = entry.addIncomingTrackback(ip, blogName, url, title,
                                              excerpt)
            if isValid == 0:
                print self.ERROR % "I suspect that your trackback is trackback spam. If you are not a spammer, please try rewording."
            else:
                entry.write()
                print self.RESPONSE % self.SUCCESS
        else:
            print self.ERROR % 'You must provide a URL.'
        
    def printTrackbacks(self):
        self.printHeader('text/xml')
        items = []
        entry = self.entries[0]
        for trackback in entry.getIncomingTrackbacks():
            items.append(self.ITEM % (trackback.url, trackback.title,
                                      trackback.text))
        print self.ITEMS % (entry.getTitle(),
                            entry.getAbsoluteURL(),
                            entry.sample(),
                            self.notebook.getLanguage(),
                            string.join(items, ''))
