import cgi

from nb.NBCGI import NBCGI

class CorpusExportScreen(NBCGI):

    """Exports the Bayesian filter corpus for this notebook in
    Cheatsheet format, which I just made up. Cheatsheet looks like
    this:

    <?xml version="1.0"?>'
    <cheatsheet version="1.0">
     <classification name="pool1">
      <token name="token1" frequency="1"/>
      <token name="token2" frequency="100"/>
      <token name="tokenN" frequency="6"/>
     </classification>
     <classification name="pool2">
      <token name="token1" frequency="6"/>
      <token name="token2" frequency="1"/>
      <token name="token3" frequency="20"/>
      <token name="tokenM" frequency="6"/>
     </classification>
     <classification name="poolK">
      <token name="token3" frequency="4"/>
     </classification>
    </cheatsheet>

    For purposes of interoperability between anti-spam programs, I
    recommend classification names "good" for known non-spam and "bad"
    for known spam.

    Coming soon: Cheatsheet import."""

    NAME = 'corpus-export'

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, pathInfo=pathInfo)

        if self.notebook.useCookies() and not self.preAuth:
            self.redirectToLogin()

        self.printHeader('text/xml')
        print '<?xml version="1.0"?>'
        print '<cheatsheet version="1.0">'
        c = self.notebook.getBayesianClassifier()
        for name, pool in c.pools.items():
            if name != '__Corpus__':
                self.dumpPool(name, pool)
        print '</cheatsheet>'

    def dumpPool(self, name, pool):
        print ' <classification name="%s">' % cgi.escape(name)
        for key, value in pool.items():
            key = cgi.escape(key, 1)
            print '  <token text="%s" frequency="%s"/>' % (cgi.escape(key),
                                                           value)
        print ' </classification>'
