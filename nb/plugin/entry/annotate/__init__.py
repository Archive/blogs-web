import string
import time
import urlparse

from nb import Notebook
from nb.BayesianClassifier import BayesianClassified
from nb.plugin.entry.filter.AutoLink import AutoLink_link
from nb.lib import HTMLStripper
from nb.CoreCGIs import DispatchCGI
import AnnotationScreens

DispatchCGI.cgiModules.append(AnnotationScreens.__name__)

def spamCutoffPercentage(self):
    return self.getOptionValue('spam-cutoff')
Notebook.Notebook.spamCutoffPercentage = spamCutoffPercentage

def makeStorable(text, minimalHTMLFormatting=0):
    """Helper method to turn arbitrary text into something capable of
    being stored in a line of an entry file."""

    if minimalHTMLFormatting:
        text = AutoLink_link(HTMLStripper.strip(text))
	text = string.replace(text, '\r\n', '\n')
        text = string.replace(text, '\n\n', '<p/>')
        text = string.replace(text, '\n', '<br/>')
    else:
        text = string.replace(text, '\n', ' ')
    text = string.replace(text, '|', ' ')
    return text

class ExternalAnnotation(BayesianClassified):

    "Some code common to trackbacks and comments."

    def getNotebook(self):
        return self.entry.notebook

    def getIPAddress(self, viewer):
        return self.ip

    def getDate(self):
        return time.strftime(self.getNotebook().getDisplayTimeFormat(),
                             time.localtime(float(self.date)))

    def getURL(self):
        return self.url

    def getTitle(self):
        return self.title
    
    def getText(self):
        return self.text

    def _appendTokens(self, tokens, text, ifNone=None):
        if text:
            tokens.extend(string.split(string.lower(text), ' '))
        elif ifNone:
            tokens.append(ifNone)
        return tokens

    def getBayesianClassificationTokens(self):
        tokens = [self.ip]
        parts = string.split(self.ip, '.')
        for i in range(1, len(parts)-1):
            tokens.append(string.join(parts[:i+1], '.'))        
        self._appendTokens(tokens, self.title, '_notitle')
        if self.url:
            tokens.append(self.url)
            parts = urlparse.urlparse(string.lower(self.url))
            tokens.extend(string.split(parts[1], '.'))
            for i in string.split(parts[2], '/'):
                if i:
                    tokens.append(i)
            for i in parts[1:]:
                if i and i != '/':
                    tokens.append(i)
        else:
            tokens.append('_nourl')
        self._appendTokens(tokens, self.text, '_notext')        
        return tokens

