try:
    import urllib2
    have_urllib2 = 1
except:
    import urllib
    have_urllib2 = 0

import string

from nb import const
from nb import CoreCGIs
from nb import NBCGI
from nb.lib.BeautifulSoup import BeautifulSoup
from nb.Notebook import Notebook
import nb.Entry
from nb.Entry import Entry

LINK_URL = 'linkURL'

def enableLinkURL(self):
    return self.getOptionValue('enable-link-url')
Notebook.enableLinkURL = enableLinkURL

def useURLTitle(self):
    return self.getOptionValue('url-title-is-entry-title')
Notebook.useURLTitle = useURLTitle

def getLinkURL(self):
    "Returns the URL associated with this entry, if any."
    return self.get(LINK_URL, '')
Entry.getLinkURL = getLinkURL

def URL_linkTitle(entry, text):
    "A filter that links the title of this entry to its URL."
    url = entry.getLinkURL()
    if url:
        if not text:
            text = url
    if text and url:
        text = '<a class="titleLink" href="%s">%s</a>' % (url, text)
    return text
nb.Entry.registerFilter('title', URL_linkTitle)

def gatherLinkURL(vars, entry):
    """Gathers the URL for this entry, and (if applicable) the title too."""
    url = vars.get(LINK_URL, '')
    if url and entry.notebook.useURLTitle():
        existingTitle = vars.get(const.TITLE_CGI_KEY)
        if not existingTitle:
            existingTitle = getattr(entry, const.TITLE_CGI_KEY)
        if not existingTitle:
            title = extractTitleFromURL(url)
            setattr(entry, const.TITLE_CGI_KEY, title)
    if url:
        setattr(entry, LINK_URL, url)
    elif hasattr(entry, LINK_URL):
        delattr(entry, LINK_URL)    
nb.Entry.registerInstantiationFromInputHook(gatherLinkURL)

def linkURLAsOutputString(entry):
    url = entry.getLinkURL()
    out = ''
    if url:
        out = '%s: %s\n' % (LINK_URL, url)
    return out
nb.Entry.registerAsOutputStringHook(linkURLAsOutputString)

# UI hooks

def printLinkInputControl(cgi, entry=None):
    if cgi.notebook.enableLinkURL():
        print '<p><b>%s:</b>' % _('URL')
        url = ''
        if entry:
            url = entry.getLinkURL()
        cgi.textField(LINK_URL, url, 40, None)

def printLinkInputControlForEdit(cgi):
    printLinkInputControl(cgi, cgi.entries[0])

NBCGI.registerDisplaySnippet(CoreCGIs.AddCGI, 'aboveEntryText',
                             printLinkInputControl, 1)
NBCGI.registerDisplaySnippet(CoreCGIs.EditCGI, 'aboveEntryText',
                             printLinkInputControlForEdit, 1)

#Helper methods

def extractTitleFromURL(url):
    title = ''
    if have_urllib2:
        try:
            f = urllib2.urlopen(url)
            success = 1
        except urllib2.HTTPError:
            #Some pages might discriminate against us because we're
            #Python, or the URL might just not exist (yet?) or otherwise
            #not be accessible. Only get and use the title on a successful
            #retrieval.
            success = 0
    else:
        f = urllib.urlopen(url)
        success = 1
    if success:
        check = f.read(1024*5)
        soup = BeautifulSoup()
        soup.feed(check)
        titleTag = soup.first('title')
        if titleTag and titleTag.contents:
            title = str(titleTag.contents[0])
            title = string.replace(title, '\n', ' ')
    return title
