import os
import string
import sys

from nb.lib import xmlrpclib
from nb.NBCGI import NBCGI
import XMLRPCHandler

class XMLRPCCGI(NBCGI):

    """An XML-RPC interface to NewsBruiser. Handlers are defined in
    nb/XMLRPCHandler.py."""

    NAME = 'xmlrpc'

    MAX_REQUEST = 50000

    def __init__(self, pathInfo=''):
        NBCGI.__init__(self, self.nullPathInfoProcessor,
                       self.nullNotebookNameProcessor,
                       self.ignoreSubmittedData,
                       pathInfo=pathInfo)

    def getHandler(self, key):
        """Finds the class of the handler with the given key. Uses
        introspection on the XMLRPCHandler module so that we don't
        have to keep a master list of handlers anywhere; if you add a
        handler in a different module you'll need to overwrite this
        method so that it looks there too."""
        handler = None
        for c in XMLRPCHandler.__dict__.values():
            if hasattr(c, 'XMLRPC_KEY') and c.XMLRPC_KEY == key:
                handler = c
        return handler

    ### Everything below this point is non-NewsBruiser-specific.

    def giveup(self, message):
        print "Status: 400"
        print
        print "sorry,", message
        sys.exit(0)

    def run(self):
        if os.environ.get("REQUEST_METHOD") != "POST":
            self.giveup("invalid request")

        bytes = int(os.environ.get("CONTENT_LENGTH", 0))
        if bytes > self.MAX_REQUEST:
            self.giveup("request too large")

        params, method = xmlrpclib.loads(sys.stdin.read(bytes))
        result = self.dispatch(method, params)

        if not isinstance(result, type(())):
            result = (result,)

        response = xmlrpclib.dumps(result, methodresponse=1)

        print 'Content-Length:', len(response)
        print 'Content-type: text/xml\n'
        print response

    def dispatch(self, method, params):        
        packages = string.split(method, '.')
        result = 0
        if len(packages) < 1:
            self.giveup("No package or method specified")
        elif len(packages) < 2:
            self.giveup('No package specified for method! (eg. I got "method" instead of "module.method")')
        m = self.getHandler(packages[0])
        if not m:
            self.giveup("Unsupported package specified: %s" % packages[0])
        m = m(self)
        for package in packages[1:]:
            m = getattr(m, package, None)
            if not m:
                break
        if m:
            result = apply(m, params)
        return result
