"""Contains NewsBruiser implementations of many popular XML-RPC handlers."""

import string
import time

from nb import const
from nb.plugin.entry.importer.RSS import ImportingRSSParser
from nb import NewsBruiserCGIs
from nb.Entry import Entry
from nb.lib import xmlrpclib

class XMLRPCHandler:

    """A base class for all NewsBruiser XML-RPC handlers."""

    #Define this in subclasses as the name of the API as accessed by
    #XML-RPC clients.
    XMLRPC_KEY = None

    def __init__(self, cgi):
        self.cgi = cgi

    #Utility methods.

    def getEntryFromPostID(self, postID, username, password):
        parts = string.split(postID, '/')
        notebookName = parts[0]
        parts = parts[1:]
        self.cgi.notebook = self.getNotebook(notebookName, username, password)
        self.cgi.pathInfo = string.join(parts, '/')
        self.cgi.processPathInfoAsEntryID()
        return Entry(self.cgi.id)

    def getNotebook(self, blogID, username=None, password=None):
        notebook = self.cgi.config.notebooks.get(blogID, None)
        if not notebook:
            raise 'No such notebook: %s' % blogID

        if password and not notebook.validatePassword(password):
            raise 'Invalid password for notebook %s' % notebook.name
        return notebook

class MetaWeblogLikeXMLRPCHandler(XMLRPCHandler):

    """A base class for XML-RPC handlers which handle an API
    based on the MetaWeblog API."""
    
    def newPost(self, blogID, username, password, item, publish):
        (body, title, publicationDate, originalURL) = ImportingRSSParser(None, []).deriveEntryFromItem(item)
        notebook = self.getNotebook(blogID, username, password)
        if not notebook.enableDrafts:
            publish = 1

        draft = not publish

        entry = notebook.addEntry(password, username, None, body, title,
                                  draft, publicationDate)
        return entry.id.asPathInfo()
        
    def editPost(self, postID, username, password, item, publish):
        #TODO: let this handle drafts and have publish do something.
        (title, body, publicationDate, originalURL) = ImportingRSSParser(None, []).deriveEntryFromItem(item)
        entry = self.getEntryFromPostID(postID, username, password)
        entry.edit(password, username, None, body, title, None)
        return entry.id.asPathInfo()

class MetaWeblogHandler(MetaWeblogLikeXMLRPCHandler):

    """A NewsBruiser implementation of the MetaWeblog API"""
    XMLRPC_KEY = 'metaWeblog'

    def getPost(self, postID, username, password):
        entry = self.getEntryFromPostID(postID, username, password)
        return entry.asRSS20Item()

    def newMediaObject(self, blogid, username, password, struct):
        #Not sure what to do with this, since in NB media objects
        #are always associated with a specific entry. We'll pass for now.
        pass

class BloggerHandler(MetaWeblogLikeXMLRPCHandler):

    """A NewsBruiser implementation of the Blogger API"""
    XMLRPC_KEY = 'blogger'

    def newPost(self, appkey, blogid, username, password, content, publish):
        return MetaWeblogLikeXMLRPCHandler.newPost \
               (self, blogid, username, password, self.makeItem(content),
                publish)

    def editPost(self, appkey, postid, username, password, content, publish):
        return MetaWeblogLikeXMLRPCHandler.editPost \
               (self,postid, username, password, self.makeItem(content),
                publish)

    def getPost(self, appkey, postId, username, password):
        entry = self.getEntryFromPostID(postId, username, password)
        return self.asBloggerPost(entry)

    def getRecentPosts(self, appkey, blogID, username, password,
                       numberOfPosts=20):
        notebook = self.getNotebook(blogID, username, password)
        entries = notebook.getPreviousEntries(numberOfPosts)
        posts = []
        for entry in entries:
            posts.append(self.asBloggerPost(entry))
        return posts
        
    def getUsersBlogs(self, appkey, username, password):
        """Just return all the notebooks."""
        notebooks = []
        for book in self.cgi.config.getNotebookList():
            if not book.isPrivate():
                from nb.NewsBruiserCGIs import PortalCGI
                notebook = {}
                notebook['url'] = book.getURL(NewsBruiserCGIs.PortalCGI)
                notebook['blogid'] = book.name
                notebook['blogName'] = book.getTitle()
                notebooks.append(notebook)
        return(notebooks)                        

    def getUserInfo(self, appkey, username, password):
        """We don't have a user account system, and if we did users would
        only have an account on a particular notebook, so we can't really
        implement this, but we'll humor the caller."""    
        user = {}
        user['nickname'] = username
        return user

    def getTemplate(self, appkey, blogid, username, password, templateType):
        """Approximation: return the entry template for 'main' and the
        entry list template for 'archive-list'"""
        templateName = self.getTemplateName(templateType)
        notebook = self.getNotebook(blogid, username, password)
        return str(notebook.getTemplate(templateName))

    def setTemplate(self, appkey, blogid, username, password, template,
                    templateType):
        templateName = self.getTemplateName(templateType)
        notebook = self.getNotebook(blogid, username, password)

        #TODO: the option setting stuff is currently in configure.py, and not
        #available for outside use.

    def getTemplateName(self, bloggerTemplateName):
        """Provides an approximate mapping of Blogger templates onto
        NewsBruiser templates."""
        templateName = None
        if bloggerTemplateName == 'main':
            templateName = 'entry'
        elif bloggerTemplateName == 'archiveIndex':
            templateName = 'entry-list'
        else:
            raise 'Unrecognized template: %s' % templateName            
        return templateName

    def makeItem(self, contents):
        item = {}
        item['description'] = contents
        return item

    def asBloggerPost(self, entry):
        post = {}
        post['content'] = entry.getProcessedText()
        post['userId'] = entry.getAuthor()
        post['postId'] = entry.id.asPathInfo()
        post['dateCreated'] = entry.getDateInFormat(const.RFC822_TIME_FORMAT)
        return post

class AdvogatoTestHandler(XMLRPCHandler):

    """A NewsBruiser implementation of the Advogato test API"""
    XMLRPC_KEY = 'test'

    def guess(self):
        return ['You guessed', 42]

    def square(self, x):
        return x * x

    def sumprod(self, x, y):
        return [x+y, x*y]

    def strlen(self, s):
        return len(s)

    def capitalize(self, s):
        capitalized = s
        if s:
            capitalized = string.capitalize(s[0])
            if len(s) > 1:
                capitalized = capitalized + s[1:]
        return capitalized

class AdvogatoDiaryHandler(XMLRPCHandler):

    """A NewsBruiser implementation of the Advogato diary API"""
    XMLRPC_KEY = 'diary'

    def authenticate(self, user, password):
        """If the password is right for the user, give the password right
        back to them."""
        notebook = self.getNotebook(user, None, password)
        return string.join((user, password), '/')

    def len(self, user):
        """Return the number of entries in a notebook."""
        return self.getNotebook(user).getNumberOfEntries()

    def get(self, user, index):
        """Return the body of a specific entry."""
        notebook = self.getNotebook(user)
        return self.getNotebook(user).getNthEntry(index).getProcessedText()

    def getDates(self, user, index):
        """Return the publication date of the entry, twice (NewsBruiser
        doesn't keep track of updated date)."""
        date = xmlrpclib.DateTime\
               (tuple(self.getNotebook(user).getNthEntry(index).getTimeTuple()))
        return [date, date]

    def set(self, cookie, index, html):
        """Set the text of an entry."""
        #parse the cookie
        notebookName, password = string.split(cookie, '/')
        
        notebook = self.getNotebook(notebookName, None, password)
        if index == -1 or index == self.len(notebookName):
            Entry = notebook.addEntry(password=password, text=html)
        else:
            entry = notebook.getNthEntry(index)
            entry.edit(password=password, newText=html)
        return index
