import re
import string
import sys

from lib.ParsedMessage import ParsedMessage
from NBConfig import NBConfig

class MailImporter:

    """Turns a mail message (possibly with attachments) into a
    NewsBruiser notebook entry. This does not yet subclass Importer,
    so you can't use it from import.cgi.

    When you send mail into this, the subject of the mail should be:

    notebook: title

    eg. "weblog: This is the title"

    If you don't specify a notebook, NewsBruiser will assume you mean
    the default notebook.

    If the notebook requires a password, the last line of the body
    should consist of the password.

    -------------------------

    I've tested this by feeding it mail files on standard input, but I
    haven't tried hooking it up to procmail. You'd need a procmail
    recipe like this:

    :0
    [Optionally, conditions such as 'mail to/from a specific address' to
    distinguish NewsBruiser mail from other types of mail.]
    | /home/httpd/nb/MailImporter.py

    You may have to make MailImporter run setuid as the webserver to
    get this to work.

    Let me know if you're willing to write directions on setting this up
    with procmail, or if there's an existing document I can modify.
    """

    NO_SUCH_NOTEBOOK_SUBJECT = 'No such notebook "%s"'
    NO_SUCH_NOTEBOOK_BODY = """You tried to add an entry to the "%s" notebook, which doesn't exist."""

    INVALID_PASSWORD_SUBJECT = "Invalid password"
    INVALID_PASSWORD_BODY = "You entered an invalid password for the %s notebook as the last line of your message."

    NO_ENTRY_TEXT_SUBJECT = "Empty entry submitted"
    NO_ENTRY_TEXT_BODY = "You tried to submit an empty entry. Please put something in the body of your email message."
        
    def __init__(self, fd):
        message = ParsedMessage(fd)
        conf = NBConfig()
        self.authorName, self.authorEmail = message.headers.getaddr('From')
        subject = message.headers['Subject']
        match = re.match('(\w+): (.*)', subject)
        if match.groups():
            name = match.group(1)
            try:
                self.notebook = conf.getNotebooks()[match.group(1)]
            except KeyError:
                self.error('NO_SUCH_NOTEBOOK', (name), (name))
            title = match.group(2)
        else:
            self.notebook = conf.getDefaultNotebook()
            title = subject

        text = message.getBody()
        password = ''
        #TODO: if they don't specify a password and the notebook allows
        #the creation of drafts without specifying a password, let them
        #create a draft.
        if not self.notebook.validatePassword(''):
            while not password:
                #Ignore blank lines after password
                lastLineStart = string.rfind(text, '\n', 0, len(text)-1)+1
                password = text[lastLineStart:-1]
                text = text[:lastLineStart]
            if not self.notebook.validatePassword(password):
                self.error('INVALID_PASSWORD', bodyArgs=(self.notebook.name))
            if not text:
                self.error('NO_ENTRY_TEXT')
            entry = self.notebook.addEntry(password, self.authorName, self.authorEmail, text, title)
            if len(message.parts) > 1:
                for part in message.parts[1:]:
                    disposition = part.headers.get('Content-Disposition', None)
                    if disposition:
                        match = re.search('filename="([^"]+)"', disposition)
                        if match and match.groups():
                            name = match.group(1)
                            entry.setAttachment(password, name, part.getContent())

    def error(self, errorKey, subjectArgs=None, bodyArgs=None):
        if self.authorEmail:
            subject = getattr(self, errorKey + '_SUBJECT')
            if hasattr(self, 'notebook'):
                subject = "Problem with your submission to " + self.notebook.getTitle() + ': ' + subject
            else:
                subject = "Problem with your submission: " + subject
            if subjectArgs:
                subject = subject % subjectArgs
            body = getattr(self, errorKey + '_BODY')
            if bodyArgs:
                body = body % bodyArgs
            #TODO: send out via mail.
            sys.exit(1)

if __name__ == '__main__':
    #Build a MailImporter out of standard input.
    MailImporter(sys.stdin)    
