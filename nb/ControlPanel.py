from types import *
import copy
import os

from lib import DateArithmetic

import const
import util
from HtmlGenerator import HtmlGenerator
from EntryID import EntryID
from Entry import Entry

class ControlPanel(HtmlGenerator):
    def __init__(self, notebook, cgi=None):
        "Initializes the panel to handle a particular notebook and link to a particular CGI script."

        if not cgi:
            from CoreCGIs import ViewCGI
            cgi = ViewCGI.NAME
        self.notebook = notebook
        self.cgi = cgi
        self.draft = 0

    def printPanel(self, id=None, header=None, searchBoxContents=''):  
        "Prints a control panel for the given ID (or for no ID) and with the given header (or no header)"
        if id and not header:
            header = id.asDescription(self.cgi)
        print '<div align="center">'
        print '<table class="controlPanel">'
        if header:
            print ' <tr>'
            print '  <td colspan="3">'
            print '   <div align="center"><h1 class="heading">%s</h1></div>' % header
            print ' </td>'
            print '</tr>'
        if id:
            print '<tr><td align="right" width="25%">'
            self.printPreviousIDLinks(id)
            print '</td>'
            if not id.draft:
                print '<td width="50%">'
                self.printDateJumpForm(id)
                print '</td>'
            print '<td align="left" width="25%">'
            self.printNextIDLinks(id)
            print '</td></tr>'
        if not id or not id.draft:
            print '<tr><td colspan="3" align="center">'
            self.searchBox(self.notebook, searchBoxContents)
            print '</tr>'
        print '</table>'
        print '</div>'

    def printNextIDLinks(self, id):
        "Given an EntryID, prints navigation links pointing to the next year, next month, next day, and/or next entry."
        arrow = '&gt;'
        methods = [id.nextYear, id.nextMonth, id.nextDay, id.nextEntryID]
        arrows = [_('Y') + arrow, _('M') + arrow, _('D') + arrow,  arrow]   
        l = len(methods)
        for i in range(0,l):
            self.printPathInfoLink(methods[i], id.draft, arrows[i], i==l-1, 1)

    def printPreviousIDLinks(self,id):
        "Given an EntryID, prints navigation links pointing to the next year, next month, next day, and/or next entry."
        arrow = '&lt;'  
        methods = [id.previousEntryID, id.previousDay, id.previousMonth, id.previousYear]
        arrows = [arrow, arrow + _('D'), arrow + _('M'), arrow + _('Y')]
        for i in range(0,len(methods)):
            self.printPathInfoLink(methods[i], id.draft, arrows[i], i==0, 0)

    def printPathInfoLink(self, method, draft, linkText, oneEntry=0,
                          direction=0):
        "Calls a method and uses its result as the PATH_INFO to a link."
        result = method()
        if result == None: return

        if type(result) == ListType or type(result) == TupleType:
            l = result
        else:
            l = [result]

        if oneEntry:
            args = list(copy.copy(result))
            args.insert(0, self.notebook)
            args.append(draft)
            id = apply(EntryID, args)
            if not id.denotesMultipleEntries():
                entry = Entry(id)

        url = util.urljoin(self.cgi, self.notebook.name)
        if draft:
            url = util.urljoin(url, 'drafts')

        for i in l:
            url = util.urljoin(url, str(i))
        url = self.notebook.nbconfig.urlRewriter.rewrite(url)

        if direction:
            if oneEntry and entry:
                if entry.getTitle(): 
                    self.printSpan('entryTitle', entry.getTitle())
                else:
                    print _('Next')
        self.linkTo(url, linkText, 'nav')
        if not direction:
            if oneEntry and entry:
                if entry.getTitle():
                    self.printSpan('entryTitle', entry.getTitle())
                else:
                    print _('Previous')

    def printDateJumpForm(self, id):
        "Prints a set of controls for skipping to a given year or year/month in one form submission."
        today = self.notebook.today()
        firstEntry = self.notebook.getFirstEntry(id.draft)
        if (firstEntry == None):
            return
        else:
            firstEntryID = firstEntry.id

        #See which years and months we need
        years = range(int(firstEntryID.year), today[0]+1)
        if len(years) == 1:
            #Less than one year of entries. Print fewer than 12 months.
            months = range(int(firstEntryID.month), today[1]+1)
        else:
            months = range(1,13)

        if id.granularity() == const.YEAR:
            currentMonth = 1
        else:
            currentMonth = id.month

        if len(years) > 1 or (id.granularity() <> const.YEAR and len(years)==1 and len(months) >1):
            action = self.notebook.getURL(self.cgi)
            self.startForm(action, "get")
            self.printYearDropdown(years, id.year)
            if (id.granularity() <> const.YEAR): months = self.printMonthDropdown(months, currentMonth)
            self.endForm(_('Jump'))

    def printYearDropdown(self, years, current):
        "Print a drop-down with an item for every year since the first entry in this notebook."
        if len(years)<=1:
            self.hiddenField(const.YEAR_CGI_KEY, years[0])
            print years[0]
        else:
            self.listBox(const.YEAR_CGI_KEY, years, years, current)

    def printMonthDropdown(self, months, current):
        "Print a drop-down with an item for the given range of months."
        self.listBox(const.MONTH_CGI_KEY, DateArithmetic.MONTHS[months[0]-1:months[-1]], months, current)
