"""A class containing the logic for the NewsBruiser SSIs. Created to
consolidate the logic and so the SSIs can be compiled into .pyc code
for performance purposes."""

from lib import DateArithmetic

from NBProcess import NBSSI
from Viewer import Viewer
from NotebookCalendar import NotebookCalendar
from CoreCGIs import ViewCGI

import const

class LastNDaysSSI(NBSSI):

    """An SSI for displaying the last few days' worth of entries
    from a notebook."""

    def __init__(self):
        self.init([['numberOfDays', 10],
                   ['notebookName', None]])

    def run(self):
        entries = self.notebook.getPreviousDaysEntries(self.numberOfDays)
        Viewer(self.notebook, entries, 0, 0).render()

class LastNEntriesSSI(NBSSI):

    """An SSI for getting the last few entries from a notebook."""

    def __init__(self):
        self.init([['numberOfEntries', 10],
                   ['notebookName', None]])

    def run(self):
        entries = self.notebook.getPreviousEntries(self.numberOfEntries)
        Viewer(self.notebook, entries, 0, 0).render()

class RandomSSI(NBSSI):

    """An SSI for getting a random entry from a notebook."""

    def __init__(self):
        self.init([['notebookName', None]])

    def run(self):
        entry = self.notebook.getRandomEntries(1)
        Viewer(self.notebook, entry).render()

class ThisMonthCalendarSSI(NBSSI):

    """An SSI for printing out a calendar for the current month for a given
    notebook."""

    def __init__(self):
        self.init([['notebookName', None],
                  ['showPageGuides', 0],
                  ['monthLinks', 0]])

    def run(self):
        (year, month) = self.notebook.today()[:2]
        self.notebook.getCalendar().printMonth(year, month,
                                               int(self.showPageGuides), 1,
                                               monthLinks = self.monthLinks)

class ThisMonthSSI(NBSSI):

    """An SSI for getting all entries posted to a notebook for this month."""

    MINIMUM_ENTRIES = 10

    def __init__(self):
        self.init([['notebookName', None],
                  ['chronological', 0]])

    def run(self):
        entries = self.notebook.getThisMonthsEntries()
        if len(entries) < self.MINIMUM_ENTRIES:
            entries = self.notebook.getPreviousEntries(self.MINIMUM_ENTRIES)
        Viewer(self.notebook, entries, self.chronological, 0).render()

class TodayInHistorySSI(NBSSI):

    """An SSI for getting entries from this date in previous years."""

    MAX_HISTORY_ENTRIES = 3

    def __init__(self):
        self.init([['notebookName', None]])

    def run(self):
        historicEntries = {}
        firstEntry = self.notebook.getFirstEntry()
        (year, month, day) = self.notebook.today()
        cursor = (year, month, day) 
        if not firstEntry:
            return ''
        firstID = firstEntry.id.asTuple()[:2]
        i = 0
        if year == firstID[0] or (year == firstID[0]-1 and month < firstID[1]):
            #There's less than one year of entries
            granularity = const.MONTH
        else:
            granularity = const.YEAR
        while self.notebook.verifyDayFit(0, cursor) and len(historicEntries.keys()) <= self.MAX_HISTORY_ENTRIES:
            i = i + 1
            if granularity == const.YEAR:
                cursor = DateArithmetic.thisDayLastYear(cursor)
            else:
                cursor = DateArithmetic.thisDayLastMonth(cursor)
            args = list(cursor)
            args.extend([const.ALL, 0])
            entries = apply(self.notebook.getEntries, args)
            if len(entries) > 0:
                historicEntries[i] = entries[0]  
        keys = historicEntries.keys()
        keys.sort()
        for key in keys:
            entry = historicEntries[key]
            plural = "s"
            if int(key) == 1:
                plural = ""
            desc = '%s %s%s ago:' % (key, granularity, plural)
            entry.id.ordinal = const.ALL
            print '<p><a href="%s" class="yearsAgo">%s</a> <a class="historicEntryText">%s</a>' % (entry.id.asLink(ViewCGI.NAME), desc, entry.sample())

