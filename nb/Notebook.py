#Each instance of the Notebook class keeps track of a particular notebook.
#It is the repository of the configuration information for the program; ie.
#each notebook knows who its users are, what path it inhabits, etc. All
#obtaining of Entry objects should be done through the associated Notebook
#object.

import commands
import copy
import mimetypes
import os
import popen2
import re
import string
import time
import types
import whrandom
import random

try:
    from lib import portalocker
except:
    #We're on a system that doesn't support file locking, so we just
    #won't do any. This can cause problems with the index, but at least
    #NewsBruiser will run.
    pass

from lib import HTMLStripper
try:
    import crypt
except ImportError:
    from lib import crypt
from lib.gnosis import indexer
from lib import DateArithmetic
from lib.IWantOptions import Context
from lib.TTM import Template, ParenthesizedArgumentsTemplatingSystem

import CommandMaps
from HtmlGenerator import HtmlGenerator
from Entry import Entry
from EntryID import EntryID
import Options
from Options import NBOptionConfig
from Options import AuthorListOption

import util
import const
from util import *
import version
from Indexer import NewsBruiserEntryIndexer
from TimeZoned import TimeZoned
from NotebookCalendar import NotebookCalendar
from NotebookStatistics import NotebookStatistics
from BayesianClassifier import BayesianClassifier

FREEFORM_AUTHOR = "[freeform]"
FREEFORM_AUTHOR_WITH_EMAIL = "[freeform-email]"

def create(config, masterPassword, name, password, ordinal):

    """Creates a new notebook."""
    encryptedPassword = util.encrypt(password)

    config.assertValidMasterPassword(masterPassword)

    path = os.path.join(config.notebookDir, name)
    if os.path.exists(path):
        raise _("Notebook %s already exists!") % name
    os.makedirs(path)
    notebook = Notebook(name, config)
    notebook.conf['ordinal'] = str(ordinal)
    notebook.conf['password'] = encryptedPassword
    notebook.writeConfiguration()

    #Make sure the notebook list is already loaded; otherwise the
    #integrity check will mess things up.
    list = config.getNotebookList()

    config.notebooks[notebook.name] = notebook
    config.notebookList.insert(notebook.getOrdinal(), notebook)
    config.updateNotebookOrdinals(masterPassword, notebook)
    return notebook

class Notebook(HtmlGenerator, Context, TimeZoned):
    "Represents a particular notebook and all its entries." 

    def __init__(self, name, nbconfig):
        """Constructor to initialize a notebook using the options of
        the provided ConfigParser for the provided notebook name,
        the options defined in the .configuration file of the notebook,
        and the theme (if any) set for the notebook."""
        self.nbconfig = nbconfig
        self.name = name
        self.did_init = 0
        self.optionConfig = Options.cache.get(self.nbconfig)
        if not self.optionConfig:
            self.optionConfig = NBOptionConfig(self.nbconfig)
            Options.cache[self.nbconfig] = self.optionConfig

    def __getattr__(self, name):
        """Helper routine. Checks if init2 initialization was done and if not
        runs init2. Used to delay reading the configuration file until it is 
        needed. Together with the ordinal cache it provides a hefty speedup."""
        if not self.did_init and name[0] != "_":
            self.did_init = 1
            self.init2()
            return getattr(self, name)
        else:
            raise AttributeError

    def init2(self):
        name = self.name
        nbconfig = self.nbconfig
        
        self.conf = { const.NAME_CONFIG_KEY: name,
                      const.WORKING_DIR_CONFIG_KEY: nbconfig.notebookDir }
        self.theme = {}
        self.templates = {}

        self.readConfiguration()

        #Theme configuration takes precedence over .configuration configuration
        theme = self.getOptionValue('theme')
        if theme:
            file = os.path.join('themes', theme, 'configuration')
            path = util.getFileInNewsBruiserBaseDirectory(file)
            if os.path.exists(path):
                self.readConfiguration(path)
            else:
                print 'Content-type: text/html\n' #Bleah
                print _('NOTE: the theme file %s is missing!') % path
        #In order to save time, these variables are set when needed
        self.indexer = None

    def getName(self):
        "Returns the name of this notebook."
        return self.name

    def getDeletedName(self):
        """Returns the name of the notebook directory if this notebook
        were to be deleted"""
        newNamePrefix = 'deleted-' + self.name
        newName = newNamePrefix
        counter = 0
        while os.path.exists(os.path.join(self.getSystemWorkingDirectory(),
                                          newName)):
            newName = newNamePrefix + str(counter)
            counter = counter + 1
        return newName

    def getURL(self, cgi, args='', pathInfo=''):
        "Returns the URL to the given CGI for this notebook."
        url = util.urljoin(cgi, self.name, pathInfo)
        if args:
            url = url + '?' + args
        return self.nbconfig.urlRewriter.rewrite(url)

    #### Methods for getting various of the notebook's paths
        
    def getDir(self, draft=0):
        """Returns the path in which this notebook's entries are stored,
        or in which this notebook's drafts are stored"""
        path = os.path.join(self.getSystemWorkingDirectory(), self.name)
        if draft:
            path = os.path.join(path, 'drafts')
        return path

    def getAttachmentDir(self):
        """Returns the directory in which this notebook's attachments
        are stored."""
        return os.path.join(self.getDir(), 'attachments')

    def getIndexDir(self):
        "Returns the directory in which this notebook's index is stored."
        return os.path.join(self.getDir(), '.index')

    def getIndexUpdateListPath(self):
        "Returns the path to the list of pending index updates."
        return os.path.join(self.getIndexDir(), '.indexUpdates')

    ### Methods pertaining to configuration information of this notebook.

    def get(self, key, default=''):
        """Returns the value of the given configuration option, or the
        given default if the option is not set."""
        value = default
        if self.theme.has_key(key):
            value = self.theme[key]
        elif self.conf.has_key(key):
            value = self.conf[key]
        return value

    def getSystemWorkingDirectory(self):
        "Returns the directory containing this notebook."
        val = self.get(const.WORKING_DIR_CONFIG_KEY)
        if (val == ''):
            vars = {'workingDir' : const.WORKING_DIR_CONFIG_KEY,
                    'notebook' : self.name}
            raise _('No %(workingDir)s given for notebook %(notebook)s!') % vars
        return val

    def validatePassword(self, givenPassword):
        "Returns true iff the given password matches the notebook's password."
        encryptedPassword = self[const.PASSWORD_CONFIG_KEY]
        encryptedGivenPassword = crypt.crypt(givenPassword,
                                             encryptedPassword[:2])
        return(encryptedGivenPassword == encryptedPassword)

    def assertValidPassword(self, givenPassword):
        """Throws an exception unless the given password matches the
        notebook's password."""
        if not self.validatePassword(givenPassword):        
            raise _("Invalid password!")        

    #### Methods for getting the values of configuration options changable
    #### through the Web interface.

    ### Notebook Information

    def getUnlinkedTitle(self):
        """Returns this notebook's title. Templates can't use
        getTitle() because that clashes with the method used to get
        the page title."""
        return self.getTitle()

    def getLinkedTitle(self):
        "Returns this notebook's title, linked to its exported site URL."
        return self.getLink(self.getExportedSiteURL(), self.getTitle())

    def getTitle(self):
        "Returns this notebook's title."
        return self.getOptionValue('title')

    def getOrdinal(self):
        "Returns the order of this notebook in the list of notebooks."
        ordinal = self.getOptionValue('ordinal')
        if not ordinal:
            ordinal = 0
        return int(ordinal)

    def getDescription(self):
        "Returns the description of this notebook."
        return self.getOptionValue('description')

    def getContactLink(self):
        """Returns the name of the person in charge of this
        notebook, linked (if appropriate) to their email address."""
        name = self.getContactName()
        email = self.getContactEmail()
        if email:
            name = self.getLink('mailto:' + email, name)
        return name

    def getContactName(self):
        """Returns the name of the person in charge of this
        notebook."""
        return self.getOptionValue('contact-name')

    def getContactEmail(self):
        """Returns the email address of the person in charge of this
        notebook."""
        return self.getOptionValue('contact-email')

    def isPrivate(self):
        """Returns true if the notebook should be private: hidden
        from lists of notebooks and (if cookie authentication is turned on)
        requiring authentication to view."""
        return self.getOptionValue('private')

    def useCookies(self):
        """Returns whether or not to use cookies to store authentication."""
        return self.getOptionValue('persistence')

    ### Features

    def allowEntryTitles(self):
        "Returns whether or not to allow this notebook to specify a title."
        return self.getOptionValue('entry-titles')

    def summarizeLongEntries(self):
        "Returns whether or not this notebook should summarize long entries in lists."
        return self.getOptionValue('summarize-entries')
    
    def allowDelete(self):
        "Returns whether or not this notebook is allowed to have its entries deleted."
        return self.getOptionValue('allow-delete')

    def freeformAuthor(self):
        selected = self.__getAuthorSelectionValue()
        return selected == AuthorListOption.FREEFORM_AUTHOR \
               or selected == AuthorListOption.FREEFORM_AUTHOR_WITH_EMAIL

    def collectAuthorEmail(self):
        "Returns true iff this notebook is set up to accept the author's email address."
        return self.__getAuthorSelectionValue() == AuthorListOption.FREEFORM_AUTHOR_WITH_EMAIL

    def __getAuthorSelectionValue(self):
        return self.getOptionValue('authors')[0]

    def getAuthors(self):
        "Returns a list of authors who use this notebook."
        if self.freeformAuthor():
            return ''
        else:
            return self.getOptionValue('authors')[1]

    def displayAuthor(self):
        "Returns whether or not to display the author as part of an entry."
        return self.freeformAuthor() or len(self.getAuthors()) > 1

    def getDefaultAuthor(self):
        "Returns the first author in this notebook's list of authors."
        author = ''
        authors = self.getAuthors()
        if len(authors):
            author = authors[0]
        return author

    ### Drafts

    def enableDrafts(self):
        "Returns whether or not to enable draft entries for this notebook."
        return self.useCookies() and self.getOptionValue('enable-drafts')

    def allowPubliclySubmittedDrafts(self):
        """Returns whether or not to enable publicly submitted
        entries for this notebook."""
        return self.enableDrafts() and self.getOptionValue('allow-publicly-submitted-drafts')

    def getValidPublicDraftHTMLTags(self):
        """Returns the HTML tags which are valid for publicly
        submitted drafts for this notebook. If this returns an empty
        list, then all tags are valid, but Javascript should be
        filtered out. If it returns None, then no filtering at all
        should be done"""

        val = self.getOptionValue('valid-public-draft-html-tags')
        if not val:
            val = []
        elif string.upper(val) == '[ALL]':
            val = None
        else:
            val = string.split(val, ',')
        return val

    def getPublicDraftSubmissionRedirect(self):
        """Returns the URL to which to redirect after a non-authenticated user submits a draft."""
        return self.getOptionValue('public-draft-submission-redirect')

    ### Images and Files

    def enableImageUpload(self):
        "Returns whether or not to allow images to be associated with entries."
        return self.enableAnyFileUpload() or (self.getOptionValue('enable-image-upload') and self.getMaximumAttachmentSize() > 0)

    def enableAnyFileUpload(self):
        "Returns true iff a file of any type can be associated with an entry."
        return self.getOptionValue('enable-any-upload') and self.getMaximumAttachmentSize() > 0

    def getMaximumAttachmentSize(self):
        "Returns the maximum size of an attachment, in kilobytes."
        default = 0
        val = self.getOptionValue('maximum-attachment-size')
        if val < 0:
            val = default
        try:
            val = int(val)
        except ValueError:
            val = int(default)
        return val

    def getDefaultNumberOfUploadBoxes(self):
        """Returns the default number of upload boxes to display when upload
        boxes are displayed."""
        default = 1
        val = self.getOptionValue('default-upload-boxes')
        try:
            val = int(val)
        except ValueError:
            val = default
        if val < 0:
            val = default
        return val        

    def allowUploadsToNewEntry(self):
        """Returns true iff display boxes should be displayed as part of the
        add screen."""
        return self.getOptionValue('allow-uploads-to-new-entry')

    def showAttachmentHelp(self):
        """Returns true iff NewsBruiser should show the attachment help message."""
        return self.getOptionValue('show-attachment-help')

    def getHeadingAttachmentDesignation(self):
        """Returns what to call an attachment in a heading; 'Image' if
        only images can be uploaded, 'File' otherwise."""
        if self.enableAnyFileUpload():
            return _('File')
        else:
            return _('Image')

    def getAttachmentDesignation(self):
        """Returns what to call an attachment; 'image' if only images can
        be uploaded, 'file' otherwise."""
        if self.enableAnyFileUpload():
            return _('file')
        else:
            return _('image')

    def getPluralAttachmentDesignation(self):
        """Returns what to call attachments; 'images' if only images can
        be uploaded, 'files' otherwise."""
        if self.enableAnyFileUpload():
            return _('files')
        else:
            return _('images')

    def isAcceptableAttachmentFilename(self, name):
        """Returns whether or not the given name means the file is
        acceptable as an attachment. It's pretty pathetic to judge
        whether a file is an image based on its guessed mime type, but
        ATM that's all I've got."""
        return self.enableAnyFileUpload() or self.isImage(name)

    def getContentType(self, name):
        "Returns the likely content type of the given filename."
        return mimetypes.guess_type(name)[0]

    def isImage(self, name):
        """Returns true iff the given name denotes an image.
        Looks at guessed mime type, not contents."""
        type = self.getContentType(name)
        return type and string.find(type, 'image') == 0

    ### Syndication/Notification

    def getExportedSiteName(self):
        """Returns the name to publish as this notebook's name when
        notifying another site of changes or doing RSS syndication."""
        val = self.getOptionValue('exported-site-name')
        if not val:
            val = self.getTitle()
        return val

    def getExportedHost(self):
        """Returns the host to use when exporting a link to a notebook
        entry."""        
        if not hasattr(self, 'exportedHost'):            
            host = self.getOptionValue('exported-host')
            if not host:
                protocol = string.lower(os.environ.get('SERVER_PROTOCOL', ''))
                port = os.environ.get('SERVER_PORT', 80)
                server = string.lower(os.environ.get('SERVER_NAME', ''))
                if not protocol or string.lower(protocol) == 'included':
                    #Wild-ass guess
                    protocol = 'http'
                else:
                    protocol = protocol[:string.index(protocol, '/')]
                if not server:
                    server = 'localhost'
                host = protocol + '://' + server
                if int(port) != 80:
                    host = host + ':' + port
            self.exportedHost = host
        return self.exportedHost

    def getExportedSiteImageURL(self):
        """Returns the URL of the image to use when syndicating this
        notebook."""
        url = self.getOptionValue('exported-site-image')
        if not url:
            url = self.getResourceURL('resources/img/export.png', 1)
        return url

    def getExportedSiteURL(self):
        """Returns the URL of the website to publish as this
        notebook's site when notifying another site of changes or
        when doing RSS syndication."""
        url = self.getOptionValue('exported-site-url')
        if not url:
            from NewsBruiserCGIs import PortalCGI
            url = self.getExportedHost() + self.getURL(PortalCGI.NAME)
        return url

    def syndicationViewOnly(self):
        """Returns whether or not the only purpose of this notebook is to
        print a syndication feed for another site."""
        return self.getOptionValue('syndication-only')

    def getLanguage(self):
        """Returns the language code representing the language in which
        the author of this notebook usually writes."""
        return self.getOptionValue('language')
        
    ### UI Customization

    def getStyleSheet(self):
        "Returns the URL to the stylesheet for this notebook."
        return self.getOptionValue('stylesheet')

    def getDayGuideImageURL(self):
        """Returns the URL to the one-pixel graphic to be used when
        displaying a day guide."""
        return self.getOptionValue('day-guide-image-url')

    def getNumberOfFrontPageEntries(self):
        """Returns the number of entries to display on the notebook's
        front page."""
        return int(self.getOptionValue('number-front-page-entries'))

    def getDisplayTimeFormat(self):
        """Returns the time format with which this notebook's entries
        are labeled."""
        return self.getOptionValue('display-time-format')

    def getEntryBoxLength(self):
        "Returns the size, in number of lines, in the entry box."
        return self.getOptionValue('entry-box-length')

    def getPermalinkText(self):
        "Returns the text to be used in permalinks."
        return self.getOptionValue('permalink-text')

    def getMinimumTimespanGranularity(self):
        return const.GRANULARITIES.index(self.getOptionValue('minimum-timespan-granularity'))

    def getTemplate(self, templateName):
        """Returns a Template object corrsponding to the named
        NewsBruiser template string."""
        template = self.templates.get(templateName)
        if not template:
            var = templateName + '-template'
            template = self.getOptionValue(var)
            if string.find(template, 'file:') == 0:
                file = template[5:]
                path = util.getFileInNewsBruiserBaseDirectory(file)
                if os.path.exists(path):
                    template = open(path).read()
                else:
                    raise _("Couldn't locate template file: %s") % path
            if not template:
                #If the template is eg. 'foo-detail', try the 'foo' template
                #as a last resort.
                i = string.rfind(templateName, '-detail')
                if i > -1:
                    return self.getTemplate(templateName[:i])
            if not hasattr(self, 'templatingSystem'):
                self.templatingSystem = ParenthesizedArgumentsTemplatingSystem()
            template = Template(template, self.templatingSystem)
            self.templates[templateName] = template
        return template

    def getNoEntriesMessage(self):
        """Returns the message to be displayed when there are no entries."""
        if self.getFirstEntry():
            msg = self.getOptionValue('no-entries-message')
        else:
            from nb import CoreCGIs
            import nb.plugin.entry.importer
            from nb.plugin.entry.importer import ImportCGI

            msg = '<p>'        
            msg = msg + _('No entries yet.')
            msg = msg + '</p><p>'
            msg = msg + _('If you switched to NewsBruiser from another blogging tool, you might be able to <a href="%s">import your old entries</a>.') % self.getURL(ImportCGI.NAME)
            msg = msg + '</p><p>'

            conf = CoreCGIs.ConfigureCGI.NAME
            args = { 'configureURL' : self.getURL(conf),
                     'contactInfoURL' : self.getURL(conf, 'group=Notebook'),
                     'licenseURL' : self.getURL(conf, 'group=License') }
            msg = msg + _('If you haven\'t already, you should also <a href="%(configureURL)s">configure your notebook</a>. Be sure to <a href="%(contactInfoURL)s">set your contact information</a> and <a href="%(licenseURL)s">choose a license</a>.') % args
            msg = msg + '</p>'
        return msg

    def getBasicTheme(self):
        "Returns the basic theme for this notebook."
        list = []
        list.append(self.getOptionValue('link-color'))
        list.append(self.getOptionValue('alink-color'))
        list.append(self.getOptionValue('vlink-color'))
        list.append(self.getOptionValue('bgcolor'))
        list.append(self.getOptionValue('text-color'))
        return tuple(list)

    ### Advanced Options

    def getRobotIndexGranularity(self):
        """Returns the granularity at which this notebook should be
        indexed by Web robots."""
        if self.summarizeLongEntries():
            val = const.ENTRY
        else:
            val = self.getOptionValue('robot-index-granularity')
        return val

    def getTimeZone(self):
        """Returns the currently defined timezone for this notebook."""
        if not hasattr(self, 'timezone'):
            self.timezone = self.getOptionValue('time-zone')
            if not self.timezone:
                self.timezone = self.getSystemTimeZone()
        return self.timezone

    def getPerEntryTimeZoneSelection(self):
        """Returns true iff the notebook allows a time zone to be
        selected when an entry is created."""
        return self.getOptionValue('per-entry-time-zone-selection')

    def getViewSortOrder(self):
        order = self.getOptionValue('view-sort-order')
        if (order != const.REVERSE_CHRONOLOGICAL_SORT_ORDER):
            order = const.CHRONOLOGICAL_SORT_ORDER
        return order

    def timeActions(self):
        "Returns whether or not to print timing information for actions."
        return self.getOptionValue('time-actions')

    #### Lower-level methods dealing with manipulating the
    #### configuration data for this notebook.

    def assertDirectory(self):
        dir = self.getDir()
        if not os.path.exists(dir):
            try:
                os.makedirs(dir)
            except:
                args = { 'notebook' : self.name,
                         'directory' : self.getSystemWorkingDirectory(),
                         'uid' : os.getuid() }
                raise _("Couldn't create working directory for notebook %(notebook)s. Please make sure the %(directory)s directory exists and is writable by the webserver (UID %(uid)s).") % args

    def getOption(self, name):
        return self.optionConfig.getOption(name)

    def getOptionValue(self, name):
        return self.optionConfig.getOptionValue(name, self)

    def getConfigurationPath(self):
        "Returns the path to this notebook's generated configuration file."
        return os.path.join(self.getDir(), '.configuration')        

    def readConfiguration(self, path=None):
        map = self.conf
        theming = 0
        if not path:
            path = self.getConfigurationPath()
            if not os.path.exists(path):
                self.writeConfiguration()
        else:
            theming = 1
            map = self.theme
        file = open(path, 'r')
        key = None
        value = None
        for line in file.readlines():
            if line[0] == ' ':
                #Continuation of previous line
                value = map[key] + '\n' + line[1:-1]
            elif line[0] != '#':
                i = string.find(line, '=')
                if i > 0:
                    key = line[0:i]                
                    value = line[i+1:-1]
            if key and value and (not theming or self.optionConfig.wrapper.isThemed(self.optionConfig.getOption(key))):
                if theming:
                    value = util.replaceBaseURLs(value,
                                                 self.nbconfig.baseContentURL)
            map[key] = value

    ### Mutator methods

    def delete(self, password):
        "Deletes this notebook."
        self.rename(password, self.getDeletedName())

    def rename(self, password, newName):
        if string.find(newName, 'deleted-') == 0:
            self.nbconfig.assertValidMasterPassword(password)
        else:
            self.assertValidPassword(password)
        systemDir = self.getSystemWorkingDirectory()
        oldPath = os.path.join(systemDir, self.name)
        newPath = os.path.join(systemDir, newName)
        os.rename(oldPath, newPath)

    def setOrdinal(self, newOrdinal):
        "Sets this notebook's ordinal."
        self.conf['ordinal'] = str(newOrdinal)
        self.writeConfiguration()

    def writeConfiguration(self, changedConfiguration=None):
        "Writes the given set of name-value pairs to the configuration file."
        if not changedConfiguration:
            changedConfiguration = self.conf
        self.assertDirectory()
        oldumask = os.umask(027)
        try:
            file = open(self.getConfigurationPath(), 'w')

            file.write(_('#Generated by NewsBruiser %s\n') % version.version)
            file.write(_('#You can change this manually if you need to, but it would be better if you used\n'))
            file.write(_('#the "configure" screen.\n'))

            for group in self.optionConfig.getOptionGroupList():
                for option in group.getOptions():
                    key = option.name
                    if changedConfiguration.has_key(key):
                        value = changedConfiguration[key]
                    elif self.conf.has_key(key):
                        value = self.conf[key]
                    elif hasattr(option, 'default'):
                        value = option.getDefault(self)
                    else:
                        value = ''
                    value = string.replace(value, '\n', '\n ')
                    file.write('%s=%s\n' % (key, value))
                file.write('\n')
            file.close()
        finally:
            os.umask(oldumask)

    ### Methods dealing with notebook entries.

    def addEntry(self, password, author=None, authorEmail=None, text='',
                 title=None, draft=0, importDateTuple=None,
                 vars={}):
        """Put the specified text in a new entry, attributed to the
        specified author."""

        timezone = vars.get(Entry.ENTRY_TIMEZONE_KEY, None)

        if importDateTuple:
            id = self.getEntryIDForTime(importDateTuple, timezone)
        else:
            id = EntryID(self, draft=draft)

        validTags = None
        if not draft or not self.allowPubliclySubmittedDrafts():
            self.assertValidPassword(password)
        else:
            validTags = self.getValidPublicDraftHTMLTags()            

        if validTags != None:
            author = HTMLStripper.strip(author, validTags)
            authorEmail = HTMLStripper.strip(authorEmail, validTags)
            title = HTMLStripper.strip(title, validTags)
            text = HTMLStripper.strip(text, validTags)        
        
        entry = Entry(id,0)
        if timezone:
            setattr(entry, Entry.ENTRY_TIMEZONE_KEY, timezone)

        if importDateTuple:            
            if time.mktime(importDateTuple) - (time.timezone-entry.getGMTOffset(importDateTuple[8])) > time.time():
                raise _("You can't import an entry into the future!")
            entry.time = time.strftime(const.STORAGE_TIME_FORMAT, importDateTuple)
        else:
            entry.time = None

        if self.displayAuthor():
            if not author:
                author = self.getDefaultAuthor()
            entry.author = author  
            if self.collectAuthorEmail() and authorEmail:
                entry.authorEmail = authorEmail

        if self.allowEntryTitles():
            entry.title = title
        entry.text = text
        entry.lastModifiedTime = time.time()

        #When importing to a date prior to the current first entry of
        #this notebook, remove .firstEntry before creating this entry.
        if importDateTuple:
            firstEntry = self.getFirstEntry()

        if id:
           #Move any entry we'd be overwriting, and subsequent
           #entries, out of the way.
           oldOrdinal = id.ordinal
           id.ordinal = const.ALL
           entries = id.getEntries()[oldOrdinal:]
           id.ordinal = oldOrdinal
           entries.reverse()
           if entries:
               indexer = self.getIndexer()
               for oldEntry in entries:
                   oldEntry.changeOrdinal(str(int(oldEntry.id.ordinal)+1))
               indexer.save_index()               
        for hook in Entry.instantiationFromInputHooks:
            hook(vars, entry)

        entry.setTimeZone()
        entry.write()
        entry.unsetTimeZone()        

        if importDateTuple and firstEntry and entry.getTimeTuple() < firstEntry.getTimeTuple():
            filename = self.__getFirstEntryFilename() 
            if os.path.exists(filename):
                os.remove(filename)

        if not draft:
            self.nbconfig.pluginEvent('EntryPublished', entry)
        return entry

    def getEntries(self, year=const.ALL, month=const.ALL, day=const.ALL,
                   ordinal=const.ALL, draft=0, limit=None): 
        """Returns all the entries matching the provided
        signature. Each of the first four arguments can be a number,
        or ALL. The fifth argument imposes a limit on the number of
        entries to process; it's useful, for instance, if you know
        you're only going to get one entry."""
        return EntryID(self, year, month, day, ordinal, draft).getEntries(limit)

    def getEntryIDForTime(self, timeTuple, timezone=None):
        """Returns an entry ID corresponding to the entry ID an entry
        retroactively published to the given time would have."""
        gmtOffset = 0
        if timezone:
            gmtOffset = TimeZoned(timezone).getGMTOffset(timeTuple[8])
        newTime = time.localtime(time.mktime(timeTuple)+gmtOffset)
        (year, month, day) = timeTuple[:3]
        id = EntryID(self, year, month, day)
        entries = id.getEntries()
        for ordinal in range(0, len(entries)):
            t = entries[ordinal].getTimeTuple(1)
            if t and t > newTime:
                id.ordinal = ordinal
                break
        if id.ordinal == const.ALL:
            id.ordinal = len(entries)
        return id

    def __getFirstEntryFilename(self, draft=0):
        return os.path.join(self.getDir(draft), '.firstEntry')

    def getFirstEntry(self, draft=0):
        "Returns the first entry ever made to this notebook."

        #This method is used a lot in ControlPanel, but when there are many
        #entries, it takes a long time to find the first entry because we
        #have to sort a big list. So we cache the first entry (which never
        #changes) in a file. If for some reason an earlier entry is created,
        #you'll have to remove the file (if the first entry is deleted,
        #NewsBruiser will recreate the file automatically). We also cache the
        #first entry in memory so that the file will only be read once over
        #the lifetime of a CGI.

        #An alternative to this would be to change the architecture so that
        #it did a series of ever-more-specific globs instead of one big
        #glob. I don't think it's worth it.
        attr = 'firstEntry-' + str(draft)
        if not hasattr(self, attr):
            filename = self.__getFirstEntryFilename(draft)
            if os.path.exists(filename):
                file = open(filename)
                firstEntryPath = file.readline()
                file.close()
                parts = re.split('/', firstEntryPath)
                first = 1
                if draft:
                    first = 2
                (year, month, day, ordinal) = parts[first:]
                try:
                    setattr(self, attr, Entry(EntryID(self, year, month, day,
                                                      ordinal, draft)))
                except:
                    #There was some problem, we'll try to get it manually.
                    pass
            if not hasattr(self, attr):
                entries = self.getEntries(const.ALL, const.ALL, const.ALL,
                                          const.ALL, draft, 1)
                if len(entries) > 0:
                    setattr(self, attr, entries[0])
                    file = open(filename, 'w')
                    file.write(getattr(self, attr).id.asPathInfo())
                    file.close()
                    os.chmod(filename, const.FILE_MODE)
                else:
                    setattr(self, attr, None)
        return getattr(self, attr)

    def getMostRecentEntry(self, draft=0):
        "Returns the most recent entry ever made to this notebook."
        entries = self.getPreviousEntries(1, draft)
        if not entries or len(entries) < 1:
            return None
        else:
            return entries[0]

    def getPreviousDaysEntries(self, numberOfDays, draft=0):
        "Returns all entries for the given number of recent days."
        return self.getPreviousEntries(numberOfDays, draft, 'day')

    def getPreviousEntries(self, number, draft=0, count='number'):
        """Returns the given number of recent entries, or entries from
        the given number of recent days with entries."""
        entries = []
        number = int(number)
        day = self.today()
        args = list(day)
        args.extend([const.ALL, draft])
        firstEntry = self.getFirstEntry(draft)
        if count == 'day':
            days = 0            
        if firstEntry != None:            
            firstDay = firstEntry.id.asDate()
            while not DateArithmetic.dayIsBefore(day, firstDay) and \
                      ((count == 'number' and len(entries) < number) \
                      or (count == 'day' and days < number)):
                newEntries = apply(self.getEntries, args)
                newEntries.reverse()
                entries.extend(newEntries)
                day = DateArithmetic.previousDay(day)   
                args = list(day)
                args.extend([const.ALL, draft])
                if count == 'day' and newEntries:
                    days = days + 1
            if count == 'number':
                entries = entries[:number]
        #Canonical form is chronological order, but we got them in reverse
        #chronological.
        entries.reverse()
        return entries

    def getThisMonthsEntries(self):
        """Returns all the entries made in this notebook during the
        current month."""        
        return apply(self.getEntries, self.today()[:2])

    def getCurrentTimespanCalendar(self, viewer=None):
        (year, month) = self.today()[:2]
        if viewer:
            entryID = None
            if viewer.entryID:
                entryID = viewer.entryID
            elif viewer.entries:
                entryID = viewer.entries[0].id
            if entryID:
                (year, month) = (entryID.year, entryID.month)
        return self.getCalendar().getMonth(year, month, displayYear=1)

    def getCurrentMonthEntryCount(self, viewer):
        """Returns the number of entries posted this month, as a
        descriptive string."""
        l = len(self.getThisMonthsEntries())
        what = _('entries')
        if l == 1:
            what = _('1 entry this month.')
        else:
            what = _('%s entries this month.') % l
        return what

    def getNumberOfEntries(self):
        "Returns the number of entries posted to this notebook."
        self.processIndexUpdateList()
        indexer = self.getIndexer()
        return len(indexer.files.keys())-1

    def getNthEntry(self, n):
        "Returns the nth entry posted to this notebook."
        self.processIndexUpdateList()
        indexer = self.getIndexer()
        files = indexer.files.keys()
        files.sort()
        idGen = EntryID(self)
        id = idGen.filenamesToEntryIDs([files[n]])[0]        
        return Entry(id)

    def getRandomEntries(self, number=1):
        """Returns one or more randomly selected entries from this
        notebook."""

        #Note: There are two ways of doing this; one can load the
        #search index into memory and look at all the keys, or one
        #can look at the filesystem and gather all the files. When the
        #filesystem is fast, the second method is slightly faster. When
        #the filesystem is slow, the first method is much faster. So
        #I've gone with the first method, but left the second method
        #commented out so that you can change it back.
        
        #time1 = time.time()
        #
        self.processIndexUpdateList()
        indexer = self.getIndexer()
        path = None
        keys = copy.copy(indexer.files.keys())
        keys.remove('_TOP')
        entriesByTime = {}
        entries = []
        i = 0
        while keys and i < number:
            path = whrandom.whrandom().choice(keys)
            keys.remove(path)
            idGen = EntryID(self)
            id = idGen.filenamesToEntryIDs([path])[0]
            entry = id.getEntries()[0]
            entriesByTime[entry.getTimeTuple()] = entry
            i = i + 1
        keys = entriesByTime.keys()
        keys.sort()
        keys.reverse()
        for key in keys:
            entries.append(entriesByTime[key])

        #allEntriesID = EntryID(self, const.ALL, const.ALL, const.ALL,
        #                       const.ALL)
        #ids = allEntriesID.getIDs()
        #id = whrandom.whrandom().choice(ids)
        #
        #time2 = time.time()
        #print "That took %s ms" % (time2-time1)

        return entries

    def entryAlreadyExists(self, timeTuple, title, text):
        """If it looks like the entry described already exists in the
        notebook (this happens if time, title, and text all match),
        returns the seemingly duplicate entry. This is probably
        because the entry was already imported."""
        duplicate = 0
        if not timeTuple:
            return duplicate

        #Ignore DST since time.strptime doesn't set the DST flag.
        #This will only break if you post an entry in the hour before
        #the DST 'fall back' and then post the exact same entry
        #exactly an hour later. Should still be fixed though.
        timeTuple = list(timeTuple)
        timeTuple[8] = 0
        timeTuple = tuple(timeTuple)
        newTime = time.mktime(timeTuple)
        (year, month, day) = timeTuple[:3]
        id = EntryID(self, year, month, day)
        for entry in id.getEntries():
            t = entry.getTimeTuple()            
            if t:
                t = list(t)
                t[8] = 0
                t = tuple(t)
                t = time.mktime(t)
            if title:
                title = string.replace(title, '\n', ' ')
            if t == newTime and entry.getTitle() == title and string.strip(entry.text) == string.strip(text):
                duplicate = entry
                break
        return duplicate

    #### Entries dealing with drafts
    def getDrafts(self):
        return EntryID(self, year=const.ALL, draft=1).getEntries()

    #### Template system methods

    def _(self, context, text):
        "Simple wrapper for I18N method."
        return _(text)

    def interpolateTemplate(self, templateName, commandMap, objects, context,
                            out=sys.stdout, backupObjects=None):
        """Interpolates the named template using the given map of
        commands to method names. Methods are called on the provided
        object and the provided context is used as an argument to the
        method (if it takes an argument)."""
        if backupObjects == None:
            backupObjects = []
        if not self in backupObjects:
            backupObjects.insert(0, self)
        return self.getTemplate(templateName).interpolate(commandMap, objects,
                                                          backupObjects, context,
                                                          out)

    def getYearSpan(self):
        """Returns the first and last years run by this notebook."""
        firstEntry = self.getFirstEntry()
        mostRecentEntry = self.getMostRecentEntry()
        
        span = ''
        if firstEntry and mostRecentEntry:
            first = firstEntry.id.year
            last = mostRecentEntry.id.year
            if first == last:
                span = str(first)
            else:
                span = '%s-%s' % (first, last)            
        return span

    def getBaseContentURL(self):
        """Returns the base content URL."""
        return self.nbconfig.baseContentURL

    def getResourceURL(self, path, full=0):
        """Returns the full or relative URL to a static resource
        underneath the base content directory."""
        toJoin = [self.getBaseContentURL(), path]
        if full:
            toJoin.insert(0, self.getExportedHost())
        return apply(util.urljoin, toJoin)

    def getMonthlyArchive(self, viewer):
        s = ''
        template = self.getTemplate('month-archive-link')
        map = CommandMaps.ARCHIVE_LINK_COMMAND_MAP
        entries = self.getEntries(limit=1)
        if not entries:
            s = 'None'
        else:
            entry = entries[0]
            monthID = entry.id.getMonthID()
            while monthID:
                entries = monthID.getEntries(1)
                if entries:
                    template.interpolate(map, monthID, context=viewer,
                                         writeTo=viewer.out) 
                    viewer.out.write(' ')
                monthID = monthID.previousMonth()

    def getNewsBruiserLink(self):
        """Returns a link to the NewsBruiser homepage."""
        return '<a href="http://newsbruiser.tigris.org/">NewsBruiser</a>'
        
    def getNewsBruiserVersion(self):
        """Returns the current version of NewsBruiser."""
        import version
        return version.version
    
    def getReaderLinksPanel(self, viewer):
        """Interpolates the reader links panel, iff there are any
        entries or categories."""
        if self.getFirstEntry() or (self.enableCategories() and (self.getCategories() or viewer.authenticated)):
            self.interpolateTemplate('panel-reader-links',
                                     CommandMaps.BASIC_COMMAND_MAP,
                                     viewer, viewer, out=viewer.out)

    def getSearchBoxPanel(self, viewer):
        """Interpolates the search box panel, iff there are any entries."""
        if self.getFirstEntry():
            self.interpolateTemplate('panel-search-box',
                                     CommandMaps.BASIC_COMMAND_MAP,
                                     viewer, viewer, out=viewer.out)

    def getAdminLinksPanel(self, viewer):
        """Interpolates the admin links panel, iff the user is authenticated."""
        self.interpolateTemplate('panel-admin-links',
                                 CommandMaps.BASIC_COMMAND_MAP,
                                 viewer, viewer, out=viewer.out)

    def getReaderLinks(self, viewer):
        """Returns links that a reader of the notebook might be
        interested in."""
        s = ''
        if self.enableCategories():
            from nb.plugin.entry.annotate.Categories import CategoryScreens
            categoryLink = self.getURL(CategoryScreens.CategoryListCGI.NAME)
            if self.getCategories():
                s = s + self.getLink(categoryLink, _('Categories')) + ' '
                if viewer.authenticated:
                    s = s + self.getPanelLink(categoryLink + '?mode=edit',
                                              _('Edit')) + ' '
            elif viewer.authenticated:
                s = s + self.getPanelLink(self.getURL(CategoryScreens.CategoryAddCGI.NAME),
                                          _('Add category')) + ' '
        if self.getFirstEntry():
            from nb.plugin.entry.render.syndication import SyndicationCGI
            from CoreCGIs import RandomCGI
            #TODO: Fold into plugins.
            s = s + self.getLink(self.getURL(RandomCGI.NAME), _('Random')) + ' '
            syndicator = self.getDefaultSyndicator()
            icon = syndicator.getIconText()
            args = {'format' : syndicator.getHumanReadableName(),
                    'notebook' : self.getTitle()}
            title = _('%(format)s feed for %(notebook)s') % args
            s = s + self.getLink(self.getURL(SyndicationCGI.NAME), icon,
                                 icon + 'SyndicationButton', title=title) + ' '
                                 
        return s

    def getAdminLinks(self, viewer):
        s = ''
        import CoreCGIs
        import NewsBruiserCGIs
        if viewer.authenticated:
            if self.enableDrafts():
                #Get all the drafts; if there are any, print a link to
                #edit them.
                drafts = self.getDrafts()
                n = len(drafts)
                if n > 0:
                    what = 'Drafts'
                    if n == 1:
                        what = 'Draft'
                    self.panelLink(self.getURL(CoreCGIs.EditCGI.NAME,
                                               pathInfo='drafts'),
                                   '%s %s' % (n, what))

            s = s + self.getLink(self.getURL(CoreCGIs.AddCGI.NAME), _('Post')) + ' '
            if self.getFirstEntry():
                s = s + self.getLink(self.getURL(CoreCGIs.EditCGI.NAME),
                                     _('Edit')) + ' '
            s = s + self.getLink(self.getURL(CoreCGIs.ConfigureCGI.NAME),
                                 _('Configure'))  + ' '
            s = s + self.getLink(self.getURL(CoreCGIs.UtilityCGI.NAME),
                                 _('Util'))  + ' '
            s = s + self.getLink(self.getURL(CoreCGIs.LogoutCGI.NAME),
                                 _('Log out'))  + ' '
        elif not viewer.authenticated:
            if self.allowPubliclySubmittedDrafts():
                s = s + self.getLink(self.getURL(CoreCGIs.AddCGI.NAME), _('Submit story'))  + ' '
            s = s + '<div class="LoginBox">'
            s = s + self.getStartForm(self.getURL(CoreCGIs.LoginCGI.NAME))
            #FIXME: This is an invalid assumption--it should be the current
            #URL, whatever that is.
            s = s + self.getHiddenField(const.REFERER_CGI_KEY,
                                        self.getURL(NewsBruiserCGIs.PortalCGI.NAME))
            s = s + viewer.cgi.getPasswordField(size=10, force=1)
            s = s + self.getEndForm(_('Login'))
            pos = string.rindex(s, "</form>") + len("</form>")
            s = s[:pos] + "</div>" + s[pos:]
        return s

    #### Methods dealing with the index

    def ensureIndexDir(self):
        """Ensure the existance of the index directory for this
        notebook. Returns true if the directory already existed."""
        existed = 1
        dir = self.getIndexDir()
        if os.path.exists(dir):
            if not os.path.isdir(dir):
                raise _("Not a directory: %s") % dir
        else:
            existed = 0
            try:
                os.makedirs(dir)
            except OSError:
                raise _("Can't create directory %s") % dir
        return existed

    def getIndexer(self, load=1):
        "Returns an indexer for this notebook."
        if not self.indexer:
            self.indexer = NewsBruiserEntryIndexer(
                INDEXDB=os.path.join(self.getIndexDir(), 'index'),
                ADD_PATTERN=re.compile('^[0-3][0-9]-[0-9]*'),
                REINDEX=1)
            self.indexer.notebook = self
            if load:
                self.indexer.load_index()
        return self.indexer

    def recreateIndex(self):
        "Deletes the index and recreates it from scratch."
        self.ensureIndexDir()
        dir = self.getIndexDir()
        for file in os.listdir(dir):
            os.remove(os.path.join(dir, file))
        self.updateIndex(None)

    def addToIndexUpdateList(self, entry):
        """Adds the file represented by the given Entry object to a list of
        entries which need to be processed before the next search."""
        if not entry.id.draft:
            self.ensureIndexDir()
            path = self.getIndexUpdateListPath()
            file = open(path, 'a')
            file.write(entry.id.asPath())
            file.write('\n')
            file.close()
            os.chmod(path, const.FILE_MODE)
                
    def processIndexUpdateList(self):
        """Updates the index with each nonduplicate file mentioned in the
        update list, then saves the index."""
        path = self.getIndexUpdateListPath()
        if os.path.exists(path):
            indexed = []
            file = open(self.getIndexUpdateListPath(), 'r+')
            process = 0
            if 'portalocker' in sys.modules.keys():
                try:                
                    portalocker.lock(file,
                                     portalocker.LOCK_EX | portalocker.LOCK_NB)
                    process = 1
                except IOError:
                    #Another process is already taking care of this for us.
                    pass
            else:
                process = 1
            if process:
                todo = file.readlines()
                if todo:
                    indexer = self.getIndexer()
                    for entryPath in todo:
                        entryPath = entryPath[:-1]
                        try:
                            indexed.index(entryPath)
                        except ValueError:
                            indexer.add_file(entryPath)
                            indexed.append(entryPath)
                    indexer.save_index()
                    file.truncate(0)
                    file.close()        
        
    def updateIndex(self, entry=None):
        """Adds (possibly overwriting) the file represented by the
        given Entry object to the index, or adds all new entry objects
        if none is given."""            
        if entry and entry.notebook.name != self.name:
                raise _('Entry from wrong notebook given to %s indexer!') % self.name
        indexer = self.getIndexer()
        if self.ensureIndexDir():
            if entry:
                indexer.add_file(entry.id.asPath())
            else:
                indexer.add_files(self.getDir())
            indexer.save_index()
        else:
            self.recreateIndex()            

    def getEntriesMatching(self, query=''):
        """Returns all the entries whose texts contain all of the
        given words."""

        if not self.ensureIndexDir():
            self.recreateIndex()
        self.processIndexUpdateList()

        words = re.split('\s', query)
        results = self.getIndexer(0).find(words)
        if results:
            filenames = results.values()
        else:
            filenames = []        

        thisIsAHack = EntryID(self)
        entryIDs = thisIsAHack.filenamesToEntryIDs(filenames)
        return map(Entry, entryIDs)

    #### Methods dealing with the calendar.
    def getCalendar(self):
        return NotebookCalendar(self)

    def getCurrentMonthCalendar(self):
        """An overly simplified interface to the notebook calendar for
        use within a template."""
        (year, month) = self.today()[:2]
        return self.getCalendar().getMonth(year, month, displayYear = 1)

    #### Methods dealing with Bayesian classification
    def getBayesianClassifier(self):
        if not hasattr(self, 'bayesianClassifier'):            
            corpusFile = os.path.join(self.getDir(), '.bayesianCorpus')
            self.bayesianClassifier = BayesianClassifier(corpusFile)
        return self.bayesianClassifier

    #### Methods dealing with statistics

    def getStatistics(self):
        return NotebookStatistics(self)

    #### Static file synchronization method

    def updateStaticWriters(self):
        if hasattr(self.nbconfig, 'staticEntryWriters'):
            for writer in self.nbconfig.staticEntryWriters:
                writer.writeAll(self)

    #### Backup methods

    def getTarball(self):
        args = 'cz -C%s ./%s --exclude .index --exclude .firstEntry' % (self.getSystemWorkingDirectory(), self.name)
        command = 'tar ' + args
        return commands.getoutput(command)

    #### Date validation methods
    def verifyDayFit(self, draft, yearmonthday):
        "Returns the given (year,month,day), or None if the (year,month,day) is in the future or before this notebook's first entry."
        first = self.getFirstEntry(draft)
        if first == None:
            return None
        firstDay = (first.id.year, first.id.month, first.id.day)
        if yearmonthday == None or DateArithmetic.dayIsAfter(yearmonthday, self.today()) or DateArithmetic.dayIsBefore(yearmonthday, firstDay):
            return None
        else:
            return yearmonthday

    def verifyMonthFit(self, draft, yearmonth):
        """Returns the given (year,month), or None if the (year,month)
        is in the future or before this notebook's first entry."""
        first = self.getFirstEntry(draft)
        if yearmonth == None or first == None or DateArithmetic.monthIsAfter(yearmonth, self.today()[:2]) or DateArithmetic.monthIsBefore(yearmonth, (first.id.year, first.id.month)):
            return None
        else:
            return yearmonth

    def verifyYearFit(self, draft, year):
        """Returns the given year, or None if the year is in the future
        or before this notebook's first entry."""
        first = self.getFirstEntry(draft)
        if year == None or first == None or year > self.today()[0] or year < first.id.year:
            return None
        else:
            return year
    
    #### Self-test
    def selfTest(self):
        """Prints out notebook configuration and PATH_INFO-style IDs
        of all the entries."""

        print _("Keys for %s:") % self.name
        for key in self.conf.keys():
            print ' %s=%s' % (key, self[key])
        print

        #TODO: Category self-test hook.
                
        print _("Locating entries...")
        entries = self.getEntries()
        print _("Found %s:") % len(entries)
        for i in entries:
            print " %s" % i.id.asPathInfo()

if __name__=='__main__':
    from NBConfig import NBConfig 
    config = NBConfig()
    config.getDefaultNotebook().selfTest()
