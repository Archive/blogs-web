"""A class containing the logic for the NewsBruiser CGIs. Created to
allow multiple .cgi files access to the same logic, and so the CGI
logic can be compiled into .pyc code for performance purposes."""

import os
import stat
import string
import sys
import time
import types
import urllib

from nb.lib import xmlrpclib
from nb.lib import IWantOptions

from nb.ControlPanel import ControlPanel
from nb.NBCGI import EntryCGI
from nb.NBCGI import NBCGI
from nb.Viewer import Viewer
from nb import Options
from nb.TimeZoned import TimeZoned

from nb.Entry import Entry

from nb import const
from nb import util
from nb import version

from nb.NewsBruiserCGIs import DispatchCGI
DispatchCGI.cgiModules.append(__name__)

class ViewCGI(NBCGI):

    """
    A viewer for by-year/month/day, or single-entry view. Also views drafts.
    """

    NAME = 'view'

    ORDER_KEY = 'order'

    def __init__(self, title='View', cgi=None, pathInfo='', out=sys.stdout,
                 writingToFile=0, forceEditLink=0):
        if not cgi:
            cgi = self.NAME
        self.title = title
        self.cgi = cgi
        self.out = out
        self.forceEditLink = 0
        self.writingToFile = writingToFile
        if writingToFile:
            self.printedHeader = 1
            self.preAuth = 0
            self.ignoreAuthCookie = 1
            self.forceEditLink = forceEditLink
        NBCGI.__init__(self, pathInfo=pathInfo)

    def run(self):        
        title = ''
        if self.id.granularity() == const.ORDINAL:
            title = self.id.getEntries()[0].sample()
        else:
            title = self.id.asDescription()
        self.processHookActions()
        self.startPage(title=title, titlePrefix=None)
        self.view()
        self.endPage()

    def headHook(self):
        head = ''
        if hasattr(self, 'id'):
            granularity = self.notebook.getRobotIndexGranularity()
            index = 'none'
            if self.id:
                index = self.id.granularity() == granularity
            follow = granularity != 'none'
            head = self.getRobotExclusionTag(index, follow)
        return head

    def buildViewer(self):
        v = NBCGI.buildViewer(self)
        if self.variables.has_key(self.ORDER_KEY):
            order = self.variables[self.ORDER_KEY]
        else:
            order = self.notebook.getViewSortOrder()
            if order == const.REVERSE_CHRONOLOGICAL_SORT_ORDER:
                order = 0
            else:
                order = 1
        order = order

        detail = (self.id.granularity() == const.ORDINAL)

        v.order = order
        v.detail = detail
        if hasattr(self, 'id'):
            v.entryID = self.id
        return v
        
    def view(self):        
        panel = ControlPanel(self.notebook)
        panel.printPanel(self.id)
        self.viewer.render(self.out)
        self.printDisplaySnippets('belowEntries')
        if len(self.entries) > 10:
            panel.printPanel(self.id, "")  
        if self.viewer.detail:
            entry = self.entries[0]
            if self.preAuth or not self.notebook.useCookies() or self.forceEditLink:
                self.addNavLink(entry.id.asLink(EditCGI.NAME), 'Edit')
            if self.preAuth or not self.notebook.useCookies():
                numAttachments = len(entry.getAttachmentNames())
                what = str(numAttachments) + ' ' + string.capitalize(self.notebook.getAttachmentDesignation())
                if numAttachments != 1:
                    what = what + 's'
                if self.notebook.enableImageUpload() or numAttachments:
                    self.addNavLink(entry.id.asLink(AttachmentManagerCGI.NAME,
                                                    'mode=edit'), what)

                if self.notebook.allowDelete():
                    self.addNavLink(entry.id.asLink(DeleteCGI.NAME), _('Delete'))
        if not self.viewer.detail and (self.preAuth or not self.notebook.useCookies()):
            self.addNavLink(self.notebook.getURL(AddCGI.NAME), _('Post'))

class AddCGI(EntryCGI):

    NAME = 'add'

    SAVE_AS_DRAFT = _("Save as draft")
    IMPORT_KEY = _('Import')
    IMPORT_DATE_KEY = 'importDate'
    PUBLISH = _('Publish')

    def run(self):
        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.add()
        else:
            self.printForm()

    def add(self):
        self.assertVars([const.ENTRY_CGI_KEY])

        draft = 0
        if self.var(self.SAVE_AS_DRAFT):
            draft = 1
        if draft and not self.notebook.enableDrafts():
            self.printError(_("You can't save this entry as a draft because this notebook doesn't have drafts enabled."))
        publicDraft = draft and self.notebook.enableDrafts() and self.notebook.allowPubliclySubmittedDrafts()

        entryID = None
        if not publicDraft:
            self.assertVars([const.PASSWORD_CGI_KEY])

        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        entry = self.var(const.ENTRY_CGI_KEY)
        if self.notebook.allowEntryTitles():
            title = self.var(const.TITLE_CGI_KEY)
        else:
            title = None
        author = None
        authorEmail = None
        if self.notebook.displayAuthor:
            author = self.var(const.AUTHOR_CGI_KEY)
        if self.notebook.collectAuthorEmail:
            authorEmail = self.var(const.AUTHOR_EMAIL_CGI_KEY)

        importDateTuple = None
        if not draft:
            importDate = self.var(self.IMPORT_DATE_KEY)
            if importDate:
                timezone = self.var(Entry.ENTRY_TIMEZONE_KEY)
                zone = TimeZoned(timezone)
                try:
                    zone.setTimeZone()
                    importDateTuple = time.strptime(importDate,
                                                    const.STORAGE_TIME_FORMAT)
                    zone.unsetTimeZone()
                except:
                    self.printError(_("Couldn't parse import date: %s") % importDate)
        if not publicDraft and title and self.notebook.validatePassword(title):
            self.printError(_("I don't think you meant to enter that as the title."))

        files = []
        if self.notebook.allowUploadsToNewEntry():
            files = self.getUploadedAttachments()

        goodPassword = self.notebook.validatePassword(givenPassword)
        if publicDraft or goodPassword:
            entry = self.notebook.addEntry(givenPassword, author, authorEmail,
                                           entry, title, draft,
                                           importDateTuple, self.variables)
            if files:
                for (name, contents) in files.items():
                    entry.setAttachment(givenPassword, name, contents)
        elif not publicDraft:
            wrong = _('Incorrect password.')
            self.startPage(wrong)
            self.printError(wrong)

        if publicDraft:
            url = self.notebook.getPublicDraftSubmissionRedirect()
            if not url:
                from NewsBruiserCGIs import PortalCGI
                url = self.notebook.getURL(PortalCGI.NAME)
            self.redirectTo(url)
        else:
            savedCode = self.PUBLISH
            if draft:
                savedCode = self.SAVE_AS_DRAFT
            self.redirectTo(entry.id.asLink(EditCGI.NAME,
                                            EditCGI.SAVED + '=' + savedCode))

    def printTimeZoneSelection(self):
        self.listBox(Entry.ENTRY_TIMEZONE_KEY, IWantOptions.timeZones.getSelectionValueDescriptions()[1:], IWantOptions.timeZones.getSelectionValues()[1:], self.notebook.getTimeZone())

    def printForm(self):
        draftMode = (not self.preAuth) and self.notebook.enableDrafts() and self.notebook.allowPubliclySubmittedDrafts()
        importMode = self.var(self.IMPORT_KEY)

        if draftMode:
            what = _('Submitting a story to %s')
        elif importMode:
            what = _('Importing into %s')
        else:
            what = _('Adding to %s')
        self.startPage(what % self.notebook.getTitle())
        encType=None
        if self.notebook.allowUploadsToNewEntry():
            encType='multipart/form-data'
        self.startForm(self.getURL(), encType=encType, name='addForm')
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')

        self.printHeading(what % self.notebook.getTitle())

        print '<p>'
        if not draftMode:
            self.passwordField()
        authorHeading = None
        emailHeading = None
        if importMode:
            print '<p><b>%s</b>' % _('Original posting date:')
            sample = time.strftime(const.STORAGE_TIME_FORMAT,
                                   time.localtime(time.time()))
            l = len(sample)
            self.textField(self.IMPORT_DATE_KEY, sample, l, l)
            print '</p>'
            
        if draftMode:
            authorHeading = _('Your name')
            emailHeading = _('Your email address')
            
        self.authorControl(self.notebook, authorHeading=authorHeading,
                           emailHeading=emailHeading)
        print '</p>'
        if self.notebook.allowEntryTitles():
            print '<p>'
            heading = None
            if draftMode:
                heading = _('Story title')
            else:
                heading = _('Title')
            self.titleField(heading=heading)
            print '</p>'

        showBoxes = self.notebook.allowUploadsToNewEntry()
        numBoxes = self.getNumberOfUploadBoxes()
        showHelp = showBoxes and numBoxes and self.notebook.showAttachmentHelp()
        self.printDisplaySnippets('aboveEntryText')
        print '<table><tr><td valign="top">'
        self.textArea(self.notebook.getEntryBoxLength(), 70,
                      const.ENTRY_CGI_KEY)
        self.printDisplaySnippets('rightOfEntryText')

        if showHelp:
            print '</td><td valign="top">'
            self.printEntryInterpolationHelp()

        print '</td></tr></table>'

        if not importMode and self.notebook.getPerEntryTimeZoneSelection():
            print '<p><b>%s</b>' % _('Your time zone:')
            self.printTimeZoneSelection()
            print '</p>'
            
        if showBoxes:
            if self.notebook.showAttachmentHelp():
                print '<p>'
                if numBoxes == 1:
                    if self.notebook.getAttachmentDesignation() == 'image':
                        print _('If you want to associate an image with this entry, upload it here:')
                    else:
                        print _('If you want to associate a file with this entry, upload it here:')
                        
                else:
                    print _('If you want to associate any %s with this entry, upload them here:') % self.notebook.getPluralAttachmentDesignation()
                print '</p>'
            self.printAttachmentUploadBoxes()

        self.printDisplaySnippets('belowEntryText')

        print '<p>'

        if self.notebook.enableDrafts() and not importMode:
            if draftMode:
                buttonText = _('Submit')
            else:
                buttonText = self.SAVE_AS_DRAFT
            self.submitButton(buttonText, self.SAVE_AS_DRAFT)
        if not draftMode:
            if importMode:
                title = _('Import')
            else:
                title = self.PUBLISH
            self.submitButton(title, 'Button')

        self.endForm(None)
        self.endPage()

class AttachmentManagerCGI(NBCGI):

    NAME = 'attachment'

    DELETE_KEY = _('Delete')
    UPLOAD_KEY = _('Upload')

    DELETE_ATTACHMENT_PREFIX = 'deleteAttachment '

    def run(self):
        edit = self.var('mode') == 'edit'

        if edit:
            if not self.id.granularity() == const.ORDINAL:
                self.printError(_("You must specify a particular notebook entry."))
            titlePrefix = _("NewsBruiser: Managing %s: ") % self.notebook.getPluralAttachmentDesignation()
            if (self.var(self.DELETE_KEY) or self.var(self.UPLOAD_KEY)):
                self.assertVars([const.PASSWORD_CGI_KEY])
                if not self.notebook.validatePassword(self.var(const.PASSWORD_CGI_KEY)):
                    self.printError(_("Invalid password."))

            if self.var(self.DELETE_KEY):
                self.delete()
            elif self.var(self.UPLOAD_KEY):
                self.upload()
            self.startPage(title=self.id.asDescription(),
                           titlePrefix=titlePrefix)        
            self.printForm()
            self.addNavLink(self.id.asLink(ViewCGI.NAME), _('View entry'))
            self.addNavLink(self.id.asLink(EditCGI.NAME), _('Edit entry'))
            self.endPage()
        else:
            if self.id.granularity() == const.ORDINAL and self.id.filename:
                what = self.notebook.getAttachmentDesignation()
                name = self.id.filename
                entry = self.entries[0]
                if not entry.getAttachmentNames().count(name):
                    self.printError(_("No such %s for this entry: %s") % (what,
                                                                       name))

                # add some headers to aid HTTP caching
                filename = entry.getAttachmentPath(name)
                try:
                    info = os.stat(filename)
                    if self.checkFreshness(info[stat.ST_MTIME],
                                           '%x-%x-%x' % (info[stat.ST_INO],
                                                         info[stat.ST_SIZE],
                                                         info[stat.ST_MTIME])):
                        return
                except OSError:
                    pass

                attachment = entry.getAttachment(name)
                print 'Content-Type: %s' % self.notebook.getContentType(name)
                print 'Content-Length: %d\n' % len(attachment)
                print attachment
            else:
                title = self.id.asDescription()
                self.startPage(title, titlePrefix=None)
                self.viewer.attachments = 1
                self.viewer.verbose = self.var('mode') == 'inline'
                panel = ControlPanel(self.notebook, AttachmentManagerCGI.NAME)
                panel.printPanel(self.id)
                self.viewer.render()
                panel.printPanel(self.id, "")  
                if self.viewer.verbose:
                    args = ''
                    desc = _("Don't show images inline")
                else:
                    args = 'mode=inline'
                    desc = _('Show images inline')
                self.addNavLink(self.id.asLink(AttachmentManagerCGI.NAME, args),
                                desc)
                self.endPage()

    def delete(self):
        "Verify password and delete selected attachments."
        entry = self.entries[0]
        password = self.var(const.PASSWORD_CGI_KEY)

        toDelete = self.getFormValuesWithPrefix(self.DELETE_ATTACHMENT_PREFIX)
        for file in toDelete.keys():
            entry.deleteAttachment(password, file)
    
    def upload(self):
        "Verify password and make changes to attachments."
        entry = self.entries[0]
        password = self.var(const.PASSWORD_CGI_KEY)
        files = self.getUploadedAttachments()
        for (name, file) in files.items():
            entry.setAttachment(password, name, file)

    def printForm(self):
        "Print a form for editing a particular entry."
        entry = self.entries[0]
        what = self.notebook.getAttachmentDesignation()
        pluralWhat = self.notebook.getPluralAttachmentDesignation()
        ifPluralWhat = pluralWhat
        if len(entry.getAttachmentNames()) != 1:
            what = ifPluralWhat
            
        desc = entry.id.asLinkedDescription(AttachmentManagerCGI.NAME)
        if entry.getTitle():
            desc = desc + ': %s' % entry.getTitle()
        args = { 'files' : what, 'entry' : desc }
        self.printHeading(_('Managing %(files)s: %(entry)s') % args)

        attachments = entry.getAttachmentNames()
        if not attachments:
            print '<p>%s</p>' % (_('This entry has no associated %s.') % pluralWhat)
            print '<hr><p>%s' % (_('You can upload one or more %s here.') % pluralWhat)
        else:
            print '<p>%s</p>' % (_('This entry has the following %s associated with it:') % ifPluralWhat)
            self.startForm(entry.id.asLink(self.NAME, 'mode=edit'))
            self.hiddenField('mode', 'edit')
            self.passwordField()
            print '<p>'
            for attachment in attachments:
                self.checkbox(self.DELETE_ATTACHMENT_PREFIX + attachment,
                              suppressHidden=1)
                self.linkTo(entry.getAttachmentURL(attachment, AttachmentManagerCGI.NAME),
                            attachment)
                print '<br/>'
            print '</p>'
            self.endForm(self.DELETE_KEY, self.DELETE_KEY)
            print '<hr><p>%s' % (_('You can upload additional %s here.') % pluralWhat)

        numBoxes = self.getNumberOfUploadBoxes()
        default = self.notebook.getDefaultNumberOfUploadBoxes()
        args = 'mode=edit&'
        moreArgs = args + const.NUMBER_OF_ATTACHMENTS_CGI_KEY + '=' + str(numBoxes * 2)
        fewerArgs = args + const.NUMBER_OF_ATTACHMENTS_CGI_KEY + '=' + str(numBoxes / 2)

        if numBoxes < 50:
            self.panelLink(entry.id.asLink(AttachmentManagerCGI.NAME, moreArgs),
                           _('Show more upload boxes'))
        if numBoxes > 2 or numBoxes > default:
            self.panelLink(entry.id.asLink(AttachmentManagerCGI.NAME, fewerArgs),
                           _('Show fewer upload boxes'))

        print '</p>'
        self.startForm(entry.id.asLink(AttachmentManagerCGI.NAME, 'mode=edit'),
                       encType='multipart/form-data')
        self.hiddenField('mode', 'edit')
        self.passwordField()
        self.printAttachmentUploadBoxes()
        print '<p></p>'
        self.endForm(self.UPLOAD_KEY, self.UPLOAD_KEY)

class CalendarCGI(NBCGI):

    """A CGI which prints out a monthly or yearly calendar for a
    notebook."""

    NAME = 'calendar'

    def __init__(self,title=_('Calendar'),cgi=ViewCGI.NAME, pathInfo=''):
        self.title = title
        self.cgi = cgi
        NBCGI.__init__(self, pathInfo=pathInfo)

    def run(self):
        self.startPage(self.title)
        self.id.ordinal = const.ALL
        self.id.day = const.ALL
        self.printCalendar()
        self.endPage()

    def headHook(self):
        granularity = self.notebook.getRobotIndexGranularity()
        index = self.id.granularity() == granularity
        follow = granularity != 'none'
        return self.getRobotExclusionTag(index, follow)

    def printCalendar(self):
        panel = ControlPanel(self.notebook, CalendarCGI.NAME)
        panel.printPanel(self.id)
        print '<div align="center">'
        gran = self.id.granularity()
        cal = self.notebook.getCalendar()

        if gran == const.YEAR:
            cal.printYear(self.id.year)
        elif gran == const.MONTH:
            cal.printMonth(self.id.year, int(self.id.month))
        print '</div>'
        panel.printPanel(self.id, "")  

class ConfigureCGI(NBCGI):

    NAME = 'configure'

    OPTION_GROUP_CGI_KEY = 'group'

    def run(self):
        self.errors = []
        self.optionConfig = Options.cache[self.config]
        if self.notebook.useCookies() and not self.preAuth:
            self.redirectToLogin()
        group = self.var(self.OPTION_GROUP_CGI_KEY)
        if group:
            self.selectedGroupName = group
        else:
            self.selectedGroupName = 'Notebook'
        self.selectedGroup = self.optionConfig.getOptionGroupMap().get \
                             (self.selectedGroupName, None)
        if not self.selectedGroup:
            self.printError(_('No such option group: "%s"') \
                            % self.selectedGroupName)

        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.errors = self.selectedGroup.validate(self.notebook,
                                                      self.variables)
            self.assertVars([const.PASSWORD_CGI_KEY])
            givenPassword = self.var(const.PASSWORD_CGI_KEY)
            if not self.notebook.validatePassword(givenPassword):
                self.printError(_('Incorrect password.'))
            configurationPairs = {}
            if self.errors:
                self.printForm()
            else:
                for option in self.selectedGroup.getOptions():
                    if option.isRelevant(self.notebook):
                        key = option.name            
                        value = option.processFormValue(self.variables)
                        value = option.processValueForStorage(self.notebook, value)
                        if not value:
                            allowEmpty = getattr(option, 'allowEmpty', None) == 'Y'
                        if value or allowEmpty:
                            configurationPairs[key] = value
                        elif not allowEmpty:
                            configurationPairs[key] = option.getDefault(self.notebook)                        
                        
                self.notebook.writeConfiguration(configurationPairs)
                self.redirectTo(self.__getGroupConfigureURL(self.selectedGroup))
        else:
            self.printForm()

    def printForm(self):
        title = _('Configuring notebook %s') % self.notebook.name
        self.startPage(title)
        self.printHeading(title)

        #Print the available groups
        i = 0                
        list = self.optionConfig.getRelevantGroups(self.notebook)
        l = len(list)
        print '['
        for group in list:
            if group.name == self.selectedGroupName:
                print '<b>%s</b>' % group.getName(self.notebook)
            else:
                self.linkTo(self.__getGroupConfigureURL(group),
                            group.getName(self.notebook))
            i = i + 1
            if i < l:
                print '|'            
        print ']'

        print '<hr/>'

        if self.errors:
            print '<span class="validationErrors">%s</span>' % _('Validation Errors')
            print '<ol class="validationErrorList">'
            for error in self.errors:
                print '<li class="validationError">%s</li>' % error
            print '</ol>'
            print '<hr/>'

        t = self.selectedGroup.getText(self.notebook)
        if t:
            print '<p>' + t + '</p>'
        if self.selectedGroup.containsRelevantOptions(self.notebook):
            self.startForm(self.notebook.getURL(ConfigureCGI.NAME))
            self.hiddenField(self.OPTION_GROUP_CGI_KEY, self.selectedGroupName)
            self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')
            self.passwordField()
            if not self.preAuth:
                print '<hr/>'
            self.selectedGroup.render(self.notebook, self.variables)
            self.endForm()
        else:
            print '<hr/><p class="alert">%s</p>' % _("All of this group's options are overridden by your selected theme.")
        self.endPage()

    def __getGroupConfigureURL(self, group=None):
        "Returns the URL to configure the given option group."
        args = ''
        if group:
            args = self.OPTION_GROUP_CGI_KEY + '=' + urllib.quote_plus(group.name)
        return self.notebook.getURL(ConfigureCGI.NAME, args)

class DeleteCGI(NBCGI):

    NAME = 'delete'
        
    def run(self):
        if not self.notebook.allowDelete():
            self.printError(_('You cannot delete entries from this notebook.'))
        if len(self.entries) > 1:
            self.printError(_('You must specify one and only one entry to delete.'))
        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.delete()
        else:
            self.printForm()

    def delete(self):
        self.assertVars([const.PASSWORD_CGI_KEY])
        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        if self.notebook.validatePassword(givenPassword):
            entry = self.entries[0]
            entry.delete(givenPassword)
            if entry.id.draft:
                url = self.notebook.getURL(EditCGI.NAME, pathInfo='drafts')
            else:
                url = entry.getDayID().asLink(ViewCGI.NAME)
            self.redirectTo(url)
        else:
            self.printError(_('Incorrect password.'))

    def printForm(self):
        entry = self.entries[0]
        pre = _("Deleting entry: ") + entry.id.asPathInfo()
        self.startPage(pre)
        self.printHeading(pre)        

        url = self.entries[0].id.asLink(DeleteCGI.NAME)
        self.startForm(url)
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')

        print '<p>'
        self.passwordField()
        print '</p>'
        print '<p>'
        print _('Please confirm the deletion of this entry.')
        print '</p>'
        self.viewer.entryHeader = '<hr />'
        self.viewer.detail = 1
        try:
            self.viewer.render()
        except Exception, e:
            print '<p><i>%s</i></p>' % _("[Sorry, I couldn't render this entry; presumably that's why you want to delete it. Original error was: %s]") % e
        print '<hr/>'        
        entries = entry.getDayID().getEntries()
        ord = int(self.id.ordinal) + 1
        if len(entries) > ord:
            entries = entries[ord:]
            print '<p><b>%s</b>' % _('Note:')
            args = {'year' : entry.id.year,
                    'month' : entry.id.month,
                    'day' : entry.id.day}
            if len(entries) == 1:
                print _("There was one entry published on %(year)s-%(month)s-%(day)s after this one. If you delete this entry, the link to that entry will change to what used to be the link to this entry.") % args
            else:
                args['number'] = len(entries)
                print _("There were %(number)s entries published on %(year)s-%(month)s-%(day)s after this one. If you delete this entry, the link to those entries will change.") % args
            print _('This might invalidate external links.')
            print '</p>'
        self.endForm(_('Delete'))
        self.endPage()

class EditCGI(EntryCGI):

    """A CGI for editing a particular entry, or displaying a number of
    entries with an edit link for each."""

    NAME = 'edit'

    PUBLISH_KEY = _('Publish')
    UPDATE_KEY = _('Update')
    DRAFTED_KEY = _('Saved as Draft')

    SAVED = 'saved'

    def run(self):
        "Runs the CGI; editing an entry, printing out a form to edit one, or printing an interface for selecting one."
        titlePrefix = None
        if self.id.granularity() == const.ORDINAL:
            titlePrefix = _("NewsBruiser: Editing: ")        
        entry = None        
        if self.usedDefaultEntryID and not self.id.draft:
            self.id.day = self.notebook.today()[2]
            self.entries = self.id.getEntries()
            if not self.entries:
                self.entries = self.notebook.getPreviousEntries(10)
        if self.entries:
            entry = self.entries[0]
        if entry and self.var(self.PUBLISH_KEY):
            if entry.id.draft:
                if self.var(const.ENTRY_CGI_KEY):
                    self.edit()
                self.publish()
        else:
            self.startPage(title=self.id.asDescription(),
                           titlePrefix=titlePrefix)
            if self.var(self.UPDATE_KEY) or self.var(self.SAVED):
                if self.var(self.UPDATE_KEY):
                    self.edit()
                    self.printStatus(_('Entry updated.'))
                else:
                    code = self.var(self.SAVED)
                    if code == AddCGI.SAVE_AS_DRAFT:
                        msg = _('Entry saved as <a href="%s">%s</a>.')
                    else:
                        msg = _('Entry published to <a href="%s">%s</a>.')
                    link = entry.id.asLink(ViewCGI.NAME)
                    path = entry.id.asPathInfo()
                    self.printStatus(msg % (link, path))
                self.viewer.render()
                self.line()
                self.printForm()            
            elif not self.processHookActions():
                if len(self.entries) == 1 and self.id.denotesSingleEntry():
                    self.printForm()
                else:
                    self.viewWithEditButtons(self.id)
            numAttachments = 0
            if entry:
                self.addNavLink(entry.id.asLink(ViewCGI.NAME), _('View'))
                numAttachments = len(entry.getAttachmentNames())
            if self.notebook.enableImageUpload() or numAttachments:
                what = self.notebook.getAttachmentDesignation()
                if numAttachments <> 1:
                    what = what + 's'
                if numAttachments:
                    whatToDo = _('Manage %s %s, or upload more') % (numAttachments ,what)
                else:
                    whatToDo = _('Upload ') + what
                if entry:
                    self.addNavLink(entry.id.asLink(AttachmentManagerCGI.NAME,
                                                    'mode=edit'), whatToDo)
            if entry and self.notebook.allowDelete():
                self.addNavLink(entry.id.asLink(DeleteCGI.NAME), _('Delete'))

            self.endPage()

    def edit(self):
        "Verify password and make change."
        self.assertVars([const.ENTRY_CGI_KEY, const.PASSWORD_CGI_KEY])
        entry = self.entries[0]
        author = None
        authorEmail = None
        if self.notebook.displayAuthor():
            author = self.var(const.AUTHOR_CGI_KEY)
            if not author:
                author = getattr(entry, 'author', '')
            if self.notebook.collectAuthorEmail():
                authorEmail = self.var(const.AUTHOR_EMAIL_CGI_KEY)
                if not authorEmail:
                    authorEmail = getattr(entry, 'authorEmail', '')

        if self.notebook.allowEntryTitles():
            title = self.var(const.TITLE_CGI_KEY)
        else:
            title = None
        text = self.var(const.ENTRY_CGI_KEY)
        password = self.var(const.PASSWORD_CGI_KEY)

        if self.notebook.validatePassword(password):
            if title and self.notebook.validatePassword(title):
                self.printError(_("I don't think you meant to enter that as the title."))
            entry.edit(password, author, authorEmail, text, title,
                       self.variables)
        else:
            self.printError(_('Incorrect password.'))

    def publish(self):
        "Verify password and publish."
        self.assertVars([const.PASSWORD_CGI_KEY])
        entry = self.entries[0]
        pw = self.var(const.PASSWORD_CGI_KEY)
        if self.notebook.validatePassword(pw):
            newEntry = entry.publish(pw)
            self.redirectTo(newEntry.id.asLink(ViewCGI.NAME))
        else:
            self.printError(_('Incorrect password.'))

    def viewWithEditButtons(self, id):
        "Show a page of the selected entries with edit buttons for each one."
        panel = ControlPanel(self.notebook, EditCGI.NAME)
        panel.printPanel(id)
        self.viewer.render()
        panel.printPanel(id, "")

    def buildViewer(self):
        v = NBCGI.buildViewer(self)
        v.chronological=0
        v.edit=1
        v.detail=1
        if hasattr(self, 'id'):
            v.entryID = self.id
        return v

    def printForm(self):
        "Print a form for editing a particular entry."
        entry = self.entries[0]        
        self.startForm(self.id.asLink(EditCGI.NAME), name='editForm')

        self.printHeading(_('Editing notebook <i>%s</i>') % self.notebook.name)

        print '<p>'
        self.passwordField()
        self.authorControl(self.notebook, getattr(entry, 'author', None),
                           getattr(entry, 'authorEmail', None))
        print '</p>'
        if self.notebook.allowEntryTitles():
            print '<p>'
            self.titleField(entry.get('title', oneLine=1))
            print '</p>'

        names = entry.getAttachmentNames()
        self.printDisplaySnippets('aboveEntryText')
        print '<table><tr><td valign="top">'

        self.textArea(self.notebook.getEntryBoxLength(), 70,
                      const.ENTRY_CGI_KEY, entry.text)
        self.printDisplaySnippets('rightOfEntryText')
        if names:
            print '</td><td valign="top">'
            self.printEntryInterpolationHelp(entry, 'editForm.entry')

        print '</td></tr></table>'

        self.printDisplaySnippets('belowEntryText')

        print '<br>'
        self.submitButton(self.UPDATE_KEY, self.UPDATE_KEY)
        if self.entries[0].id.draft:
            self.submitButton(_('Update and publish'), self.PUBLISH_KEY)

        self.printDisplaySnippets('belowSubmitButton')

        self.endForm(None)

class LoginCGI(NBCGI):

    """A CGI for setting an authentication cookie for the given
    notebook."""

    NAME = 'login'
    
    def run(self):        
        from NewsBruiserCGIs import PortalCGI
        if not self.notebook.useCookies():
            self.printError(_('This functionality requires authentication cookies, which are disabled in this notebook.'))
        dest = self.var(const.REFERER_CGI_KEY)
        if not dest:
            dest = self.getReferer()
        if not dest:
            dest = self.notebook.getURL(PortalCGI.NAME)
        if self.preAuth:
            self.redirectTo(dest)
        elif self.var(const.MAKE_CHANGE_CGI_KEY):
            self.processAction()
        else:
            self.printForm(dest)

    def assertViewability(self):
        """The user can always view the login page, else they can't get
        into a state to view other pages."""
        pass

    def processAction(self):
        from NewsBruiserCGIs import PortalCGI
        self.assertVars([const.PASSWORD_CGI_KEY])
        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        dest = self.var(const.REFERER_CGI_KEY)
        if not dest:
            dest = self.notebook.getURL(PortalCGI.NAME)
        if self.notebook.validatePassword(givenPassword):
            self.redirectTo(dest)
        else:
            self.startPage(_('Incorrect password.'))
            self.printError(_('Incorrect password.'))

    def printForm(self, referer):
        self.startPage(_('Logging in to notebook <i>%s</i>') % self.notebook.name)
        self.startForm(self.notebook.getURL(LoginCGI.NAME))
        self.printHeading((_('Enter the administrative password to log into notebook <i>%s</i>') % self.notebook.name))
        self.hiddenField(const.MAKE_CHANGE_CGI_KEY, 'true')
        self.hiddenField(const.REFERER_CGI_KEY, referer)
        print '<p>'
        self.passwordField()
        print '</p>'
        self.endForm()
        self.endPage()

class LogoutCGI(NBCGI):

    """A CGI which logs out an authenticated user from the current
    notebook."""

    NAME = 'logout'
    
    def run(self):        
        dest = self.getReferer()
        self.clearAuthCookie()
        if not dest:
            from NewsBruiserCGIs import PortalCGI
            dest = self.notebook.getURL(PortalCGI.NAME)
        self.redirectTo(dest)

class NotebookDeleteCGI(NBCGI):

    NAME = 'notebook-delete'

    def run(self):
        "Delete a notebook."
        from NewsBruiserCGIs import PortalCGI
        if self.var(const.MAKE_CHANGE_CGI_KEY):
            self.delete()
            self.redirectTo(PortalCGI.NAME)
        else:
            self.printForm()

    def delete(self):
        self.assertVars([const.MASTER_PASSWORD_CGI_KEY])
        givenPassword = self.var(const.MASTER_PASSWORD_CGI_KEY)        
        if self.config.validateMasterPassword(givenPassword):
            self.notebook.delete(givenPassword)
        else:
            self.printError(_('Incorrect password.'))

    def printForm(self):
        pre = _("Deleting notebook: ") + self.notebook.name
        self.startPage(pre)
        self.printHeading(pre)        

        badDir = ''
        if self.config.notebookDirWritable() and os.access(self.notebook.getDir(), os.W_OK):
            self.startForm(self.getURL())
            print '<p>'
            self.masterPasswordField()
            print '</p>'
            print '<p>'
            args = { 'name' : self.notebook.name,
                     'deletedName' : self.notebook.getDeletedName() }
            print _('Please confirm the deletion of the %(name)s notebook. The notebook directory will be renamed to %(deletedName)s, and the notebook will no longer be accessible.') % args
            print '</p>'
            self.endForm(_('Delete'), const.MAKE_CHANGE_CGI_KEY)
        else:
            print '<p>'
            print _("You can't delete this notebook because the notebook directory isn't writable by the webserver. You need to either delete the notebook manually or make the notebook directory writable by the webserver.")
            print '</p>'
        self.endPage()

class RandomCGI(NBCGI):

    NAME = 'random'

    def run(self):
        "Show a random entry."

        try:
            number = int(self.variables.get('number', 1))
        except ValueError:
            number = 1
        if number > 100:
            self.printError(_("You cannot ask for more than 100 random entries at a time."))

        if number == 1:
            title = _('A random entry from %s')
        else:
            title = _('%d random entries from %%s') % number
        self.title = title % self.notebook.getTitle()

        randomEntries = self.notebook.getRandomEntries(number)
        self.startPage(title = self.title, titlePrefix=None)
        self.printHeading(self.title)
        Viewer(self.notebook, randomEntries).render()
        self.endPage()

class SearchCGI(NBCGI):

    NAME = 'search'

    def __init__(self, title=_('%(notebook)s: Search results for "%(query)s"'),
                 cgi=None, pathInfo=''):
        if not cgi:
            cgi = self.NAME
        self.title = title
        self.cgi = cgi
        NBCGI.__init__(self, pathInfo=pathInfo)

    def run(self):
        "Show search results."
        self.titleArgs = { 'notebook' : self.notebook.getTitle(),
                           'query' : self.getQuery() }
        self.startPage(title = self.title % self.titleArgs, titlePrefix=None)
        self.showResults()
        self.endPage()

    def getQuery(self):
        "Returns the query requested by the user."
        return self.var(const.QUERY_CGI_KEY)

    def showResults(self):
        """Get entries matching the query and display them in reverse
        chronological order along with the search panel."""
        query = self.getQuery()

        panel = ControlPanel(self.notebook, SearchCGI.NAME)
        header = _('Search')
        if query:
            self.titleArgs = { 'notebook' : self.notebook.getTitle(),
                               'query' : self.getQuery() }
            header = self.title % self.titleArgs
        panel.printPanel(None, header, searchBoxContents=query)
        if query:
            #If they submitted a query, print matching results;
            #otherwise, just leave it as a search panel.
            entries = self.notebook.getEntriesMatching(query)
            entries.reverse()
            print '<hr/>'
            viewer = Viewer(self.notebook, entries)
            viewer.minimumGranularity = const.YEAR
            viewer.render()
            print '<hr/>'
            panel.printPanel(searchBoxContents=query)

class StatisticsCGI(NBCGI):

    """View statistics for a notebook. Requires authentication."""

    SHOW_STATISTICS_KEY = 'showStatistics'

    NAME = 'stats'

    def run(self):
        title = _('Statistics for %s') % self.notebook.getTitle()
        self.startPage(title)
        self.printHeading(title)
        if self.preAuth:
            self.printStats()
        else:
            self.startForm(self.getURL())
            self.passwordField()
            self.endForm(_('Show statistics'), self.SHOW_STATISTICS_KEY)
        self.endPage()

    def printStats(self):
        stats = self.notebook.getStatistics()
        stats.gather()
        print '<table>'
        for stat in stats.stats:
            print '<tr><td class="optionNamePanel" width="20%%" valign="top">'
            print stat.STAT_NAME
            print '</td><td valign="top">'
            stat.printStatistic()
            print '</td></tr>'
            print '<tr><td></td><td>'
            print stat.STAT_DESCRIPTION
            print '</td></tr>'
        print '</table>'

class ThemeGenerationCGI(NBCGI):

    NAME = 'theme-generate'

    def run(self):
        if self.notebook.useCookies() and not self.preAuth:
            self.redirectToLogin()
        self.printHeader('text/plain')
        print _('#Theme generated from configuration data by NewsBruiser v%s') % version.version
        args = { 'name' : self.notebook.getContactName(),
                 'email' : self.notebook.getExportedSiteURL() }
        print _('#AUTHOR: %(name)s (%(email)s)') % args
        print '#'
        groups = self.notebook.optionConfig.getOptionGroupMap()
        keys = groups.keys()
        keys.sort() 
        for key in keys:
            printed = 0
            for option in groups[key].getOptions():
                if self.notebook.optionConfig.wrapper.isThemed(option):
                    val = option.getValue(self.notebook)
                    if val:
                        print option.name + '=' + val
                        printed = 1
            if printed:
                print

class UtilityCGI(NBCGI):

    NAME = 'util'

    REBUILD_INDEX_KEY = 'rebuild'
    GET_TARBALL_KEY = 'getTarball'
    DUMP_INDEX_KEY = 'dumpIndex'
    REWRITE_ENTRY_FILES = 'rewriteEntryFiles'
    REWRITE_STATIC_FILES = 'rewriteStaticFiles'

    def run(self):
        if self.var(self.REBUILD_INDEX_KEY):            
            self.rebuildIndex()
        elif self.var(self.DUMP_INDEX_KEY):
            self.dumpIndex()
        elif self.var(self.GET_TARBALL_KEY):
            self.getTarball()
        elif self.var(self.REWRITE_STATIC_FILES):
            self.printHeader('text/plain\n')
            self.config.pluginEvent("FullStaticRewrite", self.notebook)
        else:
            self.printForm()

    def dumpIndex(self):
        title = 'Index dump for ' + self.notebook.getTitle()
        self.startPage(title)

        indexer = self.notebook.getIndexer()

        path = self.notebook.getIndexUpdateListPath()
        updates = []
        if os.path.exists(path):
            f = open(path, 'r')
            updates = f.readlines()
            f.close()
        if updates:
            print '<p><b>%s</b></p>' % _('Pending updates:')
            print '<ul><li>'
            print string.join(updates, '</li>\n<li>')
            print '</li></ul>'
        else:
            print '<p><b>%s</b></p>' % _('No pending index updates.')

        files = indexer.files.items()
        idToPath = {}
        for (path, (fileID, wordCount)) in files:
            idToPath[fileID] = path
        print _('Maximum index ID: %s') % -int(indexer.files['_TOP'][0])

        fileContents = {}
        weird = {}
        for word in indexer.words.keys():
            for fileID in indexer.words.get(word).keys():
                l = fileContents.get(fileID)
                if not l:
                    l = []
                    fileContents[fileID] = l
                l.append(word)        
                if not idToPath.get(fileID):
                    l = weird.get(fileID)
                    if not l:
                        l = []
                        weird[fileID] = l
                    l.append(word)
        if weird:
            status = _("Sanity check failed!")
        else:
            status = _("Sanity check succeeded!")
        print '<hr /><p><b>%s</b></p>' % status
        if weird:
            print '<ul>'
        keys = weird.keys()
        keys.sort()
        for file in keys:
            words = weird[file]
            args = { 'id' : idToPath.get(file),
                     'howManyWords' : len(words),
                     'wordList' : string.join(words, ' ') }
            err = _('File %(id)s is not mapped to any file on disk, but is found under %(howManyWords)s word(s): %(wordList)s') % args
            print '<li>%s</li>' % err
        if weird:
            print '</ul>'

        print '<hr /><p><b>'
        if files:
            print _('Current index contents:')
        else:
            print _('Index is currently empty.')
        print '</b></p>'
        keys = idToPath.keys()
        keys.sort()
        for fileID in keys:
            path = idToPath[fileID]
            print '%s (ID %s):' % (path, fileID)
            print '<blockquote>'
            words = fileContents.get(fileID, [])
            numWords = len(words)
            wordList = string.join(words, ' ')
            if numWords == 1:            
                print _('1 word: %s') % wordList
            else:
                args = {'howManyWords' : numWords,
                        'wordList' : wordList}
                print _('%(howManyWords)d words: %(wordList)s') % args
            print '</blockquote>'

        self.endPage()

    def rebuildIndex(self):
        from NewsBruiserCGIs import PortalCGI
        self.assertVars([const.PASSWORD_CGI_KEY])
        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        if not self.notebook.validatePassword(givenPassword):
            self.printError(_('Incorrect password.'))
        self.notebook.recreateIndex()
        self.redirectTo(self.notebook.getURL(PortalCGI.NAME))

    def getTarball(self):
        self.assertVars([const.PASSWORD_CGI_KEY])
        givenPassword = self.var(const.PASSWORD_CGI_KEY)
        if not self.notebook.validatePassword(givenPassword):
            self.printError(_('Incorrect password.'))        
        try:
            tarball = self.notebook.getTarball()
            self.printHeader('application/x-gzip')
            sys.stdout.write(tarball)
        except:
            self.printError(_("Couldn't create a tarball. Your system probably doesn't have tar."))
        
    def printForm(self):
        title = _('Utilities for notebook ') + self.notebook.name
        self.startPage(title)

        print '<p>'
        if self.preAuth:
            print 'Click a button to perform the corresponding action on this notebook.'
        else:
            print _('Enter the administrative password for this notebook and click a button to perform the corresponding action on this notebook.')
        print '<p>%s</p>' %_('Note that these operations may take a while, especially if this notebook has a lot of entries.')

        if not self.preAuth:
            print '<hr/>'
        self.startForm(self.getURL())
        self.passwordField()
        print '<hr/>'
        print '<p>'
        self.submitButton(_('Rebuild index'), self.REBUILD_INDEX_KEY)
        print '</p>'
        print '<p>'
        self.submitButton(_('Print index dump'), self.DUMP_INDEX_KEY)
        print '</p>'
        print '<p>'
        self.submitButton(_('Download notebook as tarball'),
                          self.GET_TARBALL_KEY)
        print '</p>'
        from nb.plugin.entry import export
        from nb.plugin.entry.export.StaticRendering.StaticEntryWriter import StaticEntryWriter
        for exporter in export.userDefinedExporters:
            if isinstance(exporter, StaticEntryWriter):
                print '<p>'
                self.submitButton(_('Rewrite static files'),
                                  self.REWRITE_STATIC_FILES)
                print _("If you've been changing the look and feel of this notebook, your static files are probably stuck in the past. Refresh them by clicking this button. This can take a <b>really</b> long time, which is why it doesn't happen automatically.")
                print '</p>'
                break
        print '<p>'

        self.endForm(None)
        print '<hr/><p><b>'
        print _('Other miscellaneous utilities:')
        print '</b></p><p>'
        from nb.plugin.entry.importer import ImportCGI
        self.panelLink(self.notebook.getURL(ImportCGI.NAME), _('Import entries'))
        self.panelLink(self.notebook.getURL(StatisticsCGI.NAME),
                       _('View statistics'))
        self.panelLink(self.notebook.getURL(ThemeGenerationCGI.NAME),
                       _('Generate a theme file from your configuration'))
        from nb.plugin.entry.render.syndication import SyndicationCGI
        self.panelLink(self.notebook.getURL(SyndicationCGI.NAME,
                                            'content=form'),
                       _('View customized syndication feeds'))

        self.panelLink(self.notebook.getURL(NotebookDeleteCGI.NAME),
                       _('Delete this notebook (requires master password)'))
        
        print '</p>'

        self.endPage()
