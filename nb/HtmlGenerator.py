import lib.LOMP

import const

class HtmlGenerator(lib.LOMP.HtmlGenerator):

    """This is a NewsBruiser specialization of the generic
    HtmlGenerator class."""    

    def searchBox(self, notebook, value=''):
        """Prints a search form for the given notebook."""
        from nb.CoreCGIs import SearchCGI
        if value and len(value) > 10:
            size = len(value)
        else:
            size = 10
        self.startForm(notebook.getURL(SearchCGI.NAME), "get")    
        self.textField(const.QUERY_CGI_KEY, value, size,50)
        self.endForm(_('Search'))

    def getSearchBox(self, viewer, value=''):
        """A somewhat hacky method which returns a search box as a string."""
        from nb.CoreCGIs import SearchCGI
        if value and len(value) > 10:
            size = len(value)
        else:
            size = 10
        return """<form action="%s" method="get">
        <div class="searchBox">
            <input type="text" name="q" value="%s" size="%s" maxlength="50" />
            <input type="submit" name="submit" value="%s" />
        </div>
        </form>""" % (viewer.notebook.getURL(SearchCGI.NAME, "get"), value, size, _('Search'))
