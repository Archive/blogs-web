"""Locates the notebook directory for this instantiation and provides
access to the corresponding Notebook objects. When run, it locates
notebooks and runs the self-test of each notebook."""

import ConfigParser
import glob
import locale
import os
import string
import sys
import types

try:
    import crypt
except ImportError:
    from lib import crypt
from lib import BudgetI18N

from Notebook import Notebook
from plugin.system.Rewrite.URLRewriter import URLRewriter
import const
import util

LOCALES = ['en-us', 'fr-fr']
DEFAULT_LOCALE = LOCALES[0]

class NBConfig: 

    """Provide access to the notebooks in a directory."""

    DELETED_PREFIX = 'deleted-'

    PLUGIN_DEBUG = 1

    singleton = None
    
    def __init__(self, configFile=None):
        "Initialize a configuration with the notebooks in a given directory."

        #Find out what locale we are in for this request.
        self.locale = BudgetI18N.LanguageNegotiator().findAcceptableLocale \
                      (os.environ.get('HTTP_ACCEPT_LANGUAGE', ''), LOCALES)
        try:
            locale.setlocale(locale.LC_ALL, self.locale)
        except Exception:
            #They want to use a locale that's not installed in this
            #Python installation (?). But we should be able to give them
            #keys anyway.
            #
            #TODO: Is this right? Does the locale name need to be translated?
            pass

        import __builtin__
        self.messageFilename = 'messages.%s.po' % self.locale
        if self.locale == DEFAULT_LOCALE:
            __builtin__.__dict__['_'] = lambda(x): x
        else:
            self.bundle = BudgetI18N.I18NBundle()
            __builtin__.__dict__['_'] = self.bundle.__getitem__
            baseMessageFile = util.getFileInNewsBruiserBaseDirectory \
                              (os.path.join('nb', self.messageFilename))
            if os.path.exists(baseMessageFile):
                self.bundle.read(baseMessageFile)
            libraryBundleGlob = util.getFileInNewsBruiserBaseDirectory \
                                (os.path.join('nb', 'lib',
                                              '*.' + self.messageFilename))
            for i in glob.glob(libraryBundleGlob):
                self.bundle.read(i)
            themeBundleGlob = util.getFileInNewsBruiserBaseDirectory \
                              (os.path.join('themes', '*',
                                            self.messageFilename))

            for i in glob.glob(themeBundleGlob):
                self.bundle.read(i)

        self.registerPlugins()
        self.notebooks = {}

        #Things like the URL rewriter might use the base URL, so set that
        #to a guess beforehand so that someone who wants a URL rewriter
        #doesn't also have to set a baseURL.
        if not hasattr(self, 'baseURL'):
            self.baseURL = util.guessNewsBruiserBaseURL()

        #Execute the cfg.py, if the user wrote one.
        path = util.getFileInNewsBruiserBaseDirectory(os.path.join('nb', 'cfg.py'))
        if os.path.exists(path):
            exec(open(path).read())

        configFile = getattr(self, 'configFile', configFile)
        self.configFile = self.locateConfigFile(configFile)

        if not hasattr(self, 'baseContentURL'):
            self.baseContentURL = self.baseURL
        if not hasattr(self, 'urlRewriter'):
            self.urlRewriter = URLRewriter(self.baseURL)
        if not hasattr(self, 'niceLevel'):
            self.niceLevel = None

        if self.configFile:
            self.readConfigFile()
            self.instantiateNotebooks()

    def locateConfigFile(self, configFile=None):
        if configFile:
            return configFile

        #Try to find NewsBruiserData/.nbrc, the default location
        #for a config file.
        path = util.getFileInNewsBruiserBaseDirectory(const.DEFAULT_NEWSBRUISER_DATA_DIRECTORY)
        if os.path.isdir(path):
            conf = os.path.join(path,
                                const.DEFAULT_NEWSBRUISER_CONFIG_FILE_NAME)
            if os.path.exists(conf):
                return conf

        #Look for a config.location file in the current directory or
        #the NewsBruiser base directory 
        for dir in (util.getNewsBruiserBaseDirectory(), os.getcwd()):
            path = os.path.join(dir, const.CONFIG_LOCATION)
            if os.path.exists(path):                
                file = open(path)
                c = file.readline()
                if c[-1] == '\n':
                    c = c[:-1]
                if c[-1] != os.sep:
                    c = c + os.sep
                if os.path.isdir(c):
                    c = c + const.DEFAULT_NEWSBRUISER_CONFIG_FILE_NAME
                return c

        #If that didn't work, fall back through the directory hierarchy
        #looking for a .nbrc.
        dir = os.getcwd()
        while dir and dir != os.sep:
            file = os.path.join(dir,
                                const.DEFAULT_NEWSBRUISER_CONFIG_FILE_NAME)
            if os.path.exists(file):
                return file
            newDir = os.path.split(dir)[0]
            if newDir == dir:
                #We're stuck in a loop due to a weird directory name.
                break
            dir = newDir

        #OK, there's no config file.
        return configFile

    def registerPlugins(self):
        self.enabledPlugins = {}
        self.enabledPluginDirs = []
        os.path.walk(util.getFileInNewsBruiserBaseDirectory
                     (os.path.join('nb', 'plugin')),
                     self.__registerPluginDir, None)

    def __registerPluginDir(self, ignore, dirname, files):
        "Registers the plugins in the given directory."        
        moduleDir = dirname[string.rfind(dirname, os.path.join('nb')):]
        modulePath = string.replace(moduleDir, os.sep, '.')        
        if '__init__.py' in files:
            ok = 0
            try:
                #print "Content-type: text/plain\n"
                #print "Path: ", modulePath
                __import__(modulePath)
                ok = 1
            except:
                if self.PLUGIN_DEBUG:
                    import traceback
                    print "Content-type: text/plain\n"
                    print string.join(traceback.format_exception(sys.exc_type,sys.exc_value,sys.exc_traceback, '\n'))
            if ok:
                self.enabledPlugins[modulePath] = sys.modules[modulePath]
                self.enabledPluginDirs.append(moduleDir)
                if hasattr(self, 'bundle'):
                    bundlePath = os.path.join(dirname, 'messages.%s.po' % self.locale)
                    if os.path.exists(bundlePath):
                        self.bundle.read(bundlePath)
                    
        if self.messageFilename in files:
            self.bundle.read(util.getFileInNewsBruiserBaseDirectory(os.path.join(moduleDir, self.messageFilename)))

    def registerPlugin(self, module):
        for c in module.__dict__.values():
            if type(c) == types.ClassType and c.__name__ == 'Entry':
                import Entry
                Entry.registerPlugin(module, c)        

    def pluginEvent(self, event, arg):
        messages, errors = [], []
        event = 'event' + event
        for mod in self.enabledPlugins.values():
            if hasattr(mod, event):
                ret = getattr(mod, event)(arg)
                if ret and type(ret) == type([]) and len(ret) == 2:
                    messages.extend(ret[0])
                    errors.extend(ret[1])
        return messages, errors

    def readConfigFile(self):
        if not os.path.exists(self.configFile):
            raise "The config file '%s' does not exist!" % (self.configFile)

        self.notebookDir = os.path.split(self.configFile)[0]
        
        #Next, read the master configuration file, which contains the
        #master password.

        #As of 1.13, the configuration file contains only the encrypted
        #master password. Pre-1.13, it is a ConfigParser-readable file
        #containing the notebook names, passwords and ordinals. In the
        try:
            self.masterPassword = self.__parsePre113ConfigFile()
        except (ConfigParser.ParsingError, ConfigParser.MissingSectionHeaderError):
            #This will always happen with a 1.13 config file.
            self.masterPasswordFile = 1
            self.masterPassword = open(self.configFile).readline()
            if self.masterPassword[-1] == '\n':
                self.masterPassword = self.masterPassword[:-1]

    def instantiateNotebooks(self):
        #First, instantiate the notebooks.
        for notebookName in os.listdir(self.notebookDir):
            if string.find(notebookName, self.DELETED_PREFIX) != 0:
                path = os.path.join(self.notebookDir, notebookName)
                if os.path.exists(os.path.join(path, '.configuration')):
                    #This is an actual notebook and not just some
                    #random subdirectory.
                    notebook = Notebook(notebookName, self)
                    self.notebooks[notebook.name] = notebook

    def getDefaultNotebook(self):
        "Returns the default notebook."
        notebook = None
        list = self.getNotebookList()
        if list:
            notebook = list[0]
        return notebook

    def getNotebookList(self):
        "Returns a list of all notebooks, sorted by ordinal."
        if not hasattr(self, 'notebookList') or self.notebookList == None:
            self.notebookList = []
            do_slow = True
            if os.path.exists(os.path.join(self.notebookDir, '.ordinals')):
                notebook_order = file(os.path.join(self.notebookDir, '.ordinals'), "r").read().split("\n")
                notebook_order_dict = dict(zip(notebook_order, range(0, len(notebook_order))))
                notebookList = self.notebooks.values()
                do_slow = False
                try:
                    notebookList.sort(lambda x,y: cmp(notebook_order_dict[x.name], notebook_order_dict[y.name]))
                except KeyError:
                    do_slow = True

                if not do_slow:
                    self.notebookList = notebookList
                
            if do_slow:
                for notebook in self.notebooks.values():
                    #Put the notebook in its designated place in the list
                    #of notebooks. If there's already a notebook with that
                    #ordinal, increment the ordinal until there's space.
                    myOrdinal = notebook.getOrdinal()     
                    while len(self.notebookList) <= myOrdinal:
                        self.notebookList.append(None)
                    while len(self.notebookList) > myOrdinal and self.notebookList[myOrdinal] != None:
                        myOrdinal = myOrdinal + 1
                        if len(self.notebookList) <= myOrdinal:
                            self.notebookList.append(None)
                    if myOrdinal != notebook.getOrdinal():
                        notebook.setOrdinal(myOrdinal)
                    self.notebookList[myOrdinal] = notebook        

                #The following things should never happen unless someone
                #has been messing with the config file or there was a
                #problem with the ordinal change CGI.

                #Make sure there are no gaps in the list of notebooks.
                c = 1
                while c:
                    try:                    
                        self.notebookList.remove(None)
                    except ValueError:
                        c = 0

                #Make sure the skip in ordinals is never more than one.
                ordinalShouldBe = -1
                for i in self.notebookList:
                    ordinalShouldBe = ordinalShouldBe + 1
                    if int(i.getOrdinal()) > ordinalShouldBe:
                        i.setOrdinal(ordinalShouldBe)
                
                # Save ordinals cache
                file(os.path.join(self.notebookDir, '.ordinals'), "w").write("\n".join([x.name for x in self.notebookList]))

        return self.notebookList

    def getNotebooks(self):
        "Returns a dictionary of all notebooks, keyed by name."
        return self.notebooks

    def validateMasterPassword(self, givenPassword):
        "Returns true iff the given password matches the master password."
        encryptedGivenPassword = crypt.crypt(givenPassword,
                                             self.masterPassword[:2])
        return(encryptedGivenPassword == self.masterPassword)

    def assertValidMasterPassword(self, givenPassword):
        """Throws an exception unless the given password matches the
        master password."""
        if not self.validateMasterPassword(givenPassword):        
            raise "Invalid master password!"

    ### Mutator methods: Change the master config and rearrange the
    ### notebooks.

    def notebookDirWritable(self):
        """Returns true iff the current user can add and rename directories
        in the notebook directory."""
        return os.access(self.notebookDir, os.W_OK)
        
    def masterConfigWritable(self):
        """Returns true iff the current user can write to the master
        configuration file."""
        return os.access(self.configFile, os.W_OK)

    def assertMasterConfigWritable(self):
        """Throws an exception unless the current user can write to the master
        configuration file."""
        userDesc = ''
        try:
            id = os.getuid()
            userDesc = ' by the user with UID %s' % id
        except: #Windows or other platform
            pass
        if not self.masterConfigWritable():
            raise "The NewsBruiser master config file (%s) is not writable%s. You must either fix this or make changes to the configuration manually." % (self.configFile, userDesc)

    def setMasterPassword(self, oldPassword, newPassword):
        "Changes the master NewsBruiser password."        
        self.assertValidMasterPassword(oldPassword)
        if not self.masterPasswordFile:
            raise "I can't set the master password because the NewsBruiser configuration file is in an old (pre-1.13) format, and I don't want to overwrite it. Please fix this manually by removing the old configuration file in %s and replacing it with a file containing only the encrypted master password." % self.configFile
        self.assertMasterConfigWritable()
        oldumask = os.umask(027)
        try:
            file = open(self.configFile, 'w')
            file.write(util.encrypt(newPassword))
            file.close()
        finally:
            os.umask(oldumask)
        

    def updateNotebookOrdinals(self, masterPassword, changedNotebook):
        """Changes notebook ordinals around to reflect one notebook's
        changed ordinal."""
        self.assertValidMasterPassword(masterPassword)
        notebooks = self.getNotebookList()
        notebooks.remove(changedNotebook)
        notebooks.insert(changedNotebook.getOrdinal(), changedNotebook)
        for i in range(0, len(notebooks)):
            if notebooks[i].getOrdinal() != i:
                notebooks[i].setOrdinal(i)

        # Save ordinals cache
        file(os.path.join(self.notebookDir, '.ordinals'), "w").write("\n".join([x.name for x in self.notebookList]))

    def selfTest(self):
        "Self test for NBConfig class."
        print 'Valid self.notebooks keys: %s' % self.notebooks.keys()
        print 'Here are your notebooks:'
        for notebook in self.getNotebookList():
            notebook.selfTest()
            for i in range(1,40):
                print '-',
            print 

    def __parsePre113ConfigFile(self):
        """Parse a pre-1.13 configuration file, setting notebook
        properties and finding a reasonable value for the master
        password."""
        
        data = ConfigParser.ConfigParser({const.WORKING_DIR_CONFIG_KEY: self.notebookDir})
        data.read(self.configFile)
        self.masterPasswordFile = 0
        masterPassword = data.defaults().get('password')
        for notebookName in data.sections():
            notebook = self.notebooks.get(notebookName)        
            if not notebook and os.path.exists(os.path.join(self.notebookDir, notebookName)):
                #They must have upgraded from a pre-1.4 version; there's
                #a notebook mentioned with no .configuration file.
                notebook = Notebook(notebookName, self)
                self.notebooks[notebook.name] = notebook
            if notebook:
                #Writing this notebook's configuration gives it an ordinal
                #and a password, two things previously contained only in
                #.nbrc. 
                writeConfig = not notebook.conf.has_key('ordinal') or not notebook.conf.has_key('password')
                
                for option in data.options(notebookName):
                    optionKey = option
                    if (option == '__name__'):
                        optionKey = const.NAME_CONFIG_KEY
                    #Only set if not already set by the notebook's
                    #configuration or theme.
                    value = data.get(notebookName, option)
                    if not notebook.conf.has_key(optionKey) or optionKey == 'password' and not notebook.conf.get(optionKey):
                        notebook.conf[optionKey] = value
                    if optionKey == 'password' and not masterPassword:
                        masterPassword = value
                if writeConfig:
                    notebook.writeConfiguration(notebook.conf)
        return masterPassword

def getSingleton():
    if not NBConfig.singleton:
        NBConfig.singleton = NBConfig()
    return NBConfig.singleton

if __name__=='__main__':
    getSingleton().selfTest()
