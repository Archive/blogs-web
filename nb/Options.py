import copy
import random
import string
import os

import const
from lib.IWantOptions import OptionConfig, OptionWrapper, SelectionOption, StringOption, LongStringOption, Option

import util
from HtmlGenerator import HtmlGenerator

class NBOptionConfig(OptionConfig):

    OPTION_FILE = 'options.conf'

    def __init__(self, nbConfig):
        self.nbConfig = nbConfig
        OptionConfig.__init__(self,
                              util.getFileInNewsBruiserBaseDirectory \
                              (os.path.join('nb', self.OPTION_FILE)),
                              NBOptionWrapper(self.nbConfig))

    def getOptionDefinitions(self):
        if not self.optionGroupList:
            OptionConfig.getOptionDefinitions(self)        
            self.registerPlugins()

    def registerPlugins(self):
        "Register the options.conf files for all existing plugins."
        base = util.getNewsBruiserBaseDirectory()
        for moduleDir in self.nbConfig.enabledPluginDirs:
            path = os.path.join(base, moduleDir, self.OPTION_FILE)
            if os.path.exists(path):
                self.loadOptionDefinitions(path)

class NBOptionWrapper(OptionWrapper, HtmlGenerator):

    def __init__(self, nbConfig):
        self.nbConfig = nbConfig
        OptionWrapper.__init__(self)

    def isThemed(self, option):
        """Returns true iff this option can be overridden by the selection
        of a theme. NewsBruiser-specific."""
        return getattr(option, 'themed', None) == 'Y'

    def overriddenByTheme(self, option, notebook):
        """Returns true iff the theme for this notebook overrides this option
        value. NewsBruiser-specific."""
        return notebook.theme.has_key(option.name)

    def renderGroup(self, group, notebook, presetValues={}):
        if group.containsRelevantOptions(notebook):
            print '<table width=70%>'
            OptionWrapper.renderGroup(self, group, notebook, presetValues)
            print '</table>'

    def renderControl(self, option, notebook, presetValue=None):
        """Render an HTML depiction of the given option."""
        print '<tr><td class="optionNamePanel" width="20%%" valign="top">%s</td><td valign="top">' % self.getOptionDisplayName(option, notebook)
        option.renderEditControl(notebook, presetValue)
        print '</tr><tr><td></td><td>%s</td></tr>' % self.getOptionDescription(option, notebook)

    def isRelevant(self, option, notebook):
        """Returns true iff this option can be set in the given notebook."""
        return OptionWrapper.isRelevant(self, option, notebook) and (not self.isThemed(option) or not self.overriddenByTheme(option, notebook))

    def getOptionDisplayName(self, option, notebook):
        "Retrieve an option's localized display name."
        return _(getattr(option, 'displayName', ''))

    def getOptionDescription(self, option, notebook):
        """Retrieve an option's localized description, performing
        substitution on it."""
        return util.replaceBaseURLs(_(getattr(option, 'description', '')),
                                    self.nbConfig.baseContentURL)

    def getOptionDefault(self, option, notebook):
        """Retrieve an option's localized default value, performing
        substitution on it."""
        return util.replaceBaseURLs(_(option.default),
                                    self.nbConfig.baseContentURL)

    def getGroupDescription(self, group, notebook):
        """Retrieve an group's localized description, performing
        substitution on it."""
        return util.replaceBaseURLs(_(group.text),
                                    self.nbConfig.baseContentURL)

class PasswordChangeOption(Option, HtmlGenerator):

    def renderEditControl(self, context, presetValue=None):
        presetValue = None
        print _('Enter your old password:')
        self.passwordEntryField(self.name + '-oldPw', '')
        print '<br/>'
        print _('Enter your new password:')
        self.passwordEntryField(self.name + '-newPw', '')
        print '<br/>'
        print _('Verify your new password:')
        self.passwordEntryField(self.name + '-verify', '')

    def processValueForStorage(self, context, value):
        if value:
            value = util.encrypt(value)
        else:
            value = context[const.PASSWORD_CONFIG_KEY]
        return value

    def processFormValue(self, vars):
        return vars.get(self.name + '-newPw')

    def validate(self, context, value, formValues):
        errors = []        
        oldPassword = formValues.get(self.name + '-oldPw', '')
        verify = formValues.get(self.name + '-verify', '')
        if value:
            if not oldPassword:
                errors.append(_("You must supply your old password to change it."))
            elif not context.validatePassword(oldPassword):
                errors.append(_("You specified the wrong value for the old password."))
            if not verify:
                errors.append(_("You must verify your new password."))
            elif verify != value:
                errors.append(_("Your new passwords do not match."))
        return errors

class AuthorListOption(StringOption):

    """A NewsBruiser-specific option which provides a somewhat complex
    interface to the different possibilities of entry authorship."""

    NO_AUTHOR = '[no-author]'
    FREEFORM_AUTHOR = '[freeform-author]'
    FREEFORM_AUTHOR_WITH_EMAIL = '[freeform-author-email]'
    SELECT_AUTHOR = '[select-author]'
    SELECTION_VALUES = [NO_AUTHOR, FREEFORM_AUTHOR, FREEFORM_AUTHOR_WITH_EMAIL,
                        SELECT_AUTHOR]

    def renderEditControl(self, notebook, presetValue=None):
        (selected, authors) = self.getValue(notebook, presetValue)
        radioName = self.name + '-type'
        self.wrapper.radioButton(radioName, self.NO_AUTHOR,
                                 _('No author field (If only you post entries)'),
                                 selected)
        self.wrapper.radioButton(radioName, self.FREEFORM_AUTHOR,
                                 _('Freeform author field (If many can post entries)'),
                                 selected)
        self.wrapper.radioButton(radioName, self.FREEFORM_AUTHOR_WITH_EMAIL,
                                 _('Freeform author field with email (If the general public can post entries)'), selected)
        self.wrapper.radioButton(radioName, self.SELECT_AUTHOR,
                                 _('Select from list of known authors'),
                                 selected, 0)
        self.wrapper.textField(self.name + '-authors',
                               string.join(authors, ','), 30, None)

    def processFormValue(self, vars):
        val = vars.get(self.name + '-type', self.getDefault(None))
        if val == self.SELECT_AUTHOR:
            val = val+vars.get(self.name + '-authors', '')
        return val

    def getValue(self, notebook, presetValue=None):
        """Returns a tuple of (selection value, [allowable authors])."""
        selectionValue = presetValue
        if not selectionValue:
            selectionValue = self.SELECT_AUTHOR
        allowableAuthorList = []
        authorListString = StringOption.getValue(self, notebook, presetValue)
        if not authorListString and not presetValue:
            selectionValue = self.NO_AUTHOR
        elif authorListString[0] == '[' and string.find(authorListString, ']') > -1:
            (possiblePrefix, newAuthorListString) = string.split(authorListString, ']', 1)
            possiblePrefix = string.lower(possiblePrefix + ']')
            if possiblePrefix in self.SELECTION_VALUES:
                if not presetValue:
                    selectionValue = possiblePrefix
            authorListString = newAuthorListString
        if authorListString:
            allowableAuthorList = string.split(authorListString, ',')
        return (selectionValue, allowableAuthorList)

class ThemeSelectionOption(SelectionOption):

    """A NewsBruiser-specific option which takes its list of valid
    values from the available theme directories on the filesystem."""

    def __init__(self, wrapper, properties):
        SelectionOption.__init__(self, wrapper, properties)
        self.values = None
        self.descriptions = None

    def getSelectionValues(self):
        if not self.values:
            import util
            self.values = ['']
            dir = util.getFileInNewsBruiserBaseDirectory('themes')
            files = os.listdir(dir)
            files.sort()
            for file in files:
                path = os.path.join(dir, file)
                if os.path.isdir(path):
                    configFile = os.path.join(path, 'configuration')
                    if os.path.exists(configFile):
                        if file == 'Default':
                            self.values.insert(1, file)
                        else:
                            self.values.append(file)                
        return self.values

    def getSelectionValueDescriptions(self):
        if not self.descriptions:
            self.descriptions = copy.copy(self.getSelectionValues())
            self.descriptions[0] = _('No theme - configure everything manually')
        dir = util.getFileInNewsBruiserBaseDirectory('themes')
        for i in range(1, len(self.descriptions)):
            path = os.path.join(dir, self.descriptions[i], 'description')
            if os.path.exists(path):
                text = open(path).read()
                self.descriptions[i] = self.descriptions[i] + ' - ' + text
        return self.descriptions
        
class MustContainValueOption(StringOption):

    def getRequiredValue(self):
        return ''

    def validate(self, context, value, formValues):
        req = self.getRequiredValue()
        error = None
        if value and string.find(value, req) == -1:
            args = {'option' : self.wrapper.getOptionDisplayName(self, context),
                    'required' : req }
            error = _('The value for the "%(option)s" option must contain the string "%(required)s"') % args
        return error

class YearSpanTemplateOption(MustContainValueOption):
    def getRequiredValue(self):
        return '%M'

class MonthSpanTemplateOption(MustContainValueOption):
    def getRequiredValue(self):
        return '%D'

class DaySpanTemplateOption(MustContainValueOption):
    def getRequiredValue(self):
        return '%E'

cache = {}
