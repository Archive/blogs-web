import copy
import re
import os
import string
from StringIO import StringIO
import sys
import time

import const
from const import *
from nb import exceptions
import CommandMaps
from HtmlGenerator import HtmlGenerator
from TimeZoned import TimeZoned
import cgi
import util
import version

### 'Static' methods related to plugins

def registerFilter(text, method):
    l = Entry.filters.get(text)
    if not l:
        l = []
        Entry.filters[text] = l
    l.append(method)

def registerInstantiationFromInputHook(func):
    Entry.instantiationFromInputHooks.append(func)

def registerInstantiationFromFileHook(key, func):
    Entry.instantiationFromFileHooks[key] = func

def registerAsOutputStringHook(func):
    Entry.outputStringHooks.append(func)

def registerPreDeletionHook(func):
    Entry.preDeletionHooks.append(func)

def registerIndexHook(func):
    """Registers an index hook. Index hooks must take two arguments:
    'self', an Entry object, 'gatherer', a TextGatherer (basically a
    SGMLParser which sucks the words out of HTML) and 'words', a list.
    It can add words directly to 'words' or it can feed HTML to
    'gatherer', which will parse the HTML and add the words to 'words'."""
    Entry.indexHooks.append(func)

### And now, the class
class Entry(HtmlGenerator, TimeZoned):
    """The Entry class contains information for a particular notebook
    entry.  You provide it with its identification by passing it a
    EntryID object."""

    #If a time format string matches this string, and the entry's not
    #in the notebook timezone, the entry's timezone will be
    #appended to the format string.
    FORMAT_STRINGS_TO_TRIGGER_TIMEZONE = re.compile('%[HIMSXZ]')

    ENTRY_TIMEZONE_KEY="timezone"

    filters = {}
    instantiationFromInputHooks = []
    instantiationFromFileHooks = {}
    outputStringHooks = []
    preDeletionHooks = []
    preOrdinalChangeHooks = []
    postOrdinalChangeHooks = []
 
    def __init__(self,id,read=1):
        "Constructor for an entry which already exists on disk."
        self.id = id
        self.notebook = self.id.notebook
        if not self.id.denotesSingleEntry(): raise AmbiguousIDError
        if read:
            self.read()

    ### Methods related to plugins

    def applyFilters(self, type, text):
        """Applies registered filters of the given type to the given
        piece of text."""
        for filter in self.filters.get(type, []):
            try:
                text = filter(self, text)
            except Exception, e:
                text += "<br><i>(Error processing text: %s)</i>" % (e)
        return text    

    def indexBasicFields(self, gatherer):
        """Indexes the basic fields of an entry."""
        gatherer.feed(self.getTitle(), const.TITLE_CGI_KEY)
        gatherer.feed(self.text, const.ENTRY_CGI_KEY)
        gatherer.feed(self.getAuthor(), const.AUTHOR_CGI_KEY)
    indexHooks = [indexBasicFields]

    def index(self, gatherer):
        """Indexes the contents of this entry."""
        for hook in self.indexHooks:
            hook(self, gatherer)

    ### Accessor methods

    def get(self, key, default='', oneLine=0):
        if hasattr(self, key):
            val = getattr(self, key)
        else:
            val = default
        if oneLine and val:
            val = string.replace(val, '\n', ' ')
        return val

    def getAnchorID(self):
        """Returns the ID of this entry, formatted like a NewsBruiser HTML
        anchor."""
        return string.join(map(str, self.id.asTuple()), '-')

    def getDayID(self):
        "Returns the entryID for this entry's day."
        id = copy.copy(self.id)
        id.ordinal = ALL
        return id

    def getAlternatorValue(self, viewer):
        if viewer.alternate:
            return '2'
        return ''
    
    def getAuthor(self):
        """Returns the name of this entry's author, if the notebook supports
        multiple authors."""
        if self.notebook.displayAuthor():
            author = self.get('author', oneLine=1)
        else:
            author = ''
        return author

    def getAuthorHTML(self):
        """Returns the name of this entry's author, linked to the email
        address (if present)"""
        author = self.getAuthor()
        email = self.getAuthorEmail()
        if email:
            author = self.getLink('mailto:' + email, author, 'author')
        return author

    def getAuthorEmail(self):
        """Returns the email address of this entry's author, if such
        information is collected."""
        authorEmail = None
        if self.notebook.collectAuthorEmail():
            authorEmail = self.get('authorEmail', oneLine=1)
        return authorEmail

    def getProcessedText(self):
        """May change the entry text to change URLs to attachments.
        This could use some optimization; a way of finding out ahead
        of time which entries have attachments and caching that
        information would be ideal, so we don't have to do a file
        access for every entry."""
        text = self.get('text')
        if self.notebook.enableImageUpload():
            from CoreCGIs import AttachmentManagerCGI
            for filename in self.getAttachmentNames():
                text = string.replace(text, '%' + filename + '%', self.getAttachmentURL(filename, AttachmentManagerCGI.NAME))
        if not self.entryFiltersAlterEntry():
            text = self.applyFilters('text', text)
        return text
    
    def getEntryText(self, viewer):
        if viewer.detail or not self.notebook.summarizeLongEntries() or not self.summaryApplicable():
            text = self.getProcessedText()            
        else:
            text = self.interpolateSummaryTemplate(viewer)
        return text

    def getTimeTuple(self, entryLocalTime=1):
        """Returns the date and time of this entry as a tuple."""
        tup = None
        self.setTimeZone()
        try:
            tup = time.strptime(self.get('time'), STORAGE_TIME_FORMAT)
        except ValueError:
            badTime = self.get('time')
            id = self.id.asPath()
            raise Exception, _(""""%(time)s" isn't a valid time; it looks like entry "%(entryID)s" is broken.""") % {'time' : badTime, 'entryID' : id}
        self.unsetTimeZone()
        tup = time.struct_time ((tup[0], tup[1], tup[2], tup[3], tup[4], tup[5], tup[6], tup[7], -1))
        if not entryLocalTime:
            #Convert from entry local time to GMT. We can't just use gmtime
            #because the entry timezone might not be the system
            #timezone.
            tup = time.localtime(time.mktime(tup) + self.getGMTOffset(tup[8]))
        return tup

    def getFormattedDate(self):
        """Returns the date and time of this entry, formatted in the 
        notebook's default style."""
        return self.getDateInFormat(self.notebook.getDisplayTimeFormat())

    def getModifiedDate(self, format=None, localtime=0):
        """Returns the date and time of the time this entry was last
        modified, formatted in the given style or as a raw number.
        Uses the filesystem's version of 'last modified' if there's
        nothing stored in the entry itself."""
        if not hasattr(self, 'modifiedTime'):
            self.modifiedTime = os.stat(self.id.asPath())[8]
        formattedTime = self.modifiedTime
        if format:
            toTuple = time.gmtime
            if localtime:
                toTuple = time.localtime
            try:
                formattedTime = time.strftime(format,
                                              toTuple(float(self.modifiedTime)))
            except Exception, e:
                formattedTime = _("(unknown)")
        return formattedTime

    def getDateInFormat(self, format=None, localtime=1):
        """Returns the date and time of this entry, formatted in the given
        style or in the notebook's default style."""
        if not format:
            format = self.notebook.getDisplayTimeFormat()
        #try:
        if 1:
            tuple = self.getTimeTuple(localtime)
            formattedTime = time.strftime(format, tuple)
            if localtime:
                localzone = self.getTimeZone()
                if localzone != self.notebook.getTimeZone() and \
                       self.FORMAT_STRINGS_TO_TRIGGER_TIMEZONE.search(format):
                    formattedTime = formattedTime + ' ' + self.getTimeZoneName(tuple[8])
        #except Exception, e:
        #    formattedTime = "(unknown)"
        return formattedTime

    def getPermalink(self):
        url = self.getAbsoluteURL()
        return self.getLink(url, self.notebook.getPermalinkText(), "permalink")

    def getDayURL(self, cgi):
        """Returns the URL to the given CGI for the day of this entry."""
        return self.id.getDayID().asLink(cgi)

    def getMonthURL(self, cgi):
        """Returns the URL to the given CGI for the month of this entry."""
        return self.id.getMonthID().asLink(cgi)

    def getYearURL(self, cgi):
        """Returns the URL to the given CGI for the year of this entry."""
        return self.id.getYearID().asLink(cgi)
    
    def getTitle(self):
        "Returns the title of this entry, if any."
        title = self.get('title', oneLine=1)
        return self.applyFilters('title', title)

    def getTotalSizeBytes(self):
        "Returns the size of this entry and its title, in bytes."
        if not hasattr(self, 'totalSize'):
            self.totalSize = len(self.getTitle()) + int(self.getSizeBytes())
        return self.totalSize
    
    def getSizeBytes(self):
        "Returns the size of this entry, in bytes."
        if not hasattr(self, 'size'):
            self.size = str(len(self.getProcessedText()))
        return self.size

    def entryFiltersAlterEntry(self):
        """Returns true if entry filters alter the actual entry; false if they
        merely alter the entry as displayed."""
        return self.notebook.getOptionValue('entry-filters-alter-entry')

    def getTimeZone(self):
        zone = getattr(self, self.ENTRY_TIMEZONE_KEY, None)
        if not zone:
            zone = self.notebook.getTimeZone()
        return zone

    #### Template-related methods

    def interpolateAuthorTemplate(self, viewer):
        """Interpolates the author template for this entry."""
        if self.getAuthor():
            self.notebook.interpolateTemplate('author',
                                              CommandMaps.AUTHOR_COMMAND_MAP,
                                              self, viewer, out=viewer.out)

    def interpolateTitleTemplate(self, viewer):
        """Interpolates the title template for this entry."""
        if self.getTitle():
            self.notebook.interpolateTemplate('title',
                                              CommandMaps.TITLE_COMMAND_MAP,
                                              self, viewer, out=viewer.out)

    def interpolateSummaryTemplate(self, viewer):
        """Interpolates the summary template for this entry."""
        self.notebook.interpolateTemplate('summary',
                                          CommandMaps.ENTRY_COMMAND_MAP,
                                          self, viewer, out=viewer.out)

    def _getPlural(self, number, singular='', plural='s'):
        """Returns the correct form of the given words depending on
        whether or not the given number is 1."""
        if int(number) == 1:
            return singular
        else:
            return plural

    ### Methods related to attachments.

    def getAttachmentNames(self):
        "Returns the filenames of the attachments for this entry."
        attachments = []
        dir = self.getAttachmentDirectory()
        if os.path.exists(dir) and os.path.isdir(dir):
            attachments = os.listdir(dir)
        return attachments
    
    def getAttachmentDirectory(self):        
        dir = os.path.join(self.notebook.getAttachmentDir(),
                           self.id.describe(0))
        return dir

    def getAttachmentPath(self, name):
        "Returns the path to the named attachment."
        return os.path.join(self.getAttachmentDirectory(), name)

    def getAttachmentURL(self, name, cgi):
        "Returns the url to the given CGI for the named attachment."
        return self.id.asLink(cgi, pathInfo=name)

    def getAttachment(self, name):
        "Reads the named attachment into a string."
        path = self.getAttachmentPath(name)
        attachment = None
        if os.path.exists(path):
            attachment = open(path).read()
        return attachment

    def setAttachment(self, password, name, contents):
        """Creates an attachment with the given name and the given
        contents. Overwrites any existing attachment for this entry
        with the same name."""
        if string.find(name, '/') > -1:
            name = os.path.split(name, '/')[-1]
        if not self.id.draft:
            self.notebook.assertValidPassword(password)
        if not self.notebook.isAcceptableAttachmentFilename(name):
            raise_("Bad attachment: %s. You can't upload a file of that type to this notebook.") % name
        dir = self.getAttachmentDirectory()
        if not os.path.exists(dir):
            os.makedirs(dir)
        attachment = open(self.getAttachmentPath(name), 'w')
        attachment.write(contents)
        attachment.close()

    def deleteAttachment(self, password, name):
        "Deletes the named attachment from association with this entry."
        self.notebook.assertValidPassword(password)
        path = self.getAttachmentPath(name)
        if os.path.exists(path) and os.path.isfile(path):
            os.remove(path)

    ### Mutator methods on the entry itself.

    def assertDirectories(self):
        """Ensure the existance of all directories neccessary to store
        this entry."""
        if not self.id.denotesSingleEntry(): raise AmbiguousIDError
        monthdir = self.id.asPath(0)
        if os.path.exists(monthdir):
            if not os.path.isdir(monthdir):
                raise_("Not a directory: %s") % monthdir
        else:
            os.makedirs(monthdir)
            if not os.path.exists(monthdir):
                raise_("Creation of %s didn't work.") % monthdir

    def asOutputString(self):
        "Put entry into data structure for writing to disk."
        s = "#NewsBruiser v%s\n" % version.version
        s = s + "%s: %s\n" % (ENTRY_TIME_KEY, self.time)
        s = s + "%s: %s\n" % (self.ENTRY_TIMEZONE_KEY, self.getTimeZone())
        s = s + "%s: %s\n" % (ENTRY_MODIFIED_KEY, self.getModifiedDate())
        if self.getTitle():
            if self.entryFiltersAlterEntry():
                self.title = self.applyFilters('title', self.title)
            else:
                self.title = self.get('title', oneLine=1)
            s = s + '%s: %s\n' % (ENTRY_TITLE_KEY, self.title)

        if self.notebook.displayAuthor():
            s = s + "%s: %s\n" % (ENTRY_AUTHOR_KEY, self.getAuthor())

        if self.notebook.collectAuthorEmail():            
            s = s + "%s: %s\n" % (ENTRY_AUTHOR_EMAIL_KEY, self.getAuthorEmail())
        for hook in Entry.outputStringHooks:
            a = hook(self)
            if a:
                s = s + a
                if a and s[-1] != '\n':
                    s = s + '\n'

        if s[-1] != '\n':
            s = s + '\n'
        s = s + '\n'
        if self.entryFiltersAlterEntry():
            self.text = self.applyFilters('text', self.text)
        s = s + self.text
        return s	

    def edit(self, password, newAuthor=None, newAuthorEmail=None, newText='',
             newTitle=None, vars=None):
        "Change the author, text, title, etc. of this entry"

        self.notebook.assertValidPassword(password)
        if newAuthor and self.notebook.displayAuthor():
            self.author = newAuthor
        if newAuthorEmail and self.notebook.collectAuthorEmail():
            self.authorEmail = newAuthorEmail
        if newText: self.text = newText
        self.title = newTitle

        for hook in Entry.instantiationFromInputHooks:
            hook(vars, self)
        self.notebook.setTimeZone()
        self.modifiedTime = time.time()
        self.notebook.unsetTimeZone()
        self.write()

    def publish(self, password):
        """Publish this draft as a notebook entry. Throws an exception if
        the entry is not a draft. Returns the new (non-draft) entry."""
        self.notebook.assertValidPassword(password)
        if not self.id.draft:
            raise_("You can't publish an entry that's not a draft!")
        else:            
            self.wasDraft = 1
            newEntry = self.notebook.addEntry(password, self.getAuthor(),
                                              self.getAuthorEmail(),
                                              self.text, self.getTitle(),
                                              vars=self)
            newEntry.write()
            for filename in self.getAttachmentNames():                
                #TODO: It would be more efficient to just move the file.
                contents = self.getAttachment(filename)
                newEntry.setAttachment(password, filename, contents)
                self.deleteAttachment(password, filename)
            self.delete(password)                

            self.notebook.nbconfig.pluginEvent('EntryPublished', self)

            return newEntry

    def delete(self, password):
        if not self.id.draft and not self.notebook.allowDelete():
            raise _('This notebook does not allow entry deletion!')
        self.notebook.assertValidPassword(password)

        #Remove this entry from the index.
        if not self.id.draft:
            indexer = self.notebook.getIndexer()
            indexer.purge_entry(self.id.asPath(), indexer.fileids, indexer.files,
                                indexer.words)

        for hook in Entry.preDeletionHooks:
            hook(self)

        #Remove this entry's attachments from disk.
        for filename in self.getAttachmentNames():
            self.deleteAttachment(password, filename)

        #Move all entries after this one for this day (if any) back
        #one ordinal.
        entries = self.getDayID().getEntries()
        ord = int(self.id.ordinal)+1
        lastEntryID = copy.copy(entries[-1].id)
        if len(entries) > ord:
            for entry in entries[ord:]:
                entry.changeOrdinal(str(int(entry.id.ordinal) - 1))
                                
        #Delete the last entry of the day.
        os.remove(lastEntryID.asPath())

        if not self.id.draft:
            indexer.save_index()

        self.notebook.nbconfig.pluginEvent('EntryDeleted', self)

    def changeOrdinal(self, newOrdinal):
        """Changes the ordinal of this entry. If there's already an entry with
        this ordinal, it'll be overwritten. Note: After calling this method
        you must call save_index() on the notebook's indexer."""

        if not self.id.draft:
            indexer = self.notebook.getIndexer()
            indexer.purge_entry(self.id.asPath(), indexer.fileids,
                                indexer.files, indexer.words)

        for hook in self.preOrdinalChangeHooks:
            hook(self, newOrdinal)
        self.id.ordinal = newOrdinal
        self.write()

        if not self.id.draft:
            indexer.add_file(self.id.asPath())

        for hook in self.postOrdinalChangeHooks:
            hook(self, newOrdinal)

    def renderAttachments(self, viewer):
        """Prints out a list of attachments for this entry."""
        names = self.getAttachmentNames()        
        if names:
            s = '<p>'
            if self.notebook.getHeadingAttachmentDesignation() == _('Image'):
                s = s + _('Images for this entry:')
            else:
                s = s + _('Files for this entry:')
            s = s + '</p><ul>'
            from CoreCGIs import AttachmentManagerCGI
            for name in names:
                url = self.getAttachmentURL(name, AttachmentManagerCGI.NAME)
                s = s + '<li>'
                if hasattr(viewer, 'verbose') and viewer.verbose and self.notebook.isImage(name):
                    text = '<img src="%s">' % url
                else:
                    text = name
                s = s + self.getLink(url, text, '')
            s = s + '</ul>'
        else:
            s = ''
        return s
        
    def render(self, viewer, out=sys.stdout):
        from CoreCGIs import EditCGI, DeleteCGI, AttachmentManagerCGI
        hasAttachments = self.getAttachmentNames()
        showAttachments = hasattr(viewer, 'attachments') and viewer.attachments
        if viewer.edit or (showAttachments and hasAttachments):
            out.write('<table width=100%><tr><td>')
        if viewer.entryHeader:
            out.write(viewer.entryHeader)
        if not showAttachments or hasAttachments:            
            if viewer.detail:
                template = 'entry-detail'
            else:
                template = 'entry'
            self.notebook.interpolateTemplate(template,
                                              CommandMaps.ENTRY_COMMAND_MAP,
                                              self, viewer, out, [viewer])
        if viewer.edit or (showAttachments and hasAttachments):
            out.write('</td><td align="right" valign="top">')

        if viewer.edit or (showAttachments and hasAttachments):
            out.write(self.getPanelLink(self.id.asLink(EditCGI.NAME),
                                        _('Edit&nbsp;me!')))
        if viewer.edit:
            if self.id.draft:
                out.write(self.getPanelLink(self.id.asLink(EditCGI.NAME,
                                                         'Publish=true'),
                                          _('Publish&nbsp;me!')))
                out.write(self.getPanelLink(self.id.asLink(DeleteCGI.NAME),
                                          _('Delete&nbsp;me!')))
        if showAttachments and hasAttachments:
            out.write(self.getPanelLink(self.id.asLink(AttachmentManagerCGI.NAME,
                                                       'mode=edit'),
                                      _('Manage&nbsp;my&nbsp;%s!') % self.notebook.getPluralAttachmentDesignation()))

        if showAttachments and hasAttachments:
            out.write('</td></tr><tr><td colspan="2">')
            out.write(self.renderAttachments(viewer))

        if viewer.edit or (showAttachments and hasAttachments):
            out.write('</td></tr></table>')

    def getAuthorDescription(self):
        author = self.getAuthor()
        email = self.getAuthorEmail()
        if email:
            author = '%s (%s)' % (email, author)
        return author

    def getTagURI(self):
        "Returns a Tag URI for this entry."
        domain = self.notebook.getExportedHost()
        domain = domain[string.rfind(domain, '/')+1:]
        return "tag:%s,%s-%s-%s:%s-%s" % (domain, self.id.year,
                                          string.zfill(str(self.id.month), 2),
                                          string.zfill(str(self.id.day), 2),
                                          self.notebook.name, self.id.ordinal)

    def getAbsoluteURL(self):
        "Returns the absolute URL to this entry only."
        if hasattr(self, 'originalURL') and self.notebook.getOptionValue('syndication-only'):
            return self.originalURL
        else:
            from CoreCGIs import ViewCGI
            base = self.id.asLink(ViewCGI.NAME)
            return util.urljoin(self.notebook.getExportedHost(), base)

    def read(self):
        "Read in entry from disk."
        if not self.id.denotesSingleEntry(): raise AmbiguousIDError
        filename = self.id.asPath()
        if not os.path.exists(filename):
            raise exceptions.DoesNotExist, _("No such entry: %s") % filename
        try:
            entry = open(filename, 'r')
            self.instantiateFromFile(entry)
        except OSError:
            raise_("Can't read entry: %s") % filename

    def instantiateFromFile(self, entry):
        headers = 1
        while headers:
            line = entry.readline()
            if not line or line[0] == '\n' or line[0] == '-':
                headers = 0
            elif line[0] != '#':
                pair = string.split(line, ': ', 1)
                if len(pair) == 2:
                    val = pair[1][:-1]
                    method = self.instantiationFromFileHooks.get(pair[0])
                    if method:
                        method(self, val)
                    else:
                        setattr(self, pair[0], val)
        self.text = entry.read()

    def sample(self):
        """Returns the title of this entry, or the first few words if
        it has no title."""
        text = self.getTitle()        
        if not text:        
            text = self.bodySample()
        return util.compressWhitespace(text)

    def bodySample(self):
        pos = 50
        text = self.getProcessedText()
        text = util.sample(text, pos)
        return text

    def summaryApplicable(self):
        """Returns true iff it makes sense for this entry to have a summary
        (because it's longer than one paragraph)."""
        self.getSummary()
        return self.summaryApplicable        

    def getSummary(self):
        "Returns the first paragraph of this entry."
        if not hasattr(self, 'summary'):
            text = self.getProcessedText()
            result = re.search("[^\s](\s*<\s*[Pp]\s*>)", text)
            if result:
                self.summary = text[:result.start(1)]
            else:
                self.summary = text
            self.summaryApplicable = result
        return self.summary

    def write(self, suppressUpdateNotification=0):
        "Write entry to disk and add to pending index alterations."
        if not self.id.denotesSingleEntry(): raise AmbiguousIDError
        self.assertDirectories()     
        if not hasattr(self, 'time') or not self.time:
            self.setTimeZone()
            self.time = time.strftime(STORAGE_TIME_FORMAT,
                                      time.localtime(time.time()))
            self.unsetTimeZone()

        path = self.id.asPath()
        file = open(path, 'w')
        file.write(self.asOutputString())
        file.close()
        os.chmod(path, FILE_MODE)
        if not self.id.draft and not suppressUpdateNotification:
            self.notebook.addToIndexUpdateList(self)
        self.notebook.nbconfig.pluginEvent('EntrySaved', self)
