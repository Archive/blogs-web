import os
import re
import string
from StringIO import StringIO
from sgmllib import SGMLParser

from nb.lib.gnosis.indexer import PreferredIndexer

from nb.Entry import Entry
from nb.EntryID import EntryID

class TextGatherer(SGMLParser):

    SELF_CLOSING_TAG = re.compile('(<\W*([^ >]*)[^>]*)/>', re.M)

    def fillSelfClosingTags(self, match):
        "Regex method to fill self-closing tags."
        if len(match.groups()) > 1:
            return match.group(1) + '></' + match.group(2) + '>'
        else:
            return match.group(0)
        
    def feed(self, text, fieldName=''):
        "Cleanse text so the parser won't choke, and feed it to the parser."
        return SGMLParser.feed(self, self.SELF_CLOSING_TAG.sub(self.fillSelfClosingTags,
                                                               text))

    def reset(self):
        self.words = []
        SGMLParser.reset(self)

    def unknown_starttag(self, tag, attrs):
        for (name, value)  in attrs:
            if name in ['href', 'title']:
                self.handle_data(value)

    def handle_data(self, text):
        self.words.extend(self.text_splitter(self.indexer, text, 'text/plain'))

    def handle_comment(self, text):
        self.handle_data(text)
        
    def handle_charref(self, ref):
        self.words.append("&#%(ref)s;" % locals())

    def handle_entityref(self, ref):
        self.words.append("&%(ref)s;" % locals())

class NewsBruiserEntryIndexer(PreferredIndexer):

    """Replaces the default text splitter with one that knows about
    NewsBruiser Entry objects and how to extract indexable text from
    them."""

    def add_file(self, fname, ftype='text/plain'):
        #NOT THREAD SAFE.
        if string.find(fname, os.path.join(self.notebook.name, 'drafts')) == -1:
            self.filename = fname
            PreferredIndexer.add_file(self, fname, ftype)

    def text_splitter(self, text, casesensitive):
        gatherer = TextGatherer()
        gatherer.text_splitter = PreferredIndexer.text_splitter
        gatherer.indexer = self
        id = EntryID(self.notebook).filenamesToEntryIDs([self.filename])[0]
        entry = Entry(id)
        entry.instantiateFromFile(StringIO(text))
        entry.index(gatherer)
        words = gatherer.words
        if not self.casesensitive:
            words = map(string.upper, words)
        return words
