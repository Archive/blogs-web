import os
from lib.reverend.thomas import Bayes

class BayesianClassifier(Bayes):

    """A tiny wrapper around Reverend capable of classifying submitted
    content as good or bad based on experience."""

    GOOD = 'good'
    BAD = 'bad'

    def __init__(self, file):
        Bayes.__init__(self)
        self.file = file        
        if os.path.exists(file):
            self.load(file)

    def save(self):
        Bayes.save(self, self.file)

    def getTokens(self, obj):
        "Delegate to the object; it knows how to classify its own text."
        return obj.getBayesianClassificationTokens()

class BayesianClassified:
    
    #Constants for quality status
    CONFIRMED_GOOD = 0
    UNCONFIRMED_GOOD = 1
    UNKNOWN_STATUS = 2
    UNCONFIRMED_BAD = 3
    CONFIRMED_BAD = 4 #Should never be used--just delete it

    def getNotebook(self):
        pass

    def getBayseanClassificationTokens(self):
        return []

    def trainGood(self):
        self.getNotebook().getBayesianClassifier().train(BayesianClassifier.GOOD,
                                                         self)

    def trainBad(self):
        self.getNotebook().getBayesianClassifier().train(BayesianClassifier.BAD,
                                                         self)
    def qualityPreventsDisplay(self):
        "Check whether the quality prevents display."
        return self.quality > self.UNKNOWN_STATUS

    def unconfirmedBad(self):
        return self.quality == self.UNCONFIRMED_BAD

    def unconfirmedGood(self):
        return self.quality == self.UNCONFIRMED_GOOD

    def confirmedGood(self):
        return self.quality == self.CONFIRMED_GOOD

    def calculateQuality(self):
        if self.quality not in(self.CONFIRMED_GOOD, self.CONFIRMED_BAD):
            percent = self.badProbabilityPercentage()
            if not percent:
                self.quality = self.UNKNOWN_STATUS
            elif percent < 50:
                self.quality = self.UNCONFIRMED_GOOD
            else:
                self.quality = self.UNCONFIRMED_BAD
        return self.quality

    def badProbabilityPercentage(self):

        """We have two pools: a pool of good and a pool of
        bad. We want a percentage number which is the probability
        that this object is bad. Checking with the two pools will
        give us two numbers, each < 1 but not summing to 1. We need to
        normalize the numbers to represent the ratio between them as a
        percent."""

        if not hasattr(self, 'badProbability'):
            res = self.getNotebook().getBayesianClassifier().guess(self)
            if res:
                (guess, proportion) = res[0]
                if guess == BayesianClassifier.BAD:
                    spamProportion = proportion
                else:
                    spamProportion = 1 - proportion
                if len(res) > 1:
                    (guess2, proportion2) = res[1]                    
                    if guess == BayesianClassifier.BAD:
                        spamProportion = proportion / proportion2
                    else:
                        spamProportion = proportion2 / proportion
                    spamProportion = spamProportion/(spamProportion+1)
                percent = spamProportion * 100
                self.badProbability = percent
            else:
                self.badProbability = 0
        return self.badProbability
