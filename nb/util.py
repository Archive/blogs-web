#Miscellaneous utility functions.

from types import IntType
import lib.HTMLStripper 

import os
import random
import re
import string
import sys
import urllib
from lib import xmlrpclib

try:
    import crypt
except ImportError:
    from lib import crypt
import const

class NoConfigurationException:
    pass

WHITESPACE_RE = re.compile('\s+')

def encrypt(password):
    "Encrypts a password using crypt."
    chars = string.lowercase + string.uppercase + string.digits + './'
    salt = ''
    for i in range (0,2):
        salt = salt + random.choice(chars)
    return crypt.crypt(password, salt)

def urljoin(*args):
    """Join a list of arguments with the '/' key to form part of a URL.
    Compresses multiple /s, like os.path.join."""
    sofar = ''
    for arg in args:
        if arg:
            arg = str(arg)
            if sofar and sofar[-1] != '/' and arg[0] != '/':
                sofar = sofar + '/'
            sofar = sofar + arg
    return sofar

def wrap(s, width=78):
    "Copied from the Python Cookbook. Written by Mike Brown."
    return reduce(lambda line, word, width=width: "%s%s%s" %
                  (line,
                   ' \n'[(len(line[string.rfind(line, '\n')+1:]) +
                          len(word) >= width)], word),
                  string.split(s, ' ')
                  )


def compressWhitespace(text):
    """Does what it says."""
    return WHITESPACE_RE.sub(' ', text)

def replaceBaseURLs(text, replacementURL):
    return string.replace(text, '%R', replacementURL)

def cmpFilename(a,b):
    "Compare two filenames representing NewsBruiser entries so that the earlier one chronologically comes first."
    if len(a) == len(b):
        #This will happen most of the time, because days with more than ten entries
        #are rare. Doing this check first makes a sort about twice as fast.
        return cmp(a,b)

    posa = len(a)-2
    posb = len(b)-2 
    while a[posa] <> '-': posa = posa -1
    while b[posb] <> '-': posb = posb -1

    beforeDashValue = cmp(a[:posa],b[:posb])

    if beforeDashValue <> 0:
        return beforeDashValue
    else:
        return cmp(int(a[posa+1:]),int(b[posb+1:]))
    
def zeroPad(s):
    "Prefaces a string with a zero if its integer value is less than 10."
    if s == const.ALL: return s
    if int(s) < 10 and (type(s)==IntType or s[0] <> '0'):
        return "0%s" % s
    else:
        return str(s)

def sample(text, pos):
    "Sample a certain amount of a text."
    #Strip out HTML altogether so we don't get dangling tags.
    text = re.sub("<[^>]*>", "", text)
    text = re.sub("<[^>]*/[^>]*>", "", text)
    if len(text) > pos:
        while pos < len(text) and text[pos] <> ' ':
            pos = pos + 1  
        text = text[:pos] + "..."
    return text

def formatForRSS30(s, alreadyStripped=0):
    "Strips HTML, clears up empty lines, and adds spaces after newlines."
    if s and s[-1] == '\n':
        s = s[:-1]
    if not alreadyStripped:
        s = lib.HTMLStripper.strip(s, [])
    s = re.sub('\n\s*\n', '\n', s)
    s = string.replace(s, '\n', '\n ')
    return s

def spawnXMLRPCCall(url, methodName, args):
    argString = string.join(map(repr, args), ',')
    code = """from nb.lib import xmlrpclib
import string
xmlrpclib.Server('%s').%s(%s)""" % (url, methodName, argString)    
    spawnPythonCode(code)

def spawnURLAccessor(url, data=None):
    if data:
        data = ", '''%s'''" % data
    else:
        data = ''
    code = """import urllib\nurllib.urlopen('''%s'''%s)""" % (url, data)
    spawnPythonCode(code)

def spawnPythonCode(code):
    """Spawns a new process that executes some Python code."""
    try:
        import popen2
        (stdout, stdin) = popen2.popen2(sys.executable)
        stdin.write(code)
        stdin.close()
    except:
        eval(code)

def getNewsBruiserBaseDirectory():
    #FIXME: This assumes that the script is being run in the base directory.
    invoked = sys.argv[0]
    return os.path.split(invoked)[0]

def getFileInNewsBruiserBaseDirectory(file):
    if string.find(file, '..') >= 0:
        raise "Attempt to find file outside of NewsBruiser source tree intercepted!"
    if len(sys.argv):
        dir = getNewsBruiserBaseDirectory()
        path = os.path.join(dir, file)
    return path

def guessNewsBruiserBaseURL():
    base = None
    #If we're a CGI or an #exec cgi, then SCRIPT_NAME will be set to
    #the relative URL to the script, which of course is in the
    #NewsBruiser directory. If we're in an #exec cmd, then _ will be
    #set to the relative filesystem path to the script, which, barring
    #any weird rewrite rules, should have the same name as the
    #NewsBruiser when 
    for var in ['SCRIPT_NAME', '_']:
        if not base:
            path = os.environ.get(var, '')
            i = string.rfind(path, '/')
            if i > -1 and not (var == '_' and string.find(path[i+1:], 'python') == 0):
                #If the executable name begins with 'python', then it's a
                #red herring; they fed a NewsBruiser script into a Python
                #executable and we have no way of getting path information.
                base = path[:i+1]

                #If the 'directory' is actually the NewsBruiser CGI, use
                #the directory in which that CGI resides instead.
                testBase = base
                if base[-1] == '/':
                    testBase = base[:-1]
                (possibleBase, possibleCGI) = os.path.split(testBase)
                if possibleCGI == 'nb.cgi':
                    base = possibleBase + '/'
    if not base:
        #Uh-oh. Well, they're probably running something from the
        #command line (with Python as the interpreter), so they
        #probably won't be checking URLs.  Let's make the horribly
        #misinformed assumption that the URL is '/nb/', as
        #recommended in the docs.
        base = '/nb/'
    return base
