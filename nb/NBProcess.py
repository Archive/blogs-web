"""A module containing basic process flow classes: NBProcess and NBCGI
NBCGI is in its own file because it's quite large."""

import os
import sys
import rfc822

import NBConfig

class NBProcess:

    """An abstract class representing a process invoked by a webserver
    (either a CGI or an SSI)"""

    def __init__(self):
        self.config = NBConfig.getSingleton()

        if not hasattr(self, 'printedHeader'):
            self.printedHeader = 0

    def checkFreshness(self, mtime, etag, weak=0):
        """Check to see if the user agent's copy of the resource is
        still valid based on etag and last-modified date.

        If it is up to date, generate a '304 Not Modified' response.
        Otherwise, output appropriate ETag and Last-Modified headers.

        If method returns True, processing should stop.
        """
        if self.printedHeader:
            return 0 # already printed header, so can't do 304 response

        request_etag = request_mtime = None
        if etag is not None:
            if weak:
                etag = 'W/"%s"' % etag
            else:
                etag = '"%s"' % etag
            request_etag = os.environ.get('HTTP_IF_NONE_MATCH')
        if mtime is not None:
            try:
                request_mtime = os.environ.get('HTTP_IF_MODIFIED')
                request_mtime = rfc822.mktime_tz(
                    rfc822.parsedate_tz(request_mtime))
            except:
                request_mtime = None

        # is the resource fresh?
        if etag is not None:
            isfresh = (request_etag == etag)
        elif mtime is not None:
            isfresh = (request_mtime >= mtime)
        else:
            isfresh = 0

        if isfresh:
            print 'Status: 304 Not Modified'
            print
            self.printedHeader = 1
        else:
            if etag is not None:
                print 'Etag: %s' % etag
            if mtime is not None:
                print 'Last-Modified: %s' % rfc822.formatdate(mtime)
        
        return isfresh


    def printHeader(self, contentType='text/html'):
        if not self.printedHeader:
            print 'Content-type: %s\n' % contentType
            self.printedHeader = 1
            sys.stderr = sys.stdout

    def inSSIInclude(self):
        "Returns true iff we are running from within an SSI include."
        return os.environ.get('SERVER_PROTOCOL', None) == 'INCLUDED'

    def nice(self):
        "Nices the current process "
        newNice = None
        if os.name == 'posix':
            if self.config.niceLevel:
                nice = int(self.config.niceLevel)
                if nice >= -20 and nice < 20:
                    try:
                        newNice = os.nice(nice)
                    except OSError:
                        if nice < 0:
                            raise _("You're trying to run NewsBruiser with a negative niceLevel, but your server isn't running as root so you don't have permission to do that. Please resolve this by using a non-negative value for niceLevel, rather than running the server as root.")
        return newNice

    def run(self):
        "Hook method for process-specific code."
        pass

class NBSSI(NBProcess): 

    """A scrawny little abstract SSI class which nices the process and
    calls a 'run' function (supplied by descendants)"""

    def init(self, varNamesAndDefaults):
        NBProcess.__init__(self)
        self.nice()
        if self.inSSIInclude():
            self.printHeader()

        if not self.config.configFile:
            from NewsBruiserCGIs import InstallCGI
            print _('NewsBruiser has yet to be set up on this site. <a href="%s">Click here to set it up.</a>') % InstallCGI.CGI_NAME
            return

        #Turn command-line variables into fields of this object
        cmdPos = 1
        args = len(sys.argv)
        for (varName, default) in varNamesAndDefaults:
            if cmdPos < args:
                value = sys.argv[cmdPos]
            else:
                value = default
            setattr(self, varName, value)
            if varName == 'notebookName':
                self.setNotebook()
            cmdPos = cmdPos + 1

        try:
            self.run()
        except SystemExit:
            #If we exit normally, don't print a traceback.
            pass
        except: #Something went wrong. Print a traceback.
            self.printHeader()
            print "\n\n<pre>"   
            import traceback
            traceback.print_exc()
            print "</pre>"


    def setNotebook(self):
        """Convenience method which sets self.notebook assuming that
        self.notebookName has already been set."""
        if self.notebookName:
            self.notebook = self.config.notebooks[self.notebookName]
        else:
            self.notebook = self.config.getDefaultNotebook()
