#!/usr/bin/python -S
import os
os.nice(10)
# 2005-12-30 ovitters:
# Hack to force default encoding to UTF-8.
# I tried various things to avoid this, but it just isn't possible
import sys
sys.setdefaultencoding('utf-8')
import site
try:
    #Turn off warnings so we don't get crap about obsolete modules.
    import warnings
    warnings.defaultaction='ignore'
except:
    pass
import __builtin__
__builtin__.__dict__['_'] = lambda x: x
from nb.NewsBruiserCGIs import DispatchCGI
DispatchCGI()
#import profile
#import pstats
#filename = '/tmp/profile'
#profile.run('DispatchCGI()', filename)
#p = pstats.Stats(filename)
#print '<pre>'
#p.strip_dirs().sort_stats('time').print_stats(100)
#print '</pre>'
